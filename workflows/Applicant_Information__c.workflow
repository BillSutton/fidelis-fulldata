<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Date_Completed</fullName>
        <description>This is the field update to be used with the workflow rule that is triggered when Applicant records are saved with the Applicant Status</description>
        <field>Date_Completed__c</field>
        <formula>Today()</formula>
        <name>Update Date Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Date Completed</fullName>
        <actions>
            <name>Update_Date_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Applicant_Information__c.Applicant_Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>This workflow rule will be used to update the Date Completed on all applicant records once they&apos;ve been marked as Complete.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
