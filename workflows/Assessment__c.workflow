<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Manager_Approval_Notification</fullName>
        <description>Manager Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>grodrigu@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Fidelis_System_Templates/Manager_Approval_Required</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Notificaiton</fullName>
        <description>Rejection Notificaiton</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Fidelis_System_Templates/Assessment_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Recalled</fullName>
        <description>This action will be used to update the Submission/Approval Status to Recalled when approval submission are recalled by the assessment nurses.</description>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Approval Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Sumbitted</fullName>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Approval Sumbitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assessment_Name</fullName>
        <field>Name</field>
        <formula>Member_Name__c  &amp; &quot; - &quot; &amp; TEXT(Type_of_Assessment__c) &amp; &quot; - &quot; &amp;  TEXT(Assessment_Date__c)</formula>
        <name>Assessment Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enrollment_Approved</fullName>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Enrollment Approved</literalValue>
        <name>Enrollment Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enrollment_Rejected</fullName>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Enrollment Rejected</literalValue>
        <name>Enrollment Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MSA_Approved</fullName>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Intake Coordinator Approved</literalValue>
        <name>MSA Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MSA_Rejected</fullName>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Intake Coordinator Rejected</literalValue>
        <name>MSA Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Approved</fullName>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Manager Approved</literalValue>
        <name>Managed Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Manager_Rejected</fullName>
        <field>Submission_Approval_Status__c</field>
        <literalValue>Manager Rejected</literalValue>
        <name>Manager Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assessment Name Update</fullName>
        <actions>
            <name>Assessment_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assessment__c.Assessment_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New User - YES</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Assessment__c.New_User__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
