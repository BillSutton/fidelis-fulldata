<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Expiration_Notification</fullName>
        <description>Expiration Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>avelasquez@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ggallett@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rsteinberg@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Broker_Contract_Expiration_Notification</template>
    </alerts>
    <rules>
        <fullName>Broker Contract Expiration Notification</fullName>
        <active>true</active>
        <description>Email Alert sends out 60 days prior to the broker account&apos;s Contract Expiration Date.</description>
        <formula>Not (Isblank ( Contract_Expiration_Date__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Expiration_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.Contract_Expiration_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
