<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Authorization_OOC_Form_Email</fullName>
        <description>Authorization OOC Form Email</description>
        <protected>false</protected>
        <recipients>
            <field>Send_To__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ftrequest@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Authorization_OOC_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_the_Authorize_Livery_Ambulance_Form_Email</fullName>
        <description>Send the Authorize Livery/Ambulance Form Email</description>
        <protected>false</protected>
        <recipients>
            <field>Send_To__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ftrequest@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Authorize_Livery_Ambulance_Email</template>
    </alerts>
    <rules>
        <fullName>Transportation - Authorization OOC Email</fullName>
        <actions>
            <name>Authorization_OOC_Form_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form_Request__c.Form_Name__c</field>
            <operation>equals</operation>
            <value>Authorization OOC Form</value>
        </criteriaItems>
        <description>Created for Transportation Release</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Transportation - Authorize Livery%2FAmbulance Email</fullName>
        <actions>
            <name>Send_the_Authorize_Livery_Ambulance_Form_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Form_Request__c.Form_Name__c</field>
            <operation>equals</operation>
            <value>Authorize Livery/Ambulance Form</value>
        </criteriaItems>
        <description>Created for Transportation Release</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
