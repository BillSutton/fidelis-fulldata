<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Giveaway_Request_Quantity_each_time_a_giveaway_request_is_made</fullName>
        <description>Giveaway Request Quantity each time a giveaway request is made.</description>
        <protected>false</protected>
        <recipients>
            <recipient>Supervisor_Community_Relations_Fleet</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gberiguete@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rshah@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sdiaz@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Giveaway_Request_Quantity</template>
    </alerts>
    <rules>
        <fullName>Giveaway Request</fullName>
        <actions>
            <name>Giveaway_Request_Quantity_each_time_a_giveaway_request_is_made</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email Template involving quantity of giveaway request being sent upon the creation of the giveaway request.</description>
        <formula>AND(DATEVALUE( CreatedDate ) =  TODAY(), RecordType.Name = &apos;Giveaway Request&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
