<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Env_Accommo_Name</fullName>
        <field>Name</field>
        <formula>Assessment__r.Member_Name__c &amp; &quot; - Env/Acm - &quot; &amp; TEXT( Assessment__r.Assessment_Date__c )</formula>
        <name>Env/Accommo Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Env%2FAcm Name Update</fullName>
        <actions>
            <name>Env_Accommo_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Environment_Accommodation_Assessment__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
