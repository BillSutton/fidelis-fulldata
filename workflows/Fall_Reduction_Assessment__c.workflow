<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Falls_Reduction_Name</fullName>
        <field>Name</field>
        <formula>Assessment_Name__r.Member_Name__c  &amp; &quot; - FALLS - &quot; &amp; TEXT( Assessment_Name__r.Assessment_Date__c )</formula>
        <name>Falls Reduction Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Falls Assessment Name Update</fullName>
        <actions>
            <name>Falls_Reduction_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Fall_Reduction_Assessment__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
