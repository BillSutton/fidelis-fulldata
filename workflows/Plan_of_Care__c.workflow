<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Plan_of_Care_Name</fullName>
        <field>Name</field>
        <formula>Assessment__r.Member_Name__c &amp; &quot; - POC - &quot; &amp; TEXT( Assessment__r.Assessment_Date__c )</formula>
        <name>Plan of Care Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>POC Name Update</fullName>
        <actions>
            <name>Plan_of_Care_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Plan_of_Care__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
