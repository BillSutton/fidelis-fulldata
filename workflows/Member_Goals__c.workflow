<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Member_Goals_Name</fullName>
        <field>Name</field>
        <formula>Assessment__r.Member_Name__c  &amp; &quot; - GOALS - &quot; &amp; TEXT( Assessment__r.Assessment_Date__c )</formula>
        <name>Member Goals Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Member Goals Name Update</fullName>
        <actions>
            <name>Member_Goals_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Member_Goals__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
