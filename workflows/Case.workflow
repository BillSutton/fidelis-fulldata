<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Broker_Related_Case_20_Day_Notification</fullName>
        <description>Broker Related Case 20 Day Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>avelasquez@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ggallett@fideliscare.org</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rsteinberg@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Broker_Related_Case_20_Days_w_out_Being_Modified</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email</fullName>
        <description>Case Distribution Email</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rsteinberg@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_ABarr</fullName>
        <description>Case Distribution Email ABarr</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>abarr@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_EBaranski</fullName>
        <description>Case Distribution Email EBaranski</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ebaransk@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_GBassJoh</fullName>
        <description>Case Distribution Email GBassJoh</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>gbassjoh@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_JBasile</fullName>
        <description>Case Distribution Email JBasile</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>jbasile@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_KSchlie</fullName>
        <description>Case Distribution Email KSchlie</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>kschlie@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_LOtto</fullName>
        <description>Case Distribution Email LOtto</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>lotto@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_MLTC</fullName>
        <description>Case Distribution Email MLTC</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rsteinberg@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_NY_City</fullName>
        <description>Case Distribution Email NY City</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>gmroleads@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Email_SHaywood</fullName>
        <description>Case Distribution Email SHaywood</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>shaywood@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Distribution_Emailx</fullName>
        <description>Case Distribution Email</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Fidelis_System_Templates/Case_Distribution_Email</template>
    </alerts>
    <alerts>
        <fullName>LetterOfAgreement</fullName>
        <ccEmails>loarequestscm@fideliscare.org</ccEmails>
        <description>LetterOfAgreement</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LetterOfAgreement</template>
    </alerts>
    <alerts>
        <fullName>Send_Transportation_PDF</fullName>
        <description>Send Transportation PDF</description>
        <protected>false</protected>
        <recipients>
            <field>Form_Sent_To__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ftrequest@fideliscare.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TransportationPDF</template>
    </alerts>
    <alerts>
        <fullName>Transportation_Provider_Demo_Update_Email</fullName>
        <description>Transportation - Provider Demo Update Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>Transportation_Supervisors</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Fidelis_System_Templates/Transportation_Provider_Demo_Update</template>
    </alerts>
    <fieldUpdates>
        <fullName>Blank_Out_Escalated_Date_Field</fullName>
        <field>Date_Escalated__c</field>
        <name>Blank Out Escalated Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Date_Assigned</fullName>
        <description>(Migrated from Lead Workflow Actions, Release 2)</description>
        <field>Date_Assigned__c</field>
        <formula>LastModifiedDate</formula>
        <name>Case Date Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Date_Qualified</fullName>
        <description>(Migrated with Release 2 from Lead Workflow Rule/Action Date Qualified)</description>
        <field>Date_Qualified__c</field>
        <formula>Today()</formula>
        <name>Case Date Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Plan_Name</fullName>
        <description>(Migrated from Lead Workflow Rule &apos;Plan Name.&apos; Release 2)</description>
        <field>Plan_Name__c</field>
        <formula>&quot;NYS Catholic Health Plan, DBA Fidelis Care&quot;</formula>
        <name>Case Plan Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Rep_Email</fullName>
        <description>(Migrated from Lead Workflow Actions, Release 2 - removed from Case Date Assigned workflow rule as this is no longer valid.  This field is the same as Rep&apos;s Email</description>
        <field>Rep_Email__c</field>
        <formula>BLANKVALUE( Assigned_Rep_Email__c ,   Owner:User.Email  )</formula>
        <name>Case Rep Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Update_to_Assigned</fullName>
        <description>(Migrated from Lead Workflow Action &apos;Status Update to Assigned.&apos; Release 2)</description>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>Case Status Update to Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Update_to_Closed</fullName>
        <description>(Migrated from Lead Workflow Action &apos;Status Update to Closed.&apos; Release 2)</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Case Status Update to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Update_to_In_Process</fullName>
        <description>(Migrated from Lead Workflow Action &apos;Status Update to In Process.&apos; Release 2)</description>
        <field>Status</field>
        <literalValue>In Process</literalValue>
        <name>Case Status Update to In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Update_to_Not_Eligible</fullName>
        <description>(Migrated from Lead Workflow Action &apos;Status Update to Not Eligible.&apos; Release 2)</description>
        <field>Status</field>
        <literalValue>Not Eligible</literalValue>
        <name>Case Status Update to Not Eligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Update_to_Qualified</fullName>
        <description>(Migrated from Lead Workflow Action &apos;Date Qualified.&apos; Release 2)</description>
        <field>Status</field>
        <literalValue>Qualified</literalValue>
        <name>Case Status Update to Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Update_to_Referred</fullName>
        <description>(Migrated from Lead Workflow Action &apos;Status Update to Referred.&apos; Release 2)</description>
        <field>Status</field>
        <literalValue>Referred</literalValue>
        <name>Case Status Update to Referred</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Missing_Data_Field</fullName>
        <description>(Migrated from Lead Workflow Action &apos;Update Missing Data Field.&apos; Release 2)</description>
        <field>Missing_Data__c</field>
        <literalValue>1</literalValue>
        <name>Case Update Missing Data Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Status_to_Unassigned</fullName>
        <description>(Migrated Workflow Action &apos;Update Status to Unassigned.&apos; Release 2)</description>
        <field>Status</field>
        <literalValue>Unassigned</literalValue>
        <name>Case Update Status to Unassigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Escalated_Date_Field</fullName>
        <field>Date_Escalated__c</field>
        <formula>NOW()</formula>
        <name>Populate Escalated Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Case_with_Member_Enrollment_Status</fullName>
        <field>Enrollment_Status__c</field>
        <literalValue>1</literalValue>
        <name>Stamp Case with Member Enrollment Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Undistribute</fullName>
        <description>Turn distribution off</description>
        <field>Distributed__c</field>
        <literalValue>0</literalValue>
        <name>Undistribute</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Modified_Date_Time</fullName>
        <field>Modified_DateTime__c</field>
        <formula>now()</formula>
        <name>Update Case Modified Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Blank Out Escalated Date</fullName>
        <actions>
            <name>Blank_Out_Escalated_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IsEscalated = false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Check Missing Data Indicator</fullName>
        <actions>
            <name>Case_Update_Missing_Data_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>Case.ContactPhone</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_Zip_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>(Migrated from Lead Workflow Rule &apos;Check Missing Data Indicator.&apos; Release 2)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Hasn%27t Been Modified</fullName>
        <active>true</active>
        <description>An email alert is to send to the BRMs when a broker account related case record hasn&apos;t been modified within a 20 day period.</description>
        <formula>AND(Not (IsBlank( Referring_Broker__c )),    OR(ISPICKVAL( Status , &quot;Assigned&quot; ),ISPICKVAL( Status , &quot;Unassigned&quot; ),ISPICKVAL( Status , &quot;In Process&quot; )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Broker_Related_Case_20_Day_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>19</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Unassigned Rep Status Update</fullName>
        <actions>
            <name>Case_Update_Status_to_Unassigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Rep_Name__c</field>
            <operation>equals</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <description>(Migrated from Lead Workflow Rule &apos;Unassigned Rep Status Update.&apos; Release 2)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Update Plan Name on MLTC</fullName>
        <actions>
            <name>Case_Plan_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Primary_Category__c</field>
            <operation>equals</operation>
            <value>MLTC</value>
        </criteriaItems>
        <description>(Migrated from Lead Workflow Rule &apos;Update Plan Name on MLTC.&apos; Release 2)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Escalated Date</fullName>
        <actions>
            <name>Populate_Escalated_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IsEscalated = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Transportation - CaseModifiedDateTime</fullName>
        <actions>
            <name>Update_Case_Modified_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for Transportation Release</description>
        <formula>AND(ISNEW()=False,  OR( ISCHANGED(of_Riders__c), ISCHANGED(Return_Datetime__c), ISCHANGED(Steps__c), ISCHANGED(Weight__c), ISCHANGED(Special_Request__c), ISCHANGED(Description) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Transportation - LetterOfAgreement</fullName>
        <actions>
            <name>LetterOfAgreement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Accepting_Medicare_or_Medicaid_Rates__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Authorization_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Service_Date_on_LoA__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Created for Transportation Release.
Case.Accepting_Medicare_or_Medicaid_Rates__c = No AND Case.Service_Date_on_LoA__c &lt;&gt; blank. AND Case.AuthorizationNumber &lt;&gt; Blank</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Transportation - Provider Demo Update</fullName>
        <actions>
            <name>Transportation_Provider_Demo_Update_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transportation Note</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Provider Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>equals</operation>
            <value>Request for Demographic Update</value>
        </criteriaItems>
        <description>This workflow is used to identify cases created for Transportation Vendors requesting demographic updates.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Transportation - Send Trip Request</fullName>
        <actions>
            <name>Send_Transportation_PDF</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created for Transportation Release.  Uses the &quot;Send Transportation PDF&quot; Workflow Email.  Previously formula: if( PRIORVALUE(Form_Sent_Date_Time__c) != Form_Sent_Date_Time__c &amp;&amp; ISCHANGED(Form_Sent_Date_Time__c), TRUE,FALSE)</description>
        <formula>if( OR(ISCHANGED(Form_Sent_Date_Time__c),AND(ISNEW(),NOT(ISBLANK(Form_Sent_Date_Time__c)))) , TRUE,FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Transportation Enrollment Status</fullName>
        <actions>
            <name>Stamp_Case_with_Member_Enrollment_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transportation Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Enrolled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Show what member&apos;s enrollment status was at the time of trip creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Distribution Indicator</fullName>
        <actions>
            <name>Case_Distribution_Emailx</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Rep_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Date_Assigned__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Rep_Name__c</field>
            <operation>notContain</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Primary_Category__c</field>
            <operation>notEqual</operation>
            <value>MLTC,MLTC Packet,FIDA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Rep_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketplace,Medicare</value>
        </criteriaItems>
        <description>Updated on 10/21 by shess to include Case Record type , remove &apos;distribution&apos; = true and adding date assigned not equal to null

(Migrated from Lead Workflow Rule: &apos;Lead Distribution Indicator.&apos; Release 2)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Distribution Indicator MLTC</fullName>
        <actions>
            <name>Case_Rep_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Undistribute</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Distributed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Primary_Category__c</field>
            <operation>equals</operation>
            <value>MLTC,MLTC Packet</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Rep_Name__c</field>
            <operation>notContain</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Care Management,Marketplace,Medicare</value>
        </criteriaItems>
        <description>(Migrated Lead Workflow Rule &apos;Update Lead Distribution Indicator MLTC.&apos; Release 2)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Distribution Indicator NY City</fullName>
        <actions>
            <name>Case_Distribution_Email_NY_City</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Rep_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Region__c</field>
            <operation>equals</operation>
            <value>NY City</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Rep_Name__c</field>
            <operation>notContain</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Primary_Category__c</field>
            <operation>notEqual</operation>
            <value>MLTC,MLTC Packet</value>
        </criteriaItems>
        <description>(Migrated from Lead Workflow Rule &apos;Update Lead Distribution Indicator NY City.&apos; Release 2)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
