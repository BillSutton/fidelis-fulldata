<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MMSE_Name</fullName>
        <field>Name</field>
        <formula>Assessment__r.Member_Name__c &amp; &quot; - MMSE - &quot; &amp; TEXT( Assessment__r.Assessment_Date__c )</formula>
        <name>MMSE Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MMSE Name Update</fullName>
        <actions>
            <name>MMSE_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Mini_Mental_State_Evaluation__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
