<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Nighttime_Needs</fullName>
        <description>Nighttime Needs</description>
        <protected>false</protected>
        <recipients>
            <recipient>Nighttime_Needs</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Fidelis_System_Templates/Nighttime_Needs_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Grand_Total_Minutes</fullName>
        <field>Grand_Total_Minutes__c</field>
        <formula>Grand_Total_Minutes__c</formula>
        <name>Grand Total (Minutes)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Member_Requires_Routine_Transportation</fullName>
        <field>Member_Requires_Routine_Transportation__c</field>
        <formula>TEXT( Member_Requires_Routine_Transportation__c )</formula>
        <name>Member Requires Routine Transportation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Member_Requires_Supervision</fullName>
        <field>Member_Requires_Supervision__c</field>
        <formula>TEXT( Member_Requires_Supervision__c )</formula>
        <name>Member Requires Supervision</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mutual_Case_NO</fullName>
        <field>Mutual_Case__c</field>
        <formula>&quot;No&quot;</formula>
        <name>Mutual Case - NO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mutual_Case_YES</fullName>
        <field>Mutual_Case__c</field>
        <formula>&quot;Yes&quot;</formula>
        <name>Mutual Case - YES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Number_of_Hours_Day_Recommendation</fullName>
        <field>Number_of_Hrs_Days_Recommended_by_Nurse__c</field>
        <formula>TEXT(Number_of_Hours_Recommended_by_Nurse__c) &amp; &quot; hours/&quot; &amp;  TEXT(Recommended_Number_of_Days__c) &amp; &quot; days per week&quot;</formula>
        <name>Number of Hours/Day Recommendation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recommendation_Notes_Justification</fullName>
        <field>Recommendation_Notes_Justification__c</field>
        <formula>Recommendation_Notes_Justification__c</formula>
        <name>Recommendation Notes/Justification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TTT_Name</fullName>
        <field>Name</field>
        <formula>Assessment_Name__r.Member_Name__c  &amp; &quot; - TTT - &quot; &amp; TEXT(Assessment_Name__r.Assessment_Date__c)</formula>
        <name>TTT Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Total_Hours_Per_Day</fullName>
        <field>Total_Hours_Per_Day__c</field>
        <formula>Total_Hours_Per_Day__c</formula>
        <name>Total Hours Per Day</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Approval</fullName>
        <description>When manager approval is indicated as required on the time task record, update the field on the Assessment record to YES to assist with following the appropriate steps in the approval process.</description>
        <field>Manager_Approval_Required_for_POC__c</field>
        <literalValue>Yes</literalValue>
        <name>Update Manager Approval - YES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Approval_NO</fullName>
        <field>Manager_Approval_Required_for_POC__c</field>
        <literalValue>No</literalValue>
        <name>Update Manager Approval - NO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Assessment_Name__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Days%2FHours Recommended</fullName>
        <actions>
            <name>Number_of_Hours_Day_Recommendation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Number_of_Hours_Recommended_by_Nurse__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Grand Total %28Minutes%29</fullName>
        <actions>
            <name>Grand_Total_Minutes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Grand_Total_Minutes__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Member Requires Routine Transportation - NO</fullName>
        <actions>
            <name>Member_Requires_Routine_Transportation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Member_Requires_Routine_Transportation__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Member Requires Routine Transportation - YES</fullName>
        <actions>
            <name>Member_Requires_Routine_Transportation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Member_Requires_Routine_Transportation__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Member Requires Supervision - NO</fullName>
        <actions>
            <name>Member_Requires_Supervision</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Member_Requires_Supervision__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Member Requires Supervision - YES</fullName>
        <actions>
            <name>Member_Requires_Supervision</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Member_Requires_Supervision__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mutual Case - NO</fullName>
        <actions>
            <name>Mutual_Case_NO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Mutual_Case1__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mutual Case - YES</fullName>
        <actions>
            <name>Mutual_Case_YES</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Mutual_Case1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Nighttime Needs</fullName>
        <actions>
            <name>Nighttime_Needs</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Member_Requires_Night_time_Needs__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Assessment__c.Type_of_Assessment__c</field>
            <operation>equals</operation>
            <value>Initial Assessment</value>
        </criteriaItems>
        <description>Sends an email alert to the public group &quot;Nighttime Needs&quot; when records have &quot;Yes&quot; as a value for &quot;Member Requires Night-time Needs.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Recommendation Notes%2FJustification</fullName>
        <actions>
            <name>Recommendation_Notes_Justification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Recommendation_Notes_Justification__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TTT Name Update</fullName>
        <actions>
            <name>TTT_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Total Hours Per Day</fullName>
        <actions>
            <name>Total_Hours_Per_Day</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Total_Hours_Per_Day__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Manager Approval - NO</fullName>
        <actions>
            <name>Update_Manager_Approval_NO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Is_Manager_Approval_Required__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>When manager approval is indicated as not required on the time task record, update the field on the Assessment record to NO to assist with following the appropriate steps in the approval process.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Manager Approval - YES</fullName>
        <actions>
            <name>Update_Manager_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Time_Task_Calculator__c.Is_Manager_Approval_Required__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>When manager approval is indicated as required on the time task record, update the field on the Assessment record to YES to assist with following the appropriate steps in the approval process.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
