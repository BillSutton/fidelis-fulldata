<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Comment_on_Events</fullName>
        <field>Event_Comment_Display_on_Form__c</field>
        <formula>&quot;Please ensure that all signatures are obtained as cost is greater than $1000.&quot;</formula>
        <name>Update Comment on Events</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Comment Display amount %3E %241000</fullName>
        <actions>
            <name>Update_Comment_on_Events</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Event__c.Requested_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>1000</value>
        </criteriaItems>
        <description>Comment to appear on event check requisition if the dollar amount is greater than $1000.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
