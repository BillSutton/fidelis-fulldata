/* ***************************************************************************** 
When a contact is created, create and attach to a new account record
If already an attached account, check subid's to ensure it should stay or need to reattach to existing/new account

1/14/2015  sva  Created

*********************************************************************************/

trigger contactInsertTrigger on Contact (before insert, before update) {
List<Account> accs = new List<Account>();                           // accounts whose subids or accountids match
List<Account> updateAcct = new List<Account>();                     // Accounts with null subids that will be updated with subscriber ids
Map<String, Integer> subChanges = new Map<String, Integer>();       // Track multiple non-null subid changes
Map<String, Id> accChangeBySub = new Map<String, id>();             // For multiple non-null subid changes to handle multiples
Map<String, account> accMapBySub = new Map<String, account>();      // Subid for accounts

Id FAMILY = null;
id CONMEMBER = null;
Account a;

try {
    FAMILY = [select id from recordtype where name = 'Family' and sObjectType = 'Account'].Id;
} catch (Exception e) {}

try {
    CONMEMBER = [select id from recordtype where name = 'Member/Prospect' and sObjectType = 'Contact'].Id;
} catch (Exception e) {}

// prelim loop to gather any existing subscriber id's plus accountid's
List<Id> accList = new List<Id>();
List<String> subList = new List<String>();
for (Contact c : Trigger.new)
{
    if (c.recordtypeid == CONMEMBER)
    {
        if (c.accountid != null) accList.add(c.accountid);
        if (c.Subscriber_ID__c != null) subList.add(c.Subscriber_ID__c);
    }
}

// Map by account id and by Subscriber Id
Map<Id, account> accMap = new Map<Id, account>([select id, Subscriber_ID__c, Internal_trigger_match__c  from account where id in :accList or Subscriber_ID__c in :subList]);
for (ID idx : accMap.keySet())
{
    a = accMap.get(idx);
    accMapBySub.put(a.subscriber_id__c, a);
}

Integer i = -1;

// Handle these cases:
// 1. No input sub id on contact, create new account without id
// 2. Input subid on contact, match to account with same subid (or create account if not there)
//     Note per 2. we can switch a contact to a different account based on subid
// 3. If contact is attached to an account with no subid, change the subid to the contact's
for (Contact c : Trigger.new)
{
    i++;
    a = null;
    if (c.accountid != null && c.recordtypeid == CONMEMBER)
    {
        a = accMap.get(c.accountid);
        if (a != null) // convert from null id to current contact id
        {
            if (a.subscriber_id__c == null && c.Subscriber_ID__c != null) // note we don't change the family name here
            {
                if (accMapBySub.containsKey(c.subscriber_id__c))
                    c.accountid = accMapBySub.get(c.subscriber_id__c).Id;
                else {
                    a.subscriber_id__c = c.subscriber_id__c;
                    updateAcct.add(a);
                    accMapBySub.put(c.subscriber_id__c, a); // add to map in case other contacts are related
                    accMap.put(a.id, a);
                }
            }
            else if (a.subscriber_id__c != c.Subscriber_ID__c) // need to create or attach to a created account
            {
                if (accMapBySub.containsKey(c.subscriber_id__c))
                    c.accountid = accMapBySub.get(c.subscriber_id__c).Id;
                else if (!subChanges.containsKey(c.subscriber_id__c))
                {
                    c.accountid = null; // let the next block create it
                    subchanges.put(c.subscriber_id__c,-1);
                }
            }
        }
    }
    if (c.accountid == null && c.recordtypeid == CONMEMBER)
    {
        Boolean bCreate = true;
        // no account, there is a subid and we have the account for that subid
        if (c.Subscriber_ID__c != null)
        {
            if (accMapBySub.containsKey(c.Subscriber_ID__c)) // already exists as account
            {   
                c.accountId = accMapBySub.get(c.Subscriber_ID__c).Id;
                bCreate = false;
            }
            else if (subchanges.containsKey(c.subscriber_id__c) && subchanges.get(c.subscriber_id__c) != -1) // already found this new sub id in block
            {
                bCreate = false;
            }
            else subchanges.put(c.subscriber_id__c,i); // first new sub id in this block so let it get created
            
        }
        if (bCreate) {
            a = new Account();
            a.name = c.lastname + ' Family'; 
            a.recordtypeid = FAMILY;
            a.Internal_Trigger_Match__c = i;
            a.Subscriber_ID__c = c.Subscriber_ID__c;
            accs.add(a);
        }
    }
}
update updateAcct; // add the null-to-subid guys -- same account, just new id

if (accs.size() > 0)
{
    insert accs;
    
    // So now we have accountids, stuff them into unattached contacts via trigger match
    for (Account aa : accs)
    {
        i = aa.Internal_Trigger_Match__c.intValue();
        if (aa.subscriber_id__c != null) accChangeBySub.put(aa.subscriber_id__c, aa.id);
        else Trigger.new[i].accountid = aa.id;
    }
    for (Contact c : Trigger.new)
    {
        if (c.recordtypeid == CONMEMBER && c.subscriber_id__c != null && accChangeBySub.containsKey(c.subscriber_id__c))
            c.accountid = accChangeBySub.get(c.subscriber_id__c);
    }


}
}