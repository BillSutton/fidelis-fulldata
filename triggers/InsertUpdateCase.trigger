/*
@Name           : InsertUpdateCase
@Author         : Scott Van Atta
@Date           : January 2016
@Description    : Trigger that routes inserts and updates to proper helper functions

 Revision History:-
 Version  Date        Author  Description of Action
 1.0      3/10/2015    SVA     Created
 1.1      5/18/2016    SVA     Added processing for retention
*/

trigger InsertUpdateCase on Case (before insert, before update){
    List<Case> routerList = new List<Case>(); // populate for router
    List<case> retentionList = new List<Case>();
    List<case> disenrollmentList = new List<Case>();
    List<case> initialPremiumList = new List<Case>();
    List<case> adhocList = new List<Case>();
    List<Case> delinquentList = new List<Case>();
    List<Case> webList = new List<Case>(); // populate for new web-to-case cases
    List<Case> statusList = new List<Case>(); // status reason changes
    List<Case> ownerList = new List<Case>(); // for non-route owner resets
    
    system.debug('***Starting InsertUpdateCase trigger with ' + Limits.getCpuTime() + ' ms used of total ' + Limits.getLimitCpuTime() + ' ms allowed');
    // get rectypes
    Map<id, string> recTypes = triggerHelper.buildRecTypes();
    system.debug('**TriggerHelper returned ' + recTypes.size() + ' rectypes');
    
    // Filter and build lists for appropriate helper functions
    for (Case c : Trigger.new)
    {
        if (Trigger.IsInsert && c.origin == 'Web')
        {
            webList.add(c);
            routerList.add(c); // new web cases need to be routed to marketing
        }
        else if (c.Primary_Category__c != null && c.status == 'Assigned' && recTypes.containsKey(c.recordtypeid) && 
                 recTypes.get(c.recordtypeid) != null && recTypes.get(c.recordtypeid).startsWith('Route') && 
                c.Rep_Assignment_Override__c==False && c.contactid != null)
        {
             if (recTypes.get(c.recordtypeid) == 'Route')
                routerList.add(c);
             else if (recTypes.get(c.recordtypeid) == 'RouteRet')
                retentionList.add(c);
             else if (recTypes.get(c.recordtypeid) == 'RouteDis')
                disenrollmentList.add(c);
             else if (recTypes.get(c.recordtypeid) == 'RouteInitial')
                initialPremiumList.add(c);
             else if (recTypes.get(c.recordtypeid) == 'RouteDelq')
                delinquentList.add(c);
            else adhocList.add(c);
        }
        else {
            system.debug('***case ' + c.casenumber + ' not routing: primary_category=' + c.primary_category__c + (c.primary_category__c != null ? '' : ' (**Cat ignore) ') + 
                                ' Status=' + c.status + (c.status == 'Assigned' ? '' : ' (**Status ignore) ') + ' recctypeid=' + c.recordtypeid + ' [' +
            recTypes.get(c.recordtypeid) +  
                         '] rep override = ' + c.rep_assignment_override__c + ' and contactid=' + c.contactid);
            if (c.rep_assignment__c != null) ownerList.add(c);
            if (c.status == 'Assigned')  
            {
                if (c.Rep_Assignment_Override__c==True)
                {
                    if (c.rep_assignment__c == null) 
                        c.addError('You must supply a representative when override is checked');
                    else c.status = 'In Process';
                }
                else c.status = 'Unassigned';
            }
        }
        
        // Gather status reason changes to update status
        if (Trigger.IsInsert || (Trigger.IsUpdate && c.status_reason__c != Trigger.oldMap.get(c.Id).status_reason__c))
            statusList.add(c);
        else if (c.status == 'Assigned') c.status = 'In Process';
    
    }
    
    // Call appropriate helpers based upon filters above

    system.debug('***Sizes: ' + (webList.size() > 0 ? ('web2case=' + webList.size()) : '') +
                    (routerList.size() > 0 ? (' Marketing=' + routerList.size()) : '') +
                    (retentionList.size() > 0 ? (' renewals=' +retentionList.size()) : '') +
                    (delinquentList.size() > 0 ? (' delinquents=' + delinquentList.size()) : '') +
                    (disenrollmentList.size() > 0 ? (' disenrolls=' + disenrollmentList.size()) : '') +
                    (initialPremiumList.size() > 0 ? (' InitialPremiums=' + initialPremiumList.size()) : '') +
                    (adhocList.size() > 0 ? (' adhocs=' + adhocList.size()) : '') +
                    ( webList.size() + routerList.size() + retentionList.size() + delinquentList.size() + disenrollmentList.size() + initialPremiumList.size() + adhocList.size() > 0 ? '' : '--No cases to route --'));
    if (webList.size() > 0)
        new WebToCase(webList, triggerHelper.getMedicareId()).process();  // we want to fall through to routing here
    if (routerList.size() > 0) TriggerHelper.callRouter(routerList, triggerHelper.MARKETPLACE);
    if (retentionList.size() > 0) TriggerHelper.callRouter(retentionList, triggerHelper.RENEWALS);
    if (delinquentList.size() > 0) TriggerHelper.callRouter(delinquentList, triggerHelper.DELINQUENCIES);
    if (disenrollmentList.size() > 0) TriggerHelper.callRouter(disenrollmentList, triggerHelper.DISENROLLMENT);
    if (initialPremiumList.size() > 0) TriggerHelper.callRouter(initialPremiumList, triggerHelper.INITIALPREMIUM);
    if (adhocList.size() > 0) TriggerHelper.callRouter(adhocList, triggerHelper.ADHOC);

    
    if (ownerList.size() > 0)  // update owners for remainder if not already done
        new CaseRouter(ownerList).processOwners(ownerList);
    
// update status reasons
    system.debug('***status reasons changed=' + statusList.size());
    if (statusList.size() > 0)
        new CaseStatusReasons(statusList);
    
    system.debug('***Ending InsertUpdateCase trigger with ' + Limits.getCpuTime() + ' ms used of total ' + Limits.getLimitCpuTime() + ' ms allowed');
} // end of trigger