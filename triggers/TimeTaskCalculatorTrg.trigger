/* *************************************************************************************
This trigger moves selected fields from changed/created TTT's to a transient table
for transfer to CCA
Date        Who             Change
11/20/2014  Scott Van Atta  Created
11/20/2015 Scott Van Atta  Modified for R2
04/08/2016  Sva            Function was trying to use assessment modifier name (not always in array) so changed to current user
05/10/2016  Sva            Added contrived key
*****************************************************************************************/

trigger TimeTaskCalculatorTrg on Time_Task_Calculator__c (after insert, after update){
Transient_TTT__c tran;
assessment__c ass;
List<Transient_TTT__c> trans = new List<Transient_TTT__c>();
Set<Id> userList = new Set<Id>();
Set<Id> assessList = new Set<Id>();

for (Time_Task_Calculator__c t : Trigger.new)
{
    userList.add(t.CreatedById);
    userlist.add(t.LastModifiedById);
    assessList.add(t.assessment_name__c);
}

Map<Id, User> users = new Map<Id, User>([Select id, name from user where id in :userList]);
Map<Id, Assessment__c> assess = new Map<Id, Assessment__c>(
            [select id, Case__r.Owner.name, case__r.Contact.name, CIN_Medicaid__c, Case__r.primary_category__c, 
                    LastModifiedById, Assessment_Status__c, Type_of_Assessment__c, Assigned_To__c, 
                    Date_of_Submission__c, Assessment_Date__c, case__r.contact.contrived_key__c
                    from assessment__c where id in :assessList]);

for (Time_Task_Calculator__c t : Trigger.new)
{
    if (assess.get(t.assessment_name__c).case__r.primary_category__c == 'FIDA')
    {
        tran = new Transient_TTT__c();
        ass = assess.get(t.assessment_name__c);
        tran.Original_Id__c = ass.Id;               
        tran.member_name__c = ass.case__r.contact.Name;
        tran.CreatedBy__c = users.get(t.CreatedById).name;
        // *** 4/2016 sva Change to current user for modifier name ***
        tran.ModifiedBy__c = UserInfo.getName();
        tran.CIN_Medicaid__c = ass.CIN_Medicaid__c;
        tran.Status__c = ass.Assessment_Status__c;
        tran.Type_of_Assessment__c = ass.Type_of_Assessment__c;
        tran.Assessment_Date__c = ass.Assessment_Date__c;
        tran.Assigned_To__c = ass.Assigned_To__c;
        tran.Date_of_Submission__c = ass.Date_of_Submission__c;

        tran.Number_of_Hours_Recommended_by_Nurse__c = t.Number_of_Hours_Recommended_by_Nurse__c;
        tran.Bathing_Score__c = t.Bathing_Score_Number__c;
        tran.Date_Measured__c = t.Date_Measured__c;
        tran.Dressing_Grooming_Score__c = t.Dressing_Grooming_Score_Number__c;
        tran.Feeding_Eating_Score__c = t.Feeding_Eating_Score_Number__c;
        tran.Grand_Total_Minutes__c = t.Grand_Total_Minutes__c;
        tran.Housekeeping_Score__c = t.Housekeeping_Score_Number__c;
        tran.Is_Manager_Approval_Required__c = t.Is_Manager_Approval_Required__c;
        tran.Laundry_Score__c = t.Laundry_Score_Number__c;
        tran.Meal_Planning_Prep_Score__c = t.Meal_Planning_Prep_Score_Number__c;
        tran.Medication_Reminder_Score__c = t.Medication_Reminder_Score_Number__c;
        tran.Member_Requires_Routine_Transportation__c = t.Member_Requires_Routine_Transportation__c;
        tran.Member_Requires_Supervision__c = t.Member_Requires_Supervision__c;
        tran.Recommendation_Notes_Justification__c = t.Recommendation_Notes_Justification__c;
        tran.Shopping_Score__c = t.Shopping_Score_Number__c;
        tran.Toileting_Score__c = t.Toileting_Score_Number__c;
        tran.Total_ADL_Score__c = t.Total_ADL_Score__c;
        tran.Total_Hours_Per_Day__c = t.Total_Hours_Per_Day__c;
        tran.Total_IADL_Score__c = t.Total_IADL_Score__c;
        tran.Walking_Locomotion_Score__c = t.Walking_Locomotion_Score_Number__c;
        tran.UAS_Score__c = t.UAS_Score__c;
        tran.contrived_key__c = ass.case__r.contact.contrived_key__c;
        trans.add(tran); 
    }
}
insert trans;
}