// UpdateRetentionCaseInformation
// @date: 6-27-2016
// @author: rgodard@fideliscare.org
// @description: Allows for task-driven updates to Case for Retention calls. The retention rep logs a call, 
// and the Case for that call is updated to show Call Attempts, and the time of the first and latest outreach.

// 10-12-2016 Update by Ryan: Only update cases which are of Retention record types: Ad-Hoc, Initial Premium, Disenrollment, Delinquent, and Renewal.

trigger UpdateRetentionCaseInformation on Task (after insert) {

    /* Retention Task Criteria:
    * 1. Is of the retention RT
    * 2. Has a parent Case
    * 3. Type of Task contains "Call"
    */
    
    // Gather all Retention tasks and associate them to their parent case
    Map<Id, List<Task>> caseIdToChildTasks = new Map<Id, List<Task>>();
    
    Id retentionRTId = [SELECT Id FROM RecordType WHERE SObjectType = 'Task' AND DeveloperName = 'Retention'].Id;
    
    Set<Id> originalTaskIds = new Set<Id>();
    for (Task t : Trigger.new) {
        if (t.RecordTypeId == retentionRTId) {
            if (t.whatId != null && t.WhatId.getSObjectType().getDescribe().getName() == 'Case' && t.Type != null && t.Type.contains('Call')) {
                // Confirmed that this is a task of Retention RT and with a parent Case
                if (caseIdToChildTasks.get(t.whatId) == null) {
                    List<Task> tempTaskList = new List<Task>();
                    tempTaskList.add(t);
                    caseIdToChildTasks.put(t.WhatId, tempTaskList);
                    originalTaskIds.add(t.Id);
                } else {
                    List<Task> tempTaskList = caseIdToChildTasks.get(t.WhatId);
                    tempTaskList.add(t);
                    caseIdToChildTasks.put(t.WhatId, tempTaskList);
                    originalTaskIds.add(t.Id);
                }
            }
        }
    }
    
    // Get other tasks to add to Case
    List<Task> legacyTasks = [Select Id, CreatedDate, WhatId, RecordTypeId, Type, Result__c from Task WHERE RecordTypeId = :retentionRTId AND WhatId in :caseIdToChildTasks.keySet() AND Id NOT in :originalTaskIds];
    for (Task t : legacyTasks) {
            if (t.Type.contains('Call')) {
                List<Task> tempTaskList = caseIdToChildTasks.get(t.WhatId);
                tempTaskList.add(t);
                caseIdToChildTasks.put(t.WhatId, tempTaskList);
            }
    }
    
    // Update all relevant case information
    // But only Cases which are of Retention Record Types
    
    Set<Id> caseRetentionRTIdSet = new Set<Id>();
        
    List<RecordType> retentionRTList = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND (DeveloperName = 'Renewals' OR DeveloperName = 'Delinquencies' OR DeveloperName = 'Disenrollment' OR DeveloperName = 'Initial_Premiums' OR DeveloperName = 'Ad_Hoc')];
        
    for (RecordType rt : retentionRTList) {
        caseRetentionRTIdSet.add(rt.Id);
    }
    
    List<Case> casesToUpdate = [Select Id, CaseNumber, Call_Attempts__c, First_Outreach__c, Previous_Outreach__c, Previous_Outreach_Resolution__c FROM Case WHERE ID in :caseIdToChildTasks.keySet() and RecordTypeID in :caseRetentionRTIdSet];
    
    for (Case c : casesToUpdate) {
        List<Task> tempTaskList = caseIdToChildTasks.get(c.Id);
        
        // Update relevant Case fields
        c.Call_Attempts__c = tempTaskList.size();
        
        // First Outreach
        if (tempTaskList.size() == 1) {
            c.First_Outreach__c = tempTaskList.get(0).CreatedDate;
            c.Previous_Outreach__c = tempTaskList.get(0).CreatedDate;
            c.Previous_Outreach_Resolution__c = tempTaskList.get(0).Result__c;
            
        // Multiple Outreaches
        } else if (tempTaskList.size() > 1) {
            DateTime latestDate = null;
            String previousOutreachResolution = '';
            for (Task t : tempTaskList) {
                if (latestDate == null) {
                    latestDate = t.CreatedDate;
                    previousOutreachResolution = t.Result__c;
                } else if (t.CreatedDate > latestDate) {
                    latestDate = t.CreatedDate;
                    previousOutreachResolution = t.Result__c;
                }
            }
            c.Previous_Outreach__c = latestDate;
            c.Previous_Outreach_Resolution__c = previousOutreachResolution;
        }
    }
    
    update casesToUpdate;
}