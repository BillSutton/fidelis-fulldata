/* *************************************************************************************
This trigger checks CCA transient table to pull reassessment requests from care managers.
If found, create a case for reassessments.
**Only for FIDA and CIN is required

Date        Who             Change
01/20/2015  Scott Van Atta  Modified for R2
01/20/2016  Sva             Modified for CK addition
*****************************************************************************************/
trigger fromCCA on Transient_Reassess_From_CCA__c (before insert) 
{
    List<String> subList = new List<String>();
    List<String> cinList = new List<String>();
    List<String> ckList = new List<String>();
    map<String, contact> subMap = new map<String, contact>();
    map<String, contact> cinMap = new map<String, contact>();
    map<String, contact> ckMap = new map<String, contact>();

    list<case> caseList = new List<case>();
    ID FIDAREC = [select id from recordtype where name = 'Care Management'].Id;
    
    for (Transient_Reassess_From_CCA__c trc : Trigger.new)
    {
        if (trc.Enacted__c != true)
        {
            if (trc.needed_by__c == null || trc.trigger_date__c == null) // not a valid reassess request
                trc.Enacted__c = true;
            else {
                if (trc.cin__c != null) cinList.add(trc.cin__c);
                if (trc.Subscriber_ID__c != null) subList.add(trc.Subscriber_ID__c);
                if (trc.contrived_key__c != null) ckList.add(trc.contrived_key__c);
            }
        }
    }
    List<contact> conList = [select id, Subscriber_ID__c, CIN__c, contrived_key__c from contact where CIN__c in :cinList or Subscriber_ID__c in :subList or contrived_key__c in :ckList];
    for (contact con : conList)
    {
        if (con.cin__c != null) cinMap.put(con.cin__c, con);
        if (con.Subscriber_ID__c != null) subMap.put(con.Subscriber_ID__c, con);
        if (con.contrived_key__c != null) ckMap.put(con.contrived_key__c, con);
    }
    
    Id idx;
    for (Transient_Reassess_From_CCA__c trc : Trigger.new)
    {
        if (trc.Enacted__c != true)
        {
            if (trc.contrived_key__c != null && ckMap.containsKey(trc.contrived_key__c))
                idx = ckMap.get(trc.contrived_key__c).id;
            else if (trc.Subscriber_ID__c != null && subMap.containsKey(trc.Subscriber_ID__c))
                idx = subMap.get(trc.Subscriber_ID__c).id;
            else if (trc.cin__c != null && cinMap.containsKey(trc.cin__c)) 
                idx = cinMap.get(trc.cin__c).Id;
            else idx = null;
            if (idx != null)
            {
                Case c = new Case(contactid=idx, recordtypeid=FIDAREC);
                c.primary_category__c = 'FIDA';
                c.date_received__c = trc.trigger_date__c;
                c.source_type__c = 'Other';
                c.status = 'Unassigned';
                c.status_code__c = 'AFC';
                c.status_reason__c = 'Awaiting First Contact';
                c.subject = trc.reason__c;
                c.High_Risk_Member__c = 'Yes';
                c.Source_Type__c = 'FIDA Triggered Assessment';
                c.source_detail__c = 'Reassessment';
                c.description = 'On ' + trc.trigger_date__c + ', the care manager requested that a reassessment is needed ' + trc.needed_by__c + ' (' + trc.no_later_than__c + ')';
                caseList.add(c);
            }
            trc.Enacted__c = true;
        }
    }
    insert caseList;
}