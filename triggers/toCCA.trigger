/* *************************************************************************************
This trigger moves selected fields from changed/created Casess to a transient table
for transfer to CCA - specifically refusal/declined info
Date        Who             Change
12/15/2014  Scott Van Atta  Created
01/20/2015  Scott Van Atta  Converted from leads to cases for R2
*****************************************************************************************/
trigger toCCA on Case (after update, after insert) {

Transient_Schedules_To_CCA__c x;
List<Transient_Schedules_To_CCA__c> ccas = new List<Transient_Schedules_To_CCA__c>();
List<Id> conList = new List<Id>();

for (case c : trigger.new)
{
    if (c.contactid != null && c.Primary_Category__c == 'FIDA')
        conList.add(c.contactid);
}
if (conList.size() > 0)
{
    Map<Id, Contact> conMap = new Map<Id, contact>([Select id, cin__c, contrived_key__c from contact where id in :conList and cin__c != null]);
    for (case c : trigger.new)
    {
        if (c.Primary_Category__c == 'FIDA' && (trigger.IsInsert || (trigger.IsUpdate && 
                    (trigger.oldmap.get(c.Id).Status_reason__c != c.status_reason__c ||
                     trigger.oldmap.get(c.Id).Status != c.status ||
                     trigger.oldmap.get(c.Id).refusal_reason__c != c.refusal_reason__c))))
        { // new or changed status and reason

        if ((c.status == 'In Process' && c.Status_Reason__c == 'Assessment Needed - Mbr Refused') ||
            (c.status == 'Closed' && c.Status_Reason__c == 'Not Taken Under Care'))
            {
                x = new Transient_Schedules_To_CCA__c();
                x.lead_id__c =  c.Id;
                if (conMap.containsKey(c.ContactId) && conMap.get(c.ContactId).cin__c != null)
                {
                    x.CIN__c = conMap.get(c.ContactId).cin__c;
                    x.UAS_Refusal_Date__c = date.today();
                    x.UAS_Refused__c = 'Yes';
                    x.uas_Refusal_Declination_Reason__c = c.refusal_reason__c;
                    x.contrived_key__c = conMap.get(c.ContactId).contrived_key__c;
                    ccas.add(x);
                }
            }
        }
        
    }
    insert ccas;
}
}