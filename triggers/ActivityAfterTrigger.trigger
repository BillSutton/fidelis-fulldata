/* *************************************************************************************
This trigger moves selected fields from changed/created Activities to a transient table
for transfer to CCA
Date        Who             Change
11/21/2014  Scott Van Atta  Created
04/17/15    Scott Van Atta  Convert to R2
05/10/2016  Scott Van Atta  Added contrived key
*****************************************************************************************/

trigger ActivityAfterTrigger on Event (after insert, after update){
    Transient_Schedules_To_CCA__c tran;
    List<Transient_Schedules_To_CCA__c> trans = new List<Transient_Schedules_To_CCA__c>();
    List<Id> caseList = new List<Id>();
    String obj;

    for (Event a : Trigger.new)
    {
        obj = a.whatid;
        if (obj != null && obj.startsWith('500') &&
            a.type == 'Assessment') 
            caseList.add(a.whatid);
    }

    Map<Id, Case> caseMap = new Map<Id, Case>(
            [select id, Primary_category__c, contact.cin__c, contact.contrived_key__c
                    from Case where id in :CaseList]);

    if (CaseMap.size() > 0)
    {    
        integer i = -1;
        for (Event actv : Trigger.new)
        {
            i++;
            
            if (actv.whatid != null && caseMap.containsKey(actv.whatid) &&   // is this attached to a case
                actv.type == 'Assessment' && 
                (trigger.IsInsert || (trigger.IsUpdate && actv.ActivityDateTime != ((Event) trigger.old[i]).ActivityDateTime))
                    && caseMap.get(actv.whatid).Primary_Category__c == 'FIDA')
            {
                tran = new Transient_Schedules_To_CCA__c ();
                tran.Activity_ID__c = actv.Id;
                tran.Scheduled_Date__c = actv.ActivityDateTime;
                tran.Subject__c = actv.Subject;
                tran.type__c = actv.type;
                tran.cin__c = CaseMap.get(actv.whatid).contact.cin__c;
                tran.contrived_key__c = caseMap.get(actv.whatid).contact.contrived_key__c;
                    // if trigger.isInsert -- mayneed to advise them it's an update
                    //tran.UAS_Declined__c = actv.;
                    //tran.UAS_Declined_Date__c = now();
                    //tran.UAS_Refusal_Declination_Reason__c = actv.
                    // tran.UAS_Refusal_Date__c = actv.
                    // tran.UAS_Refused__c = actv.
                trans.add(tran); 
            }
        }
        insert trans;
    }
}