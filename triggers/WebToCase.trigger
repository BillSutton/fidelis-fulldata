/* *************************************************************************************
This trigger handles web-to-case inteaction, primarily filling fields for default values.
It only trigger on insert and only deals with cases with an origin of Web

Audit info:
    Process = W2C
    Code =  AsIs if SalesForce matched to email
            FndC if we found a contact per name/zip
            CrtC if we created a contact

Date        Who             Change
10/08/2014  Scott Van Atta  Created

*****************************************************************************************/

trigger WebToCase on Case (before insert) {
ID MEDICARE = [select id from recordtype where name = 'Medicare' and sobjectType = 'Case'].id;
List <Case> candidates = new List<Case>();
List<Audit__c> auds = new List<Audit__c>();
String query = 'select id, name, otherpostalcode from contact where ';

    // check for candidate cases and build query for unattached ones
    for(Case c : Trigger.New)
    {
        if (c.origin == 'Web') // created by web-to-case, so setup defaults and search criteria
        {
            c.recordtypeid = MEDICARE;
            c.status = 'Assigned';
            c.status_reason__c = 'Awaiting First Contact';
            c.source_type__c = 'Website';
            c.source__c = 'Advertising/Internet';
            c.date_received__c = date.today();
            //c.channel__c = 'Direct';
            c.primary_category__c = 'Medicare';
            if (c.w2c_phone_type__c == null)
                c.w2c_phone_type__c = 'Self';
            if (c.contactid == null)
            {
                candidates.add(c);
                if (candidates.size() > 1) query += ' OR ';
                query += '(name = \'' + c.suppliedname + '\'';
                if (c.w2c_zip__c != null)
                    query += ' and otherpostalcode like \'' + c.w2c_zip__c + '%\'';
                query += ')';
            }
            else {
                auds.add(new Audit__c(Process__c = 'W2C', 
                                      Description2__c=makeAudDescr(c),
                                      Description__c = LoadDescr(c),
                                      Code__c='AsIs', Associated_Id__c = c.contactid));
            }
        }
    }
    
    // if any web cases are not connected to contacts, determine if we can match to existing
    // (based on first 5 of zip) or have to create a contact
    if (candidates.size() > 0)
    {
        List<Contact> contacts = database.query(query);
        List <Contact> newContact = new List<Contact>();
        Map <String, contact> newMap = new Map<String, contact>();
        Map<String, Contact> ContactMatch = new Map<String, Contact>();


        for (Contact con : contacts)
        {
            string s = con.name + (con.otherpostalcode == null ? '' : (con.otherpostalcode.length() > 5 ? con.otherpostalcode.substring(0,5) : con.otherpostalcode));
            contactMatch.put(s, con);
        }
            
        for (case c : candidates)
        {
            String s = c.suppliedname + (c.w2c_zip__c == null ? '' : (c.w2c_zip__c.length() > 5 ? c.w2c_zip__c.substring(0,5) : c.w2c_zip__c));
            Contact con = contactMatch.get(s);
            if (con != null) // matches existing contact by name and zip
            {
                c.contactid = con.id;
                auds.add(new Audit__c(Process__c = 'W2C', 
                                      Description2__c=makeAudDescr(c),
                                      Description__c = loadDescr(c),
                                      Code__c='FndC', Associated_Id__c = con.id));
            }
            else { // does not match, so create the contact
                String[] namePieces = c.SuppliedName.split(' ',2);
                if (namePieces.size() != 2)
                {
                    namePieces.add(namePieces[0]);
                    namePieces[0] = '';
                }
                    
                Contact cont = new Contact(FirstName=namePieces[0], 
                                            LastName=namePieces[1], 
                                            Email=c.SuppliedEmail, 
                                            language__c = c.w2c_language__c,
                                            phone_type__c = c.w2c_phone_type__c,
                                            otherpostalcode = c.w2c_zip__c,
                                            phone = c.suppliedphone
                                            );
                newContact.add(cont);
                newMap.put(c.suppliedName + c.SuppliedEmail + c.w2c_zip__c + c.suppliedPhone, cont);
                
            }
            
        }
        insert newContact; // add the new contacts if any
        
        // apply the contactid's to the cases for matches
        for (Case c : candidates)
        {
            if (c.contactid == null)
            {
                Contact con = newMap.get(c.suppliedName + c.SuppliedEmail + c.w2c_zip__c + c.suppliedPhone);
                if (con != null)
                {
                    c.contactid = con.id;
                    auds.add(new Audit__c(Process__c = 'W2C', 
                                          Description2__c=makeAudDescr(c), 
                                          Description__c = loadDescr(c),
                                          CaseNumber__c = c.casenumber,
                                          Code__c='CrtC', Associated_Id__c = c.contactid));
                }
                else {
                    auds.add(new Audit__c(Process__c = 'W2C', 
                                          Description2__c=makeAudDescr(c),
                                          Description__c = loadDescr(c),
                                          CaseNumber__c = c.casenumber,
                                          Code__c='CrtX', Error__c = '019-NoContact'));

                }

            }
        }
    }
    if (auds.size() > 0)
    {   
        try {
            insert auds;
        } catch (Exception e) { } // ensure logging does not interfere with case writes
    }

    
    public String makeAudDescr(Case c)
    {
        String s = 'Input=[' + c.Subject + '] [' + c.suppliedname + '] [' + c.SuppliedEmail + '] ['
                                            + c.w2c_language__c + '] [' + 
                                            c.w2c_phone_type__c + '] [' + c.w2c_zip__c + '] [' + 
                                            c.suppliedphone + ']';
        if (s.length() > 254) return s.substring(0,254);
        else return s;
    }
    
    public String LoadDescr(Case c)
    {
        if (c.description != null && c.description.length() > 254)
            return c.description.substring(0,254);
        else return c.description;
    }

}