/******************************************************************************
** Description   : Ensure terminated reps have their rep assignment records set inactive 
**                  and cleared if they are a covering rep. The work is handled in an async
**                  method since updating of records in this trigger is prevented by "Mixed DML" error
**          
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      10/16/2015    SVA   Created
**
******************************************************************************/
trigger UserRep on User (after update) {
    List <ID> termUsers = new List<ID>();

    // see if any users are retired candidates 
    for (SObject s : Trigger.new)
    {
        User u = (User) s;
        if (u.IsActive == false)
            termUsers.add(u.Id);
    }

    if (termUsers.size() > 0) Util.TermCheck(termUsers);
}