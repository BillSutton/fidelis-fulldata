<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Project Deliverables</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Project Deliverables</value>
    </caseValues>
    <fields>
        <help><!-- Please check if signed approval was recieved by client and it has been attached to the correct documentation --></help>
        <label><!-- Approval Recieved --></label>
        <name>Approval_Recieved__c</name>
    </fields>
    <fields>
        <help><!-- Please check if the request has been approved by CRM team --></help>
        <label><!-- Approved --></label>
        <name>Approved__c</name>
    </fields>
    <fields>
        <help><!-- This is the person who is responsible for building the solution in the application. --></help>
        <label><!-- Assigned Developer --></label>
        <name>Assigned_Developer__c</name>
        <relationshipLabel><!-- Project Deliverables --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Please indicate when the project was completed --></help>
        <label><!-- Completion Date --></label>
        <name>Completion_Date__c</name>
    </fields>
    <fields>
        <help><!-- This is the date that the request was received. --></help>
        <label><!-- Date Requested --></label>
        <name>Date_Requested__c</name>
    </fields>
    <fields>
        <help><!-- This is the date that work has started on this deliverable. --></help>
        <label><!-- Date Started --></label>
        <name>Date_Started__c</name>
    </fields>
    <fields>
        <label><!-- Documentation Attached --></label>
        <name>Documentation_Attached__c</name>
        <picklistValues>
            <masterLabel>Business Requirements Documentation</masterLabel>
            <translation><!-- Business Requirements Documentation --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Change Request Form</masterLabel>
            <translation><!-- Change Request Form --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Report Request Form</masterLabel>
            <translation><!-- Report Request Form --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Technical Documentation</masterLabel>
            <translation><!-- Technical Documentation --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Testing Documentation</masterLabel>
            <translation><!-- Testing Documentation --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Please choose whether it is a change request or a report request --></help>
        <label><!-- Enhancement Request Type --></label>
        <name>Enhancement_Request_Type__c</name>
        <picklistValues>
            <masterLabel>Change</masterLabel>
            <translation><!-- Change --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Report</masterLabel>
            <translation><!-- Report --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- This is the date of estimated completion.  If it is blank it may be an ongoing task.  Please see Frequency. --></help>
        <label><!-- Estimated Delivery Date --></label>
        <name>Estimated_Delivery_Date__c</name>
    </fields>
    <fields>
        <help><!-- Enter in the date for when the client finally approved the entire project after development was completed --></help>
        <label><!-- Final Approval --></label>
        <name>Final_Approval__c</name>
    </fields>
    <fields>
        <help><!-- This is the expected frequency of this deliverable. --></help>
        <label><!-- Frequency --></label>
        <name>Frequency__c</name>
        <picklistValues>
            <masterLabel>Annually</masterLabel>
            <translation><!-- Annually --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Daily</masterLabel>
            <translation><!-- Daily --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Monthly</masterLabel>
            <translation><!-- Monthly --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>One Time</masterLabel>
            <translation><!-- One Time --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quarterly</masterLabel>
            <translation><!-- Quarterly --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Scheduled</masterLabel>
            <translation><!-- Scheduled --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Weekly</masterLabel>
            <translation><!-- Weekly --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Please indicate the date of the Go Live --></help>
        <label><!-- Go Live Date --></label>
        <name>Go_Live_Date__c</name>
    </fields>
    <fields>
        <help><!-- Please select the date that the request was turned over from the BA to the Developer --></help>
        <label><!-- Handoff Date --></label>
        <name>Handoff_Date__c</name>
    </fields>
    <fields>
        <help><!-- Please select the priority of the request --></help>
        <label><!-- Priority --></label>
        <name>Priority__c</name>
        <picklistValues>
            <masterLabel>High</masterLabel>
            <translation><!-- High --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Low</masterLabel>
            <translation><!-- Low --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Medium</masterLabel>
            <translation><!-- Medium --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Please identify the project/phase that this deliverable is attributed to. --></help>
        <label><!-- Project Alignment --></label>
        <name>Project_Alignment__c</name>
        <picklistValues>
            <masterLabel>CRM for FIDA</masterLabel>
            <translation><!-- CRM for FIDA --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CRM for MLTC</masterLabel>
            <translation><!-- CRM for MLTC --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CRM for Marketing</masterLabel>
            <translation><!-- CRM for Marketing --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CRM for Member Services</masterLabel>
            <translation><!-- CRM for Member Services --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Production Support</masterLabel>
            <translation><!-- Production Support --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Release 2</masterLabel>
            <translation><!-- Release 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Release 3</masterLabel>
            <translation><!-- Release 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Release 4</masterLabel>
            <translation><!-- Release 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Release 5</masterLabel>
            <translation><!-- Release 5 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- This will autogenerate the Request Number regardless of type. --></help>
        <label><!-- Request Number --></label>
        <name>Request_Number__c</name>
    </fields>
    <fields>
        <help><!-- Please select how request was obtained --></help>
        <label><!-- Request Origin --></label>
        <name>Request_Origin__c</name>
        <picklistValues>
            <masterLabel>Email</masterLabel>
            <translation><!-- Email --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Help Desk</masterLabel>
            <translation><!-- Help Desk --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Manager</masterLabel>
            <translation><!-- Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Phone</masterLabel>
            <translation><!-- Phone --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>WalkUp</masterLabel>
            <translation><!-- WalkUp --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Please add the Sharepoint Link of where the Change Request or Report Request is located
Must be in Citrix for Sharepoint URL to work --></help>
        <label><!-- Request Sharepoint Link --></label>
        <name>Request_Sharepoint_Link__c</name>
    </fields>
    <fields>
        <help><!-- Please select if this is a new request or an existing one --></help>
        <label><!-- Request Status --></label>
        <name>Request_Status__c</name>
        <picklistValues>
            <masterLabel>Existing</masterLabel>
            <translation><!-- Existing --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New</masterLabel>
            <translation><!-- New --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Please enter the name and department of the person requesting this deliverable.  If this is part of a project/departmental initiative, you may enter just the department making the request. --></help>
        <label><!-- Requested By --></label>
        <name>Requested_By__c</name>
    </fields>
    <fields>
        <help><!-- Please explain in detail how the issue/request was resolved --></help>
        <label><!-- Resolution Details --></label>
        <name>Resolution_Details__c</name>
    </fields>
    <fields>
        <help><!-- Please summarize the deliverable in this area. --></help>
        <label><!-- Task/Request Description --></label>
        <name>Task_Request_Description__c</name>
    </fields>
    <fields>
        <label><!-- Task Status --></label>
        <name>Task_Status__c</name>
        <picklistValues>
            <masterLabel>Awaiting Signoff</masterLabel>
            <translation><!-- Awaiting Signoff --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Implemented</masterLabel>
            <translation><!-- Implemented --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Development</masterLabel>
            <translation><!-- In Development --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>On Going</masterLabel>
            <translation><!-- On Going --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Peer Review</masterLabel>
            <translation><!-- Peer Review --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Requested</masterLabel>
            <translation><!-- Requested --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revising</masterLabel>
            <translation><!-- Revising --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Stakeholder Review</masterLabel>
            <translation><!-- Stakeholder Review --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Testing</masterLabel>
            <translation><!-- Testing --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Type of Deliverable --></label>
        <name>Type_of_Deliverable__c</name>
        <picklistValues>
            <masterLabel>Custom Code</masterLabel>
            <translation><!-- Custom Code --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Design Document</masterLabel>
            <translation><!-- Design Document --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Discovery/Requirements</masterLabel>
            <translation><!-- Discovery/Requirements --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>History Tracking</masterLabel>
            <translation><!-- History Tracking --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New Field</masterLabel>
            <translation><!-- New Field --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New Functionality</masterLabel>
            <translation><!-- New Functionality --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other Configuration</masterLabel>
            <translation><!-- Other Configuration --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Production Task</masterLabel>
            <translation><!-- Production Task --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Report/Dashboard</masterLabel>
            <translation><!-- Report/Dashboard --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Role/Profile Change</masterLabel>
            <translation><!-- Role/Profile Change --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Training</masterLabel>
            <translation><!-- Training --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Validation Rule</masterLabel>
            <translation><!-- Validation Rule --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Workflow</masterLabel>
            <translation><!-- Workflow --></translation>
        </picklistValues>
    </fields>
    <layouts>
        <layout>Project Deliverables Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Explanation and Justification of the Deliverable --></label>
            <section>Explanation and Justification of the Deliverable</section>
        </sections>
        <sections>
            <label><!-- Resolution --></label>
            <section>Resolution</section>
        </sections>
        <sections>
            <label><!-- Status &amp; Approval --></label>
            <section>Status &amp; Approval</section>
        </sections>
    </layouts>
    <startsWith>Consonant</startsWith>
</CustomObjectTranslation>
