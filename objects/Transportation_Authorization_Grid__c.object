<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object contains these rules that determine whether or not the member is eligible for the transportation benefits based on the plan and/or member county associated to their plan.  Further, depending on the requested vehicle Type and non-adjacent counties, the rules determine whether or not either Authorization Number or/and Authorization Note is required.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>Authorization_Number__c</fullName>
        <description>Type of transportation requests requires an authorization number in Facets</description>
        <externalId>false</externalId>
        <inlineHelpText>Type of transportation requests requires an authorization number in Facets</inlineHelpText>
        <label>Authorization Number</label>
        <picklist>
            <picklistValues>
                <fullName>Livery</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulette</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulance</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Non-Adjacent Counties</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not Applicable</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>County_Covered__c</fullName>
        <description>Whether or not this county is covered under Fidelis for this plan</description>
        <externalId>false</externalId>
        <inlineHelpText>Whether or not this county is covered under Fidelis for this plan</inlineHelpText>
        <label>County Covered</label>
        <picklist>
            <picklistValues>
                <fullName>Yes</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>County__c</fullName>
        <externalId>false</externalId>
        <label>County</label>
        <referenceTo>County__c</referenceTo>
        <relationshipLabel>Transportation Authorization Grids</relationshipLabel>
        <relationshipName>Transportation_Authorization_Grids</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Effective_Date__c</fullName>
        <description>Start date when the grid is effective</description>
        <externalId>false</externalId>
        <inlineHelpText>Start date when the grid is effective</inlineHelpText>
        <label>Effective Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Form_Type__c</fullName>
        <description>Types of transportation form covered under this county and plan.</description>
        <externalId>false</externalId>
        <inlineHelpText>Types of transportation form covered under this county and plan.</inlineHelpText>
        <label>Form Type</label>
        <picklist>
            <picklistValues>
                <fullName>Standard</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hospital Discharge</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>HD_Auth_Form__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Doctor&apos;s authorization is required for these types of vehicle and/or non-adjacent counties.</inlineHelpText>
        <label>Hospital Discharge Authorization Form</label>
        <picklist>
            <picklistValues>
                <fullName>Non-Adjacent Counties</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not Applicable</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Livery</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulette</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulance</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Line_of_Business__c</fullName>
        <description>Created for Transportation Release.</description>
        <externalId>false</externalId>
        <formula>TEXT( Plan__r.Line_of_Business__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Line of Business</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Note_for_MSA__c</fullName>
        <description>This information will be displayed to MSA as a special Note on the top of the transportation request.  For example, member with Medicaid plans living in Rockland County requires AIM.  Or HBX does not cover Livery, routine Ambulete and Routine Ambulance.</description>
        <externalId>false</externalId>
        <inlineHelpText>This information will be displayed to MSA as a special Note on the top of the transportation request.  For example, member with Medicaid plans living in Rockland County requires AIM.  Or HBX does not cover Livery, routine Ambulete and Routine Ambulance.</inlineHelpText>
        <label>Note for MSA</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Plan__c</fullName>
        <externalId>false</externalId>
        <label>Plan Name</label>
        <referenceTo>Transportation_Plan__c</referenceTo>
        <relationshipLabel>Transportation Authorization Grids</relationshipLabel>
        <relationshipName>Transportation_Authorization_Grid_del</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Standard_Auth_Form__c</fullName>
        <description>Types of vehicles need the authorization by a doctor when scheduling of a standard request.</description>
        <externalId>false</externalId>
        <inlineHelpText>Doctor&apos;s authorization is required for these types of vehicle and/or non-adjacent counties.</inlineHelpText>
        <label>Standard Request Authorization Form</label>
        <picklist>
            <picklistValues>
                <fullName>Livery</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulette</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulance</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Non-Adjacent Counties</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not Applicable</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Termination_Date__c</fullName>
        <externalId>false</externalId>
        <label>Termination Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Vehicle_Type__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Types of Vehicle covered under this county and plan.</inlineHelpText>
        <label>Vehicle Type</label>
        <picklist>
            <picklistValues>
                <fullName>Livery</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulette</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulance</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <label>Transportation Authorization Grid</label>
    <listViews>
        <fullName>All_Plans</fullName>
        <columns>NAME</columns>
        <columns>Plan__c</columns>
        <columns>County__c</columns>
        <columns>County_Covered__c</columns>
        <columns>Effective_Date__c</columns>
        <columns>Termination_Date__c</columns>
        <columns>Form_Type__c</columns>
        <columns>Vehicle_Type__c</columns>
        <columns>Authorization_Number__c</columns>
        <columns>HD_Auth_Form__c</columns>
        <columns>Standard_Auth_Form__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Plans</label>
    </listViews>
    <listViews>
        <fullName>Transportation_Grid_NYM</fullName>
        <columns>NAME</columns>
        <columns>Plan__c</columns>
        <columns>County__c</columns>
        <columns>County_Covered__c</columns>
        <columns>Effective_Date__c</columns>
        <columns>Termination_Date__c</columns>
        <columns>Form_Type__c</columns>
        <columns>Vehicle_Type__c</columns>
        <columns>Authorization_Number__c</columns>
        <columns>HD_Auth_Form__c</columns>
        <columns>Standard_Auth_Form__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Line_of_Business__c</field>
            <operation>contains</operation>
            <value>Dual Flex</value>
        </filters>
        <label>Transportation Grid - NYM</label>
    </listViews>
    <nameField>
        <displayFormat>TAG-{000000000}</displayFormat>
        <label>Transportation Authorization Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Transportation Authorization Grid</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Plan__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>County__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Effective_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Termination_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Vehicle_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Standard_Auth_Form__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>HD_Auth_Form__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Note_for_MSA__c</customTabListAdditionalFields>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <listViewButtons>Update_MSA_Note</listViewButtons>
        <lookupDialogsAdditionalFields>Plan__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>County__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Effective_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Termination_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Vehicle_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Standard_Auth_Form__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>HD_Auth_Form__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Note_for_MSA__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Plan__c</searchFilterFields>
        <searchFilterFields>County__c</searchFilterFields>
        <searchFilterFields>Effective_Date__c</searchFilterFields>
        <searchFilterFields>Termination_Date__c</searchFilterFields>
        <searchFilterFields>Vehicle_Type__c</searchFilterFields>
        <searchFilterFields>Standard_Auth_Form__c</searchFilterFields>
        <searchFilterFields>HD_Auth_Form__c</searchFilterFields>
        <searchResultsAdditionalFields>Plan__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>County__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Effective_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Termination_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Vehicle_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Standard_Auth_Form__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>HD_Auth_Form__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Note_for_MSA__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Description_Required</fullName>
        <active>true</active>
        <errorConditionFormula>AND(  ISPICKVAL( County_Covered__c , &quot;No&quot;)  ,LEN(Note_for_MSA__c) = 0)</errorConditionFormula>
        <errorMessage>Note for MSA is required if County Covered is false</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Auth_Grid_Update</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Auth Grid Update</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/apex/authgridquickupdate</url>
    </webLinks>
    <webLinks>
        <fullName>Update_MSA_Note</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Update MSA Note</masterLabel>
        <openType>sidebar</openType>
        <page>TransportationAuthGridMassEdit</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
</CustomObject>
