<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object tracks notes that are indicating a member is authorized for a given type of service.  This will allow staff to determine whether a requested service can be scheduled.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Authorization_Type__c</fullName>
        <externalId>false</externalId>
        <label>Authorization Type</label>
        <picklist>
            <picklistValues>
                <fullName>Out of County</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Vehicle Type</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Destination_Provider_Address__c</fullName>
        <externalId>false</externalId>
        <label>Destination Provider Address</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Destination_Provider_Name__c</fullName>
        <externalId>false</externalId>
        <label>Destination Provider Name</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Destination_Provider_Specialty__c</fullName>
        <externalId>false</externalId>
        <label>Destination Provider Specialty</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Effective_Date__c</fullName>
        <externalId>false</externalId>
        <label>Effective Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Member__c</fullName>
        <externalId>false</externalId>
        <label>Member</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Transportation Authorization Notes</relationshipLabel>
        <relationshipName>Transportation_Authorization_Notes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Provider_Authorization_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Provider Authorization Notes</label>
        <length>131072</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Pending Expedited</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pending Review</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Approved</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rejected</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Termination_Date__c</fullName>
        <externalId>false</externalId>
        <label>Termination Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Vehicle_Type__c</fullName>
        <externalId>false</externalId>
        <label>Vehicle Type</label>
        <picklist>
            <picklistValues>
                <fullName>Livery</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulance</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ambulette</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Transportation Authorization Note</label>
    <nameField>
        <displayFormat>TDA-{000000000}</displayFormat>
        <label>Authorization Notes Id</label>
        <trackFeedHistory>false</trackFeedHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Transportation Authorization Notes</pluralLabel>
    <recordTypeTrackFeedHistory>false</recordTypeTrackFeedHistory>
    <recordTypes>
        <fullName>Transportation_Authorization_Note_Out_of_County</fullName>
        <active>true</active>
        <description>for Out of County Related Transportation Authorization Notes</description>
        <label>Transportation Authorization Note - Out of County</label>
        <picklistValues>
            <picklist>Authorization_Type__c</picklist>
            <values>
                <fullName>Out of County</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending Expedited</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending Review</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Vehicle_Type__c</picklist>
            <values>
                <fullName>Livery</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Transportation_Authorization_Note_Vehicle_Type</fullName>
        <active>true</active>
        <description>for Vehicle Related Transportation Authorization Notes</description>
        <label>Transportation Authorization Note - Vehicle Type</label>
        <picklistValues>
            <picklist>Authorization_Type__c</picklist>
            <values>
                <fullName>Vehicle Type</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending Expedited</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending Review</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Vehicle_Type__c</picklist>
            <values>
                <fullName>Ambulance</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Ambulette</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Livery</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Member__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Vehicle_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Effective_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Termination_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Provider_Authorization_Notes__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Member__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Vehicle_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Effective_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Termination_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Provider_Authorization_Notes__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Member__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Vehicle_Type__c</searchFilterFields>
        <searchFilterFields>Effective_Date__c</searchFilterFields>
        <searchFilterFields>Termination_Date__c</searchFilterFields>
        <searchFilterFields>Provider_Authorization_Notes__c</searchFilterFields>
        <searchResultsAdditionalFields>Member__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Vehicle_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Effective_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Termination_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Provider_Authorization_Notes__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>OutOfCountyFields</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
   ISPICKVAL( Authorization_Type__c ,&quot;Out of County&quot;)
  ,
  OR(ISPICKVAL(Status__c,&quot;Approved&quot;), ISPICKVAL(Status__c,&quot;Rejected&quot;)),
  OR(
    ISBLANK(Effective_Date__c),
    ISBLANK(Termination_Date__c),
    ISBLANK(Destination_Provider_Name__c),
    ISBLANK(Destination_Provider_Address__c)
  )
)</errorConditionFormula>
        <errorMessage>Please complete all required fields before saving.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>VehicleTypeFields</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
  ISPICKVAL(Authorization_Type__c,&quot;Vehicle Type&quot;)
  ,
  OR(ISPICKVAL(Status__c,&quot;Approved&quot;), ISPICKVAL(Status__c,&quot;Rejected&quot;)),
  
  OR(
    ISBLANK( Effective_Date__c ),
    ISBLANK( Termination_Date__c ),
    ISPICKVAL(Vehicle_Type__c,&quot;&quot;)
  )
)</errorConditionFormula>
        <errorMessage>Please complete all required fields before saving.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Healthcare_Provider_Search</fullName>
        <availability>online</availability>
        <description>Healthcare Provider Address Search</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Healthcare Provider Search</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/apex/PCPChange?id={!Transportation_Authorization_Notes__c.Id}&amp;createCase=false</url>
    </webLinks>
</CustomObject>
