<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Fidelis</label>
    <logo>System_Administrator_Documents/Smaller_FOCUS_Logo.jpg</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Rep_Assignment__c</tab>
    <tab>Project_Deliverables__c</tab>
    <tab>Assessment__c</tab>
    <tab>Audit__c</tab>
    <tab>WebServiceLog__c</tab>
    <tab>RepCaseCount__c</tab>
</CustomApplication>
