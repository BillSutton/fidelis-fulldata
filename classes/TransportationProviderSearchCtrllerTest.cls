@IsTest
public class TransportationProviderSearchCtrllerTest {
    
    public static Case getCase(Account a, DateTime dt){
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        
        Contact con = new Contact();
            con.LastName = 'Smith';
            con.Line_of_Business__c = 'HBX';
        insert con;
        
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = dt;
            c.Return_Datetime__c = dt.addHours(3);
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'Yes';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Procedure_Code__c = 'Livery - A0100';
            c.Rational_for_LoA__c = 'This is a very good reason for an LoA';
            c.Reason_for_LoA__c = 'Dead Miles';
        insert c;
        return c;
    }

public static Case getCase2(Account a, DateTime dt){
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        
        Contact con = new Contact();
            con.LastName = 'Smith';
            con.Line_of_Business__c = 'HBX';
        insert con;
        
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = dt;
            c.Return_Datetime__c = dt.addHours(3);
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'Yes';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Procedure_Code__c = 'Livery - A0100';
            c.Rational_for_LoA__c = 'This is a very good reason for an LoA';
            c.Reason_for_LoA__c = 'Dead Miles';
        insert c;
        return c;
    }
        
    public static Account getAcct(String type, Date d){
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.Name = 'test';
            a.RecordTypeId = rtAcct.Id;
            a.BillingStreet = '123 Test Ave.';
            a.BillingState = 'NY';
            a.BillingPostalCode = '14203';
            a.EIN__c = 'abc123';
            a.Type = type;
            a.Facets_ID__c = 'abc123';
            a.Status__c = 'Active';
            a.Daily_Trip_Capacity__c = 3;
            a.Servicing_Pickup_Counties__c = 'Kings';
            a.Servicing_Dropoff_Counties__c = 'Kings';
            a.Type_of_Vehicle__c  = 'Livery';
            a.Open_Hours_of_Operation_Saturday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Sunday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Weekdays__c = '12:00 AM';
            a.Closed_Hours_of_Operation_Saturday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Sunday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Weekdays__c = '10:00 PM';
        insert a;
        
        Transportation_Contract_History__c tch = new Transportation_Contract_History__c();
            tch.Account__c = a.Id;
            tch.Line_of_Business__c = 'HBX';
            tch.Effective_Date__c = d.addMonths(-1);
            tch.Termination_Date__c = d.addMonths(1);
        insert tch;
        
        return a;
    }
    
    @IsTest
    public static void test01(){
        Account a = getAcct('In Network', Date.NewInstance(2015,07,22));
        Case c = getCase(a,DateTime.NewInstance(2015,07,22,10,30,0));
        Case c2 = getCase(a,DateTime.NewInstance(2015,07,22,9,30,0));
        
        Transportation_User_Error_Messages__c tpsError = new Transportation_User_Error_Messages__c();
            tpsError.Name = 'Transp Provider Search No Result';
            tpsError.Transportation_Error_Msg_Detail__c = 'test';
        insert tpsError;
        
        Test.startTest();
            PageReference pageRef = Page.TransportationProviderSearch;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('caseId',c.Id);
            TransportationProviderSearchController tps = new TransportationProviderSearchController();
            tps.selID = a.ID;
            tps.selectAcct();
            tps.selectVOAcct();
            tps.newContact.LastName = 'Test Person';
            tps.newContact.Phone = 'Test';
            tps.finishVO();
            tps.goBackToVO();
        Test.StopTest();        
    }
    
    @IsTest
    public static void test02(){
        Account a = getAcct('In Network', Date.NewInstance(2014,07,22));
        Case c = getCase(a,DateTime.NewInstance(2015,07,22,10,30,0));
        Case c2 = getCase(a,DateTime.NewInstance(2015,07,22,9,30,0));
        
        Transportation_User_Error_Messages__c tpsError = new Transportation_User_Error_Messages__c();
            tpsError.Name = 'Transp Provider Search No Result';
            tpsError.Transportation_Error_Msg_Detail__c = 'test';
        insert tpsError;
        
        Test.startTest();
            PageReference pageRef = Page.TransportationProviderSearch;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('caseId',c.Id);
            TransportationProviderSearchController tps = new TransportationProviderSearchController();
            tps.selectVOAcct();
            //tps.acceptMedicaid = false;
            tps.newContact.LastName = 'Test Person';
            tps.newContact.Phone = 'Test';
            tps.theCase.Procedure_Code__c = 'Test';
            tps.theCase.Rational_for_LoA__c = 'Test';
            tps.selID = a.ID;
            tps.selectAcct();
            tps.finishVO();
            tps.goBack();
        Test.StopTest();        
    }
    
    @IsTest
    public static void test03(){
        Transportation_User_Error_Messages__c tpsError = new Transportation_User_Error_Messages__c();
            tpsError.Name = 'Transp Provider Search No Result';
            tpsError.Transportation_Error_Msg_Detail__c = 'test';
        insert tpsError;
        
        //Account a = getAcct('In Network', Date.NewInstance(2014,07,22));
        Case c = getCase(new Account(),DateTime.NewInstance(2015,07,22,10,30,0));
        
        Test.startTest();
            PageReference pageRef = Page.TransportationProviderSearch;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('caseId',c.Id);
            TransportationProviderSearchController tps = new TransportationProviderSearchController();
            
            tps.createAcct();
            //tps.acceptMedicaid = true;
            tps.createAcct();
        
            tps.newAcct.Name = 'test 123';
            tps.newAcct.BillingStreet = '123 Test Ave.';
            tps.newAcct.BillingCity = 'Buffalo';
            tps.newAcct.BillingState = 'NY';
            tps.newAcct.BillingPostalCode = '14203';
            tps.newAcct.Phone = '7161111111';
            tps.newAcct.Fax = '7161111111';
            tps.newAcct.EIN__c = 'abc123';
            tps.newAcct.Facets_ID__c = 'abc123';
            tps.newAcct.Status__c = 'Active';
            tps.newAcct.Daily_Trip_Capacity__c = 3;
            tps.newAcct.Servicing_Pickup_Counties__c = 'Kings';
            tps.newAcct.Servicing_Dropoff_Counties__c = 'Kings';
            tps.newAcct.Type_of_Vehicle__c  = 'Livery';
            tps.theCase.Procedure_Code__c = '12:00 AM';
            tps.theCase.Reason_for_LoA__c = '12:00 AM';
            tps.theCase.Rational_for_LoA__c = '12:00 AM';
            tps.newAcct.Closed_Hours_of_Operation_Saturday__c = '10:00 PM';
            tps.newAcct.Closed_Hours_of_Operation_Sunday__c = '10:00 PM';
            tps.newAcct.Closed_Hours_of_Operation_Weekdays__c = '10:00 PM';
            tps.newContact.LastName = 'Test Person';
            tps.newContact.Phone = 'Test';
            
            tps.createAcct();
            
        Test.StopTest();       
    }
    
    static testMethod void test04(){  
        Account account = new Account(Name = 'test');
        insert account;
        Contact contact = new Contact(LastName = 'test', AccountId = account.Id);
        insert contact;
        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        CaseSidePanelController cspc = new CaseSidePanelController(sc);
        TransportationProviderSearchController tps = new TransportationProviderSearchController(cspc);
        tps.isRecurring = true;
        String testCaseId = apexpages.currentpage().getparameters().get('');
    }
    
    @IsTest
    public static void test05(){
        //if (caseId == null || String.isEmpty(caseId))
        Account a = getAcct('In Network', Date.NewInstance(2015,07,22));
        Case c = getCase2(a,DateTime.NewInstance(2015,07,22,10,30,0));
        Case c2 = getCase2(a,DateTime.NewInstance(2015,07,22,9,30,0));               
    }
    
    @IsTest
    public static void test06(){
        //if ( theCase.Return_Datetime__c == null )
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.Name = 'test';
            a.RecordTypeId = rtAcct.Id;
            a.BillingStreet = '123 Test Ave.';
            a.BillingState = 'NY';
            a.BillingPostalCode = '14203';
            a.EIN__c = 'abc123';
            a.Type = 'In Network';
            a.Facets_ID__c = 'abc123';
            a.Status__c = 'Active';
            a.Daily_Trip_Capacity__c = 3;
            a.Servicing_Pickup_Counties__c = 'Kings';
            a.Servicing_Dropoff_Counties__c = 'Kings';
            a.Type_of_Vehicle__c  = 'Livery';
            a.Open_Hours_of_Operation_Saturday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Sunday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Weekdays__c = '12:00 AM';
            a.Closed_Hours_of_Operation_Saturday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Sunday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Weekdays__c = '10:00 PM';
        insert a;
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        
        Contact con = new Contact();
            con.LastName = 'Smith';
            con.Line_of_Business__c = 'HBX';
        insert con;
        
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 15, 12, 00, 00); //Case: if (dayOfWeek.equalsIgnoreCase('sat')){
            c.Return_Datetime__c = null;
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'Yes';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Procedure_Code__c = 'Livery - A0100';
            c.Rational_for_LoA__c = 'This is a very good reason for an LoA';
            c.Reason_for_LoA__c = 'Dead Miles';
        insert c;     
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id = :c.Id];
        TransportationProviderSearchController tps = new TransportationProviderSearchController();
        tps.theCase=theCase;
        Test.startTest();
            PageReference pageRef = Page.TransportationProviderSearch;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('caseId',c.Id);
            tps.selID = a.ID;
            tps.selectAcct();
            tps.selectVOAcct();
            tps.newContact.LastName = 'Test Person';
            tps.newContact.Phone = 'Test';
            tps.finishVO();
            tps.goBackToVO();
            tps.theCase = theCase;
        Test.stopTest();
    }
    
    @IsTest
    public static void test07(){
        //if ( theCase.Return_Datetime__c == null )
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.Name = 'test';
            a.RecordTypeId = rtAcct.Id;
            a.BillingStreet = '123 Test Ave.';
            a.BillingState = 'NY';
            a.BillingPostalCode = '14203';
            a.EIN__c = 'abc123';
            a.Type = 'In Network';
            a.Facets_ID__c = 'abc123';
            a.Status__c = 'Active';
            a.Daily_Trip_Capacity__c = 3;
            a.Servicing_Pickup_Counties__c = 'Kings';
            a.Servicing_Dropoff_Counties__c = 'Kings';
            a.Type_of_Vehicle__c  = 'Livery';
            a.Open_Hours_of_Operation_Saturday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Sunday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Weekdays__c = '12:00 AM';
            a.Closed_Hours_of_Operation_Saturday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Sunday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Weekdays__c = '10:00 PM';
        insert a;
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        
        Contact con = new Contact();
            con.LastName = 'Smith';
            con.Line_of_Business__c = 'HBX';
        insert con;
        
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 16, 12, 00, 00); //Case: if (dayOfWeek.equalsIgnoreCase('sun')){
            c.Return_Datetime__c = null;
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'Yes';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Procedure_Code__c = 'Livery - A0100';
            c.Rational_for_LoA__c = 'This is a very good reason for an LoA';
            c.Reason_for_LoA__c = 'Dead Miles';
        insert c;     
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id = :c.Id];
        
        Test.startTest();
            PageReference pageRef = Page.TransportationProviderSearch;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('caseId',c.Id);
            Transportation_User_Error_Messages__c errMessage = new Transportation_User_Error_Messages__c(Name='Transp Provider Search No Result',Transportation_Error_Msg_Detail__c='error');
            insert errMessage;
            TransportationProviderSearchController tps = new TransportationProviderSearchController();
            tps.selID = a.ID;
            tps.selectAcct();
            tps.selectVOAcct();
            tps.newContact.LastName = 'Test Person';
            tps.newContact.Phone = 'Test';
            tps.finishVO();
            tps.goBackToVO();
            tps.theCase = theCase;
        Test.stopTest();
    }
    
    @IsTest
    public static void test08(){
        //if ( theCase.Return_Datetime__c == null )
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.Name = 'test';
            a.RecordTypeId = rtAcct.Id;
            a.BillingStreet = '123 Test Ave.';
            a.BillingState = 'NY';
            a.BillingPostalCode = '14203';
            a.EIN__c = 'abc123';
            a.Type = 'In Network';
            a.Facets_ID__c = 'abc123';
            a.Status__c = 'Active';
            a.Daily_Trip_Capacity__c = 3;
            a.Servicing_Pickup_Counties__c = 'Kings';
            a.Servicing_Dropoff_Counties__c = 'Kings';
            a.Type_of_Vehicle__c  = 'Livery';
            a.Open_Hours_of_Operation_Saturday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Sunday__c = '12:00 AM';
            a.Open_Hours_of_Operation_Weekdays__c = '12:00 AM';
            a.Closed_Hours_of_Operation_Saturday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Sunday__c = '10:00 PM';
            a.Closed_Hours_of_Operation_Weekdays__c = '10:00 PM';
        insert a;
        TransportationProviderSearchController tps = new TransportationProviderSearchController();
    }
    
    @isTest
    static void test09(){
        TransportationProviderSearchController tps = new TransportationProviderSearchController();
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.RecordTypeId = rtAcct.Id;
            a.Name = 'test';
            a.BillingStreet = 'test';
            a.BillingState = 'test';
            a.BillingPostalCode = 'test';
            a.EIN__c = null;
            a.Phone = null;
            a.Fax = null;
            a.Status__c = 'Active';
        insert a;
                
        Contact con = new Contact();
            con.LastName = 'test';
            con.FirstName = null;
            con.Phone = null;
        insert con;
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 15, 12, 00, 00); //Case: if (dayOfWeek.equalsIgnoreCase('sat')){
            c.Return_Datetime__c = null;
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'No';
            c.Procedure_Code__c = 'test';
            c.Rational_for_LoA__c = 'test';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Reason_for_LoA__c = 'test';
        insert c;     
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id = :c.Id];
        Account newAcct = [select Id, Name, BillingStreet, BillingState, BillingPostalCode, Phone, Fax, EIN__c from Account where Id = :a.Id];
        Contact newContact = [select Id, LastName, FirstName, Phone from Contact where Id = :con.Id];
        tps.theCase = theCase;
        tps.newAcct = newAcct;
        tps.newContact = newContact;
        tps.createAcct();
    }
    
    @IsTest
    public static void test10(){
                TransportationProviderSearchController tps = new TransportationProviderSearchController();
       RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.RecordTypeId = rtAcct.Id;
            a.Name = 'test';
            a.BillingStreet = 'test';
            a.BillingState = 'test';
            a.BillingPostalCode = 'test';
            a.EIN__c = null;
            a.Phone = null;
            a.Fax = null;
            a.Status__c = 'Active';
        insert a;
                
        Contact con = new Contact();
            con.LastName = 'test';
            con.Phone = '718-404-2237';
        insert con;
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 15, 12, 00, 00); //Case: if (dayOfWeek.equalsIgnoreCase('sat')){
            c.Return_Datetime__c = null;
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'No';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Procedure_Code__c = '123';
            c.Rational_for_LoA__c = '123';
            c.Reason_for_LoA__c = '123';
        insert c;     
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id = :c.Id];
        Account newAcct = [select Id, Name, BillingStreet, BillingState, BillingPostalCode, Phone, Fax, EIN__c from Account where Id = :a.Id];
        Contact newContact = [select Id, LastName, FirstName, Phone from Contact where Id = :con.Id];
        tps.theCase = theCase;
        tps.newAcct = newAcct;
        tps.newContact = newContact;
        
        tps.finishVO();
    }
    
    @IsTest
    public static void test11(){
        TransportationProviderSearchController tps = new TransportationProviderSearchController();
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.RecordTypeId = rtAcct.Id;
            a.Name = 'test';
            a.BillingStreet = 'test';
            a.BillingState = 'test';
            a.BillingPostalCode = 'test';
            a.EIN__c = null;
            a.Phone = null;
            a.Fax = null;
            a.Status__c = 'Active';
        insert a;
                
        Contact con = new Contact();
            con.LastName = ' test ';
            con.FirstName = 'test';
            con.Phone = '718-404-2237';
        insert con;
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 15, 12, 00, 00); //Case: if (dayOfWeek.equalsIgnoreCase('sat')){
            c.Return_Datetime__c = null;
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'No';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Procedure_Code__c = 'test';
            c.Reason_For_LOA__c = 'test';
            c.Rational_for_LoA__c = 'test';
        insert c;     
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id = :c.Id];
        Account newAcct = [select Id, Name, BillingStreet, BillingState, BillingPostalCode, Phone, Fax, EIN__c from Account where Id = :a.Id];
        Contact newContact = [select Id, LastName, FirstName, Phone from Contact where Id = :con.Id];
        tps.theCase = theCase;
        tps.newAcct = newAcct;
        tps.newContact = newContact;
        
        tps.finishVO();
    }
    
    @IsTest
    public static void test12(){
        TransportationProviderSearchController tps = new TransportationProviderSearchController();
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.RecordTypeId = rtAcct.Id;
            a.Name = 'test';
            a.BillingStreet = 'test';
            a.BillingState = 'test';
            a.BillingPostalCode = 'test';
            a.EIN__c = null;
            a.Phone = null;
            a.Fax = null;
            a.Status__c = 'Active';
        insert a;
                
        Contact con = new Contact();
            con.LastName = ' test ';
            con.Phone = '718-404-2237';
        insert con;
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 15, 12, 00, 00); //Case: if (dayOfWeek.equalsIgnoreCase('sat')){
            c.Return_Datetime__c = null;
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'No';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Procedure_Code__c = 'test';
            c.Rational_for_LoA__c = 'test';
            c.Reason_for_LoA__c = 'test';
        insert c;     
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id = :c.Id];
        Account newAcct = [select Id, Name, BillingStreet, BillingState, BillingPostalCode, Phone, Fax, EIN__c from Account where Id = :a.Id];
        Contact newContact = [select Id, LastName, FirstName, Phone from Contact where Id = :con.Id];
        tps.theCase = theCase;
        tps.newAcct = newAcct;
        tps.newContact = newContact;
        
        tps.finishVO();
    }    
    
    @IsTest
    public static void test13(){
        TransportationProviderSearchController tps = new TransportationProviderSearchController();
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
            a.RecordTypeId = rtAcct.Id;
            a.Name = 'test';
            a.BillingStreet = 'test';
            a.BillingState = 'test';
            a.BillingPostalCode = 'test';
            a.EIN__c = null;
            a.Phone = null;
            a.Fax = null;
            a.Status__c = 'Active';
        insert a;
                
        Contact con = new Contact();
            con.LastName = 'test';
            con.Phone = null;
        insert con;
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Transportation_Provider__c = a.Id; 
            c.Pickup_County__c = 'Kings';
            c.Drop_Off_County__c = 'Kings';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 15, 12, 00, 00); //Case: if (dayOfWeek.equalsIgnoreCase('sat')){
            c.Return_Datetime__c = null;
            c.Vehicle_Type__c = 'Livery';
            c.of_Riders__c = '1';
            c.Accepting_Medicare_or_Medicaid_Rates__c = 'No';
            c.Contact_Person_for_LoA__c = 'Test';
            c.Contact_Number_for_LoA__c = '2121231234';
            c.Reason_for_LoA__c = 'test';
            c.Procedure_Code__c = 'test';
            c.Rational_for_LoA__c = 'test';
        insert c;     
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id = :c.Id];
        Account newAcct = [select Id, Name, BillingStreet, BillingState, BillingPostalCode, Phone, Fax, EIN__c from Account where Id = :a.Id];
        Contact newContact = [select Id, LastName, FirstName, Phone from Contact where Id = :con.Id];
        
        Task caseTask = new Task();
        caseTask.Subject = 'Add Claim Note to the  Authorization in Facets';
        caseTask.Description = 'If LOA is finalized with Contract Mgmt, please add the appropriate Claim Note to the corresponding authorization number in Facets';
        caseTask.IsReminderSet = TRUE;
        caseTask.ReminderDateTime = system.now().addDays(7);
        caseTask.ActivityDate = date.valueof(caseTask.ReminderDateTime);
        caseTask.WhatId = theCase.Id;
        caseTask.OwnerId = UserInfo.getUserId();
        insert caseTask;
        
        tps.theCase = theCase;
        tps.newAcct = newAcct;
        tps.newContact = newContact;
        tps.finishVO();
    }  
    
}