@isTest
public class TransportationAuthGridMassEditCtrlTest{
    public static TestMethod void test1(){
        Transportation_Plan__c p = new Transportation_Plan__c ();
        insert p;
        County__c c = new County__c();
        insert c;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(Plan__c =p.Id,County__c=c.Id);
        insert tag;
    
        ApexPages.StandardSetController standardsObjectController = new ApexPages.StandardSetController(new List<Transportation_Authorization_Grid__c >{tag});
        standardsObjectController.setSelected(new List<Transportation_Authorization_Grid__c >{ tag});
        TransportationAuthGridMassEditController myExt= new TransportationAuthGridMassEditController(standardsObjectController);
        myExt.msaNote = 'testing123';
        myExt.massSave();
        List<Transportation_Authorization_Grid__c> cl = [Select id,Note_For_MSA__c from Transportation_Authorization_Grid__c where Id = :tag.Id];
        if(cl!=null && cl.size()>0){
            System.assertEquals(cl[0].Note_For_MSA__c, myExt.msaNote);
        }
    }
}