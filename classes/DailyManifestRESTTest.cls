@isTest public class DailyManifestRESTTest {
    
    static testMethod void test0(){
        Account a = new Account(Name = 'test', Facets_ID__c = '008384', Preferred_Manifest_Delivery_Method__c = 'Email', Fax = '0000000000', Manifest_Sent_To_Email_Addresses__c = 'test', OwnerId = Userinfo.getUserId());
        insert a;
        Account account = [SELECT Id, Name, Facets_ID__c, Preferred_Manifest_Delivery_Method__c, Fax, Manifest_Sent_To_Email_Addresses__c, OwnerId FROM Account WHERE Id=:a.Id];
        Id accId = a.Id;
        
        DailyManifestREST dm = new DailyManifestREST();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Test.startTest();
        DailyManifestREST.EmailAttachPDF(accId);
        Test.stopTest();
    }
    
    //    static testMethod void test1(){      
    //    DailyManifestREST dm = new DailyManifestREST();
    //    Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
    //    Test.startTest();
    //    DailyManifestREST.EmailAttachPDFREST();
    //    Test.stopTest();
    //}
    
}