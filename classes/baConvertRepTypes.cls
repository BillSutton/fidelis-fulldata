/******************************************************************************
** Module Name   : baConvertRepTypes  
**
** Marketing rep assignments - convert default to Marketing for Retention project
******************************************************************************/

global class baConvertRepTypes implements Database.Batchable<sObject>, Database.Stateful {
    global Integer numRead=0;
    global String extra='';
    string gWhere = 'recordtype.name = null';
    string MARKETING = [select id from recordtype where name = 'Marketing' and sobjecttype = 'Rep_assignment__c'].id;
    id APIUSER;

    global baConvertRepTypes()
    {
        setup();
    }

    global baConvertRepTypes(String whr)
    {
        setup();
        gWhere = whr;
    }
    
    global void setup()
    {
        APIUSER = [select id from user where name = 'API User'].Id; 
    }
    global database.querylocator start(Database.BatchableContext BC){
            
        return Database.getQueryLocator(getQuery());
    }
        
    global String getQuery()
    {
        String query = 'Select id, recordtype.Id, REP_PHONE__C, msa__c, dual__c, medicare__c, allocation_percentage__c from rep_assignment__c where ' + gWhere;
        System.debug(query);
        extra += query + '\n';
        return query;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        rep_assignment__c rp;
        for(Sobject s : scope){
            numRead++;
            rp = (rep_assignment__c) s;
            rp.recordtypeid = MARKETING;
            if (rp.rep_phone__c == null) rp.rep_phone__c = '1';
            if (rp.msa__c == null) rp.msa__c = APIUSER;
            if (rp.medicare__c == true) rp.dual__c = true;
            if (rp.allocation_percentage__c == null) rp.allocation_percentage__c = 100;
            
        }
        update scope;
    }
    
    
    global void finish(Database.BatchableContext BC){
        String email='svanatta@fideliscare.org';
        Integer jobs=0;
        Integer totaljobitems=0;
        integer errs =0;
        string stat;
        
        if (bc != null)
        {
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.email
                from AsyncApexJob where Id = :BC.getJobId()];
            email = a.CreatedBy.email; jobs = a.TotalJobItems; errs = a.NumberofErrors; stat = a.Status;
        }
        
        util.sendmsg('Batch Apex baConvertRepTypes ' + stat + ' ' + numRead,
                'Processed ' + jobs + ' batches with ' + errs + ' failures.\n\n' + 'Numread=' + numRead
                         + '\n\n' + extra, email);
    }
    

   static testMethod void myUnitTest() {
    baConvertRepTypes ttc = new baConvertRepTypes();
    String query = ttc.getQuery() + ' LIMIT 25'; 
    List<rep_assignment__c> qrRes = new List<rep_assignment__c>();
    system.debug('***' + query);
    list<User> userList = TestCaseRetentionRouter.getUserList('1');
    Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',FHP__c = true, last_time_assigned__c=null, Rep_Phone__c= '1',  
                                                  MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null, marketing_rep_record__c = userlist[0].id);
            
    Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',FHP__c = true, last_time_assigned__c=null, Vacation__c = false, Rep_Assignment_lu__c = null, 
                                                    marketing_rep_record__c = userlist[1].id, Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId(), allocation_percentage__c = 50);
    Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep3', last_time_assigned__c=null, FHP__C = true,CHP__C = true,HBX__C = true,
                                                    Medicaid__C = true,Medicare__C = true, Vacation__c = false, Rep_Assignment_lu__c = null, Rep_Phone__c= '1', 
                                                    ep__c = true, fida__c=true, mltc__c=true,marketing_rep_record__c = userlist[2].id, MSA__c = UserInfo.getUserId());
    qrRes.add(ra1);
    qrRes.add(ra2);
    qrRes.add(ra3);

    insert qrRes;
    Database.BatchableContext bc;
    ttc.execute(bc, qrRes);
    ttc.finish(bc);
    ttc = new baConvertRepTypes();    
    }   
}