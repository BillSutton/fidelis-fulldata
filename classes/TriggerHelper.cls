/************************************************************************************************
**Name           : TriggerHelper
**Author         : Scott Van Atta
**Date           : August 2016
**Description    : Static helper classes for triggers for reusable code
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      8/24/2016   SVA     Created from original outreach trigger

*************************************************************************************************/
global class TriggerHelper {
// Case recordtypes-------------------
static public String MARKETPLACE = 'Marketplace';
static public String MEDICARE = 'Medicare';
static public String CAREMANAGEMENT = 'Care_Management';
static public String CALLCENTER = 'Call_Center';
static public String RENEWALS = 'Renewals';
static public String TRANSSREVICE = 'Transportation';
static public String TRANSNOTE = 'Transportation_Note';
static public String DELINQUENCIES = 'Delinquencies';
static public String DISENROLLMENT = 'Disenrollment';
static public String INITIALPREMIUM = 'Initial_Premiums';
static public String ADHOC = 'Ad_Hoc';
static public String MedicareId;

    public static void callRouter(List<Case> cases, String type)
    {
        if (type == MARKETPLACE)
        {
            CaseRouter c1 = new CaseRouter(cases);
            c1.process(); // route the cases
            c1.processOwners(cases); // update owners on all cases using existing built maps in caserouter
        }
        else 
        {
            CaseRetentionRouter c1 = new CaseRetentionRouter(cases, type);
            c1.process(); // route the cases
            c1.processOwners(cases); // update owners on all cases using existing built maps in retentionrouter
        }
        
    }
    
    public static Map<Id, string> buildRecTypes()
    {
        Map<id, string> recTypes = new Map<Id, String>();
        List<Recordtype> recList = [select id, name, developername from recordtype where sObjectType = 'Case'];
        for (recordtype r : recList)
        {
            if (r.developername == CAREMANAGEMENT || r.developername == MARKETPLACE || r.developername == MEDICARE)
                recTypes.put(r.id, 'Route');
            else if (r.developername == RENEWALS)
                rectypes.put(r.Id, 'RouteRet');
            else if (r.developername == DELINQUENCIES)
                rectypes.put(r.id, 'RouteDelq');
            else if (r.developername == DISENROLLMENT)
                rectypes.put(r.id, 'RouteDis');
            else if (r.developername == INITIALPREMIUM)
                rectypes.put(r.id, 'RouteInitial');
            else if (r.developername == ADHOC)
                rectypes.put(r.id, 'RouteAdhoc');
            
            if (r.developername == MEDICARE)
                MedicareId = r.id;
                
        }
        return recTypes;
    }
    
    public static String getMedicareId()
    {
        if (MedicareId == null)
            MedicareId = [select id from recordtype where developername = 'Medicare'].Id;
        return MedicareId;
    }
}