public class FidelisBenefitSummaryParser {
	
    public list<arrayItem> myArray;
    
    public class arrayItem{
       public String memberKey;
       public String bene_description;
       public String bene_COPAY_AMT;
       public String bene_BSDL_DEDE_AMT;
       public String bene_BSDL_COIN_PCT;
       public String bene_BSDL_COV_IND;
       public String bene_BSDL_LTLT_AMT;
       public String bene_NTWK_IND;
       public String bene_BSDE_TYPE;
       public String bene_LT_TYPE;
       public String bene_LT_PERIOD;
       public String bene_LT_COUNTER;
       public String bene_PDPD_ID;
       public String bene_PDBC_PFX;
       public String bene_PDBC_EFF_DT;
       public String bene_PDBC_TERM_DT;
       public String bene_USER_LABEL1;
       public String bene_USER_DATA1;
       public String bene_USER_LABEL2;
       public String bene_USER_DATA2;
       public String bene_DESC_ONE;
       public String bene_DESC_TWO;
       public String bene_USER_LABEL3;
       public String bene_USER_DATA3;
       public String bene_COPAY;
       public String bene_COINSURANCE;
       public String bene_BENEFIT_LIMIT;
       public String bene_DOLLAR_LIMIT;
       public String bene_NOT_COV_OUT_OF_NETWORK;
       public String bene_DETAIL_CLASS_PLAN;
    }
    
    public static FidelisBenefitSummaryParser parse(String json) {
		return (FidelisBenefitSummaryParser) System.JSON.deserialize(json, FidelisBenefitSummaryParser.class);
	}
}