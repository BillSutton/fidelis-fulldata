@isTest
public class AuthGridCheckTest {

    static testMethod void test0(){
        String planName, county;
        planName = 'test';
        county = 'test';
        AuthGridCheck agcBlank = new AuthGridCheck();
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        String theReturn1 = agc.ContactPlan;
        String theReturn2 = agc.ContactCounty;
        System.assertEquals(planName, theReturn1, 'Did not return correct string value');
        System.assertEquals(county, theReturn2, 'Did not return correct string value');
    }
    
    static testMethod void DoIDoIt1(){
        String planName, county;
        planName = 'something';
        county = 'something';
        
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        agc.DoIDoIt();
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(DoIDoIt, TRUE, 'Did not return correct boolean value');
    }
    
    static testMethod void NoteCheckValid1(){
        String planName, county;
        planName = 'something';
        county = 'something';
        
        
        String tripType, vType;
        tripType = 'Standard';
        vType = 'Ambulette';
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean NoteCheck = agc.noteCheck(tripType, vType);
        System.assertEquals(TRUE, NoteCheck, 'Did not return the correct boolean value');
    }

    static testMethod void NoteCheckValid2(){
        String planName, county;
        planName = 'something';
        county = 'something';
        
        
        String tripType, vType;
        tripType = 'something';
        vType = 'Ambulette';
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean NoteCheck = agc.noteCheck(tripType, vType);
        System.assertEquals(TRUE, NoteCheck, 'Did not return the correct boolean value');
    }

    static testMethod void NoteCheckInvalid1(){
        String planName, county;
        planName = 'something';
        county = 'something';
        
        
        String tripType, vType;
        tripType = 'Standard';
        vType = '';
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean NoteCheck = agc.noteCheck(tripType, vType);
        System.assertEquals(FALSE, NoteCheck, 'Did not return the correct boolean value');
    }
    
    static testMethod void NoteCheckInvalid2(){
        String planName, county;
        planName = 'something';
        county = 'something';
        
        
        String tripType, vType;
        tripType = 'something';
        vType = 'Ambulance';
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean NoteCheck = agc.noteCheck(tripType, vType);
        System.assertEquals(FALSE, NoteCheck, 'Did not return the correct boolean value');
    }

    static testMethod void NoteCheckInvalid3(){
        String planName, county;
        planName = 'something';
        county = 'something';
        
        
        String tripType, vType;
        tripType = '';
        vType = '';
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean NoteCheck = agc.noteCheck(tripType, vType);
        System.assertEquals(FALSE, NoteCheck, 'Did not return the correct boolean value');
    }
    
        static testMethod void NoteCheckInvalid4(){
        String planName, county;
        planName = 'something';
        county = 'something';
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        
        String tripType, vType;
        tripType = '';
        vType = '';
        Boolean NoteCheck = agc.noteCheck(tripType, vType);
        System.assertEquals(FALSE, NoteCheck, 'Did not return the correct boolean value');
    }
    
    static testMethod void DoIDoIt2Valid(){
        String planName, county;
        planName = 'something';
        county = 'something';        
        
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = '', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(DoIDoIt, TRUE, 'Did not return correct boolean value');
    }
    
        static testMethod void DoIDoIt2Invalid(){
        String planName, county;
        planName = 'something';
        county = 'something';        
        
        County__c c = new County__c(Name = 'test');
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = 'test');
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'No', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = '', Note_for_MSA__c = 'Note for MSA', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(FALSE, DoIDoIt, 'Did not return the correct boolean value');
    }
    
    static testMethod void DoTheQuery1(){
        String planName, county;
        planName = 'test';
        county = 'test';        
               
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(HD_Auth_Form__c = 'Ambulette', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        List<Transportation_Authorization_Grid__c> tagQuery = [select Id, County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Transportation_Authorization_Grid__c theActual = agc.doTheQuery();
        System.assertEquals(agc.doTheQuery(), tag, 'Did not return correct object');
    }
    
    static testMethod void DoTheQuery2Valid(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(HD_Auth_Form__c = 'Ambulette', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Transportation_Authorization_Grid__c theActual = agc.doTheQuery();
        System.assertEquals(tag, theActual, 'Did not return the correct Transporation_Authorization_Grid__c Object');
    }
    
    static testMethod void DoTheQuery2Invalid(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        
        County__c c = new County__c(Name = '');
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = '');
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        System.assertEquals(0, tagQuery.size());
        Transportation_Authorization_Grid__c theActual = agc.doTheQuery();
        System.assertNotEquals(tag, theActual, 'Did not return the correct Transporation_Authorization_Grid__c Object');
    }
    
    static testMethod void RideTypeElgiability1(){
        String testPlanName, testCounty;
        testPlanName = 'test';
        testCounty = 'test';
               
        County__c c = new County__c(Name = testCounty);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = testPlanName);
        insert p;        
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County__c = c.Id,
                                                                                            Plan__c = p.Id,
                                                                                            HD_AUTH_Form__c = '',
                                                                                            Vehicle_Type__c = '',
                                                                                            Standard_Auth_form__c = '',
                                                                                            Form_Type__c = '',
                                                                                            Authorization_Number__c = '',
                                                                                            Note_for_MSA__c = '');
        insert tag;
        AuthGridCheck agc = new AuthGridCheck();  
        agc.rideTypeEligiability();
        //System.assert(agc.rideTypeEligiability(tag));
    }
    
    static testMethod void RideTypeElgiability2(){
        String testPlanName, testCounty;
        testPlanName = 'test';
        testCounty = 'test';
                 
        County__c c = new County__c(Name = testCounty);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = testPlanName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County__c = c.Id,
                                                                                             Plan__c = p.Id,
                                                                                             HD_AUTH_Form__c = 'Ambulette', 
                                                                                             Vehicle_Type__c = 'Ambulette', 
                                                                                             Note_for_MSA__c = 'HBX does not cover Livery, routine Ambulete and Routine Ambulance', 
                                                                                             Authorization_Number__c = 'Ambulette', 
                                                                                             Standard_Auth_form__c = 'Ambulette', 
                                                                                             Form_Type__c = 'Standard');
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(testPlanName, testCounty);
        //System.assert(agc.rideTypeEligiability(tag));  
    }
      
    static testMethod void AuthNumberCheck1Invalid1(){
        String planName, county;
        planName = 'test';
        county = 'test';        
         

        String testVType;
        Boolean testNonCounties;
        testVType = 'Ambulette;Ambulance';
        testNonCounties = true;
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Not Applicable', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt);
        Boolean theReturn = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(FALSE, theReturn);
    }
    
        static testMethod void AuthNumberCheck1Invalid2(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        

        String testVType;
        Boolean testNonCounties;
        testVType = 'Ambulance';
        testNonCounties = true;
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt);
        Boolean theReturn = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(FALSE, theReturn);
    }
    
    static testMethod void AuthNumberCheck1Invalid3(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        

        String testVType;
        Boolean testNonCounties;
        testVType = '';
        testNonCounties = true;
        County__c c = new County__c(Name = 'test');
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = 'test');
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = '', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt);
        Boolean theReturn = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(FALSE, theReturn);
    }
    
    static testMethod void AuthNumberCheck1Invalid4(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        AuthGridCheck agc = new AuthGridCheck(planName, county); 

        String testVType;
        Boolean testNonCounties;
        testVType = 'Ambulette;Ambulance';
        testNonCounties = true;
        County__c c = new County__c(Name = 'something');
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = 'something');
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Not Applicable', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(FALSE, DoIDoIt);
        Boolean theReturn = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(FALSE, theReturn);
    }
    
    static testMethod void AuthNumberCheck1Valid1(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        
        
        String testVType;
        Boolean testNonCounties;
        testVType = 'Ambulette; Ambulance';
        testNonCounties = true;  
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette; Ambulance', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean theReturn = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(TRUE, theReturn);
    }
    
    static testMethod void AuthNumberCheck1Valid2(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        
        
        String testVType;
        Boolean testNonCounties;
        testVType = 'Ambulette';
        testNonCounties = true;  
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Ambulette', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county);
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean theReturn = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(TRUE, theReturn);
    }
    
    static testMethod void AuthNumberCheck1Valid3(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        
        
        String testVType;
        Boolean testNonCounties;
        testVType = 'Ambulette';
        testNonCounties = true;  
        County__c c = new County__c(Name = county);
        insert c;
        Transportation_Plan__c p = new Transportation_Plan__c(Name = planName);
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c(County_Covered__c = 'Yes', HD_Auth_Form__c = 'Ambulette', Vehicle_Type__c = 'Ambulette', Note_for_MSA__c = '', Authorization_Number__c = 'Non-Adjacent Counties', Standard_Auth_form__c = 'Ambulette', Form_Type__c = 'Standard', County__c = c.Id, Plan__c = p.Id);
        insert tag;
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        List<Transportation_Authorization_Grid__c> tagQuery = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c from Transportation_Authorization_Grid__c where (county__r.name = :county OR county__r.name = 'Default') and plan__r.name =:planName ORDER BY Effective_Date__c limit 1];
        Boolean DoIDoIt = agc.DoIDoIt();
        System.assertEquals(TRUE, DoIDoIt, 'Did not return the correct boolean value');
        Boolean theReturn = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(TRUE, testNonCounties);
    }
     
    static testMethod void AuthNumberCheck2Valid1(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        
        String testVType, testProviderId, testLineOfBusiness, testCaseId;
        Boolean testNonCounties; 
        testVType = '';
        testNonCounties = true;
        testLineOfBusiness = '';
        
        String year = '2015';
        String month = '08';
        String day = '14';
        String hour = '11';
        String minute = '10';
        String second = '20';
        String stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
        
        Account account = new Account(Name = 'account', Type = 'In Network');
        insert account;
        Case c = new Case(Accepting_Medicare_or_Medicaid_Rates__c = 'Yes ', Pickup_DateTime__c = Datetime.valueOf(stringDate),Contact_Person_for_LoA__c = 'Test',Contact_Number_for_LoA__c = '2121231234');
        insert c;
        testProviderId = account.Id;
        testCaseId = c.Id;
        List<Account> testProvList = [select Id, Type from Account where Id = :testProviderId limit 1];
        List<Case> testProvCases = [select Id, Accepting_Medicare_or_Medicaid_Rates__c, Pickup_Datetime__c from Case where Id = :testCaseId limit 1];

        Boolean authNumberCheck = agc.authNumberCheck(testVType, testNonCounties, testProviderId, testLineOfBusiness, TestCaseId,'Yes');
        System.assertEquals(TRUE, authNumberCheck, 'Did not return the right object');
    }
    
        static testMethod void AuthNumberCheck2Valid2(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        
        String testVType, testProviderId, testLineOfBusiness, testCaseId;
        Boolean testNonCounties; 
        testVType = '';
        testNonCounties = false;
        testLineOfBusiness = '';

        String year = '2015';
        String month = '08';
        String day = '14';
        String hour = '11';
        String minute = '10';
        String second = '20';
        String stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;    
            
        Account account = new Account(Name = 'account', Type = 'Out of Network');
        insert account;
        Case c = new Case(Accepting_Medicare_or_Medicaid_Rates__c = 'Yes ', Pickup_DateTime__c = Datetime.valueOf(stringDate),Contact_Person_for_LoA__c = 'Test',Contact_Number_for_LoA__c = '2121231234');
        insert c;
        testProviderId = account.Id;
        testCaseId = c.Id;
        List<Account> testProvList = [select Id, Type from Account where Id = :testProviderId];
        List<Case> testProvCases = [select Id, Accepting_Medicare_or_Medicaid_Rates__c, Pickup_Datetime__c from Case where Id = :testCaseId limit 1];
        Boolean authNumberCheck = agc.authNumberCheck(testVType, testNonCounties, testProviderId, testLineOfBusiness, TestCaseId,'Yes');
        System.assertEquals(TRUE, authNumberCheck, 'Did not return the right object');
    }
    
        static testMethod void AuthNumberCheck2Valid3(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        
        String testVType, testProviderId, testLineOfBusiness, testCaseId;
        Boolean testNonCounties; 
        testVType = '';
        testNonCounties = true;
        testLineOfBusiness = '';
        
        String year = '2015';
        String month = '08';
        String day = '14';
        String hour = '11';
        String minute = '10';
        String second = '20';
        String stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
            
        Account account = new Account(Name = 'account', Type = 'In Network');
        insert account;
        Case c = new Case(Accepting_Medicare_or_Medicaid_Rates__c = 'Yes ', Pickup_DateTime__c = Datetime.valueOf(stringDate),Contact_Person_for_LoA__c = 'Test',Contact_Number_for_LoA__c = '2121231234');
        insert c;
        testProviderId = account.Id;
        testCaseId = c.Id;
        List<Account> testProvList = [select Id, Type from Account where Id = :testProviderId limit 1];
        List<Case> testProvCases = [select Id, Accepting_Medicare_or_Medicaid_Rates__c, Pickup_Datetime__c from Case where Id = :testCaseId limit 1];
        
        Boolean authNumberCheck = agc.authNumberCheck(testVType, testNonCounties, testProviderId, testLineOfBusiness, TestCaseId,'Yes');
        System.assertEquals(TRUE, authNumberCheck, 'Did not return the right object');
    }
    
    static testMethod void AuthNumberCheck2Valid4(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        
        String testVType, testProviderId, testLineOfBusiness, testCaseId;
        Boolean testNonCounties; 
        testVType = '';
        testNonCounties = true;
        testLineOfBusiness = '';

        String year = '2015';
        String month = '08';
        String day = '14';
        String hour = '11';
        String minute = '10';
        String second = '20';
        String stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;    
            
        Account account = new Account(Name = 'account', Type = 'In Network');
        insert account;
        Case c = new Case(Accepting_Medicare_or_Medicaid_Rates__c = 'No',Contact_Person_for_LoA__c = 'Test',Contact_Number_for_LoA__c = '2121231234',Procedure_Code__c = 'Test',Rational_for_LoA__c = 'Test',Reason_for_LoA__c = 'Test');
        insert c;
        testProviderId = account.Id;
        testCaseId = c.Id;
        List<Account> testProvList = [select Id, Type from Account where Id = :testProviderId];
        List<Case> testProvCases = [select Id, Accepting_Medicare_or_Medicaid_Rates__c, Pickup_Datetime__c from Case where Id = :testCaseId limit 1];
        Boolean authNumberCheck = agc.authNumberCheck(testVType, testNonCounties, testProviderId, testLineOfBusiness, TestCaseId,'No');
        System.assertEquals(TRUE, authNumberCheck, 'Did not return the right object');
    }

    static testMethod void AuthNumberCheck2Valid5(){
        String planName, county;
        planName = 'test';
        county = 'test';        
        AuthGridCheck agc = new AuthGridCheck(planName, county); 
        
        String testVType, testProviderId, testLineOfBusiness, testCaseId;
        Boolean testNonCounties; 
        testVType = '';
        testNonCounties = true;
        testLineOfBusiness = '';

        String year = '2015';
        String month = '08';
        String day = '14';
        String hour = '11';
        String minute = '10';
        String second = '20';
        String stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;    
            
        Account account = new Account(Name = 'account', Type = 'In Network');
        insert account;
        Case c = new Case(Accepting_Medicare_or_Medicaid_Rates__c = 'No',Contact_Person_for_LoA__c = 'Test',Contact_Number_for_LoA__c = '2121231234',Procedure_Code__c = 'Test',Rational_for_LoA__c = 'Test',Reason_for_LoA__c = 'Test');
        insert c;
        testProviderId = '';
        testCaseId = '';
        List<Account> testProvList = [select Id, Type from Account where Id = :testProviderId];
        List<Case> testProvCases = [select Id, Accepting_Medicare_or_Medicaid_Rates__c, Pickup_Datetime__c from Case where Id = :testCaseId limit 1];
        Boolean authNumberCheck = agc.authNumberCheck(testVType, testNonCounties, testProviderId, testLineOfBusiness, TestCaseId,'No');
        Boolean theExpected = agc.authNumberCheck(testVType, testNonCounties);
        System.assertEquals(theExpected, authNumberCheck, 'Did not return the right object');
    }    
    
}