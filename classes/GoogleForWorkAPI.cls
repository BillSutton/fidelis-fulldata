public class GoogleForWorkAPI {
    private static Blob key;
    public static String getAPIEncodedSignature(String inputParams){
        Google_Maps_API_Key__c apiKeySetting = Google_Maps_API_Key__c.getOrgDefaults();
        if(inputParams==null || inputParams == ''){return null;}

        String keyString = apiKeySetting.API_Signature__c;
        // Convert the key from 'web safe' base 64 to binary
        keyString = keyString.replace('-', '+');
        keyString = keyString.replace('_', '/');
        // Base64 is JDK 1.8 only - older versions may need to use Apache Commons or similar.
        key = EncodingUtil.base64Decode(keyString);
        return signRequeset(inputParams);
    }
    private static String signRequeset(String inputParams){
        //Get a SHA1 key
        //Blob sha1Key = Crypto.generateDigest('SHA1', key);
        Url url = new Url(inputParams);
        String resource = url.getPath() + '?' + url.getQuery();
        system.debug(resource);
        //Get an HMAC-SHA1 Mac instance
        

        Blob mac = Crypto.generateMac('hmacSHA1', Blob.valueOf(resource), key);
        


        String signature = EncodingUtil.base64Encode(mac);
        signature = signature.replace('+', '-');
        signature = signature.replace('/', '_');
        system.debug(signature);
        system.debug(url.getProtocol()+'://'+url.getHost()+resource+'&signature='+signature);
        return signature;
    }

}