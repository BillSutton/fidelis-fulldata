/*
@Name            : FidelisAPIHelper
@Author          : customersuccess@cloud62.com
@Date            : May 11, 2015
@Description     : Connects to Fidelis API
@Revision History: 05-11-15 dan@cloud62.com: Created Class.
                    - 2016-09-19 (Greg Hacic): modified the getPremiumInfoBySubscriberID method
*/
public class FidelisAPIHelper {
    
    public static String getPremiumInfoBySubscriberID(String SubscriberId) {
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults(); //grab the beginning of the Facets API endpoint (https://partnerservices-test.fideliscare.org)
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Member/GetPremiumInformationForASubscriber?subscriberId='+SubScriberId;
        
        Http h = new Http(); //construct an HTTP request and response
        HttpRequest req = new HttpRequest(); //create a new instance of the HttpRequest class
        req.setEndpoint(path); //specify the endpoint for the request
        req.setMethod('GET'); //set the type of method to be used for the HTTP request to GET
        req.setTimeout(120000); //set the request timeout to 120 seconds
        HttpResponse response = new HttpResponse(); //construct the HttpResponse class to handle the HTTP response returned by the Http class
        if (Test.IsRunningTest()) { //if we are in testing context
            if (SubscriberId == '999990001') { //if the subscriberId is 999990001
                response.setBody(FidelisAPIHelperTest.mockDataPremiumResponse); //grab test data from FidelisAPIHelperTest.class
            } else {
                response.setBody('{ "memberPremiumLatestPayment": [], "memberPaymentAppliedDetails": [], "memberPremiumInvoicePaidDetails": [], "memberPremiumInvoiceDueDetails": []}'); //set to a null response body
            }
        } else { //otherwise, we are not in testing context
            response = h.send(req); //send the HttpRequest and build the response
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + response.getBody());
        }
        return response.getBody(); //grab the body of the JSON response
    }
    
    public static String CancelLineItem(String authNumber, Integer seqNumber, id caseId, id contactid, WebServiceLog__c[] wsls){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c +'MemberPortal/api/UtilizationManagement/CancelLineItem';
        String body = '{"appId": 480,';
        body += '"umumRefId": "'+authNumber+'",';
        body += '"umumSeqNo": '+seqNumber+'}';


        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setBody(body);
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type', 'application/JSON;charset=utf-8');
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            system.debug('REQUEST BODY>>>>>>>>>>>> '  + req.getBody());
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            //WebServiceLog.write('CancelLineItem', caseid, contactid, path, body, res.getBody());
            wsls.add(WebServiceLog.loadRecord('CancelLineItem', caseid, contactid, path, body, res.getBody()));
            return res.getBody();
        }
    }

    public static String AddAuthorization(Integer memberId, String authDate, String startDate, String endDate, String providerId, String note,String procedureCode, String lineOfBusiness, 
                                            String vType, ID caseId, Id contactid){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c +'MemberPortal/api/UtilizationManagement/AddAuthorization';
        String body = '{"appId": 480,';
        //body += '"umumRefId": "152150005",';
        //body += '"umumSeqNo": ,';
        body += '"memberId": '+memberId+',';
        body += '"umsvAuthDt": "'+authDate+'",';
        body += '"umsvFromDt": "'+startDate+'",';
        body += '"umsvToDt": "'+endDate+'",';
        body += '"umsvPrprIdReq": "'+providerId+'",';
        body += '"umsvPrprIdSvc": "'+providerId+'",';
        body += '"noteCat": "'+note+'",';
        body += '"procedureCode": "'+procedureCode+'",';
        body += '"lob": "'+lineOfBusiness+'",';
        body += '"mctrRind": "'+vType+'"}';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setBody(body);
        req.setHeader('Content-Type', 'application/JSON;charset=utf-8');
        req.setMethod('POST');
        req.setTimeout(120000);
        system.debug('REQUEST BODY>>>>>>>>>>>> '  + req.getBody());
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            WebServiceLog.write('AddAuth', caseid, contactid, path, body, res.getBody());
            return res.getBody();
        }
    }
    public static String AddAuthorization(Integer memberId, String authDate, List<Date> dateLst, String providerId, String note,String procedureCode, String lineOfBusiness, String vType,String providerContact, 
                                            String providerContactNum, ID caseId, Id contactId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c +'MemberPortal/api/UtilizationManagement/AddAuthorization';
        String body = '{"appId": 480,';
        //body += '"umumRefId": "152150005",';
        //body += '"umumSeqNo": ,';
        body += '"memberId": '+memberId+',';
        body += '"umsvAuthDt": "'+authDate+'",';
        //body += '"umsvFromDt": "'+startDate+'",';
        //body += '"umsvToDt": "'+endDate+'",';
        body += '"umsvPrprIdReq": "'+providerId+'",';
        body += '"umsvPrprIdSvc": "'+providerId+'",';
        body += '"noteCat": "'+note+'",';
        body += '"procedureCode": "'+procedureCode+'",';
        body += '"lob": "'+lineOfBusiness+'",';
        body += '"mctrRind": "'+vType+'",';
        body += '"umsvDateList":[';
        for(Date d : dateLst){
            body+= '"'+d.format()+'",';
        }
        if(dateLst.size()>0){
            body = body.substring(0,body.length()-1);
        }
        body+='],';
        body+='"NoteContactName":"'+providerContact+'",';
        body+='"NoteContactPhone":"'+providerContactNum+'"}';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setBody(body);
        req.setHeader('Content-Type', 'application/JSON;charset=utf-8');
        req.setMethod('POST');
        req.setTimeout(120000);
        system.debug('REQUEST BODY>>>>>>>>>>>> '  + req.getBody());
        if(Test.IsRunningTest()){
            return 'testing';
        } else {
            try{
                HttpResponse res = h.send(req);
                system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
                WebServiceLog.write('AddAuth', caseid, contactid, path, body, res.getBody());
                return res.getBody();
            }catch(Exception e){
                return 'Err Message: '+e.getMessage();
            }
        }
    }
    ///////////////////////////
    //     GENERAL CALLS     //
    ///////////////////////////
    
    // returns API documentation in HTML form
    public static String HelpCall(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/HELP';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //     ID CARD CALLS     //
    ///////////////////////////
    
    public static String GetIDCardRequestHistory(String memKey, String person1Id, String person2Id){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/IDCard/GetIDCardRequestHistory?memberKey='+memKey;
               path += '&responsiblePersonIds[0]='+person1Id;
               path += '&responsiblePersonIds[1]='+person2Id;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetIDCardInfo(String memKey){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/IDCard/GetIDCardInfo?memberKey='+memKey;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String RequestIdCard(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/IDCard/RequestIdCard';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //     ACCOUNT CALLS     //
    ///////////////////////////
    
    public static String GetAddress(String memKey, String person1Id, String person2Id){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/GetAddress?memberKey='+memKey;
               path += '&responsiblePersonIds[0]='+person1Id;
               path += '&responsiblePersonIds[1]='+person2Id;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetPhoneNumber(String memKey, String person1Id, String person2Id){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/GetPhoneNumber?memberKey='+memKey;
               path += '&responsiblePersonIds[0]='+person1Id;
               path += '&responsiblePersonIds[1]='+person2Id;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetLanguage(String memKey, String person1Id, String person2Id){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/GetLanguage?memberKey='+memKey;
               path += '&responsiblePersonIds[0]='+person1Id;
               path += '&responsiblePersonIds[1]='+person2Id;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetContactPreference(String memKey, String person1Id, String person2Id){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/GetContactPreference?memberKey='+memKey;
               path += '&responsiblePersonIds[0]='+person1Id;
               path += '&responsiblePersonIds[1]='+person2Id;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String PostMailingAddress(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/PostMailingAddress';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String PostPhoneNumber(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/PostPhoneNumber';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String PostLanguage(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/PostLanguage';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String PostContactPreference(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Account/PostContactPreference';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //       FIDA CALLS      //
    ///////////////////////////
    
    public static String GetParticipants(String eMail){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/FIDA/GetParticipants?email='+eMail;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetPCSPS(String fidaId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/FIDA/GetPCSPS/'+fidaId;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetPCSPSDetails(String cId, String version){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/FIDA/GetPCSPDetails?cid='+cId;
               path += '&version='+version;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String SavePCSPStatus(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/FIDA/SavePCSPStatus';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String SaveTraining(String trainingId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/FIDA/SaveTraining/'+trainingId;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String SavePCSPComment(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/FIDA/SavePCSPComment';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //     BENEFITS CALLS    //
    ///////////////////////////
    
    public static String GetBenefits(String memKey, String asOfDate, Id contactId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Benefits/Get?meme_key='+memKey;
               path += '&AS_OF_DATE='+asOfDate;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        system.debug('path: '+path);
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            WebServiceLog.write('GetBenefits', null, contactid, path, path, res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //   RESPONSIBLES CALLS  //
    ///////////////////////////
    
    public static String GetMembersForResponsiblePerson(String rpLName, String rpFName, String rpSSN, String subId, String lName, String fName, String dob){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Responsibles/GetMembersForResponsiblePerson?rpLastName='+rpLName;
               path += '&rpFirstName='+rpFName;
               path += '&rpSSN='+rpSSN;
               path += '&SubscriberId='+subId;
               path += '&LastName='+lName;
               path += '&FirstName='+fName;
               path += '&DateOfBirth='+dob;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetResponsibles(String resId, String dups){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Responsibles/Get/'+resId;
               path += '?dups='+dups;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //      CLAIMS CALLS     //
    ///////////////////////////
    
    public static String GetClaims(String memKey, String person1Id, String person2Id){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Claim/GetClaims?memberKey='+memKey;
               path += '&responsiblePersonIds[0]='+person1Id;
               path += '&responsiblePersonIds[1]='+person2Id;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetClaimDetails(String claimId, String memKey, String person1Id, String person2Id){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Claim/GetClaimDetails/'+claimId;
               path += '?memberKey='+memKey;
               path += '&responsiblePersonIds[0]='+person1Id;
               path += '&responsiblePersonIds[1]='+person2Id;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //   ACCUMULATOR CALLS   //
    ///////////////////////////
    
    public static String GetAccumulators(String memKey, String asOfDate){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Accumulators/GetAccumulators?member_key='+memKey;
               path += '&AS_OF_DATE='+asOfDate;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //   ELIGIBILITY CALLS   //
    ///////////////////////////
    
    public static String GetEligibility(String memeKey, String asOfDate){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Eligibility/GetEligibility?meme_key='+memeKey;
               path += '&AS_OF_DATE='+asOfDate;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //       USER CALLS      //
    ///////////////////////////
    
    public static String CheckIfUserMadeInitialPayment(String confId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/User/CheckIfUserMadeInitialPayment?confirmationId='+confId;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String IsAuthorizedForMbrSvcsLogin(String userName, String pWord){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/User/IsAuthorizedForMbrSvcsLogin?userName='+userName;
               path += '&password='+pWord;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //        PCP CALLS      //
    ///////////////////////////
    
    public static String GetProviderHistoryForMember(String memKey){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/PCP/GetProviderHistoryForMember?memberKey='+memKey;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetProviderDetails(String provId, String addId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/PCP/GetProviderDetails?ProviderID='+provId;
               path += '&AddressID='+addId;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String GetProvidersBySearch(String fName, String lName, String address, String city, String state, String zip, String radius,String specialty, String phone, String facilityName, id caseid, id contactid){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        fName = (fName != null) ? fName = fName.replace(' ', '%20') : fName;
        lName = (lName != null) ? lName = lName.replace(' ', '%20') : lName;
        address = (address != null) ? address = address.replace(' ', '%20') : address;
        city = (city != null) ? city = city.replace(' ', '%20') : city;
        state = (state != null) ? state = state.replace(' ', '%20') : state;
        zip = (zip != null) ? zip = zip.replace(' ', '%20') : zip;
        radius = (radius != null) ? radius = radius.replace(' ', '%20') : radius;
        specialty = (specialty != null) ? specialty = specialty.replace(' ', '%20') : specialty;
        phone = (phone != null) ? phone = phone.replace(' ', '%20') : phone;
        facilityName = (facilityName != null) ? facilityName = facilityName.replace(' ', '%20') : facilityName;
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/PCP/GetProvidersBySearch?FirstName='+fName;
               path += '&LastName='+lName;
               path += '&Address='+address;
               path += '&City='+city;
               path += '&State='+state;
               path += '&ZipCode='+zip;
               path += '&SearchRadius='+radius;
               path += '&Specialty='+specialty;
               path += '&PhoneNumber='+phone;
               path += '&FacilityName='+facilityName;
               path += '&PCPOnly=False';
               path += '&ReturnPanel=False';

        system.debug('the path: '+path);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        system.debug('path: '+path);
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            WebServiceLog.write('GetProvidersBySearch', caseid, contactid, path, path, res.getBody());

            return res.getBody();
        }
    }
    
    public static String UpdateMemberProvider(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/PCP/UpdateMemberProvider';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    ///////////////////////////
    //      MEMBER CALLS     //
    ///////////////////////////
    
    public static String GetMember(String memId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Member/Get/'+memId;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }

    public static String GetMembersBySubscriber(String memId, String fName, String lName, String dob, String ssn){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path =  apiHelp.Endpoint__c + 'MemberPortal/api/Member/GetMembersBySubscriber?SubscriberId='+memId;
               path += '&FirstName='+fName;
               path += '&LastName='+lName;
               path += '&DOB='+dob;
               path += '&SSN='+ssn;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }   
    
    public static String GetMembersBySubscriberId(String memId, Boolean dups){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Member/GetMembersBySubscriberId?SubscriberId='+memId;
               path += '&dups='+dups;
        
        system.debug('path: '+path);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
    
    public static String CheckIfMemberIsDelinquent(String memId){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c + 'MemberPortal/api/Member/CheckIfMemberIsDelinquent?memberId='+memId;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }

    ///////////////////////////
    //     AuthNum CALLS     //
    ///////////////////////////

    public static String generateAuthNum(){
        Facets_API__c apiHelp = Facets_API__c.getOrgDefaults();
        String path = apiHelp.Endpoint__c+'MemberPortal/api/UtilizationManagement/AddPreauthorization?appId=480&memberId=230573350&umsv_from_dt=2015-07-31&umsv_to_dt=2015-07-31&umsv_prpr_id_req=18';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod('POST');
        req.setTimeout(120000);
        
        if(Test.IsRunningTest()){
            return 'testing';
        } else {         
            HttpResponse res = h.send(req);
            system.debug('RESPONSE BODY >>>>>>>>>>>>> ' + res.getBody());
            return res.getBody();
        }
    }
}