// InvocableDateTimeMethods
// @date: 9/22/2016
// @author: rgodard@fideliscare.org
// @description: Invocable methods for Salesforce visual flow that, as of now, just parses a string and turns it into a valid DateTime.
public class InvocableDateTimeMethods {
    
    @InvocableMethod(label = 'Get Parsed Date Time' description = 'Parses date field and text field representing time, and returns a valid DateTime.') 
    public static List<List<DateTime>> getParsedDateTime(List<String> dateTimeStringList) {
        
        List<List<DateTime>> dateTimeList = new List<List<DateTime>>();
        DateTime dateTimeValue = DateTime.parse(dateTimeStringList.get(0));
        List<DateTime> dateTimeInnerList = new List<DateTime>();
        dateTimeInnerList.add(dateTimeValue);
        dateTimeList.add(dateTimeInnerList);
        return dateTimeList;
    }
}