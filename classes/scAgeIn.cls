/******************************************************************************
** Module Name   : scAgeIn  
** Description   : Schedulable class to execute baAgeIn
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      3/20/2015    SVA     Created
**
******************************************************************************/
global class scAgeIn implements Schedulable{
     global void execute(SchedulableContext sc) {
        database.executebatch(new baAgeIn());
     }
  }