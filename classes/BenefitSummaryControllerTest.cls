/*
@Name            : BenefitSummaryControllerTest
@Author          : customersuccess@cloud62.com
@Date            : May 27, 2015
@Description     : Test code coverage for BenefitSummaryController
*/
@IsTest
public class BenefitSummaryControllerTest {
	@IsTest
    public static void test01(){
        PageReference pageRef = Page.BenefitSummary;
        Test.setCurrentPage(pageRef);
        
        Account tstAccount = New Account(Name = 'Test');
        insert tstAccount;
        
        Contact tstContact = New Contact(LastName = 'Test', AccountId=tstAccount.Id,Contrived_Key__c='123');
        insert tstContact;
        
        ApexPages.currentPage().getParameters().put('id',tstcontact.Id);
        BenefitSummaryController ctrl = new BenefitSummaryController();   
        
        BenefitSummaryController.Benefit tstBenefit = new BenefitSummaryController.Benefit('a','b','c');
        BenefitSummaryController.BenefitItem tstBenefitItem = new BenefitSummaryController.BenefitItem('a','b','c','a','b','c','a','b');
        
    }
}