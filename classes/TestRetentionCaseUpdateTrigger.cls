/*
@name           : TestRetentionCaseUpdateTrigger
@author         : rgodard@fideliscare.org
@date           : 8-16-2016
@description    : Test code for the trigger that prevents retention cases from being closed if there are related records on the case that are not closed.
*/
@isTest
public class TestRetentionCaseUpdateTrigger {
    static testMethod void testMethodOne() {
    
        ID rtId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Renewals'].Id;
        Case c = new Case(Subject='TestCase', RecordTypeId=rtId, Status = 'Assigned');
        
        insert c;
        
        List<Applicant_Information__c> aiList = new List<Applicant_Information__c>();
        Applicant_Information__c ai = new Applicant_Information__c(Case_Number__c = c.Id, Recert_date__c = Date.Today(), Member_Id__c = '0005922199', Applicant_First_Name__c = 'John', Renewal_Method__c= 'Exchange');
        aiList.add(ai);
        Applicant_Information__c ai2 = new Applicant_Information__c(Case_Number__c = c.Id, Recert_date__c = Date.Today()+1, Member_Id__c = '0005922190', Applicant_First_Name__c = 'Clarence', Renewal_Method__c= 'Exchange');
        aiList.add(ai2);
        
        insert aiList;
        
        ID taskRTId = [SELECT Id FROM RecordType WHERE SObjectType = 'Task' AND DeveloperName = 'Retention'].Id;
        List<Task> taskList = new List<Task>();
        Task t = new Task(WhatId = c.Id, Status = 'Completed', RecordTypeId = taskRTId);
        taskList.add(t);
        Task t2 = new Task(WhatId = c.Id, Status = 'Open', RecordTypeId = taskRTId);
        taskList.add(t2);
        
        insert taskList;
        
        ID eventRTId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Retention Event').getRecordTypeId();
        List<Event> eventList = new List<Event>();
        Event e = new event(WhatId = c.Id, Appointment_Result__c = 'Canceled', RecordTypeId = eventRTId, Subject='Test', ActivityDate=Date.Today(), ActivityDateTime=DateTime.Now(), DurationInMinutes = 60);
        eventList.add(e);
        Event e2 = new event(WhatId = c.Id, Appointment_Result__c = 'No Show', RecordTypeId = eventRTId, Subject='Test', ActivityDate=Date.Today(), ActivityDateTime=DateTime.Now(), DurationInMinutes = 60);
        eventList.add(e2);
        
        insert eventList;
        
        c.Status = 'Closed';
        c.Status_Reason__c = 'All Case Items Resolved';
        
        try {
            update c;
        } catch (Exception ex) {
            
        }
    }
    
}