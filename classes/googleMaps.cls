public class googleMaps {

    //Varible Declaraion
    public static String Duration{get;set;}
    public static Integer travelTime{get;set;}
    public static Case theCase {get;set;}
    public static String results {get;set;}
    
    public googleMaps(){
    }
    
    //@Future (callout=true)
    public static decimal getJson(String startAddress, String endAddress){
        HttpRequest req = new HttpRequest();
        Http http=new Http();
        req.setMethod('GET');
        
        //Make Sure to add https://maps.googleapis.com to the remote site settings
        String url = 'https://maps.googleapis.com/maps/api/distancematrix/json';
        url += '?origins='+startAddress;
        url += '&destinations='+endAddress;
        url += '&mode=driving&sensor=false&language=en&units=imperial';
        Google_Maps_API_Key__c apiKeySetting = Google_Maps_API_Key__c.getOrgDefaults();
        if(apiKeySetting!=null && apiKeySetting.API_Client__c!=null && apiKeySetting.API_Client__c!=''){
            url += '&client='+apiKeySetting.API_Client__c;
            url += '&signature='+GoogleForWorkAPI.getAPIEncodedSignature(url);
        }
        req.setEndPoint(url);
        system.debug(url);
        if(!test.isRunningTest()){
            HttpResponse resp = http.send(req);
            results = resp.getBody();
        } else {
            results = '{"destination_addresses" : [ "593 T313-3, Port Matilda, PA 16870, USA" ],"origin_addresses" : [ "576 Taunton Place, Buffalo, NY 14216, USA" ],"rows" : [{"elements" : [{"distance" : {"text" : "191 mi","value" : 307636},"duration" : {"text" : "3 hours 48 mins","value" : 13656},"status" : "OK"}]}],"status" : "OK"}';
        }
        system.debug('results: ' + results);
        Decimal dist = 0;
        googleMapsJsonParser thingy = googleMapsJsonParser.parse(results);
        for(googleMapsJsonParser.Rows r :thingy.rows){
            for(googleMapsJsonParser.Elements e:r.elements){
                if(e.status == 'NOT_FOUND'){
                    dist = -1;
                    break;
                }
                
                if(e.distance.text != null){
                    String tempString = e.distance.text.substring(0,(e.distance.text.indexOf(' ')));
                    if(tempString.contains(',')){
                        tempString = tempString.remove(',');                        
                    }
                    
                    dist = Decimal.ValueOf(tempString.trim());
                }
                if(e.duration.text != null){
                    Duration= e.duration.text;
                }
            }
        }
        system.debug('dist: '+dist);
        return dist;
    }
    
    //DEPRECATED
    @Future (callout=true)
    public static void getJson(Map<String,String> theRoutes, Id caseId){
        Decimal distTotal = 0;
        theCase = [SELECT ID, Total_Trip_Mileage__c FROM Case WHERE Id=:caseID];

        for(String startAddress : theRoutes.KeySet()){
            String endAddress = theRoutes.get(startAddress);
            HttpRequest req = new HttpRequest();
            Http http=new Http();
            req.setMethod('GET');
            
            //Make Sure to add https://maps.googleapis.com to the remote site settings
            String url = 'https://maps.googleapis.com/maps/api/distancematrix/json';
            url += '?origins='+startAddress;
            url += '&destinations='+endAddress;
            url += '&mode=driving&sensor=false&language=en&units=imperial';
            
            req.setEndPoint(url);
            system.debug(url);
            if(!test.isRunningTest()){
                HttpResponse resp = http.send(req);
                results = resp.getBody();
            } else {
                results = '{"destination_addresses" : [ "593 T313-3, Port Matilda, PA 16870, USA" ],"origin_addresses" : [ "576 Taunton Place, Buffalo, NY 14216, USA" ],"rows" : [{"elements" : [{"distance" : {"text" : "191 mi","value" : 307636},"duration" : {"text" : "3 hours 48 mins","value" : 13656},"status" : "OK"}]}],"status" : "OK"}';
            }
            Decimal dist = 0;
            googleMapsJsonParser thingy = googleMapsJsonParser.parse(results);
            for(googleMapsJsonParser.Rows r :thingy.rows){
                for(googleMapsJsonParser.Elements e:r.elements){
                    if(e.distance.text != null){
                        String tempString = e.distance.text.substring(0,(e.distance.text.indexOf(' ')));
                        dist = Decimal.ValueOf(tempString.trim());
                    }
                    if(e.duration.text != null){
                        Duration= e.duration.text;
                    }
                }
            }
            distTotal += dist;
            system.debug('dist: '+dist);
            system.debug('distTotal:'+ distTotal);
        }
        
        system.debug('final distTotal:'+ distTotal);
        theCase.Total_Trip_Mileage__c = distTotal;
        C62Utilities.cSetExecuted.add('dontFireTrigger');
        update theCase;
    }
}
/*
TESTING DATA DISTANCE WILL BE 191
String account1Address = EncodingUtil.urlEncode(
            + '576 Taunton Place' + ' '
            + 'Buffalo' + ', '
            + 'NY' + ' '
            + '14216' + ' '
            + 'USA' ,
            'UTF-8');
            
String account2Address = EncodingUtil.urlEncode( 
            + '593 Laurel Run Rd' + ' '
            + 'Port matilda' + ', '
            + 'PA'  + ' '
            + '16870' + ' '
            + 'USA',
            'UTF-8');

googleMaps gm = new googleMaps(account1Address,account2Address);
system.debug(gm.distance);
*/