public class googleMapsJsonParser {

    public class Rows {
        public List<Elements> elements;
    }

    public class Distance {
        public String text;
        public Decimal value;
    }

    public List<String> destination_addresses;
    public List<String> origin_addresses;
    public List<Rows> rows;
    public String status;

    public class Elements {
        public Distance distance;
        public Distance duration;
        public String status;
    }

    
    public static googleMapsJsonParser parse(String json) {
        return (googleMapsJsonParser) System.JSON.deserialize(json, googleMapsJsonParser.class);
    }
}