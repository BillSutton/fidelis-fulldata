/* **********************************************************************************
Name            : TestWebServiceLog
Author          : Scott Van Atta
Date            : February 2016
Description     : Test Class for WebServiceLog
*************************************************************************************/

@isTest 
private class TestWebServiceLog{
    static testMethod void testLog1(){
        
        
        Test.startTest();
        WebServiceLogSettings__c wsl = new WebServiceLogSettings__c();
        wsl.name = 'Settings';
        insert wsl;
        
        WebServiceLog.writeIt('AddAuth', null, null, 'path', 'request', 'response');
        WebServiceLog.writeIt('CancelLineItem', null, null, 'path', 'request', 'response');
        WebServiceLog.writeIt('GetProvidersBySearch', null, null, 'path', 'request', 'response');
        WebServiceLog.writeIt('GetBenefits', null, null, 'path', 'request', 'response');
        WebServiceLog.getEmail();
        System.assertEquals(4, [select count() from webservicelog__c ]);
        
        WebServiceLog.write('AddAuth', null, null, 'path', 'request', 'response'); // do async call for coverage

        
        Test.stopTest();
         
    }
 
}