public class DailyManifestController {
    public Account theAcct {get;set;}
    public String nowDT {get;set;}
    Public String imageURL {get;set;}
    public List<DailyManifestWrapper> newLst {get;set;}
    public List<DailyManifestWrapper> updateLst {get;set;}
    public List<DailyManifestWrapper> cancelLst {get;set;}

    public DailyManifestController(ApexPages.StandardController controller){
        theAcct = [SELECT Id, Name, Last_Manifest_Sent_Date_Time__c, Preferred_Manifest_Delivery_Method__c, PersonEmail, Fax FROM Account WHERE Id = :controller.getId()];
        List<Case> caseLst = new List<Case>();
        caseLst = [SELECT Id, of_trips__c, CaseNumber, Request_Type__c, Status, Pickup_Datetime__c, Return_Datetime__c, LastModifiedDate, Cancellation_DateTime__c, Special_Request__c, of_Riders__c, Description, 
                   Drop_Off_Location__c, Multi_Leg__c, Vehicle_Type__c, Contact.Name, Contact.Gender__c, Contact.Subscriber_ID__c, Contact.Plan_ID__c, Authorization_Number__c, 
                   Contact.Birthdate, Contact.Phone, CreatedBy.Alias, Transportation_Provider__c, Last_Manifest_Sent_Date_Time__c, Pickup_Location__c, Modified_DateTime__c, Steps__c, Weight__c
                   FROM Case 
                   WHERE Transportation_Provider__c =:theAcct.Id 
                   AND Pickup_Datetime__c > :system.today()]; 
        
        List<Document> documentList=[select name from document where Name='FidelisLogo_PDF'];  
        if(documentList.size()>0){
            imageURL='/servlet/servlet.FileDownload?file='+documentList[0].id;
        }
        newLst = new List<DailyManifestWrapper>();
        updateLst = new List<DailyManifestWrapper>();
        cancelLst = new List<DailyManifestWrapper>();
        nowDT = system.now().format('MM/dd/yyyy h:mm a');
        for(Case c: caseLst){
            if(c.Last_Manifest_Sent_Date_Time__c == null && c.Status == 'Scheduled' && (c.Request_Type__c == 'Standard Request' || c.Request_Type__c == 'Courtesy/Auth Pending')){
                newLst.add(new DailyManifestWrapper(c));
            } else if (c.Modified_DateTime__c != null && c.Pickup_Datetime__c > system.today() && (c.Last_Manifest_Sent_Date_Time__c == null || c.Last_Manifest_Sent_Date_Time__c < c.Modified_DateTime__c) && c.Status == 'Scheduled' && (c.Request_Type__c == 'Standard Request' || c.Request_Type__c == 'Courtesy/Auth Pending')){
                updateLst.add(new DailyManifestWrapper(c));
            } else if (c.Cancellation_DateTime__c != null && c.Pickup_Datetime__c > system.today() && (c.Last_Manifest_Sent_Date_Time__c!=null && c.Last_Manifest_Sent_Date_Time__c < c.Cancellation_DateTime__c) && (c.Request_Type__c == 'Standard Request' || c.Request_Type__c == 'Courtesy/Auth Pending')){
                system.debug(c.Pickup_Datetime__c);
                cancelLst.add(new DailyManifestWrapper(c));
            }         
        }
        newLst.sort();
        updateLst.sort();
        cancelLst.sort();
    }
    public class DailyManifestWrapper implements Comparable{
        public Case c{get;set;}
        public String pickupDatetime{get;set;}
        public String returnDateTime{get;set;}
        public DailyManifestWrapper(Case c){
            this.c = c;
            if(c.Pickup_Datetime__c!=null)
                pickupDatetime = c.Pickup_Datetime__c.format('MM/dd/yy hh:mm a');
            if(c.Return_Datetime__c!=null)
                returnDateTime = c.Return_Datetime__c.format('MM/dd/yy hh:mm a');
        }
        public Integer compareTo(Object o){
            DailyManifestWrapper dmw = (DailyManifestWrapper)o;
            
            if(c.Pickup_Datetime__c==dmw.c.Pickup_Datetime__c){
                return 0;
            }else if(c.Pickup_Datetime__c<dmw.c.Pickup_Datetime__c){
                return -1;
            }else{
                return 1;
            }
        }
    }
}