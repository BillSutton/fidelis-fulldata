public class RestrictedCounties {
    
    public void restrictedCounties(){}
    
    public static void runLogic(List<Applicant_Information__c> newApplicantMap, boolean isUpdate){
        Set<String> contactCountySet = new Set<String>();
        List<String> contactIdList = new List<String>();
        Map<String, Restricted_County__c> restrictedCountyMap = new Map<String,Restricted_County__c>();
        Map<String, Contact> contactMap = new Map<String, Contact>();
        List<String> caseIdList = new List<String>();
        List<id> applicantIdList = new List<id>();
        List<Applicant_Information__c> existingApplicants = new List<Applicant_Information__c>();
        
        ID rtCheck= [select ID from RecordType where SObjectType = 'Applicant_Information__c' AND DeveloperName = 'Renewal_Applicant' limit 1].id;
        
        for(Applicant_Information__c ai :newApplicantMap){
            if(ai.Contact__c != null && ai.recordTypeId == rtCheck){
                contactIdList.add(ai.Contact__c);
            }
            if(ai.Case_Number__c != null && ai.recordTypeId == rtCheck){
                caseIdList.add(ai.Case_Number__c);
            }
            applicantIdList.add(ai.id);
        }         
        
        for(applicant_Information__c ai :[select id, contact__c, Case_Number__c,Recert_Date__c,RecordTypeId from applicant_information__c where case_number__c in :caseIdList and ID not in :applicantIdList]){
            if(ai.Contact__c != null && ai.recordTypeId == rtCheck){
                contactIdList.add(ai.Contact__c);
            }
            if(ai.Case_Number__c != null && ai.recordTypeId == rtCheck){
                caseIdList.add(ai.Case_Number__c);
            }
            if(ai.RecordTypeId == rtCheck){
                existingApplicants.add(ai);
            }
        }
		
        For(Contact c :[select id, Home_County__c,Line_of_Business__c from contact where id in :contactIdList]){
            contactMap.put(c.id,c);
            contactCountySet.add(c.home_County__c);
        }
        
        For(Restricted_County__c rc :[select id, Name, MLTC_Days_Prior__c, MA_Days_Prior__c from Restricted_County__c where name in :contactCountySet]){
            restrictedCountyMap.put(rc.Name, rc);
        }

        Map<id,case> caseMap = new Map<id,case>([select id,Restricted_Applicant_Date__c from case where recordType.DeveloperName = 'Renewals' AND id in :caseIdList]);
        if(isUpdate){
            for(case c :caseMap.values()){
                c.Restricted_Applicant_Date__c = null;
            }
        }

        For(Applicant_Information__c ai :newApplicantMap){
            if(ai.Contact__c != null && contactMap.containsKey(ai.Contact__c) && restrictedCountyMap.get(contactMap.get(ai.Contact__c).Home_County__c) != null && caseMap.containsKey(ai.case_number__c) ){
                ai.Restricted_County__c = restrictedCountyMap.get(contactMap.get(ai.Contact__c).Home_County__c).id;
                //	if(contactMap.get(ai.Contact__c).Line_of_Business__c == 'MLTC' || contactMap.get(ai.Contact__c).Line_of_Business__c == 'NYM'){
                    if(contactMap.get(ai.Contact__c).Line_of_Business__c == 'MLTC'){
                        date tempDate = date.valueof(ai.Recert_Date__c - Integer.valueOf(restrictedCountyMap.get(contactMap.get(ai.Contact__c).home_County__c).MLTC_Days_Prior__c));
                        system.debug('tempDate Val: '+tempDate);
                        if(caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c == null || tempDate > caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c){
                            caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c = tempDate;
                        	
                        }
                    }
                    else if(contactMap.get(ai.Contact__c).Line_of_Business__c == 'NYM'){
                        date tempDate = (ai.Recert_Date__c - Integer.valueOf(restrictedCountyMap.get(contactMap.get(ai.Contact__c).home_County__c).MA_Days_Prior__c));
                       	if(caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c == null || tempDate > caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c){
                            caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c = tempDate;
                        }
                    }
                //}
            }
        }
        
        if(isUpdate){
            for(applicant_Information__c ai :existingApplicants){
                if(restrictedCountyMap.get(contactMap.get(ai.Contact__c).home_County__c) != null){
                    if(contactMap.get(ai.Contact__c).Line_of_Business__c == 'MLTC'){
                        date tempDate = date.valueof(ai.Recert_Date__c - Integer.valueOf(restrictedCountyMap.get(contactMap.get(ai.Contact__c).home_County__c).MLTC_Days_Prior__c));
                        system.debug('tempDate Val: '+tempDate);
                        if(caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c == null || tempDate > caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c){
                            caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c = tempDate;                    
                        }
                    }
                    else if(contactMap.get(ai.Contact__c).Line_of_Business__c == 'NYM'){
                        date tempDate = date.valueof(ai.Recert_Date__c - Integer.valueOf(restrictedCountyMap.get(contactMap.get(ai.Contact__c).home_County__c).MA_Days_Prior__c));
                        if(caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c == null || tempDate > caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c){
                            caseMap.get(ai.Case_Number__c).Restricted_Applicant_Date__c = tempDate;
                        }
                	}
            	}
            }
        }
		system.debug('RC Case Update');
        update caseMap.values();
    }
}