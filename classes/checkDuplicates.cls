/*
    Created by: Greg Hacic
    Last Update: 2 September 2016 by Greg Hacic
    Questions?: ghacic@fideliscare.org
    
    Notes:
        - built for the Retention project
        - API endpoint accepts a set of Contrived Keys and returns those Contrived Keys that have not been linked to a Retention Case in the last 3 months
*/
@RestResource(urlMapping='/retention/applicants/checkDuplicates/*') //{Salesforce Base URL}/services/apexrest/retention/applicants/checkDuplicates/
global class checkDuplicates {
    
    //primary logic for the class
    @HttpPost //HttpPost annotation exposes the method as a REST resource
    global static responseWrapper checkForDuplicates(List<ckWrapper> contrivedKeyList) {
        
        List<ckWrapper> nonDupeContrivedKeys = new List<ckWrapper>(); //list for all of the contrived keys to return
        responseWrapper responseJSON = new responseWrapper(); //responseWrapper object for API response
        Set<String> passedCKs = new Set<String>(); //set for holding distinct Contrived Keys, which were passed to the API
        
        for (ckWrapper ck : contrivedKeyList) { //for all of the passed Contrived Keys
            passedCKs.add(ck.contrivedKey); //add the Contrived Key to the set
        }
        //system.debug('CK LIST: '+[SELECT Contact__r.Contrived_Key__c, createddate FROM Applicant_Information__c WHERE Contact__c != null AND Case_Number__c != null AND Case_Number__r.RecordType.DeveloperName = 'Renewals' AND Case_Number__r.CreatedDate >= LAST_N_MONTHS:3 AND Contact__r.Contrived_Key__c IN : passedCKs]);
        if (!passedCKs.isEmpty()) { //if there were any passed Contrived Keys
            for (Applicant_Information__c applicant : [SELECT Contact__r.Contrived_Key__c FROM Applicant_Information__c WHERE Contact__c != null AND Case_Number__c != null AND Case_Number__r.RecordType.DeveloperName = 'Renewals' AND Case_Number__r.CreatedDate >= LAST_N_MONTHS:3 AND Contact__r.Contrived_Key__c IN : passedCKs]) { //query for any records that have been linked to a Case in the last 3 months
                if (applicant.Contact__r.Contrived_Key__c != null) { //as long as the Contact__r.Contrived_Key__c is not null
                    Boolean removedKey = passedCKs.remove(applicant.Contact__r.Contrived_Key__c); //remove the value from the set
                }
            }
            for (String s : passedCKs) { //for all the remaining contrived keys
                nonDupeContrivedKeys.add(new ckWrapper(s)); //add the contrived key to our wrapper
            }
            if (nonDupeContrivedKeys.isEmpty()) { //if there are no contrived keys to return
                responseJSON.Message = 'All passed contrived keys were duplicates.'; //provide a message to denote that this was purposeful
            } else {
                responseJSON.Size = nonDupeContrivedKeys.size(); //denote the number of records being returned
            }
        } else { //otherwise, there were no Contrived Keys passed
            responseJSON.Status = 'ERROR'; //denote error
            responseJSON.Message = 'No Contrived Keys were passed to API.'; //provide some context for the error
        }
        responseJSON.contrivedKeyNonDupes = nonDupeContrivedKeys; //add the list of contrived keys to the response object
        return responseJSON; //return the JSON response
    }
    
    //wrapper class for the response to any API request
    global class responseWrapper {
        
        global String Status {get;set;} //status to be returned
        global List<ckWrapper> contrivedKeyNonDupes {get;set;} //list of Contrived Keys to be returned
        global String Message {get;set;} //message string to be returned
        global Integer Size {get;set;} //the number of contrived keys being returned
        
        //constructor
        global responseWrapper() {
            this.Status = 'Okay'; //default to Okay
            this.contrivedKeyNonDupes = new List<ckWrapper>(); //list of Contrived Keys
            this.Message = ''; //default to null
            this.Size = 0; //default to zero
        }
    }
    
    //wrapper class representing the contrived key
    global class ckWrapper {
        
        global String contrivedKey {get;set;} //contrived key
        
        //constructor
        global ckWrapper() {
            this.contrivedKey = ''; //set contrivedKey to null (initially)
        }
        
        //constructor when passed a contrived key
        global ckWrapper(String passedContrivedKey) {
            this.contrivedKey = passedContrivedKey; //set the contrived key
        }
    }

}