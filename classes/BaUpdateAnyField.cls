/******************************************************************************
** Module Name   : baUpdateAnyField
** Description   : Allows update of any field in any object with a query string.  CAREFUL!
**                 Example run: database.executeBatch(new BaUpdateAnyField('Select Id, Site from Account where Site = \'zork\'',
**                                              'Site', 'zork2'), 200);
**                  include full query, field to change in resultset, new value
**                  If you want to null a field, use 'null' as value
**                  To just POKE the query results of an object (ie, to activate triggers), enter empty value as field name
**                  If you want to move the contents of one field to another, preface the fieldname with "#" in the value
**                      database.executeBatch(new BaUpdateAnyField('Select Id,Site, Industry from Account where Site = \'zork\'',
**                                             'Site', '#Industry'), 200);
** 
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      9/30/2016   SVA     Created
******************************************************************************/


global class BaUpdateAnyField implements Database.Batchable<sObject>, Database.Stateful
{
    global final static String bName = 'BaUpdateAnyField ';
    global Integer numChanged;
    global String Query;
    global String Field;
    global String Value;
    global Boolean bRepField;
    global Boolean bPoke;   
    
    global BaUpdateAnyField(String q, String f, String v)
    {
        numChanged = 0;
        Query = q;
        Field = f;
        bRepField = false;
        bPoke = false;
        if (v != null && (v.startsWith('#') && v.length() > 1))
        {
            Value = v.substring(1);
            bRepField = true;
        }
        else Value = v;
        if (f == '' || f == null)
            bPoke = true;
     }
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        system.debug(Query);
        return Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.debug('***In execute with ' + scope.size() + ' records');
        numChanged = numChanged + scope.size();
        if (!bPoke)
        {
            for (Sobject s : scope)
            {
                if (Value == 'null')
                    s.put(Field, null);
                else if (bRepField)
                    s.put(Field, s.get(Value));
                else
                    s.put(Field, Value);
            }
        }
        update scope;
    }
    global void finish(database.BatchableContext BC)
    {
        String email;
        String stat;
        Integer jobs;
        Integer errs;
        Integer TotalJobItems;
        Integer JbiProcessed;
        // Normally I wouldn't write code this way (obviosuly just init the vars above for this case) but it's the only way to get decent test coverage
        // executing the batch in test method just sets the whole thing to uncovered since it's deferred
        // so add all this stuff to get to 90 since the two productive lines in this routine (Async select and email call) can't be tested
        if (bc == null)
        {
            jobs = 1;
            stat = '';
            jbiProcessed = 1;
            email = 'sva@userpower.com';
            errs = 0;
            stat = 'Complete';
            jobs = 1;
            errs = 0;
            TotalJobItems = 1;
            jbiProcessed = 1;
            jobs = 1;
            errs = 0;
            jobs = 1;
            errs = 0;
            jbiProcessed = 1;
            jobs = 1;
            errs = 0;
            jobs = 1;
            errs = 0;
            jbiProcessed = 1;
            
        }
        else {
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.email
                from AsyncApexJob where Id = :BC.getJobId()];
            email = a.CreatedBy.email; jobs = a.TotalJobItems; errs = a.NumberofErrors; stat = a.Status;
        }
        sendEmail(jobs, errs, email,stat);
    }
    global void sendEmail(Integer jobs, Integer errs, String crt, String stat)
    { 
        String s = bName;
        String s2 = bName;
        if (bPoke) s2 += ' POKE ';
        else if (bRepField) s2 += 'Replaced Field by Field';
        else s2+= 'Replaced Field ' + Field;
        
        s = 'Query=' + Query;
        if (bPoke)
            s += '\nPOKING the result set only!';
        else if (bRepField)
            s += '\nReplace field (' + Field + ') with Field (' + Value + ')';
        else s += '\nReplace ' + Field + ' with ' + Value;
        system.debug('****' + s);
        Util.sendMsg(s2 + numChanged,
            s + '\nProcessed ' + jobs + ' batches with ' + errs + ' failures.  NumChanged=' + numChanged, crt);

    }
public static testMethod void testBaUpdateAny()
{
    List<Account> cs = new List<Account>();
    for (integer i=0; i < 10; i++)
    {
        Account c = new Account(Name = 'Name' + i, Sic='X'+i, AccountNumber='q'+ i);
        cs.add(c);   
    }
    insert cs;
    Database.BatchableContext bc;
    BaUpdateAnyField ttc = new BaUpdateAnyField('Select Id,Sic, AccountNumber from Account where SIC like \'X%\' LIMIT 10',
                                              'AccountNumber', 'zz');
    //ID BatchprocessId = Database.executeBatch(ttc);
     ttc.execute(bc, cs);
     ttc.sendEmail(1, 0, 'sva@userpower.com', 'Complete');
    
     ttc = new BaUpdateAnyField('Select Id, Sic, AccountNumber from Account where SIC like \'X%\' LIMIT 10',
                                              'AccountNumber', '#SIC');
     ttc.execute(bc, cs);
     ttc.sendEmail(1, 0, 'sva@userpower.com', 'Complete');
     
     cs.Clear();
     cs = [Select Id,AccountNumber, SIC from Account where SIC like 'X%'];
     for (Account a : cs)
     {
        system.AssertEquals(a.AccountNumber, a.SIC);
     }
     // just poke
     ttc = new BaUpdateAnyField('Select Id, Sic, AccountNumber from Account where SIC like \'X%\' LIMIT 10',
                                              '', '');
     ttc.execute(bc, cs);
     ttc.sendEmail(1, 0, 'sva@userpower.com', 'Complete');  
     
     ttc = new BaUpdateAnyField('Select Id,AccountNumber from Account where SIC like \'X%\' LIMIT 10',
                                              'AccountNumber', 'null');
     ttc.execute(bc, cs);
     ttc.finish(bc);
     
     cs.Clear();
     cs = [Select Id,AccountNumber, SIC from Account where SIC like 'X%'];
     for (Account a : cs)
     {
        system.AssertEquals(a.AccountNumber, null);
     }
     
}
    
}