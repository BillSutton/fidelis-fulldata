public class BenefitSummaryController {
    
    public List<Benefit> benList{get;set;}
    public List<BenefitItem> benItemList{get;set;}
    public Contact theContact{get;set;}
    
    public BenefitSummaryController(){
        benList = new list<Benefit>();
        benItemList = new list<BenefitItem>();
        theContact = [select id, accountId, Contrived_Key__c From Contact where ID = :ApexPages.currentPage().getParameters().get('id') limit 1];
        getBenefits(theContact.Contrived_Key__c);
    }
    
    public void getBenefits(string theKey){
        if(theKey != null && theKey != ''){
            Try{
                FidelisBenefitSummaryParser result = FidelisParserTool.bSummaryParser('{"myArray": '+FidelisAPIHelper.GetBenefits(theKey, EncodingUtil.urlDecode(String.valueOf(System.Today()), 'UTF-8'),theContact.id)+'}');
                for(FidelisBenefitSummaryParser.arrayItem r : result.myArray){
                    benItemList.add(new BenefitItem(r.bene_USER_DATA3 , r.bene_description, r.bene_NTWK_IND, r.bene_COPAY, r.bene_COINSURANCE, r.bene_BENEFIT_LIMIT, r.bene_BSDL_DEDE_AMT , r.bene_DESC_ONE));
                }
            }
            Catch(Exception e){
                if(e.getMessage().contains('apologize')){
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Facets Are Currently Down, Please Try Again Later')); 
                    System.debug('FACETS ERROR: '+e.getMessage());
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Something Went Wrong With Parsing the Benefit Response, Please Contact a System Administrator'));
                    System.debug('FACETS ERROR: '+e.getMessage());
                }               
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'The Member Does not Have a Contrived Key'));
        }
    }
    
    public class Benefit{
        public String type{get;set;}
        public String Description{get;set;}
        public String Note{get;set;}
        
        public Benefit(String t, String d, String n){           
            type = t;
            Description = d;
            Note = n;
        }
    }
    
    public class BenefitItem{
        public String Auth{get;set;}
        public String Description{get;set;}
        public String network{get;set;}
        public String copay{get;set;}
        public string coins{get;set;}
        public string benlim{get;set;}
        public string deduc{get;set;}
        public string notes{get;set;}
        
        public BenefitItem(String a, String d, String networ, string cop, string coin, string benl, string ded, string note){           
            Auth = a;
            Description = d;
            network = networ;
            copay = cop;
            coins = coin;
            benlim = benl;
            deduc = ded;
            notes = note;
        }
    }

}