/************************************************************************************************
**Name           : WebToCase
**Author         : Scott Van Atta
**Date           : January 2016
**Description    : Helper classes for web-to-case new inserts (called by trigger)
**                 Expects ONLY new webtocase records

*************************************************************************************************/
global class WebToCase{
List<Case> gCases;
List<Audit__c> auds = new List<Audit__c>();
List <Case> candidates = new List<Case>();
String query = 'select id, name, otherpostalcode from contact where ';
String MEDICARE;


global WebToCase(List<Case> cases, String medicareId)
{
    gCases = cases;
    MEDICARE = medicareId;
}
global void process()
{
    // build query for unattached cases
    for(Case c : gCases)
    {
        c.recordtypeid = MEDICARE;
        c.status = 'Assigned';
        c.status_reason__c = 'Awaiting First Contact';
        c.source_type__c = 'Website';
        c.source__c = 'Advertising/Internet';
        c.date_received__c = date.today();
        //c.channel__c = 'Direct';
        c.primary_category__c = 'Medicare';
        if (c.w2c_phone_type__c == null)
            c.w2c_phone_type__c = 'Self';
        if (c.contactid == null)
        {
            candidates.add(c);
            if (candidates.size() > 1) query += ' OR ';
            query += '(name = \'' + c.suppliedname + '\'';
            if (c.w2c_zip__c != null)
                query += ' and otherpostalcode like \'' + c.w2c_zip__c + '%\'';
            query += ')';
        }
        else {
            auds.add(new Audit__c(Process__c = 'W2C', 
                                  Description2__c=makeAudDescr(c),
                                  Description__c = LoadDescr(c),
                                  Code__c='AsIs', Associated_Id__c = c.contactid));
        }
    }
    
    // if any web cases are not connected to contacts, determine if we can match to existing
    // (based on first 5 of zip) or have to create a contact
    if (candidates.size() > 0)
    {
        List<Contact> contacts = database.query(query);
        List <Contact> newContact = new List<Contact>();
        Map <String, contact> newMap = new Map<String, contact>();
        Map<String, Contact> ContactMatch = new Map<String, Contact>();


        for (Contact con : contacts)
        {
            string s = con.name + (con.otherpostalcode == null ? '' : (con.otherpostalcode.length() > 5 ? con.otherpostalcode.substring(0,5) : con.otherpostalcode));
            contactMatch.put(s, con);
        }
            
        for (case c : candidates)
        {
            String s = c.suppliedname + (c.w2c_zip__c == null ? '' : (c.w2c_zip__c.length() > 5 ? c.w2c_zip__c.substring(0,5) : c.w2c_zip__c));
            Contact con = contactMatch.get(s);
            if (con != null) // matches existing contact by name and zip
            {
                c.contactid = con.id;
                auds.add(new Audit__c(Process__c = 'W2C', 
                                      Description2__c=makeAudDescr(c),
                                      Description__c = loadDescr(c),
                                      Code__c='FndC', Associated_Id__c = con.id));
            }
            else { // does not match, so create the contact
                String[] namePieces = c.SuppliedName.split(' ',2);
                if (namePieces.size() != 2)
                {
                    namePieces.add(namePieces[0]);
                    namePieces[0] = '';
                }
                    
                Contact cont = new Contact(FirstName=namePieces[0], 
                                            LastName=namePieces[1], 
                                            Email=c.SuppliedEmail, 
                                            language__c = c.w2c_language__c,
                                            phone_type__c = c.w2c_phone_type__c,
                                            otherpostalcode = c.w2c_zip__c,
                                            phone = c.suppliedphone
                                            );
                newContact.add(cont);
                newMap.put(c.suppliedName + c.SuppliedEmail + c.w2c_zip__c + c.suppliedPhone, cont);
                
            }
            
        }
        insert newContact; // add the new contacts if any
        
        // apply the contactid's to the cases for matches
        for (Case c : candidates)
        {
            if (c.contactid == null)
            {
                Contact con = newMap.get(c.suppliedName + c.SuppliedEmail + c.w2c_zip__c + c.suppliedPhone);
                if (con != null)
                {
                    c.contactid = con.id;
                    auds.add(new Audit__c(Process__c = 'W2C', 
                                          Description2__c=makeAudDescr(c), 
                                          Description__c = loadDescr(c),
                                          CaseNumber__c = c.casenumber,
                                          Code__c='CrtC', Associated_Id__c = c.contactid));
                }
                else {
                    auds.add(new Audit__c(Process__c = 'W2C', 
                                          Description2__c=makeAudDescr(c),
                                          Description__c = loadDescr(c),
                                          CaseNumber__c = c.casenumber,
                                          Code__c='CrtX', Error__c = '019-NoContact'));

                }

            }
        }
    }
    if (auds.size() > 0)
    {   
        try {
            insert auds;
        } catch (Exception e) { } // ensure logging does not interfere with case writes
    }
} // end process

    
    public String makeAudDescr(Case c)
    {
        String s = 'Input=[' + c.Subject + '] [' + c.suppliedname + '] [' + c.SuppliedEmail + '] ['
                                            + c.w2c_language__c + '] [' + 
                                            c.w2c_phone_type__c + '] [' + c.w2c_zip__c + '] [' + 
                                            c.suppliedphone + ']';
        if (s.length() > 254) return s.substring(0,254);
        else return s;
    }
    
    public String LoadDescr(Case c)
    {
        if (c.description != null && c.description.length() > 254)
            return c.description.substring(0,254);
        else return c.description;
    }

}