Public Class AuthGridCheck{
    /*this is a helper class to the Case Side panel, we will pass in the name of the plan the user has and preform a query
    based on this plan name, we will need to check for nulls here, then we will need to do a compare and return to the page
    what services we can use with this plan*/

    Public String ContactCounty{get;set;}
    Public String ContactPlan{get;set;}
    Public Date pickupDate{get;set;}
    Public List<Transportation_Authorization_Grid__c> tagList{get;set;}
    Public List<Transportation_Authorization_Grid__c> defaultPlans;
    Public Transportation_Authorization_Grid__c theReturnTAG{get;set;}

    Public AuthGridCheck(){

    }

    Public AuthGridCheck(String planName, String county){
        ContactPlan = planName;
        ContactCounty = county;
        tagList = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c,Effective_Date__c,Termination_Date__c from Transportation_Authorization_Grid__c where county__r.name = :county and plan__r.name =:planName ORDER BY Effective_Date__c];
        defaultPlans = [select ID,County_Covered__c,HD_Auth_Form__c,Vehicle_Type__c,Note_for_MSA__c,Authorization_Number__c,Standard_Auth_Form__c,Form_Type__c,Effective_Date__c,Termination_Date__c from Transportation_Authorization_Grid__c where county__r.name = 'Default' and plan__r.name =:planName  ORDER BY Effective_Date__c];
        calculateEligableTags();
    }
    Public AuthGridCheck(String planName, String county, Datetime pd){
        this(planName,county);
        pickupDate = pd.date();
    }


    //Returns if a note is needed for specific vehicle type and form type
    public boolean noteCheck(String tripType, String vType){
        Map<string,list<String>> theCheck = rideTypeEligiability();
        if(DoIDoIt()){
            if(!String.isBlank(tripType) && tripType.contains('Standard')){
                for(String s :theCheck.get('Standard_Auth_Form__c')){
                    if(s == vType){
                        return True;
                    }
                }
                return False;
            }
            else if(!String.isBlank(tripType)){
                for(String s :theCheck.get('HD_Auth_Form__c')){
                    if(s == vType){
                        return True;
                    }
                }
                return False;
            }
            else{
                return FALSE;
            }

        }
        else{
            return False;
        }
    }

    public void calculateEligableTags(){
        system.debug('pickupDate'+pickupDate); 
        theReturnTAG = null;
        if(pickupDate!=null){
            for(Transportation_Authorization_Grid__c tag : tagList){
                if((tag.Effective_Date__c==null || tag.Effective_Date__c <= pickupDate) && (tag.Termination_Date__c==null || tag.Termination_Date__c>=pickupDate)){
                    theReturnTAG = tag;
                    system.debug('settingreturntotag');
                }
            }
            if(defaultPlans!=null && theReturnTAG == null ){
                for(Transportation_Authorization_Grid__c defaultPlan : defaultPlans){
                    if((defaultPlan.Effective_Date__c==null || defaultPlan.Effective_Date__c<=pickupDate) && (defaultPlan.Termination_Date__c==null || defaultPlan.Termination_Date__c>=pickupDate)){
                        theReturnTAG = defaultPlan;
                        system.debug('settingreturntodefault');
                    }
                }
            }
        }else if(tagList.size()>0){
            for(Transportation_Authorization_Grid__c tag : tagList){
                system.debug(tag);
                if((tag.Effective_Date__c==null || tag.Effective_Date__c <= date.today()) && (tag.Termination_Date__c==null || tag.Termination_Date__c>=date.today())){
                    theReturnTAG = tag;
                    break;
                }
            }
        }
         if(pickupDate==null && defaultPlans!=null && theReturnTAG == null){
            for(Transportation_Authorization_Grid__c defaultPlan : defaultPlans){
                if((defaultPlan.Effective_Date__c==null || defaultPlan.Effective_Date__c<=system.today()) && (defaultPlan.Termination_Date__c==null || defaultPlan.Termination_Date__c>=system.today())){
                    theReturnTAG = defaultPlan;
                }
            }    
        }
    }
    public Date calculateMultipleTags(List<DateTime> dates){
        Set<Transportation_Authorization_Grid__c> tags = new Set<Transportation_Authorization_Grid__c>();
        if(tagList!=null){
            for(DateTime d : dates){
                Boolean foundTag = false;
                for(Transportation_Authorization_Grid__c tag : tagList){
                    if((tag.Effective_Date__c==null || tag.Effective_Date__c <= d.date()) && (tag.Termination_Date__c==null || tag.Termination_Date__c>=d.date())){
                        tags.add(tag);
                        foundTag = true;
                        break;
                    }
                }
                if(!foundTag){
                    if(defaultPlans!=null){
                        for(Transportation_Authorization_Grid__c defaultPlan : defaultPlans){
                            if((defaultPlan.Effective_Date__c==null || defaultPlan.Effective_Date__c <= d.date()) && (defaultPlan.Termination_Date__c==null || defaultPlan.Termination_Date__c>=d.date())){
                                tags.add(defaultPlan);
                            }
                        }
                    }
                }
            }
        }
        Date retDate;
        if(tags.size()>1){
            for(Transportation_Authorization_Grid__c tag : tags){
                if(retDate==null || retDate <=tag.Effective_Date__c){
                    retDate = tag.Effective_Date__c;
                }
            }
        }
        return retDate;
    }

    //Performs query to see if an auth grid exists for this county or Default
    Public boolean DoIDoIt(){ 
        if(theReturnTAG!=null){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    //Pulls the AUth Grid for this member
    Public Transportation_Authorization_Grid__c doTheQuery(){
        if(!DoIDoIt()){
            try{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Member’s plan does not cover the transportation benefit');
            }
            catch(Exception e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Member’s plan does not cover the transportation benefit');
                system.debug('There was an error with the AuthGridCheck.doTheQuery Method: '+e.getMessage());
            }
        }
        return theReturnTAG;
    }

    Public Map<string,list<String>> rideTypeEligiability(){
            return rideTypeEligiability(doTheQuery());
    }

    //Gets all AUth Grid data into a Map
    Public Map<string,list<String>> rideTypeEligiability(Transportation_Authorization_Grid__c cp){
        Map<string,list<string>> theReturn = new Map<String,list<string>>();
        if(cp!=null){
            if(cp.County_Covered__c != null && cp.County_Covered__c != ''){
                theReturn.put('County_Covered__c', cp.County_Covered__c.split(';'));
            }
            else{
                theReturn.put('County_Covered__c', new list<string>());
            }
            if(cp.HD_Auth_Form__c != null && cp.HD_Auth_Form__c != ''){
                theReturn.put('HD_Auth_Form__c', cp.HD_Auth_Form__c.split(';'));
            }
            else{
                theReturn.put('HD_Auth_Form__c', new list<string>());
            }
            if(cp.Vehicle_Type__c != null && cp.Vehicle_Type__c != ''){
                theReturn.put('Vehicle_Type__c', cp.Vehicle_Type__c.split(';'));
            }
            else{
                theReturn.put('Vehicle_Type__c', new list<string>());
            }
            if(cp.Standard_Auth_Form__c != null && cp.Standard_Auth_Form__c != ''){
                theReturn.put('Standard_Auth_Form__c', cp.Standard_Auth_Form__c.split(';'));
            }
            else{
                theReturn.put('Standard_Auth_Form__c', new list<string>());
            }
            if(cp.Form_Type__c != null && cp.Form_Type__c != ''){
                theReturn.put('Form_Type__c', cp.Form_Type__c.split(';'));
            }
            else{
                theReturn.put('Form_Type__c', new list<string>());
            }
            
            if(cp.Authorization_Number__c != null && cp.Authorization_Number__c != ''){
                theReturn.put('Authorization_Number__c', cp.Authorization_Number__c.split(';'));
            }
            else{
                theReturn.put('Authorization_Number__c', new list<string>());
            }

            if(cp.Note_for_MSA__c != null && cp.Note_for_MSA__c != ''){
                theReturn.put('Note_for_MSA__c', cp.Note_for_MSA__c.split(';'));
            }
            else{
                theReturn.put('Note_for_MSA__c', new list<string>());
            }

            return theReturn;
        }
        return null;
    }

    //Check if auth number is needed
    public boolean authNumberCheck(string vType, boolean nonCounties){
        System.debug('AUTHGRIDCHECK');
        Map<string,list<String>> theList = rideTypeEligiability(doTheQuery());
        if(theList!=null){
            for(String s :theList.get('Authorization_Number__c')){
                system.debug('The Full AuthNumber List: '+theList.get('Authorization_Number__c'));
                system.debug('The vType: '+vtype);
                system.debug('The Current S: '+s);
                if(s.contains('Not Applicable') ){
                    System.debug('NA');
                    return FALSE;
                }                       
                else if(vType.contains(';')){
                    for(String s1 :vType.split(';')){
                        if(s.contains(s1)){
                            system.debug('The First True Messed Up');
                            return TRUE;
                        }
                    }
                }
                else if(s.contains(vType)){
                    system.debug('The Second True Messed Up');
                    return TRUE;
                }
                else if(s.contains('Non-Adjacent Counties')){
                    system.debug('Non Counties returned True');
                    return nonCounties;
                }
            }
        }
        return FALSE;
    }
    
    //Authorization Number Exceptions - If providerId is null then we don't override
    /*  //Override Conditions
        //InNetwork->Accpeting Rates->Not Contracted with Member's LoB    
        //InNetwork->NotAcceptingRates
        //OutOfNetwork   
    */
   public boolean authNumberCheck(string vType, boolean nonCounties,String providerId,String lineOfBusiness,String caseId,String acceptingFromCase){
        Boolean authGridValue = authNumberCheck(vType,nonCounties);
        if(providerId!=null && providerId!=''){
            List<Account> provList = [Select id,Type from Account where Id = :providerId limit 1];
            List<Case> provCases;
            if(caseId!=null && caseId!= ''){
                provCases = [Select Id,Accepting_Medicare_or_Medicaid_Rates__c,Pickup_Datetime__c from Case where Id = :caseId limit 1];
            }
            system.debug('provCases: '+provCases);
            if(provList!=null && provList.size()>0){
                
                Account prov = provList[0];
                Case provCase;
                if(provCases!=null && provCases.size()>0){
                    provCase = provCases[0];
                }
                
                //Determine if we are accepting rates based on case
                Boolean acceptingRates = false;
                //if(provCase!=null && acceptingFromCase==null){
                //    String acceptingRatesText = provCase.Accepting_Medicare_or_Medicaid_Rates__c+'';
                //    system.debug('acceptingRatesText: '+acceptingRatesText);
                //    system.debug('The Results of the Next If: '+(acceptingRatesText == 'null'));
                //    if(acceptingRatesText == 'null' || acceptingRatesText == null || acceptingRatesText == '' || acceptingRatesText.equalsIgnoreCase('true') || acceptingRatesText.equalsIgnoreCase('yes')){
                //        acceptingRates = true;
                //    }
                //}
                if(acceptingFromCase == 'null' || acceptingFromCase == null || acceptingFromCase == '' || acceptingFromCase.equalsIgnoreCase('true') || acceptingFromCase.equalsIgnoreCase('yes')){
                    acceptingRates = true;  
                }
                //InNetwork
                if(prov.Type == 'In Network'){
                    if(acceptingRates){
                        //If is not contracted with member's LoB
                        
                        if(provCase.Pickup_DateTime__c != null){
                            Date pickupDate = provCase.Pickup_DateTime__c.date();
                            List<Transportation_Contract_History__c> provContractHistory = [Select Id from Transportation_Contract_History__c where Effective_Date__c<=:pickupDate and (Termination_Date__c = null or Termination_Date__c>=:pickupDate) and Line_Of_Business__c=:lineOfBusiness and account__c = :providerId limit 1];
                            //If contracted provider does NOT match LoB
                            if(!(provContractHistory!=null && provContractHistory.size()>0)){
                                system.debug('Returning LOB True');
                                return true;
                            }
                        }                  
                    }else{
                        system.debug('Returning Accepting Rates Else Statement True');
                        return true;
                    }
                }else if(prov.Type == 'Out of Network'){
                    system.debug('Returning Out Of Network True');
                    return true;
                }
            }
        }
        return authGridValue;
    }
}