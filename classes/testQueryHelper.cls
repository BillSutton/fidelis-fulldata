@isTest
private class testQueryHelper {

	@testSetup
	static void createTestData(){
		list<account> accounts = new list<account>();
		accounts.add(new account(name='Test Fam 1'));
		accounts.add(new account(name='Test Fam 2'));
		insert accounts;

		list<contact> contacts = new list<contact>();
		contacts.add(new contact(accountId = accounts[0].id, firstName = 'ted', lastname='test1',contrived_key__c='123456',Enrolled__c = true, Family_Link_ID__c = '456', Birthdate = system.today() - 12));
		contacts.add(new contact(accountId = accounts[1].id, firstName = 'norbert', lastname = 'test2', contrived_key__c='987654',Enrolled__c = true, Family_Link_ID__c = '123', Birthdate = system.today()-20));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563298',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 45));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563297',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 360));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563296',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 500));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563295',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 600));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563294',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 425));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563292',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 35));
		insert contacts;

		list<Reponsible_Person__c> rpList = new list<Reponsible_Person__c>();
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '1', name = 'test1'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '2', name = 'test2'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '3', name = 'test3'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '4', name = 'test4'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '5', name = 'test5'));
		insert rpList;
	}
	
	@isTest static void initalizeQueryHelperTest() {
		list<String> cList = new list<String>();
		list<String> rpList = new list<String>();

		for(contact c :[select id, contrived_key__c from contact]){
			cList.add(c.contrived_key__c);
		}
		for(Reponsible_Person__c rp : [select id, Resp_Person_ID__c from Reponsible_Person__c]){
			rpList.add(rp.Resp_Person_ID__c);
		}

		QueryHelper.populateRetentionMaps(cList,rpList);
	}
	
	@isTest static void gettingIdsTest() {
		list<String> cList = new list<String>();
		list<String> rpList = new list<String>();

		for(contact c :[select id, contrived_key__c from contact]){
			cList.add(c.contrived_key__c);
		}
		for(Reponsible_Person__c rp : [select id, Resp_Person_ID__c from Reponsible_Person__c]){
			rpList.add(rp.Resp_Person_ID__c);
		}

		QueryHelper.populateRetentionMaps(cList,rpList);

		for(String s :cList){
			QueryHelper.getContactIdByContrivedKey(s);
		}
		QueryHelper.getContactIdByContrivedKey('fail');
		for(String s :rpList){
			QueryHelper.getRpByRpId(s);
		}
		QueryHelper.getRpByRpId('fail');
	}

	@isTest static void gettingNamesTest() {
		list<String> cList = new list<String>();
		list<String> rpList = new list<String>();

		for(contact c :[select id, contrived_key__c from contact]){
			cList.add(c.contrived_key__c);
		}
		for(Reponsible_Person__c rp : [select id, Resp_Person_ID__c from Reponsible_Person__c]){
			rpList.add(rp.Resp_Person_ID__c);
		}

		QueryHelper.populateRetentionMaps(cList,rpList);

		for(String s :cList){
			QueryHelper.getContactFNameByContrivedKey(s);
			QueryHelper.getContactLNameByContrivedKey(s);
		}
		QueryHelper.getContactFNameByContrivedKey('fail');
		QueryHelper.getContactLNameByContrivedKey('fail');
	}

	@isTest static void gettingBirthdayTest() {
		list<String> cList = new list<String>();
		list<String> rpList = new list<String>();

		for(contact c :[select id, contrived_key__c from contact]){
			cList.add(c.contrived_key__c);
		}
		for(Reponsible_Person__c rp : [select id, Resp_Person_ID__c from Reponsible_Person__c]){
			rpList.add(rp.Resp_Person_ID__c);
		}

		QueryHelper.populateRetentionMaps(cList,rpList);

		for(String s :cList){
			QueryHelper.getContactBirthdayByContrivedKey(s);
		}
		QueryHelper.getContactBirthdayByContrivedKey('fail');
	}
}