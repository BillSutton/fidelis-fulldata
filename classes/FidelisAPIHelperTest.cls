/*
@Name            : FidelisAPIHelperTest
@Author          : customersuccess@cloud62.com
@Date            : May 11, 2015
@Description     : Connects to Fidelis API
@Revision History: Test Coverage for FidelisAPIHelper
                    - Greg Hacic (2016-09-12): added mockDataPremiumResponse
*/

@IsTest
public class FidelisAPIHelperTest {
    
    @IsTest
    public static void test01(){
        FidelisAPIHelper.HelpCall();
    }
    
    @IsTest
    public static void test02(){
        FidelisAPIHelper.GetIDCardRequestHistory('test','test','test');
    }
    
    @IsTest
    public static void test03(){
        FidelisAPIHelper.GetIDCardInfo('test');
    }
    
    @IsTest
    public static void test04(){
        FidelisAPIHelper.RequestIdCard();
    }
    
    @IsTest
    public static void test05(){
        FidelisAPIHelper.GetAddress('test','test','test');
    }
    
    @IsTest
    public static void test06(){
        FidelisAPIHelper.GetPhoneNumber('test','test','test');
    }
    
    @IsTest
    public static void test07(){
        FidelisAPIHelper.GetLanguage('test','test','test');
    }
    
    @IsTest
    public static void test08(){
        FidelisAPIHelper.GetContactPreference('test','test','test');
    }
    
    @IsTest
    public static void test09(){
        FidelisAPIHelper.PostMailingAddress();
    }
    
    @IsTest
    public static void test10(){
        FidelisAPIHelper.PostPhoneNumber();
    }
    
    @IsTest
    public static void test11(){
        FidelisAPIHelper.PostLanguage();
    }
    
    @IsTest
    public static void test12(){
        FidelisAPIHelper.PostContactPreference();
    }
    
    @IsTest
    public static void test13(){
        FidelisAPIHelper.GetParticipants('test');
    }
    
    @IsTest
    public static void test14(){
        FidelisAPIHelper.GetPCSPS('test');
    }
    
    @IsTest
    public static void test15(){
        FidelisAPIHelper.GetPCSPSDetails('test','test');
    }
    
    @IsTest
    public static void test16(){
        FidelisAPIHelper.SavePCSPStatus();
    }
    
    @IsTest
    public static void test17(){
        FidelisAPIHelper.SaveTraining('test');
    }
    
    @IsTest
    public static void test18(){
        FidelisAPIHelper.SavePCSPComment();
    }
    
    @IsTest
    public static void test19(){
        id contactId;
        FidelisAPIHelper.GetBenefits('test','test',contactId);
    }
    
    @IsTest
    public static void test20(){
        FidelisAPIHelper.GetMembersForResponsiblePerson('test','test','test','test','test','test','test');
    }
    
    @IsTest
    public static void test21(){
        FidelisAPIHelper.GetResponsibles('test','test');
    }
    
    @IsTest
    public static void test22(){
        FidelisAPIHelper.GetClaims('test','test','test');
    }
    
    @IsTest
    public static void test23(){
        FidelisAPIHelper.GetClaimDetails('test','test','test','test');
    }
    
    @IsTest
    public static void test24(){
        FidelisAPIHelper.GetAccumulators('test','test');
    }
    
    @IsTest
    public static void test25(){
        FidelisAPIHelper.GetEligibility('test','test');
    }
    
    @IsTest
    public static void test26(){
        FidelisAPIHelper.CheckIfUserMadeInitialPayment('test');
    }
    
    @IsTest
    public static void test27(){
        FidelisAPIHelper.IsAuthorizedForMbrSvcsLogin('test','test');
    }
    
    @IsTest
    public static void test28(){
        FidelisAPIHelper.GetProviderHistoryForMember('test');
    }
    
    @IsTest
    public static void test29(){
        FidelisAPIHelper.GetProviderDetails('test','test');
    }
    
    @IsTest
    public static void test30(){
        FidelisAPIHelper.GetProvidersBySearch('test','test','test','test','test','test','test','test','test','test',null,null);
    }
    
    @IsTest
    public static void test31(){
        FidelisAPIHelper.UpdateMemberProvider();
    }
    
    @IsTest
    public static void test32(){
        FidelisAPIHelper.GetMember('test');
    }
    
    @IsTest
    public static void test33(){
        FidelisAPIHelper.GetMembersBySubscriber('test','test','test','test','test');
    }
    
    @IsTest
    public static void test34(){
        FidelisAPIHelper.GetMembersBySubscriberId('test',true);
    }
    
    @IsTest
    public static void test35(){
        FidelisAPIHelper.CheckIfMemberIsDelinquent('test');
    }
    
    @IsTest
    public static void test36(){
        FidelisAPIHelper.generateAuthNum();
    }
    
    @IsTest
    public static void test37(){
        FidelisAPIHelper.AddAuthorization(1, 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', null, null);
    }
    
    @IsTest
    public static void test38(){
        FidelisAPIHelper.AddAuthorization(1, 'test', new List<Date>{Date.today()}, 'test', 'test', 'test', 'test', 'test','','',null,null);
    }
    
    @IsTest
    public static void test39(){
        List<WebServiceLog__c> wsl = new List<WebServiceLog__c>();
        FidelisAPIHelper.CancelLineItem('test', 1, null, null,wsl);
    }
    
    //added by Greg Hacic on 19 September 2016
    //creates a mock JSON response for Apex test classes
    //represents the response from https://partnerservices-test.fideliscare.org/MemberPortal/api/Member/GetPremiumInformationForASubscriber?subscriberId=743634861
    public static String mockDataPremiumResponse {
		get {
			String json = '{'+
					'	"memberPremiumLatestPayment": ['+
					'	{'+
					'		"subscriberId": "999990001",'+
					'		"monthlyPremium": 244.83,'+
					'		"paymentAmount": 244.83,'+
					'		"paymentDateReceived": "07/01/2016",'+
					'		"totalCurrentDue": 244.83,'+
					'		"paymentPostedDate": "07/02/2016",'+
					'		"subsidyTotal": 36,'+
					'		"aptcDeliquencyDate": "11/01/2016",'+
					'		"aptcStatus": "5 - Status unknown awaiting delinquency batch"'+
					'	}],'+
					'	"memberPaymentAppliedDetails": ['+
					'	{'+
					'		"subscriberId": "999990001",'+
					'		"paymentAppliedDate": "07/02/2016",'+
					'		"appliedAmount": 244.83,'+
					'		"checkNumber": "62476490",'+
					'		"paymentMethod": "C - Credit Card",'+
					'		"suspenseIndicator": "N - Non Suspense Item"'+
					'	},{'+
					'		"subscriberId": "999990001",'+
					'		"paymentAppliedDate": "07/02/2015",'+
					'		"appliedAmount": 244.83,'+
					'		"checkNumber": "62476489",'+
					'		"paymentMethod": "C - Credit Card",'+
					'		"suspenseIndicator": "N - Non Suspense Item"'+
					'	}],'+
					'	"memberPremiumInvoicePaidDetails": ['+
					'	{'+
					'		"subscriberId": "999990001",'+
					'		"premiumDueDate": "07/01/2016",'+
					'		"amountBilled": 244.83,'+
					'		"remainingInvoiceBalance": 0,'+
					'		"activityDate": "07/02/2016"'+
					'	},{'+
					'		"subscriberId": "999990001",'+
					'		"premiumDueDate": "07/01/2015",'+
					'		"amountBilled": 244.83,'+
					'		"remainingInvoiceBalance": 0,'+
					'		"activityDate": "07/02/2016"'+
					'	}],'+
					'	"memberPremiumInvoiceDueDetails": ['+
					'	{'+
					'		"subscriberId": "999990001",'+
					'		"billedAmount": 244.83,'+
					'		"unpaidPremiumDueDate": "08/01/2016",'+
					'		"invoiceBalanceDue": 244.83'+
					'	},{'+
					'		"subscriberId": "999990001",'+
					'		"billedAmount": 12.21,'+
					'		"unpaidPremiumDueDate": "08/15/2016",'+
					'		"invoiceBalanceDue": 12.21'+
					'	}]'+
					'}';
			return json;
		}
	}
}