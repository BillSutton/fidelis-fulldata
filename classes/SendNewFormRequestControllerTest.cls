@isTest public class SendNewFormRequestControllerTest {
	
    @testSetup static void CreateData(){
        Form_Request__c testNewForm = new Form_Request__c();
        insert testNewForm;
        Boolean testShowSentMessage = false;
    }
    
    static testMethod void test0(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        frc.newForm = new Form_Request__c();
        insert frc.newForm;
        frc.showSentMessage = false;
    }
    
    static testMethod void HideSendMessage(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        frc.hideSentMessage();
    }
    
    static testMethod void ResetTheForm(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        frc.resetTheForm();
    }
    
    static testMethod void SendButton(){
    	sendNewFormRequestController frc = new sendNewFormRequestController();
        
        //Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = 'test@email.com', Delivery_Method__c = 'Email');
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = '12121231234', Delivery_Method__c = 'eFax');
        
        
        
        frc.newForm = testNewForm;

        Boolean Validate = frc.validate();     
        frc.sendButton();
        Boolean EmailPhoneValidation = frc.emailPhoneValidation(testNewForm.Send_To_Temp__c, testNewForm.Delivery_Method__c);
        System.assertEquals(TRUE, EmailPhoneValidation);
        System.assertEquals(TRUE, Validate);
    }
   
    static testMethod void ValidateValid(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = 'test@email.com', Delivery_Method__c = 'Email', Form_Category__c = 'Transportation Department', Form_Name__c = 'Authorize Livery/Ambulance Form');
        insert testNewForm;
        
        System.debug(testNewForm);
        Boolean EmailPhoneValidation = frc.emailPhoneValidation(testNewForm.Send_To_Temp__c, testNewForm.Delivery_Method__c);
        System.assertEquals(TRUE, EmailPhoneValidation);
        frc.newForm = testNewForm;
        Boolean Validate = frc.validate();
        System.assertEquals(TRUE, Validate);
    }
    
    static testMethod void ValidateInvalid(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = '', Delivery_Method__c = '', Form_Category__c = '', Form_Name__c = '');
        insert testNewForm;
        Boolean validate = frc.validate();
        System.assertEquals(FALSE, validate);
    }
    
    static testMethod void EmailPhoneValidationValid1(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = 'test@email.com', Delivery_Method__c = 'Email');
        insert testNewForm;
        
        Boolean Validate = frc.validate();     
        Boolean EmailPhoneValidation = frc.emailPhoneValidation(testNewForm.Send_To_Temp__c, testNewForm.Delivery_Method__c);
        System.assertEquals(TRUE, EmailPhoneValidation);
    }
    
    static testMethod void EmailPhoneValidationValid2(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = '00456945832', Delivery_Method__c = 'eFax');
        insert testNewForm;
        
        Boolean Validate = frc.validate();     
        Boolean EmailPhoneValidation = frc.emailPhoneValidation(testNewForm.Send_To_Temp__c, testNewForm.Delivery_Method__c);
        System.assertEquals(TRUE, EmailPhoneValidation);
    }

    static testMethod void EmailPhoneValidationInalid1(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = 'test', Delivery_Method__c = 'Email');
        insert testNewForm;
        
        Boolean Validate = frc.validate();     
        Boolean EmailPhoneValidation = frc.emailPhoneValidation(testNewForm.Send_To_Temp__c, testNewForm.Delivery_Method__c);
        System.assertEquals(FALSE, EmailPhoneValidation);
    }
    
    static testMethod void EmailPhoneValidationInalid2(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = '004569458321', Delivery_Method__c = 'eFax');
        insert testNewForm;
        
        Boolean Validate = frc.validate();     
        Boolean EmailPhoneValidation = frc.emailPhoneValidation(testNewForm.Send_To_Temp__c, testNewForm.Delivery_Method__c);
        System.assertEquals(FALSE, EmailPhoneValidation);
    }
    
    static testMethod void EmailPhoneValidationInalid3(){
        sendNewFormRequestController frc = new sendNewFormRequestController();
        
        Form_Request__c testNewForm = new Form_Request__c(Send_To_Temp__c = 'helloo458321', Delivery_Method__c = 'eFax');
        insert testNewForm;
        
        Boolean Validate = frc.validate();     
        Boolean EmailPhoneValidation = frc.emailPhoneValidation(testNewForm.Send_To_Temp__c, testNewForm.Delivery_Method__c);
        System.assertEquals(FALSE, EmailPhoneValidation);
    }
}