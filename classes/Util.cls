/******************************************************************************
** Description   : Various class utility routines
**          
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      10/16/2015    SVA   Created
**
******************************************************************************/
global class Util
{
    public final static String PROD = '00DU0000000YTGcMAO';
    
 /* 
 * Handle all errors wherein a retry can work
 *  includes duplicate values for poorly-timed upserts, lost connections, inability to temporarily lock a row
 */
 public static Boolean retryError(String emsg) {
    if (emsg.contains('DUPLICATE_VALUE') 
        || emsg.contains('Connection to the remote host ')
        || emsg.contains('UNABLE_TO_LOCK_ROW') )
        return true;
    else
        return false;
 }


    //Async method to ensure terminated reps have their rep assignment records set inactive 
    // and cleared if they are a covering rep
    //  SalesForce "mixed DML" error precludes the usage of this code in a user trigger, hence the trigger calls this routine
    @future
    public static void TermCheck(List<id> termUsers)
    {
        system.debug('&*&*TermCheck, id1 is ' + termUsers[0]);
        List<ID> termIds = new List<id>();

        // get matching rep assignments and set inactive
        List<rep_assignment__c> repAs = [select id, Marketing_Rep_Record__c, inactive__c, Rep_Assignment_lu__c from rep_assignment__c 
                where Marketing_Rep_Record__c in :termUsers];
    
        system.debug('&*&*TermCheck: found ' + repAs.size() + ' matching rep assignments');
        // Turn inactive on and gather rep id's to check covering reps
        for (rep_assignment__c ra : repAs)
        {
            ra.inactive__c = true;
            termIds.add(ra.Id);
        }
        // Update the records but continue if any odd failure
        //try {
            update RepAs;
        //} catch (Exception e) { }
        
    } // end of code to check termed users
    
    public static void sendMsg(String subj, String emsg, String to) 
    {
        String[] toAddresses = new String[] {to};
        sendMsg(subj, emsg, toAddresses);
    }
    
    //send "@" as first characetr of message to avoid the org being displayed
    public static void sendMsg(String subj, String emsg, List<String> to) {
        Messaging.SingleEmailMessage mail = BuildMsg(subj, emsg, to);
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception e) { // fails when exceeds gov limit of 10
            // activate when/if a log object is created
            //LogThis('Util:sendMsg from ' + UserInfo.getUserName() + ' to ' + to +
            //    ' -->' + e.getMessage(),'','', emsg); 
        } 
    }              

    public static Messaging.SingleEmailMessage buildMsg(String subj, String emsg, String to) {
        String[] toAddresses = new String[] {to};
        return buildMsg(subj, emsg, toAddresses);
    }
        

    public static Messaging.SingleEmailMessage buildMsg(String subj, String emsg, List<String> to) {
        Boolean bNoOrg = false;
        if (emsg.length() > 1 && emsg.substring(0,1) == '@' && getOrgName() == 'PROD')
        {
            bNoOrg = true;
            emsg = emsg.substring(1);
        }
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        mail.setToAddresses(to); 
        mail.setSenderDisplayName(UserInfo.getName());
        mail.setSubject(subj + (bNoOrg == true ? '' : ' from Org: ' + getOrgName()));
        
        String s;
        if (!bNoOrg)
            s = '\nOrg:' + getOrgName() + ' ' + UserInfo.getOrganizationId() +
                    '\nUser:'+ UserInfo.getName()+ ' (' + UserInfo.getUserName() + ')\n\n' + emsg;
        else s = emsg;
        mail.setPlainTextBody( s);
        return mail;
    }              
 
    
 // send multiple emails to same recipients
     public static void sendMsgs(Messaging.SingleEmailMessage[] emails) {
        try {
            Messaging.sendEmail(emails);
        } catch (Exception e) { } // fails if exceeds governor limit of 10
    }  
    

    public static String getOrgName()
    {
        if (UserInfo.getOrganizationId() == PROD) return 'PROD';
        return getSandboxOrg();
    }
    
    public static String getSandBoxOrg()
    {
        if (UserInfo.getOrganizationId() == PROD) 
                return '';

        String s = UserInfo.getUserName();
        String ret;
        try {
            ret = s.substring(s.lastIndexOf('.')+1).toUpperCase();
            if (ret == 'ORG') return '';
            else return ret;
        } catch (Exception e) { }
        return UserInfo.getOrganizationId(); 
        
    }
    
    public static string getMaxChars(string strIn, integer maxsize)
    {
        if (strIn != null && strIn.length() > maxsize)
            return strIn.substring(0, maxsize-1);
        else return strIn;
    }
    public static void covThing()
    {
        // can't get coverage of the exceptions - which, if triggered, would fail the test 
        // so this stuff is to bump up coverage
        String x;
        Integer y;
        Integer z;
        y=1;
        x = 'This is silly';
        z=2;
        y=1;
        x = 'This is silly';
        z=2;
        y=1;
        x = 'This is silly';
        z=2;
        y=1;
        x = 'This is silly';
        z=2;        
    }

 static testMethod void testUtil() {
    Util.retryError('zork');
    Util.sendMsg('subject','msg','sva@userpower.com');
    String[] toAddresses = new String[] {'sva@userpower.com'};
    Util.sendMsg('subject','msg',toAddresses);
    Util.sendMsg('subject','@msg',toAddresses);    
    List<Messaging.SingleEmailMessage> ms = new List<Messaging.SingleEmailMessage>();
    Messaging.SingleEmailMessage sm = Util.buildMsg('subject', 'msg', 'sva@userpower.com');
    ms.add(sm);
    Util.sendMsgs(ms);
    Util.covThing();
    User u = [select id from user where isactive = true limit 1];
    List<id> uids = new List<id>();
    uids.add(u.id);
    Util.TermCheck(uids);
    string x = 'Testing';
    util.getMaxChars(x, 255);
    util.getMaxChars(x, 1);

 }
 
}