public class FidelisProviderSearchParser {
	//public Provider provider;
	//public String proximity;
	public List<arrayItem> myArray;
    
    public class arrayItem{
        public Provider provider;
        public String proximity;
    }
    
	public class Provider {
		public String id;
		public String addressID;
		public String npi;
		public String name;
		public String phoneNumber;
		public String phoneExtension;
		public String speciality;
		public String faxNumber;
		public String faxExtension;
		public String type;
		public String taxId;
		public String address;
		public String city;
		public String state;
		public String zip;
		public String address2;
		public String address3;
		public String groupId;
		public String groupName;
		public String chpPanel;
		public String fhpPanel;
		public String nymPanel;
		public String hbxPanel;
		public String hlpPanel;
		public String mcdPanel;
		public String mcrPanel;
	}

	
	public static FidelisProviderSearchParser parse(String json) {
		return (FidelisProviderSearchParser) System.JSON.deserialize(json, FidelisProviderSearchParser.class);
	}
    
	/*
	static testMethod void testParse() {
		String json = '{'+
		'    \"provider\": {'+
		'      \"id\": \"040426000268\",'+
		'      \"addressID\": \"040426000268\",'+
		'      \"npi\": \"1518922137\",'+
		'      \"name\": \"Smith, Glennell R.                                     \",'+
		'      \"phoneNumber\": \"7168814300          \",'+
		'      \"phoneExtension\": null,'+
		'      \"speciality\": null,'+
		'      \"faxNumber\": null,'+
		'      \"faxExtension\": null,'+
		'      \"type\": null,'+
		'      \"taxId\": null,'+
		'      \"address\": \"135 Grant Street                        \",'+
		'      \"city\": \"Buffalo            \",'+
		'      \"state\": \"NY\",'+
		'      \"zip\": \"14213      \",'+
		'      \"address2\": null,'+
		'      \"address3\": null,'+
		'      \"groupId\": null,'+
		'      \"groupName\": null,'+
		'      \"chpPanel\": \"Open\",'+
		'      \"fhpPanel\": \"Open\",'+
		'      \"nymPanel\": \"Open\",'+
		'      \"hbxPanel\": \"N/A\",'+
		'      \"hlpPanel\": \"N/A\",'+
		'      \"mcdPanel\": \"Open\",'+
		'      \"mcrPanel\": \"Open\"'+
		'    },'+
		'    \"proximity\": \"1.82\"'+
		'  },'+
		'  {'+
		'    \"provider\": {'+
		'      \"id\": \"040511000132\",'+
		'      \"addressID\": \"040511000132\",'+
		'      \"npi\": \"1629006580\",'+
		'      \"name\": \"Smith, Barbara A.                                      \",'+
		'      \"phoneNumber\": \"7166913400          \",'+
		'      \"phoneExtension\": null,'+
		'      \"speciality\": null,'+
		'      \"faxNumber\": null,'+
		'      \"faxExtension\": null,'+
		'      \"type\": null,'+
		'      \"taxId\": null,'+
		'      \"address\": \"3950 East Robinson Road                 \",'+
		'      \"city\": \"West Amherst       \",'+
		'      \"state\": \"NY\",'+
		'      \"zip\": \"14228      \",'+
		'      \"address2\": null,'+
		'      \"address3\": null,'+
		'      \"groupId\": null,'+
		'      \"groupName\": null,'+
		'      \"chpPanel\": \"Open\",'+
		'      \"fhpPanel\": \"Open\",'+
		'      \"nymPanel\": \"Open\",'+
		'      \"hbxPanel\": \"N/A\",'+
		'      \"hlpPanel\": \"N/A\",'+
		'      \"mcdPanel\": \"N/A\",'+
		'      \"mcrPanel\": \"N/A\"'+
		'    },'+
		'    \"proximity\": \"10.12\"'+
		'  },'+
		'  {'+
		'    \"provider\": {'+
		'      \"id\": \"110526000098\",'+
		'      \"addressID\": \"110526000098\",'+
		'      \"npi\": \"1629006580\",'+
		'      \"name\": \"Smith, Barbara A.                                      \",'+
		'      \"phoneNumber\": \"7166914311          \",'+
		'      \"phoneExtension\": null,'+
		'      \"speciality\": null,'+
		'      \"faxNumber\": null,'+
		'      \"faxExtension\": null,'+
		'      \"type\": null,'+
		'      \"taxId\": null,'+
		'      \"address\": \"6477 Transit Road                       \",'+
		'      \"city\": \"East Amherst       \",'+
		'      \"state\": \"NY\",'+
		'      \"zip\": \"14051      \",'+
		'      \"address2\": null,'+
		'      \"address3\": null,'+
		'      \"groupId\": null,'+
		'      \"groupName\": null,'+
		'      \"chpPanel\": \"Open\",'+
		'      \"fhpPanel\": \"Open\",'+
		'      \"nymPanel\": \"Open\",'+
		'      \"hbxPanel\": \"N/A\",'+
		'      \"hlpPanel\": \"N/A\",'+
		'      \"mcdPanel\": \"N/A\",'+
		'      \"mcrPanel\": \"N/A\"'+
		'    },'+
		'    \"proximity\": \"15.85\"'+
		'  }';
		FidelisProviderSearchParser obj = parse(json);
		System.assert(obj != null);
	}*/
}