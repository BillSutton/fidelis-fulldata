/*
    Created by: Scott Van Atta
    Last Update: 22 September 2016 by Greg Hacic
    Questions?: ghacic@fideliscare.org
    
    Notes:
        - tests assessmentRedirect.class (100.00% coverage)
*/
@isTest
public class TestAssessRedirect {
    
    public static testMethod void TestAssessRedirect() {
        //BEGIN: some setup items...
        //create an Account
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name = 'Tess Trucking Co.'));
        insert accounts;
        //create a Contact
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(AccountId = accounts[0].Id, FirstName = 'Tess', Home_County__c = 'Erie', LastName = 'Dachshund', Market__c = 'WNY', Phone = '(716) 555-1234', Region__c = 'Buffalo'));
        insert contacts;
        //create a Case
        List<Case> cases = new List<Case>();
        cases.add(new Case(primary_category__c = 'FIDA', Status = 'Assigned', ContactId = contacts[0].Id, date_received__c = date.today(), Source_Type__c = 'Ad Event'));
        insert cases;
        List<Assessment__c> assessments = new List<Assessment__c>();
        assessments.add(new Assessment__c(Case__c = cases[0].Id, Name='Test', Type_Of_Assessment__c = 'Initial Assessment', assessment_status__c = 'In Process', Servicing_agency__c = 'Some Value'));
        //END: some setup items...
        
        Test.startTest(); //denote testing context
        
        PageReference pageRef = Page.NewAssessmentRedirect; //create a page reference to NewAssessmentRedirect.page
        Test.setCurrentPage(pageRef); //set page context
        ApexPages.StandardController standardController = new ApexPages.standardController(assessments[0]); //instantiate the standard Assessment__c object controller
        assessmentRedirect ext = new assessmentRedirect(standardController); //instantiate the extension
        
        ApexPages.currentPage().getParameters().put(ext.IDCASENUM, cases[0].CaseNumber);
        ApexPages.currentPage().getParameters().put(ext.IDCASEID, cases[0].Id);
        ext = new assessmentRedirect(standardController);
        insert assessments;
        
        Test.stopTest(); //revert from test context
    }

}