/* **********************************************************************************
Name            : TestCaseRouter
Author          : Scott Van Atta
Date            : February 2016
Description     : Test Class for CaseRouter and case trigger

Revision History:-
Version  Date        Author  Description of Action
1.0      2/2016      SVA     Created
1.1      5/18/2016   SVA     Added allocation percent plus recordtype checks (rep assignments, cases) since Retention added

*************************************************************************************/

@isTest 
private class TestCaseRouter{
static public Id MKTCASE = [select id from recordtype where developername = 'Care_Management' and sObjectType = 'Case'].Id;
static public Id MKTREP = [select id from recordtype where developername = 'Marketing' and sObjectType = 'Rep_Assignment__c'].Id;
    static testMethod void repAssignmentFunctionalityTest(){
        
        List<User> userList = getUserList('t1');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        contact con = cons[0];

        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact', primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller1', date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        cases.add(c1);
        cases.add(c2);
        cases.add(c3);

        system.debug('&***TEST: Loading three cases');
        insert cases;
        system.debug('&***TEST: Done loading 3 cases, now assert and add 1 case');
        
        // each rep shoud have gotten one assignment
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[0].Id]);
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[2].Id]);
        
        raLst[1].last_time_assigned__c = date.parse('11/01/1995');
        update raLst[1];
        system.debug('*****Updated last time to ' + raLst[1].last_time_assigned__c);
        case c4 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller4', date_received__c=date.parse('02/04/2016'));
        
        insert c4; // should be assigned to rep2, so his count now 2
        System.assertEquals(2, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);
        
        Test.stopTest();
         
    }
 
 // OOO check
 // rep 1 doesn't match but rep3 OOO's to rep 1 so should assign
    static testMethod void repAssignmentFunctionalityTest2(){
        
        List<User> userList = getUserList('t2');
        List<Rep_Assignment__c> raLst = getRAListWithOOO(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACListWithOOO(raLst);
        List<Contact> cons = getContact(raLst);
        contact con = cons[0];
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        insert cases;
        
        // rep2 shoud have gotten all assignments
        System.assertEquals(3, [select count() from case where Rep_Assignment__c= :raLst[0].Id]);
        Test.stopTest();
         
    }

 // OOO check
 // rep 1 doesn't match but rep3 OOO's to rep 1 so should assign BUT have circular loop
    static testMethod void repAssignmentFunctionalityTest3(){
        
        List<User> userList = getUserList('t3');
        List<Rep_Assignment__c> raLst = getRAListWithOOOCirc(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACListWithOOO(raLst);
        List<Contact> cons = getContact(raLst);
        
        contact con = cons[0];
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        insert c1;
        // It should initially set assignment to ra2 because it has the lesser last-assigned date
        // So nitial assignment stands because a circular loop
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);

        Test.stopTest();
         
    }
 
 // Nonexistent language checked
    static testMethod void repAssignmentFunctionalityTest4(){
        
        List<User> userList = getUserList('t4');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        
        contact con = cons[3];
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        insert c1;
        // Make sure it assigned to one of them
        System.assertEquals(1, [select count() from case where Rep_Assignment__c != null]);

        Test.stopTest();
         
    }
// Exercise Unassigned clause and miscellaneous coverage
    static testMethod void repAssignmentFunctionalityTest5(){
        
        List<User> userList = getUserList('t42');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        
        contact con = cons[0];
        
        Test.startTest();
        

        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'NYM', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c2);
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'Dual', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c3);
        case c4 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'EP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c4);
        case c5 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'HBX', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c5);
        case c5a = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'FIDA', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c5a);
        case c6 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c6);
        insert cases;
        case c7 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'MLTC', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c7;
        List<Case> cs = [select id, rep_assignment__c, date_assigned__c from case where rep_assignment__c != null];
        system.debug('***TestCaseRouter: Found ' + cs.size() + ' cases with rep assignments below');
        for (case cc : cs)
            system.debug(cc.rep_assignment__c + ' with case date assigned=' + cc.date_assigned__c);
        system.debug('**Rep3 has ' + [select count() from case where rep_assignment__c = :raLst[2].Id] + ' cases assigned!');

        Test.stopTest();
         
    }
 
  // Tele rep logic check1
    static testMethod void repAssignmentFunctionalityTest6(){
        ID tele = [select id from userrole where name = 'Marketing Representative NYC III (Team 1A)'].Id;
        List<User> userList = getUserListWithTele1(tele,'t6');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        
        contact con = cons[0];
        //raLst[2] is now a telemarketing rep
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        insert c1;
        c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c1;

        // Make sure it assigned to tele rep
        System.assertEquals(2, [select count() from case where Rep_Assignment__c = :raLst[2].Id]);

        Test.stopTest();
         
    }

  // Tele rep logic check1
    static testMethod void repAssignmentFunctionalityTest7(){
        ID tele = [select id from userrole where name = 'Marketing Representative NYC III (Team 1A)'].Id;   
        
        List<User> userList = getUserListWithTele2(tele, 't7');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        
        contact con = cons[0];
        userList[2].userroleid = [select id from userrole where name = 'Marketing Representative NYC III (Team 1A)'].Id;
        update userList[2];
        //raLst[1 & 2] is now telemarketing reps
        
        Test.startTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {

            List<Contact> lList = new List<Contact>();

            List<Case> cases = new list<Case>();
            case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
            insert c1;
            c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
            insert c1;
            c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
            insert c1;
            
            System.assertEquals(3,[select count() from case where Rep_Assignment__c = :raLst[2].Id or Rep_Assignment__c = :raLst[1].Id]);
        }
        Test.stopTest();
         
    }

  // Tele rep logic check3
    static testMethod void repAssignmentFunctionalityTest8(){
        ID tele = [select id from userrole where name = 'Marketing Representative NYC III (Team 1A)'].Id;
        List<User> userList = getUserListWithTele1(tele,'t8');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        
        contact con = cons[0];
        //raLst[2] is now a telemarketing rep
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        for (integer i=1; i < 12; i++)
            cases.add(new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  date_received__c=date.parse('02/01/2016')));
        insert cases;

        // Make sure it assigned 10 to tele rep and 1 to another
        System.assertEquals(10, [select count() from case where Rep_Assignment__c = :raLst[2].Id]);
        System.assertEquals(1, [select count() from case where Rep_Assignment__c != :raLst[2].Id]);

        Test.stopTest();
         
    }

    static testMethod void repAssignmentFunctionalityTest9(){
        
        List<User> userList = getUserList('t1');
        List<Rep_Assignment__c> raLst = getRAListAlloc(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        contact con = cons[0];

        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact', primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller1', date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c4 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c5 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c6 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c7 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c8 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c9 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c10 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = MKTCASE, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));

        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        cases.add(c4);
        cases.add(c5);
        cases.add(c6);
        cases.add(c7);
        cases.add(c8);
        cases.add(c9);
        cases.add(c10);
        

        system.debug('&***TEST: Loading ten cases for alloc test');
        insert cases;
        system.debug('&***TEST: Done loading 10 cases, now assert to ensure rep2 has only 2 case and the others 4');
        
        // each rep shoud have gotten one assignment
        System.assertEquals(4, [select count() from case where Rep_Assignment__c= :raLst[0].Id]);
        System.assertEquals(2, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);
        System.assertEquals(4, [select count() from case where Rep_Assignment__c= :raLst[2].Id]);
        
        Test.stopTest();
         
    }


 
    
    private static List<Contact> getContact(List<Rep_Assignment__c> ra){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<Contact> lList = new List<Contact>();

        System.runAs ( thisUser ) {
            Contact l1 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l2 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l3 = new Contact(LastName='testName',otherpostalcode='11111',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l4 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='Spanglish',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            lList.add(l1);
            lList.add(l2);
            lList.add(l3);
            lList.add(l4);
            insert lList;
        }
        return lList;
    }
    
    private static List<User> getUserList(string rndm)
    {
        list<User> userList = new List<User>();

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {

            user u1 = new user(firstname='Huck', lastname = 'urep1', alias='rep1',email='rep1@test.com',username='rep1test'+rndm+'@test.com', communitynickname='rep1',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u1);
            
            user u2 = new user(firstname='Huck', lastname = 'urep2', alias='rep2',email='rep2@test.com',username='rep2test'+rndm+'@test.com', communitynickname='rep2',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u2);
            
            user u3 = new user(firstname='Huck', lastname = 'urep3', alias='rep3',email='rep3@test.com',username='rep3test'+rndm+'@test.com', communitynickname='rep3',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u3);
            insert userList;
        }
        return userList;
    }

    private static List<User> getUserListWithTele1(id tele, string rndm)
    {
        list<User> userList = new List<User>();

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {

            user u1 = new user(firstname='Huck', lastname = 'urep1', alias='rep1',email='rep1@test.com',username='rept1test'+rndm+'@test.com', communitynickname='rep1',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u1);
            user u2 = new user(firstname='Huck', lastname = 'urep2', alias='rep2',email='rep2@test.com',username='rept2test'+rndm+'@test.com', communitynickname='rep2',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u2);
            user u3 = new user(firstname='Huck', lastname = 'urep3', alias='rep3',email='rep3@test.com',username='rept3test'+rndm+'@test.com', communitynickname='rep3',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', userroleid=tele, localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u3);
            insert userList;
    }
        return userList;
    }

    private static List<User> getUserListWithTele2(id tele, string rndm)
    {
        list<User> userList = new List<User>();
        user u1 = new user(firstname='Huck', lastname = 'urep1', alias='rep1',email='rep1@test.com',username='rept21test'+rndm+'@test.com', communitynickname='rep1',emailencodingkey='ISO-8859-1',
                                    timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
        userList.add(u1);
        user u2 = new user(firstname='Huck', lastname = 'urep2', alias='rep2',email='rep2@test.com',username='rept22@test'+rndm+'@test.com', communitynickname='rep2',emailencodingkey='ISO-8859-1',
                                    timezonesidkey='America/New_York', userroleid=tele, localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
        userList.add(u2);
        user u3 = new user(firstname='Huck', lastname = 'urep3', alias='rep3',email='rep3@test.com',username='rept23@test'+rndm+'@test.com', communitynickname='rep3',emailencodingkey='ISO-8859-1',
                                    timezonesidkey='America/New_York', userroleid=tele, localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
        userList.add(u3);
        insert userList;
        return userList;
    }
    
    
    
    private static List<Rep_Assignment__c> getRAList(list<User> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        user thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',CHP__c = true, last_time_assigned__c=null, Rep_Phone__c= '1',  recordtypeid = MKTREP,
                                                            MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null, marketing_rep_record__c = userlist[0].id);
            
            Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',CHP__c = true, last_time_assigned__c=null, Vacation__c = false, Rep_Assignment_lu__c = null,recordtypeid = MKTREP, 
                                                            marketing_rep_record__c = userlist[1].id, Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId());
            raLst.add(ra1);
            raLst.add(ra2);
            Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep3',last_time_assigned__c=null, CHP__C = true,HBX__C = true, recordtypeid = MKTREP,
                                                            Medicaid__C = true,Medicare__C = true, Vacation__c = false, Rep_Assignment_lu__c = null, Rep_Phone__c= '1', 
                                                            ep__c = true, fida__c=true, mltc__c=true,marketing_rep_record__c = userlist[2].id, MSA__c = UserInfo.getUserId());

            raLst.add(ra3);
            insert raLst;
        }
        return raLst;
    }
 
     private static List<Rep_Assignment__c> getRAListAlloc(list<User> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        user thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',CHP__c = true, last_time_assigned__c=null, Rep_Phone__c= '1',  recordtypeid = MKTREP,
                                                            MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null, marketing_rep_record__c = userlist[0].id);
            
            Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',CHP__c = true, last_time_assigned__c=null, Vacation__c = false, Rep_Assignment_lu__c = null,recordtypeid = MKTREP, 
                                                            marketing_rep_record__c = userlist[1].id, Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId(), allocation_percentage__c = 50);
            raLst.add(ra1);
            raLst.add(ra2);
            Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep3',last_time_assigned__c=null, CHP__C = true,HBX__C = true,recordtypeid = MKTREP,
                                                            Medicaid__C = true,Medicare__C = true, Vacation__c = false, Rep_Assignment_lu__c = null, Rep_Phone__c= '1', 
                                                            ep__c = true, fida__c=true, mltc__c=true,marketing_rep_record__c = userlist[2].id, MSA__c = UserInfo.getUserId());

            raLst.add(ra3);
            insert raLst;
        }
        return raLst;
    }
 

    private static List<Rep_Assignment__c> getRAListWithOOO(list<user> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',CHP__c = true, last_time_assigned__c=null, Rep_Phone__c= '1', recordtypeid = MKTREP, 
                                                        marketing_rep_record__c = userlist[0].id,MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null);
        
        Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',CHP__c = true, last_time_assigned__c=null, Vacation__c = false, Rep_Assignment_lu__c = null, recordtypeid = MKTREP,
                                                        marketing_rep_record__c = userlist[1].id,Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId());
        raLst.add(ra1);
        raLst.add(ra2);
        insert raLst;
        Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep3',last_time_assigned__c=null, CHP__C = true,HBX__C = true,recordtypeid = MKTREP,
                                                        Medicaid__C = true,Medicare__C = true, Vacation__c = true, Rep_Assignment_lu__c = ra1.id, Rep_Phone__c= '1', 
                                                        marketing_rep_record__c = userlist[2].id, MSA__c = UserInfo.getUserId());
        insert ra3;
        raLst.add(ra3);
        return raLst;
    }
    
     private static List<Rep_Assignment_Criteria__c> getRACListWithOOO(List<Rep_Assignment__c> ra){
        List<Rep_Assignment_Criteria__c> racLst = new List<Rep_Assignment_Criteria__c>();
        Rep_Assignment_Criteria__c rac1 = new Rep_Assignment_Criteria__c(Zip_Code__c='99999',Language__c='English',Rep_Assignment__c=ra[0].Id);    
        Rep_Assignment_Criteria__c rac2 = new Rep_Assignment_Criteria__c(Zip_Code__c='67890',Language__c='Spanish',Rep_Assignment__c=ra[1].Id);    
        Rep_Assignment_Criteria__c rac3 = new Rep_Assignment_Criteria__c(Zip_Code__c='45678',Language__c='English',Rep_Assignment__c=ra[1].Id);
        Rep_Assignment_Criteria__c rac4 = new Rep_Assignment_Criteria__c(Zip_Code__c='88888',Language__c='English',Rep_Assignment__c=ra[1].Id);
        Rep_Assignment_Criteria__c rac5 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[2].Id);      
        racLst.add(rac1);
        racLst.add(rac2);
        racLst.add(rac3);
        racLst.add(rac4);
        racLst.add(rac5);
        insert racLst;
        return racLst;     
    }
    private static List<Rep_Assignment__c> getRAListWithOOOCirc(List<user> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',CHP__c = true, Rep_Phone__c= '1',recordtypeid = MKTREP,  
                                                        marketing_rep_record__c = userlist[0].id,MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null,
                                                        Last_Time_Assigned__c = date.parse('12/01/2016'));
        insert ra1;
        Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',CHP__c = true, Vacation__c = true, Rep_Assignment_lu__c = ra1.Id, recordtypeid = MKTREP,
                                                        marketing_rep_record__c = userlist[1].id,Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId(),
                                                        Last_Time_Assigned__c = date.parse('11/01/2016'));
        insert ra2;
        ra1.vacation__c = true;
        ra1.rep_assignment_lu__c = ra2.id;
        update ra1;
        raLst.add(ra1);
        raLst.add(ra2);
        ralst.add(ra2);
        
        return raLst;
    }

    private static List<Rep_Assignment_Criteria__c> getRACList(List<Rep_Assignment__c> ra){
        List<Rep_Assignment_Criteria__c> racLst = new List<Rep_Assignment_Criteria__c>();
        user thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Rep_Assignment_Criteria__c rac1 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[0].Id);    
            Rep_Assignment_Criteria__c rac2 = new Rep_Assignment_Criteria__c(Zip_Code__c='67890',Language__c='Spanish',Rep_Assignment__c=ra[1].Id);    
            Rep_Assignment_Criteria__c rac3 = new Rep_Assignment_Criteria__c(Zip_Code__c='45678',Language__c='English',Rep_Assignment__c=ra[1].Id);
            Rep_Assignment_Criteria__c rac4 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[1].Id);
            Rep_Assignment_Criteria__c rac5 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[2].Id);      
            racLst.add(rac1);
            racLst.add(rac2);
            racLst.add(rac3);
            racLst.add(rac4);
            racLst.add(rac5);
            insert racLst;
        }
        return racLst;     
    }
          
    

  
}