/************************************************************************************************
**Name           : CaseRouter
**Author         : Scott Van Atta
**Date           : January 2016
**Description    : Routing for cases
**
** Assumptions for input: All cases are routeable, status = Assigned, Primary_Category__c cannot be null, rep_override must not be true
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      1/25/2016   SVA     Created from original outreach trigger
** 2.0      9/13/2016   SVA     Allocation percentage plus shareable components with Retention

*************************************************************************************************/
global class CaseRouter {
//Constants
private List<String>                                                gLanguageOrder = new List<String>{'Other','Spanish','English'};
private List<String>                                                gCategoryOrder = new List<String>{'HBX', 'EP', 'CHP','FHP','NYM','Dual','Medicare','MLTC','MLTC Packet', 'FIDA'};
private Integer                                                     TELEMAX = 10;
private string                                                      RECTYPE = 'Marketing';

private List<Rep_Assignment__c>                                     gQueryList;
private Map<Id, Rep_Assignment__c>                                  gCoveringRepMap;
List<Case>                                                          gCases;
private Map<Id, Integer>                                            gRaCount = new Map<Id, Integer>();
//gRacMap = ZIP --> Language --> list of eligible RACs
private Map<String, Map<String, List<Rep_Assignment_Criteria__c>>>  gRacMap = new Map<String, Map<String, List<Rep_Assignment_Criteria__c>>>();
private Map<Id, Rep_Assignment__c>                                  gUpdateRa = new Map<Id, Rep_Assignment__c>(); // for updating date of last assign
private Map<ID,Rep_Assignment__c>                                   gRepAssignments = new Map<ID,Rep_Assignment__c>();
private Map<id, Contact>                                            gContactMap;
private Set<ID>                                                     gRepAssignmentsToTotal = new Set<Id>();
private Map<String, Default_Region_Rep__c>                          gDefaultRepMap = Default_Region_Rep__c.getAll();
private List<Case>                                                  gCaseProcessList = new List<Case>();
private Map<id, Boolean>                                            gUpdatedReps = new Map<Id, Boolean>();
private List<Id>                                                    gIgnoreCases = new List<Id>();
private String                                                      gLog;


global CaseRouter(List<Case> cases)
{
    gCases = cases;
}

//**********************************************************************************************************************************
// Called by trigger to process all routable cases
//**********************************************************************************************************************************
global void process()
{
    getRepAssignments(); // get all relevant (by zip) rep assignment records, sorted by last assigned descending
    getCoveringRepAssignments(); // get covering reps for OOO folks (needs recursion but blows DML limits)
    buildMaps(); // build RA maps
    getAndSortCases(); // sort today's cases in language and category order
    routeThem(); // do the routing
    updateRepAssign(); // update rep assignments with latest dates
}

//**********************************************************************************************************************************
// Final routine called by trigger to ensure owner and rep id are the same for ALL records
// We'll use gRepAssignments and add to it for id's not found
//**********************************************************************************************************************************
global void processOwners(List<Case> cases) 
{
    rep_assignment__c rep;
    List<id> ids = new List<Id>();

    // get the unknowns, if any,  into a list for selection
    for (case c : cases)
    {
        if (c.rep_assignment__c != null && !gRepAssignments.containsKey(c.rep_assignment__c))
        {
            if (gCoveringRepMap != null && gCoveringRepMap.containsKey(c.rep_assignment__c)) // do anything to avoid DML
                gRepAssignments.put(c.rep_assignment__c, gCoveringRepMap.get(c.rep_assignment__c));
            else
                ids.add(c.rep_assignment__c);
        }
    }
    
    if (ids.size() > 0) // we found some unmatched so gotta read them
    {   
        List<rep_assignment__c> reps = [select id, Marketing_Rep_Record__c from rep_assignment__c where id in :ids];
        for (rep_assignment__c ra : reps)
            gRepAssignments.put(ra.id, ra);
    }
    
    // ok, all we need is in gRepAssignments now, so update case owners
    system.debug('***ProcessOwner: Handling ' + cases.size() + ' cases');
    for (case c2 : cases)
    {
        if (c2.rep_assignment__c != null) 
        {
            rep = gRepAssignments.get(c2.rep_assignment__c);
            if (rep != null && rep.marketing_rep_record__c != null)
            {
                system.debug('***ProcessOwners: Setting owner to ' + rep.marketing_rep_record__c);
                c2.ownerid = rep.marketing_rep_record__c;
            }
        }
    }
}

//**********************************************************************************************************************************
// Grab the active rep assignments where their criteria uses the zip(s) in question
// Order by vacation (so false at top of list), then by date of last route
private void getRepAssignments()
{
    Set<String> zipSet = new Set<String>();
    List<Id> cons = new List<Id>();
    // Get the contacts referenced
    for (Case c : gCases){
        if (c.contactid != null) cons.add(c.contactId);
        if (c.rep_assignment__c != null && c.id != null) // don't doublecount in later case count
        {
            logit('***Will ignore casenumber ' + c.casenumber + ' when counting todays cases since it is being reassigned');
            gIgnoreCases.add(c.Id);
        }

    }
    
    gContactMap = new Map<Id, Contact>([select Id, OtherPostalCode, language__c, region__c from contact where id in :cons]);

    // Map contacts for reference
    for (Case c : gCases){
            if (c.contactid != null && gContactMap.containsKey(c.contactId)) 
            {
                String zip = gContactMap.get(c.contactId).OtherPostalCode;
                if (zip != null) zipSet.add(zip);
            }
    }
    // sort by putting vacations last (false sorts to top) and last time assigned
    gQueryList = [SELECT ID,Name,CHP__c,County__c,HBX__c,EP__c, FIDA__C, Market__c, FHP__c, dual__c,
                  Medicaid__c,Medicare__c,MLTC__c,Region__c,Representative_Name__c,
                  Vacation__c,Covering_Representative__c, Rep_Assignment_lu__c, Inactive__c, allocation_percentage__c,
                  rep_supervisor__c,Marketing_Rep_Record__c,Rep_Full_Name__c, Last_Time_Assigned__c,  telephone_priority_route__c,
                  Rep_Assignment_lu__r.Id, Rep_Assignment_lu__r.Rep_Supervisor__c,Rep_Assignment_lu__r.Marketing_Rep_Record__c, Rep_Assignment_lu__r.Inactive__c,
                    (SELECT ID,Name,Language__c,Zip_Code__c,Rep_Assignment__c,Rep_Assignment__r.Vacation__c,Rep_Assignment__r.Rep_Assignment_lu__c,
                                Rep_Assignment__r.MLTC__C, Rep_Assignment__r.CHP__C, Rep_Assignment__r.FIDA__C, Rep_Assignment__r.FHP__C, Rep_Assignment__r.dual__C, Rep_Assignment__r.representative_name__c, 
                                Rep_Assignment__r.HBX__C, Rep_Assignment__r.Medicaid__C, Rep_Assignment__r.Medicare__C, Rep_Assignment__r.EP__C FROM 
                                Rep_Assignment_Criterias__r WHERE Zip_Code__c IN : zipSet) 
                  FROM Rep_Assignment__c
                  where inactive__c = false
                  and recordtype.name = :RECTYPE
                  order by vacation__c, Last_Time_Assigned__c];

}

//**********************************************************************************************************************************
// Pull in all reps who are named as covering reps since any can be used in an OOO chain    
private void getCoveringRepAssignments() {

    //2014-04-01 must get covering reps in a map. need to get all possible covering reps for recursion
    List<rep_assignment__c> coveredReps = [select id, Rep_Assignment_lu__c from rep_assignment__c where rep_assignment_lu__c != null and recordtype.name = :RECTYPE];
    List<id> coveringRepIds = new List<id>();
    for ( rep_assignment__c delegator : coveredReps)
        coveringRepIds.add(delegator.rep_assignment_lu__c);
        
    if (coveringRepIds.size() == 0)
        gCoveringRepMap = new Map<Id, rep_assignment__c>();
    else
        gCoveringRepMap = new Map<Id, Rep_Assignment__c>([SELECT ID,Name,CHP__c,County__c,HBX__c,EP__c, FIDA__C, Market__c, FHP__c, dual__c,
                                                            Medicaid__c,Medicare__c,MLTC__c,Region__c,Representative_Name__c, allocation_percentage__c, 
                                                            Vacation__c,Covering_Representative__c, Rep_Assignment_lu__c, Inactive__c,
                                                            rep_supervisor__c,Marketing_Rep_Record__c,Rep_Full_Name__c, Last_Time_Assigned__c,  telephone_priority_route__c,
                                                            Rep_Assignment_lu__r.Id, Rep_Assignment_lu__r.Rep_Supervisor__c,Rep_Assignment_lu__r.Marketing_Rep_Record__c, Rep_Assignment_lu__r.Inactive__c
                                                        FROM REP_ASSIGNMENT__C WHERE ID IN : coveringRepIds]);
    
}

//**********************************************************************************************************************************
// Build gRepAssignments and establish counters
private void buildMaps()
{
    Set<Id> reps = new set<Id>();
    for (Rep_Assignment__c ra : gQueryList){
        String thisRAId = ra.Id;
        gRepAssignments.put(thisRAId, ra);
        gRaCount.put(thisRAId, 0);
        if (ra.Vacation__c && ra.Rep_Assignment_lu__c != null){
            thisRAId = ra.Rep_Assignment_lu__c;
            gRepAssignments.put(thisRAId, gCoveringRepMap.get(ra.Rep_Assignment_lu__c));
            gRaCount.put(thisRAId, 0);
            system.debug('***Buildmap:Adding vacation delegate to asgn total: ' + thisRAId);
        }

        //store the eligible reps by ZIP and language for use later
        for (Rep_Assignment_Criteria__c rac : ra.Rep_Assignment_Criterias__r){
            if (!gRacMap.containsKey(rac.Zip_Code__c)){
                gRacMap.put(rac.Zip_Code__c, new Map<String, List<Rep_Assignment_Criteria__c>>());
            }
            if (!gRacMap.get(rac.Zip_Code__c).containsKey(rac.Language__c)){
                gRacMap.get(rac.Zip_Code__c).put(rac.Language__c, new List<Rep_Assignment_Criteria__c>());
            }
            gRacMap.get(rac.Zip_Code__c).get(rac.Language__c).add(rac);
            reps.add(rac.Rep_Assignment__c);
            system.debug('***BuildMap:Adding rep to asgn total: ' + rac.Rep_Assignment__c + ' ' + rac.rep_assignment__r.representative_name__c);

        }
    }
    for (id ix : reps)
        gRepAssignmentsToTotal.add(ix);
    
    /*
    integer td = 0;
    system.debug('***gRepAssignmentsToTotal');
    for (id ix2 : grepAssignmentsToTotal)
    {
        td++;
        system.debug('*** #' + td + ' [' + ix2 + '] ' + gRepAssignments.get(ix2).representative_name__c);
    }
    */


}

//**********************************************************************************************************************************
// Establish today's counts of cases already routed
private void getAndSortCases()
{  
    /**************************************************************************
 
    //record how many Cases each rep already has
    //2014-03-11 only get Cases from today
    List<Case> cases = [SELECT ID, REP_ASSIGNMENT__C FROM Case
            WHERE REP_ASSIGNMENT__C IN: gRepAssignmentsToTotal
            and recordtype.name = :RECTYPE
            //AND STATUS IN ('Assigned','Distributed')
            //and isclosed = false
            AND DATE_ASSIGNED__C = today];
    system.debug('***GetAndSortCases found ' + cases.size() + ' cases for ' + gRepAssignmentsToTotal.size() + ' rep assignments to total');
    for (Case c : cases)
    {
        if (!gRaCount.containsKey(c.Rep_Assignment__c)){
            gRaCount.put(c.Rep_Assignment__c, 1);
        } else {
            gRaCount.put(c.Rep_Assignment__c, gRaCount.get(c.Rep_Assignment__c) + 1);
            //system.debug('putting in: ' + gRepAssignments.get(c.Rep_Assignment__c).Rep_Full_Name__c + ', ' + gRaCount.get(c.Rep_Assignment__c));
        }
    }
    ***********************************************************************/
// replacement code for above sva 3/2016
    AggregateResult[] results = [select rep_assignment__c Rep, count(id) Quantity from case  
                                    where createddate = today and rep_assignment__c in :gRepAssignmentsToTotal and id not in :gIgnoreCases
                                    group by rep_assignment__c 
                                    having rep_assignment__c != null];

    system.debug('***GetAndSortCases found ' + results.size() + ' aggregate results for ' + gRepAssignmentsToTotal.size() + ' rep assignments to total');

    for (AggregateResult ar : results)
    {
        ID arid = (ID) ar.get('Rep');
        Integer count = (Integer) ar.get('Quantity');
        gRaCount.put(arid, count);
        system.debug('putting in: ' + gRepAssignments.get(arid).Rep_Full_Name__c + ', ' + arid);
    }


 //2014-03-11 sort the Cases coming in a certain order
    system.debug('***Cases input to router=' + gCases.size());
 // Requirements are unclear as to rationale
    String str;
    for (String thisLanguage : gLanguageOrder){
        for (String thisCategory : gCategoryOrder){
            List<Case> tmpList = new List<Case>();
            for (Case c : gCases){
                String lang = gContactMap.get(c.contactId).language__c;
                //system.debug('&** lang=' + lang + ' c.cat-' + c.primary_category__c);
                if (thisLanguage == 'Other'){
                    if (lang != 'Spanish' && lang != 'English' && c.Primary_Category__c == thisCategory){
                        tmpList.add(c);
                    }
                } else {
                    if (lang == thisLanguage && c.Primary_Category__c == thisCategory)
                        tmpList.add(c);
                }
            } 
            gCaseProcessList.addAll(tmpList);
        }
    }
}
        
 //**********************************************************************************************************************************
 //**********************************************************************************************************************************
 //**********************************************************************************************************************************
// Route the case by walking thru available reps by zip
 private void routeThem()
 {
    Rep_Assignment__c AssignedRep;
    Integer AssigendAlloc;
    system.debug('***Cases input to routeThem=' + gCases.size());
   //begin round robin assignment trigger
    for(Case c : gCaseProcessList){
        logIt('&**Starting case for '+c.primary_category__c + ' case #' +  (c.casenumber == null ? 'new' : c.casenumber) + ' for caller ' + c.caller_name__c);
        String zip = gContactMap.get(c.contactId).OtherPostalCode;
        String lang = gContactMap.get(c.contactId).language__c;
        AssignedRep = null;
        system.debug('&**zip is ' + zip + ' language is ' + lang);
        
        if (!gRacMap.containsKey(zip)){
            system.debug('***No rep covers zip ' + zip);
        }
        else {          
            //first check if someone can take this on with the right language
            if (gRacMap.get(zip).containsKey(lang)){ 
                for (Rep_Assignment_Criteria__c rac : gRacMap.get(zip).get(lang)){ // walk rep zip & language criteria
                    logIt('&**Checking rep ' + rac.rep_assignment__r.Representative_Name__c);
                    if (matchCriteria(c, rac)) // returns true if this rep can accept case
                        AssignedRep = maybeReplaceRep(rac, AssignedRep); 
                }
            }
            else logIt('No rep with zip ' + zip + ' and ' + lang + ' language coverage');
            
            if (AssignedRep == null) // couldn't match, try again with all languages
            {
                for (String languageKeys : gRacMap.get(zip).keySet()){
                    for (Rep_Assignment_Criteria__c rac : gRacMap.get(zip).get(languageKeys)){
                        logIt('Non-language loop -- Checking rep ' + rac.rep_assignment__r.Representative_Name__c);
                        if (matchCriteria(c, rac)) // returns true if this rep can accept case
                            AssignedRep = maybeReplaceRep(rac, AssignedRep);
                    }
                }
            }
        }
        
        // done walking thru appropriate rep criteria so now update case
        if (AssignedRep != null)
        {
            //increment the Count
            if (!gRaCount.containsKey(AssignedRep.id))
                gRaCount.put(AssignedRep.Id, 1);
            else gRaCount.put(AssignedRep.Id, gRaCount.get(AssignedRep.Id) + 1);
            gUpdatedReps.put(AssignedRep.id, true);
            c.Rep_Assignment__c = AssignedRep.id;
            AssignedRep.Last_Time_Assigned__c = system.now();
            c.Date_Assigned__c = system.today();
            c.status = 'In Process';
            c.Distributed__c = getDistribute(c);
            logIt('**Final assign to ' + AssignedRep.Representative_name__c);
        }
        else {
            logIt('***No rep to take this case, assigning to default');
            String Region;
            if (gContactMap.containsKey(c.contactid)) Region = gContactMap.get(c.contactId).Region__c;
            
            if (gDefaultRepMap.containsKey(Region))
                c.Rep_Assignment__c = gDefaultRepMap.get(Region).Rep_ID__c;
            else if (gDefaultRepMap.containsKey('Default'))
                c.Rep_Assignment__c = gDefaultRepMap.get('Default').Rep_ID__c;
            c.status = 'Unassigned';
            
        }
        //system.debug(gLog);
                  
    } // end of case loop
}

//**********************************************************************************************************************************
// Determine distribute setting based on org id and 

private Boolean getDistribute(Case c)
{
    if (Util.getOrgName() == 'PROD' && c.primary_category__c.startsWith('MLTC')) return true;
    else return false;
}
//**********************************************************************************************************************************
// Update rep assignments with last case routed date and time
// for sorting in this routine to choose those who haven't had cases for awhile to get them first 
private void updateRepAssign()
{
    List<Rep_Assignment__c> updateReps = new List<Rep_Assignment__c>();
    // find rep assignments that were changed (stored as true in gUpdatedReps map)
    // build list of rep assignments to update
    try {
        rep_assignment__c ra;
        for (id repid : gUpdatedReps.keySet())
        {
            if (gUpdatedReps.get(repid) && (gRepAssignments.containsKey(repid) || gCoveringRepMap.containsKey(repid)))
            {
                if (gRepAssignments.containsKey(repid))
                    ra = gRepAssignments.get(repid);
                else ra = gCoveringRepMap.get(repid);
                updateReps.add(ra);
            }
        }   
        update updateReps;
        system.debug('&**Updated ' + updateReps.size() + 'rep_assignemnt__c for last time assigned');
    } catch (Exception e) { system.debug(e); }
    
}

//**********************************************************************************************************************************
// Determine if incoming rep should become the owner of this case
private Rep_Assignment__c maybeReplaceRep (Rep_Assignment_Criteria__c rac, Rep_Assignment__c AssignedRep)
{
    //if no assignment yet, assign it
    logIt(rac.rep_assignment__r.Representative_Name__c + '[' + rac.rep_assignment__c + '] could accept case, prior count is ' + gRaCount.get(rac.rep_Assignment__r.Id));
    rep_assignment__c repin = findReplacementRep(gRepAssignments.get(rac.rep_assignment__c)); // resolve to actual rep given vacations
    if (AssignedRep == null){ // no assignment yet, so use this match
        AssignedRep = repin;
        logIt('Initial assignment set to ' + repin.representative_name__c);
    }   
        
    else { //assignment is already set, compare the set assignment to this one to see who has less Cases already
    
        Decimal currentTotal = getCountTotal(AssignedRep, gRaCount); // to reduce and share code
        Decimal compareTotal = getCountTotal(repin, gRaCount);

        logit('Assigned ' + assignedRep.representative_name__c + ' adjusted count is ' + currentTotal + ', ' + repin.representative_name__c + ' rep in is ' + compareTotal);
        //2014-03-11 add in logic to accommodate NYC reps getting 10 before going to round robin
        if (repin.telephone_priority_route__c && compareTotal < TELEMAX)
        {
            IF (AssignedRep.telephone_priority_route__c) // already assigned to tele rep
            {
                if (compareTotal < currentTotal)
                {
                    AssignedRep = repin;
                    logIt('Changing tele rep to ' + AssignedRep.representative_name__c + ' since case total = ' + compareTotal + 
                            ' and previous tele rep total is less at ' + currentTotal);
                }
                // else leave as assigned
            }
            else {
                AssignedRep = repin;
                logit ('Setting assignment to ' + AssignedRep.representative_name__c + ' since a tele rep with count of ' + compareTotal);
            }
        }
        else if (AssignedRep.telephone_priority_route__c && compareTotal < TELEMAX) // do nothing
        { }
        else if (compareTotal < currentTotal)
        {
            AssignedRep = repin;
            logIt('Setting assignment to ' + AssignedRep.representative_name__c + ' after compare of ' +
                                                            compareTotal + ' to ' + currentTotal);
        }
        else if (compareTotal == currentTotal)
        {
            logIt('Totals same, this last time assigned is ' + repin.last_time_assigned__c + ' and current assigned is ' + AssignedRep.last_time_assigned__c);
            if (repin.last_time_assigned__c != AssignedRep.last_time_assigned__c &&
                    AssignedRep.last_time_assigned__c != null &&
                    repin.last_time_assigned__c < AssignedRep.last_time_assigned__c)
            {
                logIt('Changing assignment to ' + repin.representative_name__c + '. Counts equal but time is less.');
                AssignedRep = repin;
            }
        }
    }
    return AssignedRep;
}

// called by other routers as well
public static decimal getCountTotal(rep_assignment__c Rep, Map<Id, Integer> raCount)
{
        Decimal Total = raCount.get(Rep.Id);
        Decimal Alloc = 1;
        
        if (Rep.Allocation_percentage__c == null) Rep.Allocation_percentage__c = 100;
        else if (Rep.Allocation_percentage__c <= 0) return 999999999;
        else if (Rep.Allocation_percentage__c != 100) { 
            if (Rep.Allocation_percentage__c < 100 && Total > 0) Total += .5; // to make it slightly higher for better robins
            Alloc = Rep.Allocation_percentage__c / 100;
            system.debug('Rep ' + Rep.representative_name__c + ' has allocation percent of ' + Rep.Allocation_percentage__c);
        }

        if (Total > 0)
            Total = Total / Alloc;
        return Total;
}
//**********************************************************************************************************************************
// debug or logging routine
private void logIt(String s)
{
    system.debug(s);
    gLog += s + '\n';
}
//**********************************************************************************************************************************
    // traverse Out Of Office covering rep chain and quit on circular loop 
    private rep_assignment__c findReplacementRep(Rep_Assignment__c ra)
    {
        if (!ra.vacation__c) return ra; // quick checkout if initial rep is not out
        
        Set<Id> covSet = new Set<Id>();
        covSet.add(ra.id);
        Integer i = 0;
        while (ra.vacation__c && ra.Rep_Assignment_lu__c != null)
        {
            ra = gCoveringRepMap.get(ra.Rep_Assignment_lu__c);
            if (covSet.contains(ra.id)) { 
                    system.debug('&**Circular reference in OOO chain');
                    break; 
            }
            covSet.add(ra.Id);
            i++;
            logIt('  OOO so on to -->' + ra.representative_name__c); 
        }
        if(!graCount.containsKey(ra.id))
                graCount.put(ra.id, 0);
        return ra;
    }

    //**********************************************************************************************************************************
    // Criteria match for products the rep handles
    private Boolean matchCriteria(Case c, Rep_Assignment_Criteria__c rac)
    {
        logit('MatchCriteria: case category is ' + c.primary_category__c + ' and rep criteria has ' +
            (rac.Rep_Assignment__r.MLTC__C  ? 'MLTC ' : '') +
            (rac.Rep_Assignment__r.chp__C   ? 'CHP ' : '') +
            (rac.Rep_Assignment__r.FIDA__C  ? 'FIDA ' : '') +
            //(rac.Rep_Assignment__r.FHP__C   ? 'FHP ' : '') +
            (rac.Rep_Assignment__r.HBX__C   ? 'HBX ' : '') +
            (rac.Rep_Assignment__r.EP__C    ? 'EP ' : '') +
            (rac.Rep_Assignment__r.dual__C    ? 'DUAL ' : '') +
            (rac.Rep_Assignment__r.medicare__C  ? 'Medicare ' : '') +
            (rac.Rep_Assignment__r.medicaid__C  ? 'Medicaid ' : ''));
            
        if ((c.Primary_Category__c == 'MLTC' || c.Primary_Category__c == 'MLTC Packet') && rac.Rep_Assignment__r.MLTC__C)
            return true;
        if (c.Primary_Category__c == 'CHP' && rac.Rep_Assignment__r.CHP__C)
            return true;
        if (c.Primary_Category__c == 'FIDA' && rac.Rep_Assignment__r.FIDA__C)
            return true;
        if (c.Primary_Category__c == 'FHP' && rac.Rep_Assignment__r.FHP__C)
            return true;
        if (c.Primary_Category__c == 'HBX' && rac.Rep_Assignment__r.HBX__C)
            return true;
        //if (c.Primary_Category__c == 'EP' && rac.Rep_Assignment__r.EP__C)
        //    return true;
        if (c.Primary_Category__c == 'Dual' && rac.Rep_Assignment__r.Dual__C)
            return true;
        if (c.Primary_Category__c == 'Medicare' && rac.Rep_Assignment__r.Medicare__C)
            return true;
        if (c.Primary_Category__c == 'NYM' && rac.Rep_Assignment__r.Medicaid__C)
            return true;
                
        return false;
        }

} // end class