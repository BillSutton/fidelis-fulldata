@isTest
public class GoogleGeoLocationParserTest{

    static testMethod void testParse() {
        String json='{"results":[{"address_components":[{"long_name":"1600","short_name":"1600","types":["street_number"]},{"long_name":"AmphitheatrePkwy","short_name":"AmphitheatrePkwy","types":["route"]},{"long_name":"MountainView","short_name":"MountainView","types":["locality","political"]},{"long_name":"SantaClaraCounty","short_name":"SantaClaraCounty","types":["administrative_area_level_2","political"]},{"long_name":"California","short_name":"CA","types":["administrative_area_level_1","political"]},{"long_name":"UnitedStates","short_name":"US","types":["country","political"]},{"long_name":"94043","short_name":"94043","types":["postal_code"]}],"formatted_address":"1600AmphitheatreParkway,MountainView,CA94043,USA","geometry":{"location":{"lat":37.4224764,"lng":-122.0842499},"location_type":"ROOFTOP","viewport":{"northeast":{"lat":37.4238253802915,"lng":-122.0829009197085},"southwest":{"lat":37.4211274197085,"lng":-122.0855988802915}}},"place_id":"ChIJ2eUgeAK6j4ARbn5u_wAGqWA","types":["street_address"]}],"status":"OK"}';
        GoogleGeoLocationParser obj = GoogleGeoLocationParser.parse(json);
        System.assert(obj != null);
    }
    
}