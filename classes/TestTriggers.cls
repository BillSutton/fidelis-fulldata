/*
@Name            : TestTriggers
@Author          : sva
@Date            : January 2015
@Description     : Test Scheduling, CCA triggers
*/

@isTest(SeeAllData=true)
public class TestTriggers{

static testmethod void TestEr()
{

// Test Rep Assignments
    List<Contact> conList = [select id, name, firstname, lastname, cin__c from contact where otherpostalcode = '10001' order by cin__c desc LIMIT 1];
    user u = [select id, name from user where isactive=true and userrole.name = 'Intake Nurse NY City' LIMIT 1];
    if (conList[0].cin__c == null) {
        conList[0].cin__c = '123456';
        update conList[0];
    }
     Case c = new Case(primary_category__c = 'FIDA', Status = 'Assigned', ContactId = conList[0].Id, 
           ownerid = u.Id, date_received__c = date.today(), Source_Type__c = 'Ad Event');
    
    insert c;   // test basic scheduler
    // exercise to cca trigger
    c.Status = 'Closed';
    c.Status_Reason__c = 'Not Taken Under Care';
    c.refusal_reason__c = 'Whatever';
    update c;
 
    event a = new event(whatid=c.Id, type = 'Assessment', ActivityDateTime = dateTime.now(), subject = 'test', durationinminutes=5);
    insert a;
    
    Time_Task_Calculator__c ttc = [select id, assessment_name__c from Time_Task_Calculator__c LIMIT 1];
    system.debug('ttc assname = ' + ttc.assessment_name__c);
    Assessment__c ass = [select id, case__c from assessment__c where id = :ttc.assessment_name__c];
    ass.case__c = c.Id;
    ass.servicing_agency__c = 'xyz'; // needed since this was a new and required field

    update ass;
    update ttc;
    
    // contact trigger
    contact con = conList[0].clone();
    insert con;
    
    Transient_Reassess_From_CCA__c trc = new Transient_Reassess_From_CCA__c();
    trc.CIN__c = '123456';
    trc.needed_by__c = 'Whenever';
    trc.no_later_than__c = date.today();
    trc.trigger_date__c = date.today();
    trc.reason__c = 'Gotta have it now';
    //trc.Subscriber_ID__c 
    insert trc;
    
    /* mixed dml issues
    // rep assignment/user trigger
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    system.runAs (thisUser) {
        List<Rep_Assignment__c> ras = [select id, marketing_rep_record__c, Rep_Assignment_lu__c, vacation__c from rep_assignment__c 
            where inactive__c = false and marketing_rep_record__c != null and marketing_rep_record__r.IsActive = true LIMIT 2];
        u = [select id, IsActive from user where id = :ras[0].marketing_rep_record__c];
    
        ras[0].vacation__c = false;
        ras[1].vacation__c = true;
        ras[1].Rep_Assignment_lu__c = ras[0].Id;
        u.IsActive = false;
    }
    update u;
    */
    

 }   

}