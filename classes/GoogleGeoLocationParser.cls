public class GoogleGeoLocationParser{
    
    public cls_results[] results;
    public String status;   //OK
    
    public class cls_results {
        public cls_address_components[] address_components;
        public String formatted_address;    //1600 Amphitheatre Parkway, Mountain View, CA 94043, USA
        public cls_geometry geometry;
        public String place_id; //ChIJ2eUgeAK6j4ARbn5u_wAGqWA
        public String[] types;
    }
    
    public class cls_address_components {
        public String long_name;    //1600
        public String short_name;   //1600
        public String[] types;
    }
    
    public class cls_types {
        public String string0;    //s
        public String string1;    //t
        public String string2;    //r
        public String string3;    //e
        public String string4;    //e
        public String string5;    //t
        public String string6;    //_
        public String string7;    //a
        public String string8;    //d
        public String string9;    //d
        public String string10;   //r
        public String string11;   //e
        public String string12;   //s
        public String string13;   //s
    }
    
    public class cls_geometry {
        public cls_location location;
        public String location_type;    //ROOFTOP
        public cls_viewport viewport;
    }
    
    public class cls_location {
        public Double lat;  //37.4224764
        public Double lng;  //-122.0842499
    }
    
    public class cls_viewport {
        public cls_northeast northeast;
        public cls_southwest southwest;
    }
    
    public class cls_northeast {
        public Double lat;  //37.4238253802915
        public Double lng;  //-122.0829009197085
    }
    
    public class cls_southwest {
        public Double lat;  //37.4211274197085
        public Double lng;  //-122.0855988802915
    }
    
    public static GoogleGeoLocationParser parse(String json){
        return (GoogleGeoLocationParser) System.JSON.deserialize(json, GoogleGeoLocationParser.class);
    }
}