/* **********************************************************************************
Name            : TestCaseRetentionRouter
Author          : Scott Van Atta
Date            : May 17, 2016
Description     : Test Class for CaseRetentionRouter and case trigger

Revision History:-
Version  Date        Author  Description of Action
1.0      May 2016        SVA     Created

*************************************************************************************/

@isTest 
public class TestCaseRetentionRouter{
static string REPRECTYPE = [select id from recordtype where sObjectType = 'Rep_Assignment__c' and name = 'Retention'].Id;
private static Id RENEWALS = [select id from recordtype where sObjectType = 'Case' and developername = :TriggerHelper.RENEWALS].Id;

    static testMethod void repAssignmentFunctionalityTest(){
        
        List<User> userList = getUserList('t1');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        contact con = cons[0];
        createDebug();
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact', primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller1', date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        system.debug('&***TEST: Loading three cases');
        insert cases;
        system.debug('&***TEST: Done loading 3 cases, now assert and add 1 case');
        
        // each rep shoud have gotten one assignment
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[0].Id]);
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[2].Id]);
        
        raLst[1].last_time_assigned__c = null;
        update raLst[1];

        case c4 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller4', date_received__c=date.parse('02/04/2016'));
        
        insert c4; // should be assigned to rep2, so his count now 2
        System.assertEquals(2, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);
        
        Test.stopTest();
         
    }
 
 // OOO check
 // rep 1 doesn't match but rep3 OOO's to rep 1 so should assign
    static testMethod void repAssignmentFunctionalityTest2(){
        
        List<User> userList = getUserList('t2');
        List<Rep_Assignment__c> raLst = getRAListWithOOO(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACListWithOOO(raLst);
        List<Contact> cons = getContact(raLst);
        contact con = cons[0];
        createDebug();
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        insert cases;
        
        // rep2 shoud have gotten all assignments
        System.assertEquals(3, [select count() from case where Rep_Assignment__c= :raLst[0].Id]);
        Test.stopTest();
         
    }

 // OOO check
 // rep 1 doesn't match but rep3 OOO's to rep 1 so should assign BUT have circular loop
    static testMethod void repAssignmentFunctionalityTest3(){
        
        List<User> userList = getUserList('t3');
        List<Rep_Assignment__c> raLst = getRAListWithOOOCirc(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACListWithOOO(raLst);
        List<Contact> cons = getContact(raLst);
        createDebug();
        
        contact con = cons[0];
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        insert c1;
        // It should initially set assignment to ra2 because it has the lesser last-assigned date
        // So nitial assignment stands because a circular loop
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);

        Test.stopTest();
         
    }
 
  // test zip pass where unknown language is assigned to rep with zip
    static testMethod void repAssignmentFunctionalityTest4(){
        
        List<User> userList = getUserList('t4');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        createDebug();
        
        contact con = cons[3];
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        insert c1;
        // Make sure it assigned to one of them
        System.assertEquals(1, [select count() from case where Rep_Assignment__c != null]);

        Test.stopTest();
         
    }

 // test unassigned -- since no custom setting, none will be assigned
 // need to ensure no matching zip or language
    static testMethod void repAssignmentFunctionalityTest4A(){
        
        List<User> userList = getUserList('t4');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        contact con = new Contact(LastName='testName',otherpostalcode='ABCEF',Language__c='English',Region__c = 'Zork', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
        createDebug();
        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        insert c1;
        // Make sure it assigned to one of them
        System.assertEquals(0, [select count() from case where Rep_Assignment__c != null]);

        Test.stopTest();
         
    }
// Exercise Unassigned clause and miscellaneous coverage
    static testMethod void repAssignmentFunctionalityTest5(){
        
        List<User> userList = getUserList('t42');
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        createDebug();
        
        contact con = cons[0];
        
        Test.startTest();
        

        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        cases.add(c1);
        insert c1;
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'NYM', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c2;
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'Dual', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c3;
        case c4 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'EP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c4;
        case c5 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'HBX', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c5;
        c5 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'FIDA', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c5;
        case c6 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c6;
        case c7 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'MLTC', recordtypeid = RENEWALS, contactid=con.id,  date_received__c=date.parse('02/01/2016'));
        insert c7;
        List<Case> cs = [select id, rep_assignment__c, date_assigned__c from case where rep_assignment__c != null];
        system.debug('***TestCaseRouter: Found ' + cs.size() + ' cases with rep assignments below');
        for (case cc : cs)
            system.debug(cc.rep_assignment__c + ' with case date assigned=' + cc.date_assigned__c);
        system.debug('**Rep3 has ' + [select count() from case where rep_assignment__c = :raLst[2].Id] + ' cases assigned!');

        Test.stopTest();
         
    }
 
    // test facility
    static testMethod void repAssignmentFacilityTest6(){
        
        List<User> userList = getUserList('t6');
        List<Rep_Assignment__c> raLst = getRAListFac(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContactFac(raLst);
        contact con = cons[0];
        createDebug();

        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact', primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller1', date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',/*provider_group_id__c='Mcd0001',*/primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        system.debug('&***TEST: Loading three cases');
        insert cases;
        
        // each rep shoud have gotten one assignment
        System.assertEquals(3, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);
        
       
        Test.stopTest();
         
    }
    
        // test language
    static testMethod void repAssignmentLangTest7(){
        
        List<User> userList = getUserList('t6');
        List<Rep_Assignment__c> raLst = getRAList(userlist); 
        List<Rep_Assignment_Criteria__c> racLst = getRACListWithLanguage(raLst); // repassign 1=spanish for 67890, 2 = spanglish (cons3)
        List<Contact> cons = getContactLang(raLst);  // 2=spanish for 11111, 3=spanglish for 12345
        contact con = cons[3];
        createDebug();

        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact', primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller1', date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        system.debug('&***TEST: Loading three cases');
        insert cases;
        system.debug('&***TEST: Done loading 3 cases, now assert and add 1 case');
        
        // Spanglish rep for 12345 should have gotten this
        System.assertEquals(3, [select count() from case where Rep_Assignment__c= :raLst[2].Id]); // all cases should goto rep5 for zip/language match
        
        con = cons[2]; // spanish for 11111, no direct zip-lang hit
        case c4 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        insert c4;
        // spanish for any zip should have gotten
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[1].Id]); // all cases should goto rep5 for zip/language match

        Test.stopTest();
         
    }
    
    // test ignore case count thingie
    static testMethod void repAssignmentCaseUpdateTest8(){
        
        List<User> userList = getUserList('t6');
        List<Rep_Assignment__c> raLst = getRAList(userlist); 
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst); 
        List<Contact> cons = getContact(raLst);  
        contact con = cons[0];
        createDebug();

        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact', primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller1', date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        system.debug('&***TEST: Loading three cases');
        insert cases;
        system.debug('&***TEST: Done loading 3 cases, now assert and add 1 case');
        
        // each 12345 English rep shuld have one case
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[0].Id]); // round robin to reps 0, 1 and 2
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[1].Id]); 
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[2].Id]); 

        c3 = [select id, status, rep_assignment__c from case where id = :c1.Id];
        Integer ix;
        if (c3.rep_assignment__c == raLst[0].Id) ix = 0;
        if (c3.rep_assignment__c == raLst[1].Id) ix = 1;
        if (c3.rep_assignment__c == raLst[2].Id) ix = 2;
        c3.status = 'Assigned'; // reset it
        update c3;
        
        System.assertEquals(1, [select count() from case where Rep_Assignment__c= :raLst[ix].Id]); // case should be reset back to original rep based on counts

        Test.stopTest();
         
    }

    // Test allocationns
    static testMethod void repAssignmentFunctionalityTest9(){
        
        List<User> userList = getUserList('t1');
        List<Rep_Assignment__c> raLst = getRAListAlloc(userlist);
        List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
        List<Contact> cons = getContact(raLst);
        contact con = cons[0];
        createDebug();

        
        Test.startTest();
        
        List<Case> cases = new list<Case>();
        case c1 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact', primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller1', date_received__c=date.parse('02/01/2016'));
        case c2 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c3 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c4 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c5 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c6 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c7 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller2', date_received__c=date.parse('02/02/2016'));
        case c8 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c9 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));
        case c10 = new case(Status='Assigned', status_reason__c = 'Awaiting First Contact',primary_category__c = 'CHP', recordtypeid = RENEWALS, contactid=con.id,  caller_name__c = 'Caller3', date_received__c=date.parse('02/03/2016'));

        cases.add(c1);
        cases.add(c2);
        cases.add(c3);
        cases.add(c4);
        cases.add(c5);
        cases.add(c6);
        cases.add(c7);
        cases.add(c8);
        cases.add(c9);
        cases.add(c10);
        

        system.debug('&***TEST: Loading ten cases for alloc test');
        insert cases;
        system.debug('&***TEST: Done loading 10 cases, now assert to ensure rep2 has only 2 case and the others 4');
        
        // each rep shoud have gotten one assignment
        System.assertEquals(4, [select count() from case where Rep_Assignment__c= :raLst[0].Id]);
        System.assertEquals(2, [select count() from case where Rep_Assignment__c= :raLst[1].Id]);
        System.assertEquals(4, [select count() from case where Rep_Assignment__c= :raLst[2].Id]);
        
        Test.stopTest();
         
    }

    private static void createDebug()
    {
        Debug__c gDebug = new debug__c(Name='CaseRetentionRouter',Case_Previously_Assigned__c=true, Could_Accepts__c=true, Criteria_Test__c =true,Facility_Listing__c = true,
                                        Facility_Match__c = true, Facility_No_Match__c = true, Last_Time_Assigned__c=true, Match_Count_Detail__c = true,
                                        Non_English_Detail__c=true, Process_Owner_Detail__c=true,Rep_Assigned_Final_Listing__c=true,Rep_Assigned_RT__c=true,
                                        Rep_Assigned_Totals__c=true, Set_Assigned__c=true, Vacation_Delegates__c=true,ZipAdd__c=true,Zip_Plus_Language__c=true);
        upsert gDebug;
    }
    
    // ******************************************************************Contacts***********************************************************************************************
    private static List<Contact> getContact(List<Rep_Assignment__c> ra){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<Contact> lList = new List<Contact>();

        System.runAs ( thisUser ) {
            Contact l1 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l2 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l3 = new Contact(LastName='testName',otherpostalcode='11111',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l4 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='Spanglish',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            lList.add(l1);
            lList.add(l2);
            lList.add(l3);
            lList.add(l4);
            insert lList;
        }
        return lList;
    }
     private static List<Contact> getContactFac(List<Rep_Assignment__c> ra){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<Contact> lList = new List<Contact>();

        System.runAs ( thisUser ) {
            Contact l1 = new Contact(LastName='testName',otherpostalcode='12345',provider_group_id__c='Mcd0001', Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l2 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l3 = new Contact(LastName='testName',otherpostalcode='11111',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l4 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='Spanglish',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            lList.add(l1);
            lList.add(l2);
            lList.add(l3);
            lList.add(l4);
            insert lList;
        }
        return lList;
    }
  
 
    private static List<Contact> getContactLang(List<Rep_Assignment__c> ra){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<Contact> lList = new List<Contact>();

        System.runAs ( thisUser ) {
            Contact l1 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l2 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l3 = new Contact(LastName='testName',otherpostalcode='11111',Language__c='Spanish',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            Contact l4 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='Spanglish',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
            lList.add(l1);
            lList.add(l2);
            lList.add(l3);
            lList.add(l4);
            insert lList;
        }
        return lList;
    }
  
    // ************************************************************User**********************************************************************************************************
    public static List<User> getUserList(string rndm)
    {
        list<User> userList = new List<User>();

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {

            user u1 = new user(firstname='Huck', lastname = 'urep1', alias='rep1',email='rep1@test.com',username='rep1test'+rndm+'@test.com', communitynickname='rep1',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u1);
            
            user u2 = new user(firstname='Huck', lastname = 'urep2', alias='rep2',email='rep2@test.com',username='rep2test'+rndm+'@test.com', communitynickname='rep2',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u2);
            
            user u3 = new user(firstname='Huck', lastname = 'urep3', alias='rep3',email='rep3@test.com',username='rep3test'+rndm+'@test.com', communitynickname='rep3',emailencodingkey='ISO-8859-1',
                                        timezonesidkey='America/New_York', localesidkey='en_US', profileid=UserInfo.getprofileId(), languagelocalekey='en_US');
            userList.add(u3);
            insert userList;
        }
        return userList;
    }

    // ******************************************************Rep Assigns***********************************************************************************************
    private static List<Rep_Assignment__c> getRAList(list<User> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        user thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',recordtypeid=REPRECTYPE, CHP__c = true, last_time_assigned__c=null, Rep_Phone__c= '1', renewals__c=true, 
                                                            MSA__c = UserInfo.getUserId(),region__c='Buffalo', Vacation__c = false, Rep_Assignment_lu__c = null, marketing_rep_record__c = userlist[0].id);
            
            Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',recordtypeid=REPRECTYPE, CHP__c = true, last_time_assigned__c=null, Vacation__c = false, Rep_Assignment_lu__c = null,renewals__c=true, 
                                                            marketing_rep_record__c = userlist[1].id, region__c='Buffalo',Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId());
            raLst.add(ra1);
            raLst.add(ra2);
            Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep3',recordtypeid=REPRECTYPE, last_time_assigned__c=null, CHP__C = true,HBX__C = true,renewals__c=true,
                                                            Medicaid__C = true,Medicare__C = true, Vacation__c = false, Rep_Assignment_lu__c = null, Rep_Phone__c= '1', 
                                                            ep__c = true, fida__c=true, mltc__c=true,marketing_rep_record__c = userlist[2].id, region__c='Buffalo', MSA__c = UserInfo.getUserId());

            raLst.add(ra3);
            insert raLst;
        }
        return raLst;
    }
     private static List<Rep_Assignment__c> getRAListFac(list<User> userlist){
        List<Rep_Assignment__c> raLst = getRAList(userlist);
        user thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Rep_Assigned_Facilities__c fac1 = new Rep_Assigned_Facilities__c();
            fac1.rep_assignment__c = raLst[1].Id;
            fac1.provider_group_id__c = 'Mcd0001';
            fac1.name = 'Zork';
            insert fac1;
            
        }
        return raLst;
    }
 

     private static List<Rep_Assignment__c> getRAListAlloc(list<User> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        user thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',recordtypeid=REPRECTYPE, CHP__c = true, last_time_assigned__c=null, Rep_Phone__c= '1',  renewals__c=true,
                                                            MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null, marketing_rep_record__c = userlist[0].id);
            
            Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',recordtypeid=REPRECTYPE, CHP__c = true, last_time_assigned__c=null, Vacation__c = false, Rep_Assignment_lu__c = null, renewals__c=true,
                                                            marketing_rep_record__c = userlist[1].id, Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId(), allocation_percentage__c = 50);
            raLst.add(ra1);
            raLst.add(ra2);
            Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep3',recordtypeid=REPRECTYPE, last_time_assigned__c=null, CHP__C = true,HBX__C = true,renewals__c=true,
                                                            Medicaid__C = true,Medicare__C = true, Vacation__c = false, Rep_Assignment_lu__c = null, Rep_Phone__c= '1', 
                                                            ep__c = true, fida__c=true, mltc__c=true,marketing_rep_record__c = userlist[2].id, MSA__c = UserInfo.getUserId());

            raLst.add(ra3);
            insert raLst;
        }
        return raLst;
    }
 

    private static List<Rep_Assignment__c> getRAListWithOOO(list<user> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',recordtypeid=REPRECTYPE, CHP__c = true, last_time_assigned__c=null, Rep_Phone__c= '1', renewals__c=true, 
                                                        marketing_rep_record__c = userlist[0].id,MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null);
        
        Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',recordtypeid=REPRECTYPE, CHP__c = true, last_time_assigned__c=null, Vacation__c = false, Rep_Assignment_lu__c = null, renewals__c=true,
                                                        marketing_rep_record__c = userlist[1].id,Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId());
        raLst.add(ra1);
        raLst.add(ra2);
        insert raLst;
        Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep3',recordtypeid=REPRECTYPE, last_time_assigned__c=null, CHP__C = true,HBX__C = true,renewals__c=true,
                                                        Medicaid__C = true,Medicare__C = true, Vacation__c = true, Rep_Assignment_lu__c = ra1.id, Rep_Phone__c= '1', 
                                                        marketing_rep_record__c = userlist[2].id, MSA__c = UserInfo.getUserId());
        insert ra3;
        raLst.add(ra3);
        system.debug('***Adding rep assign for rectype ' + REPRECTYPE);
        return raLst;
    }
   
   // *********************************************************************RACS*****************************************************************************
     private static List<Rep_Assignment_Criteria__c> getRACListWithLanguage(List<Rep_Assignment__c> ra){
        List<Rep_Assignment_Criteria__c> racLst = new List<Rep_Assignment_Criteria__c>();
        Rep_Assignment_Criteria__c rac1 = new Rep_Assignment_Criteria__c(Zip_Code__c='99999',Language__c='English',Rep_Assignment__c=ra[0].Id);    
        Rep_Assignment_Criteria__c rac2 = new Rep_Assignment_Criteria__c(Zip_Code__c='67890',Language__c='Spanish',Rep_Assignment__c=ra[1].Id);    
        Rep_Assignment_Criteria__c rac3 = new Rep_Assignment_Criteria__c(Zip_Code__c='45678',Language__c='English',Rep_Assignment__c=ra[1].Id);
        Rep_Assignment_Criteria__c rac4 = new Rep_Assignment_Criteria__c(Zip_Code__c='88888',Language__c='English',Rep_Assignment__c=ra[1].Id);
        Rep_Assignment_Criteria__c rac5 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='Spanglish',Rep_Assignment__c=ra[2].Id);      
        racLst.add(rac1);
        racLst.add(rac2);
        racLst.add(rac3);
        racLst.add(rac4);
        racLst.add(rac5);
        insert racLst;
        return racLst;     
    }
   
     private static List<Rep_Assignment_Criteria__c> getRACListWithOOO(List<Rep_Assignment__c> ra){
        List<Rep_Assignment_Criteria__c> racLst = new List<Rep_Assignment_Criteria__c>();
        Rep_Assignment_Criteria__c rac1 = new Rep_Assignment_Criteria__c(Zip_Code__c='99999',Language__c='English',Rep_Assignment__c=ra[0].Id);    
        Rep_Assignment_Criteria__c rac2 = new Rep_Assignment_Criteria__c(Zip_Code__c='67890',Language__c='Spanish',Rep_Assignment__c=ra[1].Id);    
        Rep_Assignment_Criteria__c rac3 = new Rep_Assignment_Criteria__c(Zip_Code__c='45678',Language__c='English',Rep_Assignment__c=ra[1].Id);
        Rep_Assignment_Criteria__c rac4 = new Rep_Assignment_Criteria__c(Zip_Code__c='88888',Language__c='English',Rep_Assignment__c=ra[1].Id);
        Rep_Assignment_Criteria__c rac5 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[2].Id);      
        racLst.add(rac1);
        racLst.add(rac2);
        racLst.add(rac3);
        racLst.add(rac4);
        racLst.add(rac5);
        insert racLst;
        return racLst;     
    }
    private static List<Rep_Assignment__c> getRAListWithOOOCirc(List<user> userlist){
        List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
        Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',CHP__c = true, Rep_Phone__c= '1', recordtypeid = REPRECTYPE, renewals__c=true,
                                                        marketing_rep_record__c = userlist[0].id,MSA__c = UserInfo.getUserId(), Vacation__c = false, Rep_Assignment_lu__c = null,
                                                        Last_Time_Assigned__c = date.parse('12/01/2016'));
        insert ra1;
        Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',CHP__c = true, Vacation__c = true, Rep_Assignment_lu__c = ra1.Id, recordtypeid = REPRECTYPE, renewals__c=true,
                                                        marketing_rep_record__c = userlist[1].id,Rep_Phone__c= '1',  MSA__c = UserInfo.getUserId(),
                                                        Last_Time_Assigned__c = date.parse('11/01/2016'));
        insert ra2;
        ra1.vacation__c = true;
        ra1.rep_assignment_lu__c = ra2.id;
        update ra1;
        raLst.add(ra1);
        raLst.add(ra2);
        ralst.add(ra2);
        
        return raLst;
    }

    private static List<Rep_Assignment_Criteria__c> getRACList(List<Rep_Assignment__c> ra){
        List<Rep_Assignment_Criteria__c> racLst = new List<Rep_Assignment_Criteria__c>();
        user thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Rep_Assignment_Criteria__c rac1 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[0].Id);    
            Rep_Assignment_Criteria__c rac2 = new Rep_Assignment_Criteria__c(Zip_Code__c='67890',Language__c='Spanish',Rep_Assignment__c=ra[1].Id);    
            Rep_Assignment_Criteria__c rac3 = new Rep_Assignment_Criteria__c(Zip_Code__c='45678',Language__c='English',Rep_Assignment__c=ra[1].Id);
            Rep_Assignment_Criteria__c rac4 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[1].Id);
            Rep_Assignment_Criteria__c rac5 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[2].Id);      
            racLst.add(rac1);
            racLst.add(rac2);
            racLst.add(rac3);
            racLst.add(rac4);
            racLst.add(rac5);
            insert racLst;
        }
        return racLst;     
    }
          
    

  
}