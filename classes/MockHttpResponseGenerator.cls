@isTest global class MockHttpResponseGenerator implements HttpCalloutMock{
	 global HTTPResponse respond(HTTPRequest req) {
        //Send a mock response for a specific endpoint and get response
        System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());

        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"foo":"bar"}');
        res.setStatusCode(200);
        return res;
    }
}