// TestInlineContactFamilyListController
// @date: 9-19-2016
// @author: rgodard@fideliscare.org
// @description: Test code for a VF page controller. This controller, for a given Case, traverses to the related Contact's family in order to determine and list the family members of the Contact, 
// and mark who the Head of Household or Responsible person is for that contact.

@isTest
public class TestInlineContactFamilyListController {

    static testMethod void testMethodOne() {
        Contact con = new Contact(LastName='Test', FirstName = 'Contact', Line_of_Business__c = 'CHP', BirthDate = Date.Today(), Family_Link_Id__c = 'THX1138');
        insert con;
        
        Contact con2 = new Contact(LastName='Test', FirstName = 'Miller', Line_of_Business__c = 'CHP', BirthDate = Date.Today()+2, Family_Link_Id__c = 'THX1138');
        insert con2;
        
        Reponsible_Person__c resPer = new Reponsible_Person__c(First_Name__c = 'Test', Last_Name__c = 'Person', Resp_Person_Id__c = 'a');
        insert resPer;
        
        Responsible_Person_Relationship__c rpr = new Responsible_Person_Relationship__c(Member_Prospect__c = con.Id, Responsible_Person__c = resPer.Id, Resp_Person_Type__c = 'Resp');
        insert rpr;
        
        Case c = new Case(ContactId = con.Id);
        insert c;
        PageReference pageRef = Page.InlineCaseFamilyListPage;
        Test.setCurrentPage(new PageReference('/apex/InlineCaseFamilyListPage?id='+c.Id));
        
        InlineContactFamilyListController icfl = new InlineContactFamilyListController(new ApexPages.StandardController(c));
    }
    
    static testMethod void testMethodTwo() {
        Contact con = new Contact(LastName='Test', FirstName = 'Contact', Line_of_Business__c = 'CHP', BirthDate = Date.newInstance(1990, 1, 1), Family_Link_Id__c = 'THX1138');
        insert con;
        
        Contact con2 = new Contact(LastName='Test', FirstName = 'Miller', Line_of_Business__c = 'CHP', BirthDate = Date.Today(), Family_Link_Id__c = 'THX1138');
        insert con2;
        
        Reponsible_Person__c resPer = new Reponsible_Person__c(First_Name__c = 'Test', Last_Name__c = 'Person', Resp_Person_Id__c = 'a');
        insert resPer;
        
        Responsible_Person_Relationship__c rpr = new Responsible_Person_Relationship__c(Member_Prospect__c = con.Id, Responsible_Person__c = resPer.Id, Resp_Person_Type__c = 'Resp');
        insert rpr;
        
        PageReference pageRef = Page.InlineContactFamilyListPage;
        Test.setCurrentPage(new PageReference('/apex/InlineContactFamilyListPage?id='+con.Id));
        
        InlineContactFamilyListController icfl = new InlineContactFamilyListController(new ApexPages.StandardController(con));
    }

}