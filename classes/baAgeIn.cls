/******************************************************************************
** Module Name   : baAgeIn  
** Description   : Generate cases prior to member's 65th birthday
**                 This routine will examine the FHP and NYM contacts in the covered counties. If their birthdate falls within 65 years plus 5 months, 
**                 a case will be generated for them for follow-up.  This is expected to run on the first of each month as-is using database.executebatch(new baAgeIn()),
**                 although the results will be the same when run anytime within the month.  Note that cases will not be created if a similar case already exists and is not closed.
**                 E.g., the process is rerunnable with no damage.
**
**                 There are various parameters that can be optionally set:
**                  1) database.executebatch(new baAgeIn(bTest) - do not write cases
**                  2)  database.executebatch(new baAgeIn(birthmonth, birthyear, [counties], [test])
**                      Any contacts with their birthdate in this month will have cases created.
**                  3) database.executebatch(new baAgeIn(monthsAhead, [counties], [test])
**                      This will reset the monthsAhead parameter from 5 months ahead to the specified value for this run.
**          
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      3/10/2015    SVA     Created
** 1.1      1/07/2016    SVA     Added 10 counties; display county count in email; allow included county list
**
******************************************************************************/

global class baAgeIn implements Database.Batchable<sObject>, Database.Stateful {
    global Integer numRead=0;
    global Integer numCases=0;
    global Integer numNoCases = 0;
    global ID CASETYPE = [select id from recordtype where name = 'Medicare' and sobjectType = 'Case'].Id;
    global List<String> gCounties = new List<String>();
    global List<String> gNoDuals = new List<String>();
    global date gReportStart;
    global date gReportEnd;
    Global String SOURCETYPE = 'Age-In (Medicare)';
    Global Integer gMonthsAhead = 5;
    Global String xtra = '';
    Global Map<String, Integer> gCountyCount = new Map<String, Integer>();
    Global Boolean gbTest=false;
    Global List<String> gOnlyTheseCounties = null;


    
    // Normal: create cases for all listed counties 5 months ahead
    global baAgeIn()
    {
        setup();
    }
    
    // Create cases for only those counties submitted (standard 5 months ahead)
    global baAgeIn(List<String> counties)
    {
        gOnlyTheseCounties = counties;
        setup();
    }
    
    // Normal process but cases are not written if test is true
    global baAgeIn(Boolean test)
    {
        gbTest = test;
        setup();
    }
    
    // Operate with all defaults except use only submittted counties and do not write cases if test is true
    global baAgeIn(List<String> counties, Boolean test)
    {
        gOnlyTheseCounties = counties;
        gbTest = test;
        setup();
    }

    global void setup()
    {
        loadCounties();
        gReportStart = date.today().ToStartOfMonth();
        if (gMonthsAhead > 0)
            gReportStart = gReportStart.addMonths(gMonthsAhead);
        gReportStart = gReportStart.addYears(-65);
        gReportEnd = gReportStart.AddMonths(1);

    }
    
    // Set the starting month and year for calculations (versus using current date)
    global baAgeIn(Integer startmonth, Integer startyear)
    {
        setupWithDates(startmonth, startyear, null, false);
    }
 
    // Set the starting month and year for calculations (versus using current date) for only the submitted counties
    global baAgeIn(Integer startmonth, Integer startyear, List<String> counties)
    {
        setupWithDates(startmonth, startyear, counties, false);
    }
    
    // Set the starting month and year for calculations (versus using current date) and do not write if test is true
    global baAgeIn(Integer startmonth, Integer startyear, List<String> counties, Boolean test)
    {
        setupWithDates(startmonth, startyear, counties, test);
    }
    
    global void setupWithDates(Integer startmonth, Integer startyear, List<String> counties, Boolean test)
    {
        gOnlyTheseCounties = counties;
        gbTest = test;
        loadCounties();
        gReportStart = date.newInstance(startyear, startmonth, 1);
        gReportEnd = gReportStart.AddMonths(1);

    }
 
    // Set a different monthsahead parameter (versus 5 months ahead)
    global baAgeIn(Integer monthsAhead)
    {
        gMonthsAhead = monthsAhead;
        setup();
    }

    // Set a different monthsahead and process only the submitted counties
    global baAgeIn(Integer monthsAhead, List<String> counties)
    {
        gOnlyTheseCounties = counties;
        gMonthsAhead = monthsAhead;
        setup();
    }
    
    // Override monthsahead and use only submitted counties; do not write if Test is true
     global baAgeIn(Integer monthsAhead, List<String> counties, Boolean test)
    {
        gbTest = test;
        gOnlyTheseCounties = counties;
        gMonthsAhead = monthsAhead;
        setup();
    }
    
   
    global database.querylocator start(Database.BatchableContext BC){
            
        return Database.getQueryLocator(getQuery());
    }
        
    global String getQuery()
    {
        String s1='(';
        String cm = '';
        for (String s : gCounties)
        {
            s1 += cm + '\'' + s + '\'';
            cm = ',';
        }
        String s2='(';
        cm = '';
        for (String s : gNoDuals)
        {
            s2 += cm + '\'' + s + '\'';
            cm = ',';
        }
        
        String d1 = gReportStart.Year() + '-';
        if (gReportStart.Month() < 10) d1 += '0';
        d1 += gReportStart.Month() + '-01';
        String d2 = gReportEnd.Year() + '-';
        if (gReportEnd.Month() < 10) d2 += '0';
        d2 += gReportEnd.Month() + '-01';
        
        String query='select id, Subscriber_ID__c, CIN__c, firstname, lastname, otherstreet, othercity, otherstate, otherpostalcode, home_county__c, region__c, mi__c, ' +
                ' phone, language__c, birthdate, plan_id__c ' +
                    'from contact where enrolled__c = true and home_county__c in ' + s1 + ') and birthdate >= ' + d1 +
                    ' and birthdate < ' + d2 + ' and ' +
                    '(plan_id__c like \'FHP%\' or (plan_id__c like \'NYM%\' and (not plan_id__c like \'NYMSSI%\') and home_county__c not in ' + s2 + ')))';
                    
        System.debug(query);
        xtra += query + '\n';
        return query;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Id> contacts = new List<Id>();
        Map<Id, Id> hasCase = new Map<Id, Id>();
        List<Case> newCases = new List<Case>();
        
    // walk the records to pull relevant contacts for case check
        for(Sobject s : scope){
            numRead++;
            Contact c = (Contact) s;
            contacts.add(c.Id);
        }
        List<Case> cases = [select contactid from case where contactid in :contacts and source_type__c = :SOURCETYPE and isClosed = false];
        for (Case cs : cases)
            hasCase.put(cs.contactid, cs.Id);
            
        for(Sobject s : scope)
        {
            Contact c = (Contact) s;
            if (!hasCase.containsKey(c.Id))
            {
                case cc = new Case();
                cc.recordtypeid = CASETYPE;
                cc.contactid = c.Id;
                if (c.plan_id__c.startsWith('FHP'))
                    cc.primary_category__c = 'Medicare';
                else cc.primary_category__c = 'Dual';
                cc.status = 'Assigned';
                cc.status_reason__c = 'Awaiting First Contact';
                cc.status_code__c = 'AFC';
                cc.priority = 'Low';
                cc.subject = 'Current Member turning 65 (' + gMonthsAhead + ' month advanced outreach attempt)';
                cc.source_type__c = SOURCETYPE;
                cc.source__c = 'Member';
                cc.Date_Received__c = date.today();
                newCases.add(cc);
                if (gCountyCount.containsKey(c.home_county__c))
                    gCountyCount.put(c.home_county__c, gCountyCount.get(c.home_county__c)+1);
                else gCountyCount.put(c.home_county__c, 1);

            }
            else numNoCases++;
        }
        numCases += newCases.size();
        if (!gbTest) insert newCases;
    }
    
    
    global void finish(Database.BatchableContext BC){
        String email;
        String stat;
        Integer jobs;
        Integer errs;
        Integer TotalJobItems;
        Integer JbiProcessed;
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.email
                from AsyncApexJob where Id = :BC.getJobId()];
            email = a.CreatedBy.email; jobs = a.TotalJobItems; errs = a.NumberofErrors; stat = a.Status;
        sendEmail(jobs, errs, stat);
    }
    
    global void sendEmail(Integer jobs, Integer errs, String stat)
    { 
        List <String> sortedCounties = new list<String>();
        sortedCounties.AddAll(gCountyCount.keySet());
        sortedCounties.sort();
        
        String s = '\n';
        if (gOnlyTheseCounties != null && gOnlyTheseCounties.size() > 0)
            s += '***SPECIAL RUN OF SPECIFIC COUNTIES***\n';

        for (String county : sortedCounties)
        {
            Integer i = gCountyCount.get(county);
            s += '\n' + i + ' ' + county;
        }
            
        sendmsg('Batch Apex baAgeIn ' + (gbTest ? '**NO WRITE**' : '') + stat + ' ' + numRead,
                'Processed ' + jobs + ' batches with ' + errs + ' failures.\n\n' + 'Numread=' + numRead + '\nCases created: ' + numCases
                        + '\nCases already existed: ' + numNoCases 
                        + '\ngReportStart=' + gReportStart + '\ngReportEnd=' + gReportEnd + '\n\n' + xtra + '\n' + s, 'svanatta@fideliscare.org');
    }
    public static void sendMsg(String subj, String emsg, String to) 
    {
        String[] toAddresses = new String[] {to};
        Messaging.SingleEmailMessage mail = BuildMsg(subj, emsg, to);
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception e) { // fails when exceeds gov limit of 10
        } 
    }              

    public static Messaging.SingleEmailMessage buildMsg(String subj, String emsg, String to) {
        String[] toAddresses = new String[] {to};
        return buildMsg(subj, emsg, toAddresses);
    }
        

    public static Messaging.SingleEmailMessage buildMsg(String subj, String emsg, List<String> to) {
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        mail.setToAddresses(to); 
        mail.setSenderDisplayName(UserInfo.getName());
        mail.setSubject(subj);
        
        String s;
        s = emsg;
        mail.setPlainTextBody( s);
        return mail;
    }              

        global void loadCounties()
    {
        if (gOnlyTheseCounties != null && gOnlyTheseCounties.size() > 0)
        {
            gCounties.AddAll(gOnlyTheseCounties);
        }
        else {
            gCounties.add('Albany');
            gCounties.add('Allegany'); 
            gCounties.add('Bronx'); 
            gCounties.add('Broome'); 
            gCounties.add('Cattaraugus'); 
            gCounties.add('Chenango'); 
            gCounties.add('Columbia'); 
            gCounties.add('Cortland'); 
            gCounties.add('Delaware'); 
            gCounties.add('Dutchess'); 
            gCounties.add('Erie'); 
            gCounties.add('Essex'); 
            gCounties.add('Fulton'); 
            gCounties.add('Greene'); 
            gCounties.add('Hamilton');
            gCounties.add('Herkimer'); 
            gCounties.add('Kings'); 
            gCounties.add('Lewis');
            gCounties.add('Manhattan');         
            gCounties.add('Manhatten');  // getting them both ways
            gCounties.add('Montgomery'); 
            gCounties.add('Nassau'); 
            gCounties.add('New York'); 
            gCounties.add('Niagara'); 
            gCounties.add('Oneida'); 
            gCounties.add('Onondaga'); 
            gCounties.add('Orange'); 
            gCounties.add('Orleans'); 
            gCounties.add('Oswego'); 
            gCounties.add('Putnam'); 
            gCounties.add('Queens'); 
            gCounties.add('Rensselaer'); 
            gCounties.add('Richmond'); 
            gCounties.add('Rockland'); 
            gCounties.add('Saratoga'); 
            gCounties.add('Schenectady'); 
            gCounties.add('Seneca'); 
            gCounties.add('Suffolk'); 
            gCounties.add('Sullivan'); 
            gCounties.add('Ulster'); 
            gCounties.add('Warren'); 
            gCounties.add('Washington'); 
            gCounties.add('Westchester'); 
            gCounties.add('Wyoming'); 
            gCounties.add('Yates'); 
            // 1/1/2016
            gCounties.add('Clinton');
            gCounties.add('Franklin');
            gCounties.add('Schoharie');
            gCounties.add('Steuben');
            gCounties.add('Schuyler');
            gCounties.add('Chemung');
            gCounties.add('Cayuga');
            gCounties.add('St. Lawrence');
            gCounties.add('Tioga');
            gCounties.add('Otsego');
        }
        
        
        gNoDuals.add('Essex'); 
        gNoDuals.add('Fulton'); 
        gNoDuals.add('Lewis');
        gNoDuals.add('Orleans'); 
        gNoDuals.add('Saratoga'); 
        gNoDuals.add('Seneca'); 
        gNoDuals.add('Warren'); 
        gNoDuals.add('Washington'); 
        gNoDuals.add('Yates');      
        
    }
   static testMethod void myUnitTest() {
    baAgeIn ttc = new baAgeIn();
    String query = ttc.getQuery() + ' LIMIT 25'; 
    system.debug('***' + query);
    //List<Contact> qrRes = database.query(query); 
    List<Contact> qrRes = new List<Contact>();
    date bdate = date.today().AddMonths(5);
    bdate = bdate.addYears(-65);
    Contact c1 = new contact(firstname='zork1', lastname='zork2', plan_id__c='NYM12345', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY',phone='1234567890', birthdate=bdate);
    Contact c2 = new contact(firstname='zork3', lastname='zork4', plan_id__c='NYM12345', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY',phone='1234567890', birthdate=bdate);
    Contact c3 = new contact(firstname='zork5', lastname='zork6', plan_id__c='FHP12345', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY',phone='1234567890', birthdate=bdate);
    qrRes.add(c1); qrRes.add(c2); qrRes.add(c3);
    insert qrRes;
    Database.BatchableContext bc;
    ttc.execute(bc, qrRes);
    ttc.sendEmail(1, 2,'svanatta@fideliscare.org');
    //ttc.finish(bc);
    ttc = new baAgeIn(12, 1945);
    ttc = new baAgeIn(3);
        
    }   
}