// UpdateRetentionCaseInformationTest
// @date: 6-27-2016
// @author: rgodard@fideliscare.org
// @description: Allows for task-driven updates to Case for Retention calls. The retention rep logs a call, 
// and the Case for that call is updated to show Call Attempts, and the time of the first and latest outreach.
@isTest
public class UpdateRetentionCaseInformationTest {

    static testMethod void testMethodOne() {
        ID rtId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Renewals'].Id;
        
        // Changing Picked_Up__c on Case runs a process which creates a task
        Case c = new Case(Subject='TestCase', RecordTypeId=rtId, Picked_Up__c = 'No Answer');
        
        insert c;
    }
    
    static testMethod void testMethodTwo() {
        ID rtId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Renewals'].Id;
        Case c = new Case(Subject='TestCase', RecordTypeId=rtId);
        insert c;
        
        ID taskRtId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Retention').getRecordTypeId();
        Task t = new Task(WhatId = c.Id, Subject='Call', Type = 'Call', RecordTypeId = taskRtId, Status = 'In Progress');
        insert t;
        
        Task t2 = new Task(WhatId = c.Id, Subject='Call 2', Type = 'Call', RecordTypeId = taskRtId, Status = 'In Progress');
        insert t2;
    }
    
    static testMethod void testMethodThree() {
        ID rtId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Renewals'].Id;
        Case c = new Case(Subject='TestCase', RecordTypeId=rtId);
        insert c;
        
        List<Task> taskList = new List<Task>();
        ID taskRtId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Retention').getRecordTypeId();
        Task t = new Task(WhatId = c.Id, Subject='Call', Type = 'Call', RecordTypeId = taskRtId, Status = 'In Progress');
        taskList.add(t);
        
        Task t2 = new Task(WhatId = c.Id, Subject='Call 2', Type = 'Call', RecordTypeId = taskRtId, Status = 'In Progress');
        taskList.add(t2);
        
        insert taskList;
    }
}