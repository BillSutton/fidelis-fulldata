public class CaseMilageCalculations {
	public Double roundTripDistance{get;set;}
	public Double leg1Distance{get;set;}
	public Double leg2Distance{get;set;}
	public Double leg3Distance{get;set;}
	public Double leg4Distance{get;set;}
	public Double leg5Distance{get;set;}
	public list<String> listOfStops{get;set;}
	public Integer numberOfStops{get;set;}
	public String homeAddress{get;set;}

	public CaseMilageCalculations(){

	}

	public CaseMilageCalculations(list<String> stops){
        roundTripDistance = 0.00;
		listOfStops = stops;
		numberOfStops = stops.size();
        roundTripDistance = caluclateRoundTripDistance(listOfStops);
        caluclateLegDistances(listOfStops);
	}

    public void caluclateLegDistances(list<String> stops){
        Integer numOfAddresses = stops.size();
        if(stops.size() > 1){
            for(Integer i =0; i < stops.size() -1; i++){
                system.debug('Leg Number: '+i);
                if(i == 0){
                    leg1Distance = getDistance(stops[0],stops[1]);
                }
                else if(i == 1){
                    leg2Distance = getDistance(stops[0],stops[2]);
                }
                else if(i == 2){
                    leg3Distance = getDistance(stops[0],stops[3]);
                }
                else if(i == 3){
                    leg4Distance = getDistance(stops[0],stops[4]);
                }
                else if(i == 4){
                    leg5Distance = getDistance(stops[0],stops[5]);
                }
            }
        }
    }

	public Double caluclateRoundTripDistance(list<String> stops){
		Double totalDistance = 0.00;
		Integer loopcount = 0;
		if(stops.size() > 1){			
			for(String theStop :stops){
				loopcount++;
				if(loopcount < stops.size()){
					totalDistance+=getDistance(theStop,stops[loopcount]);
				}
			}
		}
		else{
			return 0.00;
		}
        totalDistance+= getDistance(stops[0],stops[stops.size() -1]);
        roundTripDistance = totalDistance;
		return totalDistance;
	}

	public Double getDistance(String startAddress, String endAddress){
        system.debug('The Start Address: '+startAddress);
        system.debug('the End Address: '+endAddress);
        HttpRequest req = new HttpRequest();
        Http http=new Http();
        req.setMethod('GET');
        String theStartAddress = EncodingUtil.urlEncode(startAddress,'UTF-8');
        String theEndAddress = EncodingUtil.urlEncode(endAddress,'UTF-8');
        String results;
        //Make Sure to add https://maps.googleapis.com to the remote site settings
        String url = 'https://maps.googleapis.com/maps/api/distancematrix/json';
        url += '?origins='+theStartAddress;
        url += '&destinations='+theEndAddress;
        url += '&mode=driving&sensor=false&language=en&units=imperial';
        
        req.setEndPoint(url);
        system.debug(url);
        if(!test.isRunningTest()){
            HttpResponse resp = http.send(req);
            results = resp.getBody();
        } else {
            results = '{"destination_addresses" : [ "593 T313-3, Port Matilda, PA 16870, USA" ],"origin_addresses" : [ "576 Taunton Place, Buffalo, NY 14216, USA" ],"rows" : [{"elements" : [{"distance" : {"text" : "191 mi","value" : 307636},"duration" : {"text" : "3 hours 48 mins","value" : 13656},"status" : "OK"}]}],"status" : "OK"}';
        }
        Decimal dist = 0;
        googleMapsJsonParser thingy = googleMapsJsonParser.parse(results);
        system.debug('TheParserResponse: '+thingy);
        for(googleMapsJsonParser.Rows r :thingy.rows){
            for(googleMapsJsonParser.Elements e:r.elements){
                if(e.distance.text != null){
                    String tempString = e.distance.text.substring(0,(e.distance.text.indexOf(' ')));
                    dist = Decimal.ValueOf(tempString.trim());
                }
                if(e.duration.text != null){
                   // Duration= e.duration.text;
                }
            }
        }
        return dist;
    }

}