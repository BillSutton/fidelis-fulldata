global class DailyManifestREST{

    global static void EmailAttachPDF( Id accId )
    {
        
        System.debug('recordId==>'+accId);
        PageReference pdf = Page.DailyManifest;
        pdf.getParameters().put('id',accid);
        Blob b;
        if(!Test.isRunningTest()){
            b = pdf.getContentAsPDF();
        }else{
            b = Blob.valueOf('Unit Testing');
        }
        Account tmpAcc = [SELECT Id, Name, Facets_ID__c, Preferred_Manifest_Delivery_Method__c, Fax, Manifest_Sent_To_Email_Addresses__c, OwnerId FROM Account WHERE Id=:accId];
        system.debug('account: '+tmpAcc.Id+':Name:'+tmpAcc.Name);
        DateTime dt = system.now();
        String dtFormat = dt.format('MM-dd-yyyy');
        /* Post to Chatter
        FeedItem post = new FeedItem();
            post.ParentId = tmpAcc.Id;
            post.Body = 'Daily Manifest for: '+dtFormat;
            post.ContentData = b;
            post.ContentFileName = 'DailyManifest '+dtFormat+'.pdf';
        Insert post;
        system.debug('made chatter item: '+post.Id);
        */
        
        // Post to Attachment
        Attachment attach = new Attachment();
            attach.ParentId = tmpAcc.Id;
            attach.Name = 'DailyManifest '+dtFormat;
            attach.Description = 'Daily Manifest for: '+dtFormat;
            attach.ContentType = '.pdf';
            attach.Body = b;
        Insert attach;
        system.debug('made attachment item: '+attach.Id);
        
        List<Case> caseLst = [SELECT Id, Request_Type__c, Status, Pickup_Datetime__c, LastModifiedDate, Cancellation_DateTime__c, Special_Request__c, of_Riders__c, Description, Modified_DateTime__c,
                              Contact.Name, Contact.Gender__c, Contact.Subscriber_ID__c, Contact.Plan_ID__c, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState, Authorization_Number__c,
                              Contact.MailingPostalCode, Contact.MailingCountry, Contact.Birthdate, Contact.Phone, CreatedBy.Name, Transportation_Provider__c, Last_Manifest_Sent_Date_Time__c
                              FROM Case WHERE Transportation_Provider__c =:tmpAcc.Id AND Pickup_Datetime__c > :system.today()];
        system.debug('# of cases for Acct: ' + caseLst.size());
        List<Case> updateCase = new List<Case>();
        //List<ContentDocumentLink> lstCDL = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :tmpAcc.Id ORDER BY ContentDocument.CreatedDate DESC LIMIT 1];
         
        tmpAcc.Last_Manifest_Sent_Date_Time__c = system.now();
        update tmpAcc;
        String sfDomain = 'https://'+System.URL.getSalesforceBaseUrl().getHost().remove('-api' )+'/';
        
        // loop through cases and update Manifest File Link and Last Sent
        List<Case> updateCases = new List<Case>();
        for(Case c : caseLst){
            if(c.Last_Manifest_Sent_Date_Time__c == null && c.Status == 'Scheduled' && (c.Request_Type__c == 'Standard Request' || c.Request_Type__c == 'Courtesy/Auth Pending')){
                c.Last_Manifest_Sent_Date_Time__c = system.now();
                c.Manifest_File_Link__c = sfDomain + attach.Id;
                updateCases.add(c);
            } else if (c.Modified_DateTime__c != null && c.Pickup_Datetime__c > system.today() && (c.Last_Manifest_Sent_Date_Time__c == null || c.Last_Manifest_Sent_Date_Time__c < c.Modified_DateTime__c ) && c.Status == 'Scheduled' && (c.Request_Type__c == 'Standard Request' || c.Request_Type__c == 'Courtesy/Auth Pending')){
                c.Last_Manifest_Sent_Date_Time__c = system.now();
                c.Manifest_File_Link__c = sfDomain + attach.Id;
                updateCases.add(c);
            } else if (c.Cancellation_DateTime__c != null && c.Pickup_Datetime__c > system.today() && (c.Last_Manifest_Sent_Date_Time__c==null || c.Last_Manifest_Sent_Date_Time__c < c.Cancellation_DateTime__c ) && (c.Request_Type__c == 'Standard Request' || c.Request_Type__c == 'Courtesy/Auth Pending')){
                c.Last_Manifest_Sent_Date_Time__c = system.now();
                c.Manifest_File_Link__c = sfDomain + attach.Id;
                updateCases.add(c);
            }
            
        }
        update updateCases;
             
        //Create the email attachment
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('DailyManifest '+dtFormat+'.pdf');
        efa.setContentType('application/pdf');
        efa.setBody(b);
        
        // Create the Single Email Message
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(dtFormat+ ' - Transportation Orders Report from Fidelis Care NY for Provider '+tmpAcc.Facets_ID__c+' '+tmpAcc.Name+' ');
        List<String> sendToAddress = new List<String>();
        if(tmpAcc.Preferred_Manifest_Delivery_Method__c == 'Fax'){
            system.debug('fax');
            String formatFax = tmpAcc.Fax;
            if(formatFax!=null){
                formatFax = formatFax.replaceAll('\\D','');
                if(formatFax.left(1) != '1'){
                    formatFax = '1'+formatFax;
                }
            }
            if(formatFax.contains('@concordsend.com')){
                sendToAddress.add(formatFax);
            }else{
                sendToAddress.add(formatFax+'@concordsend.com');
            }
        } else {
            system.debug('email');
            if(tmpAcc.Manifest_Sent_To_Email_Addresses__c != null && tmpAcc.Manifest_Sent_To_Email_Addresses__c.contains(';')){
                for (String emailAddr : tmpAcc.Manifest_Sent_To_Email_Addresses__c.split(';')){
                    sendToAddress.add(emailAddr);
                }
            } else if (tmpAcc.Manifest_Sent_To_Email_Addresses__c != null){
                sendToAddress.add(tmpAcc.Manifest_Sent_To_Email_Addresses__c);
            }
        }
        
        OrgWideEmailAddress[] owea = [select Id, Address from OrgWideEmailAddress where Address = 'ftrequest@fideliscare.org'];
        if ( owea.size() > 0 ) {
            email.setOrgWideEmailAddressId(owea.get(0).Id);
            sendToAddress.add(owea.get(0).Address);
        }
        
        /*
        OrgWideEmailAddress[] owea = [select Id, Address from OrgWideEmailAddress where Address = 'mwatson@fideliscare.org'];
        if ( owea.size() > 0 ) {
            email.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        */
        // Send email to Account Owner if Fax / Manifest_Sent_To_Email_Addresses__c not populated
        if(sendToAddress == null){
            User u = [Select Id, Email from User where Id = :tmpAcc.OwnerId LIMIT 1];
            sendToAddress.add(u.Email);
        }
        
        email.setToAddresses(sendToAddress);
        system.debug(sendToAddress);
        
        // Only send email if the Account has Fax / Manifest_Sent_To_Email_Addresses__c populated
        if(sendToAddress.size() > 0){
            email.setPlainTextBody('Good Afternoon, \n\n'+
                                   'Attached is your report for '+dtFormat+' - Transportation Order report from Fidelis Care NY. \n\n'+
                                   'Please review your assigned new trips and contact Fidelis Care '+
                                   'Transportation by 5:00 PM at 888-343-3547 to inform of any changes or '+
                                   'cancellations -- DO NOT REPLY TO THIS EMAIL.  Thank You');
            
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            List<Messaging.SingleEmailMessage> theEmails = new List<Messaging.SingleEmailMessage>();
            theEmails.add(email);
            // Sends the email
            if(!Test.isRunningTest()){
                Messaging.sendEmail(theEmails);
            }
        }
    } 
}