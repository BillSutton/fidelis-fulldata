// InlineContactFamilyListController
// @date: 6-7-2016
// @author: rgodard@fideliscare.org
// @description: This controller, for a given Case, traverses to the related Contact's family in order to determine and list the family members of the Contact, 
// and mark who the Head of Household or Responsible person is for that contact.

public class InlineContactFamilyListController {

    public Contact member {get;set;}
    public List<FamilyListItem> familyList {get;set;}
    public List<Case> caseList {get;set;}

    public InlineContactFamilyListController(ApexPages.StandardController stdController){
        familyList = new List<FamilyListItem>();
        caseList = new List<Case>();

        // Crawl our relationships differently based on the sObject of the current page
        Id currentPageId = ApexPages.currentPage().getParameters().get('id');
        String sobjectName = currentPageId.getSObjectType().getDescribe().getName();
    
    
        if (sobjectName == 'Case') {
            Case c = [SELECT Id, ContactId, Primary_Category__c FROM Case WHERE Id = :currentPageId];
            this.member = [Select ID, Line_of_Business__c, Age__c, Member_Suffix__c, Subscriber_ID__c, Family_Link_ID__c, Contrived_Key__c, (SELECT Id, Responsible_Person__r.Name, RP_Effective_Date__c FROM Effective_Responsible_Person__r) FROM Contact WHERE id = :c.ContactId limit 1];
        } else if (sobjectName == 'Contact') {
            this.member = [Select ID, Line_of_Business__c, Age__c, Member_Suffix__c, Subscriber_ID__c, Family_Link_ID__c, Contrived_Key__c, (SELECT Id, Responsible_Person__r.Name, RP_Effective_Date__c FROM Effective_Responsible_Person__r) FROM Contact WHERE id = :currentPageId limit 1];
        } else {
        }
        Boolean memberIsHOH = false;
            
        // Traverse the Responsible Persons related to this Contact -- we want the "active" RP dependent on the Effective Date
        Responsible_Person_Relationship__c activeRPR = null;
        Date mostRecentDate = null;
        for (Responsible_Person_Relationship__c rpr : member.Effective_Responsible_Person__r) {
            if (mostRecentDate == null || rpr.RP_Effective_Date__c > mostRecentDate) {
                mostRecentDate = rpr.RP_Effective_Date__c;
                activeRPR = rpr;
            }
        }
        if (activeRPR != null) {
            FamilyListItem f = new FamilyListItem();
            f.name = activeRPR.Responsible_Person__r.Name;
            f.link = activeRPR.Responsible_Person__c;
            
            //Depending on the plan, this responsible person may be the HOH
            if (member.Line_of_Business__c == 'CHP' || member.Line_of_Business__c == 'NYM') {
                // For CHP and NYM, if the current member is 19 or over, the current member is their own HOH
                if (member.Age__c < 19) {
                    f.isHOH = true;
                } else {
                    memberIsHOH = true;
                }
            }
            familyList.add(f);
        }
        
        // Find other family members through family link ID
        List<Contact> contactList = [Select Id, Name, Member_Suffix__c, Subscriber_ID__c, Family_Link_ID__c, Contrived_Key__c, (SELECT Id, Contact.Name, CaseNumber, Subject, Primary_Category__c, isClosed FROM Cases) FROM Contact WHERE Family_Link_Id__c != null AND Family_Link_Id__c = :member.Family_Link_Id__c AND Id != :member.Id];
        for (Contact con : contactList) {
            FamilyListItem f = new FamilyListItem();
            f.name = con.Name;
            f.link = con.Id;
            
            //Depending on the plan, and the Member suffix, this contact may be the HOH
            if (member.Line_of_Business__c != 'CHP' && member.Line_of_Business__c != 'NYM' && member.Member_Suffix__c != '00' && con.Member_Suffix__c == '00') {
                f.isHOH = true;
            }
            
            familyList.add(f);
            
            // Get a list of cases related to the contact
            for (Case ca : con.Cases) {
                if (!ca.isClosed) {
                    caseList.add(ca);
                }
            }
        }
    }
    
    public class FamilyListItem {
        public String name{get;set;}
        public String link {get;set;}
        public Boolean isHOH {get;set;}
        
        public FamilyListItem() {
            this.name = '';
            this.link = '';
            this.isHOH = false;
        }
    }

    

}