/*
@Name            : AuthGridQuickUpdateController
@Author          : customersuccess@cloud62.com
@Date            : May 08, 2015
@Description     : Allows user to quickly update auth grid settings for many counties/plans.
@Revision History: 05-08-15 dan@cloud62.com: Created controller.  
*/
public class AuthGridQuickUpdateController {
   
    public List<County__c> lstCounty {get;set;}
    public List<Transportation_Plan__c> lstPlan {get;set;}
    public Map<Id, Transportation_Plan__c> mapPlan {get;set;}
    public List<Transportation_Authorization_Grid__c> lstCountyPlan {get;set;}
    public Map<Id, List<Transportation_Authorization_Grid__c>> mapCountyPlan {get;set;}
    public List<SelectOption> soCounty {get;set;}
    public List<String> selCounty {get;set;}
    public List<String> selPlan {get;set;}
    public String selGroup {get;set;}
    public Transportation_Authorization_Grid__c tmpCP {get;set;}
    public Transportation_Authorization_Grid__c tmpCP2 {get;set;}
    public List<Transportation_Plan__c> tmpPlanLst {get;set;}
    public List<SelectOption> soGroup {get;set;}
    public Boolean countyCovered {get;set;}
    
    public List<SelectOption> getsoPlan() {
        system.debug('updating list.  plan group: '+selGroup);
        if(selGroup != null){
            tmpPlanLst = [SELECT Id, Name, CSPI_ID__c FROM Transportation_Plan__c WHERE Line_of_Business__c = :selGroup ORDER BY Name ASC];
        } else {
            tmpPlanLst = [SELECT Id, Name, CSPI_ID__c FROM Transportation_Plan__c ORDER BY Name ASC];
        }
        List<SelectOption> so = new List<SelectOption>();
        for (Transportation_Plan__c p: tmpPlanLst){
            String planName = p.Name.trim();
            String planId =p.Id;
            planId = planId.trim();
            if (planName.contains('  ')){
                planName = planName.replace('  ', ' ');
            }
            so.add(new SelectOption(planId,p.CSPI_ID__c+' - '+planName ));
        }
        return so;
    }
    
    public AuthGridQuickUpdateController(){
        lstCounty = [SELECT Id, Name FROM County__c];
        lstPlan = [SELECT Id, Name, CSPI_ID__c FROM Transportation_Plan__c ORDER BY Name ASC];
        mapPlan = new Map<Id, Transportation_Plan__c>([SELECT Id, Name FROM Transportation_Plan__c ]);
        mapCountyPlan = new Map<Id, List<Transportation_Authorization_Grid__c>>();
        tmpCP = New Transportation_Authorization_Grid__c();
        tmpCP2 = New Transportation_Authorization_Grid__c();
        countyCovered = false;
        
        for (County__c c : lstCounty){
            mapCountyPlan.put(c.Id, new List<Transportation_Authorization_Grid__c>());
        }
        
        for (Transportation_Authorization_Grid__c cp : [SELECT Id, County__c, Plan__c, Vehicle_Type__c, Form_Type__c, HD_Auth_Form__c, Authorization_Number__c, Effective_Date__c,
                                         				Standard_Auth_Form__c, Note_for_MSA__c, County_Covered__c, Termination_Date__c FROM Transportation_Authorization_Grid__c]){
            if (mapCountyPlan.get(cp.County__c) == null){
                mapCountyPlan.put(cp.County__c, new List<Transportation_Authorization_Grid__c>());
            }
            mapCountyPlan.get(cp.County__c).add(cp);
        }

        soGroup = new list<SelectOption>();
        
        // Get the list of picklist values for this field.
        Schema.sObjectType objType = Transportation_Plan__c.getSObjectType(); 
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        list<Schema.PicklistEntry> values = fieldMap.get('Line_of_Business__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values){ 
            soGroup.add(new SelectOption(a.getLabel(), a.getLabel()));
            if(SelGroup == null){
                selGroup = a.getLabel();
            }
        } 
                
        soCounty = new list<SelectOption>();
        for (County__c c: lstCounty){
            soCounty.add(new SelectOption(c.Id,c.Name));
        }
        /*
        soPlan = new list<SelectOption>();
        for (Transportation_Plan__c p: lstPlan){
            String planName = p.Name.trim();
            String planId =p.Id;
            planId = planId.trim();
            if (planName.contains('  ')){
                planName = planName.replace('  ', ' ');
            }
            soPlan.add(new SelectOption(planId,p.CSPI_ID__c+' - '+planName ));
        }*/
        system.debug('controller completed');
    }
    
    public void updatePlanList() {
        system.debug(selGroup);
    	//return soPlan;
    }
    
    public PageReference updateRecords(){
        
        lstCountyPlan = new List<Transportation_Authorization_Grid__c>();
        Boolean foundMatch;
        system.debug('validation');
        if ( tmpCP.County_Covered__c == 'No' && tmpCP.Note_for_MSA__c == null ){
            system.debug('error 1');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Note for MSA is required when County Covered is No.'));
            return null;
        } else if (tmpCP.County_Covered__c == 'Yes' && (tmpCP.Form_Type__c == null || tmpCP.HD_Auth_Form__c == null || tmpCP.Vehicle_Type__c == null || tmpCP.Standard_Auth_Form__c == null || tmpCP.Authorization_Number__c == null || tmpCP.Termination_Date__c == null || tmpCP.Effective_Date__c == null)){
            system.debug('tmpCP.County_Covered__c == '+tmpCP.County_Covered__c);
            
            system.debug('tmpCP.Form_Type__c == '+tmpCP.Form_Type__c);
            system.debug('tmpCP.HD_Auth_Form__c == '+tmpCP.HD_Auth_Form__c);
            system.debug('tmpCP.Vehicle_Type__c == '+tmpCP.Vehicle_Type__c);
            system.debug('tmpCP.Standard_Auth_Form__c == '+tmpCP.Standard_Auth_Form__c);
            system.debug('tmpCP.Authorization_Number__c == '+tmpCP.Authorization_Number__c);
            system.debug('tmpCP.Termination_Date__c == '+tmpCP.Termination_Date__c);
            system.debug('tmpCP.Effective_Date__c == '+tmpCP.Effective_Date__c);
   
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Effective Date, Termination Date, Vehicle Type, Form Type, Hospital Discharge Authorization Form, Auth #, and Standard Authorization Form are required when County Covered is Yes.'));
            return null;
        }
        
        system.debug('selCounty: '+selCounty);
        system.debug('selPlan: '+selPlan);
        
        system.debug('updating records');
        if (selCounty != null && selPlan != null){
            for(Id c: selCounty){
                for (Id p : selPlan){
                    foundMatch = false;
                    // search mapCountyPlan for match, update fields and add to lstCountyPlan.
                    for (Transportation_Authorization_Grid__c cp: mapCountyPlan.get(c)){
                        if(cp.Plan__c == p){
                            // update the records
                            if(tmpCP.Vehicle_Type__c != null) {
                                cp.Vehicle_Type__c = tmpCP.Vehicle_Type__c;
                            } else {
                                cp.Vehicle_Type__c = '';
                            }
                            if(tmpCP.Form_Type__c != null){
                                cp.Form_Type__c = tmpCP.Form_Type__c;
                            } else {
                                cp.Form_Type__c = '';
                            }   
                            if(tmpCP.Authorization_Number__c != null){
                                cp.Authorization_Number__c = tmpCP.Authorization_Number__c;
                            } else {
                                cp.Authorization_Number__c = '';
                            }       
                            if(tmpCP.Standard_Auth_Form__c != null){
                                cp.Standard_Auth_Form__c = tmpCP.Standard_Auth_Form__c;
                            } else {
                                cp.Standard_Auth_Form__c = '';
                            }  
                            if(tmpCP.Note_for_MSA__c != null){
                                cp.Note_for_MSA__c = tmpCP.Note_for_MSA__c;
                            } else {
                                cp.Note_for_MSA__c = '';
                            }
                            if(tmpCP.Effective_Date__c != null){
                                cp.Effective_Date__c = tmpCP.Effective_Date__c;
                            } else {
                                cp.Effective_Date__c = null;
                            }  
                            if(tmpCP.Termination_Date__c != null){
                                cp.Termination_Date__c = tmpCP.Termination_Date__c;
                            } else {
                                cp.Termination_Date__c = null;
                            }  
                            cp.County_Covered__c = tmpCP.County_Covered__c;
                            cp.HD_Auth_Form__c = tmpCP.HD_Auth_Form__c;
                            
                            lstCountyPlan.add(cp);
                            foundMatch = true;
                            system.debug('found match');
                        }
                    }
                    // if no match found, create one.
                    if (foundMatch == false){
                        Transportation_Authorization_Grid__c cp = new Transportation_Authorization_Grid__c();   
                            cp.County__c = c;
                            cp.Plan__c = p;
                            if(tmpCP.Vehicle_Type__c != null) {
                                cp.Vehicle_Type__c = tmpCP.Vehicle_Type__c;
                            } else {
                                cp.Vehicle_Type__c = '';
                            }
                            if(tmpCP.Form_Type__c != null){
                                cp.Form_Type__c = tmpCP.Form_Type__c;
                            } else {
                                cp.Form_Type__c = '';
                            }    
                            if(tmpCP.Authorization_Number__c != null){
                                cp.Authorization_Number__c = tmpCP.Authorization_Number__c;
                            } else {
                                cp.Authorization_Number__c = '';
                            }       
                            if(tmpCP.Standard_Auth_Form__c != null){
                                cp.Standard_Auth_Form__c = tmpCP.Standard_Auth_Form__c;
                            } else {
                                cp.Standard_Auth_Form__c = '';
                            } 
                            if(tmpCP.Note_for_MSA__c != null){
                                cp.Note_for_MSA__c = tmpCP.Note_for_MSA__c;
                            } else {
                                cp.Note_for_MSA__c = '';
                            }
                            if(tmpCP.Effective_Date__c != null){
                                cp.Effective_Date__c = tmpCP.Effective_Date__c;
                            } else {
                                cp.Effective_Date__c = null;
                            }  
                            if(tmpCP.Termination_Date__c != null){
                                cp.Termination_Date__c = tmpCP.Termination_Date__c;
                            } else {
                                cp.Termination_Date__c = null;
                            }  
                            cp.County_Covered__c = tmpCP.County_Covered__c;
                            cp.HD_Auth_Form__c = tmpCP.HD_Auth_Form__c;
                        lstCountyPlan.add(cp);
                        system.debug('match not found');
                    }
                }
            }
             
            if(lstCountyPlan.size() > 0) {
            	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'The selected Transportation Authorization Grids have been updated.'));
                upsert lstCountyPlan; 
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You must select at least one county and one plan.'));
            }
        }
        return null;
    }
}