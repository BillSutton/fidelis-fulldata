@isTest
private class testRetention {

	@testSetup
	static void createTestData(){
		list<account> accounts = new list<account>();
		accounts.add(new account(name='Test Fam 1'));
		accounts.add(new account(name='Test Fam 2'));
		insert accounts;

		list<contact> contacts = new list<contact>();
		contacts.add(new contact(accountId = accounts[0].id, firstName = 'ted', lastname='test1',contrived_key__c='123456',Enrolled__c = true, Family_Link_ID__c = '456', Birthdate = system.today() - 12));
		contacts.add(new contact(accountId = accounts[1].id, firstName = 'norbert', lastname = 'test2', contrived_key__c='987654',Enrolled__c = true, Family_Link_ID__c = '123', Birthdate = system.today()-20));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563298',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 45));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563297',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 360));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563296',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 500));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563295',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 600));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563294',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 425));
		contacts.add(new contact(accountid = accounts[1].id, firstName='kelly', lastname='test2',contrived_key__c='4563292',Enrolled__c = true, Family_Link_ID__c='123', Birthdate = system.today() - 35));
		insert contacts;

		list<Reponsible_Person__c> rpList = new list<Reponsible_Person__c>();
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '1', name = 'test1'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '2', name = 'test2'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '3', name = 'test3'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '4', name = 'test4'));
		rpList.add(new Reponsible_Person__c(Resp_Person_ID__c = '5', name = 'test5'));
		insert rpList;
	}
	
	 public static testMethod void HappyPath() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		for(contact mem : c){
			retention.TempCaseObject t = new retention.TempCaseObject();
			t.ContrivedKey = mem.contrived_key__c;
			t.lob = 'nym'; 
			t.caseType = 'r';
			t.responsiblePersonId = ''; 
			t.subscriberId = '123';
			t.recertDate = '';
			t.enrollmentSource = 'MarketPlace'; 
			t.APTCIndicator = 'false';
			t.suffix = '00';
			t.familyLinkId = mem.Family_Link_ID__c;
			requestObject.add(t);
		}

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		system.assertEquals(2,insertedCases.size());
		system.assertEquals(8,insertedApp.size());
		


	}
	
	public static testMethod void chpTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		for(contact mem : c){
			retention.TempCaseObject t = new retention.TempCaseObject();
			t.ContrivedKey = mem.contrived_key__c;
			t.lob = 'chp'; 
			t.caseType = 'r';
			t.responsiblePersonId = ''; 
			t.subscriberId = '123';
			t.recertDate = '';
			t.enrollmentSource = 'MarketPlace'; 
			t.APTCIndicator = 'false';
			t.suffix = '00';
			t.familyLinkId = mem.Family_Link_ID__c;
			requestObject.add(t);
		}

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void blankCKTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '';
		t.lob = 'chp'; 
		t.caseType = 'r';
		t.responsiblePersonId = ''; 
		t.subscriberId = '123';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '00';
		t.familyLinkId = '';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void blankCaseTypeTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '13456';
		t.lob = 'chp'; 
		t.caseType = '';
		t.responsiblePersonId = ''; 
		t.subscriberId = '123';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '00';
		t.familyLinkId = '';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void blankSubAndRespIdsTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '13456';
		t.lob = 'nym'; 
		t.caseType = 'r';
		t.responsiblePersonId = ''; 
		t.subscriberId = '';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '00';
		t.familyLinkId = '';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void badSuffixTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '13456';
		t.lob = 'chp'; 
		t.caseType = 'r';
		t.responsiblePersonId = ''; 
		t.subscriberId = '123';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '0';
		t.familyLinkId = '';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void noFamilyLinkTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '13456';
		t.lob = 'chp'; 
		t.caseType = 'r';
		t.responsiblePersonId = ''; 
		t.subscriberId = '123';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '00';
		t.familyLinkId = '';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void rpTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '13456';
		t.lob = 'chp'; 
		t.caseType = 'r';
		t.responsiblePersonId = '3'; 
		t.subscriberId = '123';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '00';
		t.familyLinkId = '123';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void badRpTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '13456';
		t.lob = 'chp'; 
		t.caseType = 'r';
		t.responsiblePersonId = '30'; 
		t.subscriberId = '123';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '00';
		t.familyLinkId = '123';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}

	public static testMethod void badCKTest() {
		list<contact> c = [select id, contrived_key__c,Family_Link_ID__c from contact];
		list<retention.TempCaseObject> requestObject = new list<retention.TempCaseObject>();
		retention.TempCaseObject t = new retention.TempCaseObject();
		t.ContrivedKey = '999999999';
		t.lob = 'nym'; 
		t.caseType = 'r';
		t.responsiblePersonId = ''; 
		t.subscriberId = '123456';
		t.recertDate = '';
		t.enrollmentSource = 'MarketPlace'; 
		t.APTCIndicator = 'false';
		t.suffix = '00';
		t.familyLinkId = '';
		requestObject.add(t);

		retention.generateList(requestObject);

		List<case> insertedCases = [select id from case];
		list<applicant_information__c> insertedApp = [select id from applicant_information__c];

		//system.assertEquals(2,insertedCases.size());
		//system.assertEquals(3,insertedApp.size());
	}
}