@isTest public class TransportationPDFControllerTest {
    
    static testMethod void test0(){
        TransportationPDFController tc = new TransportationPDFController();
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
        a.Name = 'test';
        a.RecordTypeId = rtAcct.Id;
        a.EIN__c = '123456789';
        a.BillingStreet = '123 Test Ave.';
        a.BillingState = 'NY';
        a.BillingPostalCode = '14203';
        insert a;
        List<Account> aList = [select Id, Name, RecordTypeId, EIN__c, BillingStreet, BillingState, BillingPostalCode from Account where Id = :a.Id limit 1];
        System.debug(aList);
        System.assertEquals(1, aList.size());
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];     
        Contact con = new Contact();
        con.LastName = 'Smith';
        con.Line_of_Business__c = 'HBX';
        con.Subscriber_ID__c = 'Test';
        con.Birthdate = Date.newInstance(2015, 08, 15);
        con.Phone = '0000000000';
        insert con;
        List<Contact> conList = [select Id, LastName, Line_Of_Business__c, Subscriber_ID__c, Birthdate, Phone from Contact where Id = :con.Id limit 1];
        System.debug(conList);
        
        Case c = new Case();
        c.RecordTypeId = rtCase.Id;
        c.entrance__c  = 'test';
        c.room_number__c = 'test';
        c.bed_number__c = 'test';
        c.floor_number__c = 'test';
        c.Contact = con;
        c.ContactId = con.Id;
        c.Contact.Subscriber_ID__c = con.Subscriber_ID__c;
        c.Contact.Birthdate = con.Birthdate;
        c.Contact.Phone = con.Phone;
        c.Pickup_Location__c = 'some pickup location';
        c.Transportation_Provider__r = a;
        c.Transportation_Provider__r.name = a.Id;
        c.Drop_Off_Location__c = 'some dropoff location';
        c.Pickup_dateTime__c = Date.newInstance(2015, 08, 15);
        c.form_type__c = 'Hospital Discharge'; 
        c.of_Riders__c = '1';
        c.Steps__c = Decimal.valueOf('10');
        c.Authorization_Number__c = 'test';
        c.weight__c = Decimal.valueOf('200');
        c.Vehicle_Type__c = 'Livery';
        c.Description = 'some description';
        c.Special_Request__c = 'Has a Cane';        
        insert c;
        Case cList = [select id, entrance__c, room_number__c, bed_number__c, floor_number__c, contact.Name, contact.subscriber_id__c,Pickup_Location__c,Transportation_Provider__r.name,Drop_Off_Location__c,contact.Birthdate,contact.Phone,Pickup_dateTime__c,form_type__c,of_Riders__c,Steps__c,Authorization_Number__c,weight__c,Vehicle_Type__c,Description,Special_Request__c from Case where ID = :c.Id limit 1];  
        System.debug(cList);
        
        tc.theCase = cList;
        String Id = c.Id;
        tc.theId1 = Id;
        String theId1 = tc.theId1;
        tc.theUser = 'test';
        tc.theTime = '12:20';
        //System.assertEquals(Id, theId1);
        System.assert(tc.theCase != null);
    }
    
    static testMethod void test1(){
        TransportationPDFController tc = new TransportationPDFController();
        
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
        a.Name = 'test';
        a.RecordTypeId = rtAcct.Id;
        a.EIN__c = '123456789';
        a.BillingStreet = '123 Test Ave.';
        a.BillingState = 'NY';
        a.BillingPostalCode = '14203';
        insert a;
        List<Account> aList = [select Id, Name, RecordTypeId, EIN__c, BillingStreet, BillingState, BillingPostalCode from Account where Id = :a.Id limit 1];
        System.debug(aList);
        System.assertEquals(1, aList.size());
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];     
        Contact con = new Contact();
        con.LastName = 'Smith';
        con.Line_of_Business__c = 'HBX';
        con.Subscriber_ID__c = 'Test';
        con.Birthdate = Date.newInstance(2015, 08, 15);
        con.Phone = '0000000000';
        insert con;
        List<Contact> conList = [select Id, LastName, Line_Of_Business__c, Subscriber_ID__c, Birthdate, Phone from Contact where Id = :con.Id limit 1];
        System.debug(conList);
        
        Case c = new Case();
        c.RecordTypeId = rtCase.Id;
        c.entrance__c  = 'test';
        c.room_number__c = 'test';
        c.bed_number__c = 'test';
        c.floor_number__c = 'test';
        c.Contact = con;
        c.ContactId = con.Id;
        c.Contact.Subscriber_ID__c = con.Subscriber_ID__c;
        c.Contact.Birthdate = con.Birthdate;
        c.Contact.Phone = con.Phone;
        c.Pickup_Location__c = 'some pickup location';
        c.Transportation_Provider__r = a;
        c.Transportation_Provider__r.name = a.Id;
        c.Drop_Off_Location__c = 'some dropoff location';
        c.Pickup_dateTime__c = Date.newInstance(2015, 08, 15);
        c.form_type__c = 'Hospital Discharge'; 
        c.of_Riders__c = '1';
        c.Steps__c = Decimal.valueOf('10');
        c.Authorization_Number__c = 'test';
        c.weight__c = Decimal.valueOf('200');
        c.Vehicle_Type__c = 'Livery';
        c.Description = 'some description';
        c.Special_Request__c = 'Has a Cane';        
        insert c;
        Case cList = [select id, entrance__c, room_number__c, bed_number__c, floor_number__c, contact.Name, contact.subscriber_id__c,Pickup_Location__c,Transportation_Provider__r.name,Drop_Off_Location__c,contact.Birthdate,contact.Phone,Pickup_dateTime__c,form_type__c,of_Riders__c,Steps__c,Authorization_Number__c,weight__c,Vehicle_Type__c,Description,Special_Request__c from Case where ID = :c.Id limit 1];  
        System.debug(cList);
        
        tc.theCase = cList;
        String Id = c.Id;
        tc.theId1 = Id;
        String theId1 = tc.theId1;
        tc.theUser = 'test';
        tc.theTime = '12:20';
        System.assert(tc.theCase.Form_Type__c == 'Hospital Discharge');
    }
    
    static testMethod void test2(){
        TransportationPDFController tc = new TransportationPDFController();
        
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
        a.Name = 'test';
        a.RecordTypeId = rtAcct.Id;
        a.EIN__c = '123456789';
        a.BillingStreet = '123 Test Ave.';
        a.BillingState = 'NY';
        a.BillingPostalCode = '14203';
        insert a;
        List<Account> aList = [select Id, Name, RecordTypeId, EIN__c, BillingStreet, BillingState, BillingPostalCode from Account where Id = :a.Id limit 1];
        System.debug(aList);
        System.assertEquals(1, aList.size());
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];     
        Contact con = new Contact();
        con.LastName = 'Smith';
        con.Line_of_Business__c = 'HBX';
        con.Subscriber_ID__c = 'Test';
        con.Birthdate = Date.newInstance(2015, 08, 15);
        con.Phone = '0000000000';
        insert con;
        List<Contact> conList = [select Id, LastName, Line_Of_Business__c, Subscriber_ID__c, Birthdate, Phone from Contact where Id = :con.Id limit 1];
        System.debug(conList);
        
        Case c = new Case();
        c.RecordTypeId = rtCase.Id;
        c.entrance__c  = 'test';
        c.room_number__c = 'test';
        c.bed_number__c = 'test';
        c.floor_number__c = 'test';
        c.Contact = con;
        c.ContactId = con.Id;
        c.Contact.Subscriber_ID__c = con.Subscriber_ID__c;
        c.Contact.Birthdate = con.Birthdate;
        c.Contact.Phone = con.Phone;
        c.Pickup_Location__c = 'some pickup location';
        c.Transportation_Provider__r = a;
        c.Transportation_Provider__r.name = a.Id;
        c.Drop_Off_Location__c = 'some dropoff location';
        c.Pickup_dateTime__c = Date.newInstance(2015, 08, 15);
        c.form_type__c = 'Hospital Discharge';
        c.of_Riders__c = '1';
        c.Steps__c = Decimal.valueOf('10');
        c.Authorization_Number__c = 'test';
        c.weight__c = Decimal.valueOf('200');
        c.Vehicle_Type__c = 'Livery';
        c.Description = 'some description';
        c.Special_Request__c = 'Has a Cane';        
        insert c;
        Case cList = [select id, entrance__c, room_number__c, bed_number__c, floor_number__c, contact.Name, contact.subscriber_id__c,Pickup_Location__c,Transportation_Provider__r.name,Drop_Off_Location__c,contact.Birthdate,contact.Phone,Pickup_dateTime__c,form_type__c,of_Riders__c,Steps__c,Authorization_Number__c,weight__c,Vehicle_Type__c,Description,Special_Request__c from Case where ID = :c.Id limit 1];  
        System.debug(cList);
        
        tc.theCase = cList;
        System.assert(tc.renderHospitalInformation == true);
    }   
    
    static testMethod void tes3(){
        TransportationPDFController tc = new TransportationPDFController();
        
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
        a.Name = 'test';
        a.RecordTypeId = rtAcct.Id;
        a.EIN__c = '123456789';
        a.BillingStreet = '123 Test Ave.';
        a.BillingState = 'NY';
        a.BillingPostalCode = '14203';
        insert a;
        List<Account> aList = [select Id, Name, RecordTypeId, EIN__c, BillingStreet, BillingState, BillingPostalCode from Account where Id = :a.Id limit 1];
        System.debug(aList);
        System.assertEquals(1, aList.size());
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];     
        Contact con = new Contact();
        con.LastName = 'Smith';
        con.Line_of_Business__c = 'HBX';
        con.Subscriber_ID__c = 'Test';
        con.Birthdate = Date.newInstance(2015, 08, 15);
        con.Phone = '0000000000';
        insert con;
        List<Contact> conList = [select Id, LastName, Line_Of_Business__c, Subscriber_ID__c, Birthdate, Phone from Contact where Id = :con.Id limit 1];
        System.debug(conList);
        
        Case c = new Case();
        c.RecordTypeId = rtCase.Id;
        c.entrance__c  = 'test';
        c.room_number__c = 'test';
        c.bed_number__c = 'test';
        c.floor_number__c = 'test';
        c.Contact = con;
        c.ContactId = con.Id;
        c.Contact.Subscriber_ID__c = con.Subscriber_ID__c;
        c.Contact.Birthdate = con.Birthdate;
        c.Contact.Phone = con.Phone;
        c.Pickup_Location__c = 'some pickup location';
        c.Transportation_Provider__r = a;
        c.Transportation_Provider__r.name = a.Id;
        c.Drop_Off_Location__c = 'some dropoff location';
        c.Pickup_dateTime__c = Date.newInstance(2015, 08, 15);
        c.form_type__c = 'Standard';
        c.of_Riders__c = '1';
        c.Steps__c = Decimal.valueOf('10');
        c.Authorization_Number__c = 'test';
        c.weight__c = Decimal.valueOf('200');
        c.Vehicle_Type__c = 'Livery';
        c.Description = 'some description';
        c.Special_Request__c = 'Has a Cane';        
        insert c;
        Case cList = [select id, entrance__c, room_number__c, bed_number__c, floor_number__c, contact.Name, contact.subscriber_id__c,Pickup_Location__c,Transportation_Provider__r.name,Drop_Off_Location__c,contact.Birthdate,contact.Phone,Pickup_dateTime__c,form_type__c,of_Riders__c,Steps__c,Authorization_Number__c,weight__c,Vehicle_Type__c,Description,Special_Request__c from Case where ID = :c.Id limit 1];  
        System.debug(cList);
        
        tc.theCase = cList;
        System.assert(tc.renderHospitalInformation == false);
    }    
    
    static testMethod void tes4(){
        TransportationPDFController tc = new TransportationPDFController();
        
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
        a.Name = 'test';
        a.RecordTypeId = rtAcct.Id;
        a.EIN__c = '123456789';
        a.BillingStreet = '123 Test Ave.';
        a.BillingState = 'NY';
        a.BillingPostalCode = '14203';
        insert a;
        List<Account> aList = [select Id, Name, RecordTypeId, EIN__c, BillingStreet, BillingState, BillingPostalCode from Account where Id = :a.Id limit 1];
        System.debug(aList);
        System.assertEquals(1, aList.size());
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];     
        Contact con = new Contact();
        con.LastName = 'Smith';
        con.Line_of_Business__c = 'HBX';
        con.Subscriber_ID__c = 'Test';
        con.Birthdate = Date.newInstance(2015, 08, 15);
        con.Phone = '0000000000';
        insert con;
        List<Contact> conList = [select Id, LastName, Line_Of_Business__c, Subscriber_ID__c, Birthdate, Phone from Contact where Id = :con.Id limit 1];
        System.debug(conList);
        
        Case c = new Case();
        c.RecordTypeId = rtCase.Id;
        c.entrance__c  = 'test';
        c.room_number__c = 'test';
        c.bed_number__c = 'test';
        c.floor_number__c = 'test';
        c.Contact = con;
        c.ContactId = con.Id;
        c.Contact.Subscriber_ID__c = con.Subscriber_ID__c;
        c.Contact.Birthdate = con.Birthdate;
        c.Contact.Phone = con.Phone;
        c.Pickup_Location__c = 'some pickup location';
        c.Transportation_Provider__r = a;
        c.Transportation_Provider__r.name = a.Id;
        c.Drop_Off_Location__c = 'some dropoff location';
        c.Pickup_dateTime__c = Date.newInstance(2015, 08, 15);
        c.form_type__c = 'Standard';
        c.of_Riders__c = '1';
        c.Steps__c = Decimal.valueOf('10');
        c.Authorization_Number__c = 'test';
        c.weight__c = Decimal.valueOf('200');
        c.Vehicle_Type__c = 'Livery';
        c.Description = 'some description';
        c.Special_Request__c = 'Has a Cane';        

        System.assert(tc.renderHospitalInformation == false);
    }   
    
    static testMethod void test5(){
        
        
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider'];
        Account a = new Account();
        a.Name = 'test';
        a.RecordTypeId = rtAcct.Id;
        a.EIN__c = '123456789';
        a.BillingStreet = '123 Test Ave.';
        a.BillingState = 'NY';
        a.BillingPostalCode = '14203';
        insert a;
        List<Account> aList = [select Id, Name, RecordTypeId, EIN__c, BillingStreet, BillingState, BillingPostalCode from Account where Id = :a.Id limit 1];
        System.debug(aList);
        System.assertEquals(1, aList.size());
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation'];     
        Contact con = new Contact();
        con.LastName = 'Smith';
        con.Line_of_Business__c = 'HBX';
        con.Subscriber_ID__c = 'Test';
        con.Birthdate = Date.newInstance(2015, 08, 15);
        con.Phone = '0000000000';
        insert con;
        List<Contact> conList = [select Id, LastName, Line_Of_Business__c, Subscriber_ID__c, Birthdate, Phone from Contact where Id = :con.Id limit 1];
        System.debug(conList);
        
        Case c = new Case();
        c.RecordTypeId = rtCase.Id;
        c.entrance__c  = 'test';
        c.room_number__c = 'test';
        c.bed_number__c = 'test';
        c.floor_number__c = 'test';
        c.Contact = con;
        c.ContactId = con.Id;
        c.Contact.Subscriber_ID__c = con.Subscriber_ID__c;
        c.Contact.Birthdate = con.Birthdate;
        c.Contact.Phone = con.Phone;
        c.Pickup_Location__c = 'some pickup location';
        c.Transportation_Provider__r = a;
        c.Transportation_Provider__r.name = a.Id;
        c.Drop_Off_Location__c = 'some dropoff location';
        c.Pickup_dateTime__c = Date.newInstance(2015, 08, 15);
        c.form_type__c = 'Hospital Discharge';
        c.of_Riders__c = '1';
        c.Steps__c = Decimal.valueOf('10');
        c.Authorization_Number__c = 'test';
        c.weight__c = Decimal.valueOf('200');
        c.Vehicle_Type__c = 'Livery';
        c.Description = 'some description';
        c.Special_Request__c = 'Has a Cane';        
        insert c;
        Case cList = [select id, entrance__c, room_number__c, bed_number__c, floor_number__c, contact.Name, contact.subscriber_id__c,Pickup_Location__c,Transportation_Provider__r.name,Drop_Off_Location__c,contact.Birthdate,contact.Phone,Pickup_dateTime__c,form_type__c,of_Riders__c,Steps__c,Authorization_Number__c,weight__c,Vehicle_Type__c,Description,Special_Request__c from Case where ID = :c.Id limit 1];  
        System.debug(cList);
        PageReference pageRef = Page.TransportationPDF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', c.Id);
        TransportationPDFController tc = new TransportationPDFController();
        tc.theId1 = c.Id;
        System.assert(tc.theCase != null);
        System.assert(tc.theCase.Form_Type__c.contains('Hospital'));
        System.assert(tc.renderHospitalInformation == true);
        system.assertNotEquals(null,tc.tz);
        system.assertNotEquals(null,tc.currentTime);
    }   

}