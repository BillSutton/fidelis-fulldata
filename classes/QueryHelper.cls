/*  
 * Retention Project
 *  Helper class that can be used to limit queries
 *  Developed By: Bill Sutton (Huron Consulting Group)
 *  Email: wsutton@huronconsultinggroup.com
 * 
 * NOTES:
 * All 'get' methods return null if nothing is found.
 * Any method related to contacts will use contrived keys
 * Any method related to Responsible Person will use the facets Responsible Person Id
 */ 
public class QueryHelper {
    //used to map contrived key to contact member ID
    public Static Map<String,contact> contactMap = new Map<String,contact>();
    public Static Map<String, Reponsible_Person__c> rpMap = new Map<String,Reponsible_Person__c>();
    
    Public QueryHelper(){
        
    }

    //combination call to allow us to populate both the contact map and the responsible person map at the same time.
    public static void populateRetentionMaps(List<String> ContactList, List<String>RpList){
        populateContactMap(ContactList);
        populateRpMap(RpList);
    }
    
    //used to map contrived key to contact member ID
    public static void populateContactMap(List<String> contriveList){
        list<Contact> contacList = [select id, Contrived_Key__c,Birthdate, FirstName, LastName, Enrolled__c from Contact where Contrived_Key__c in :contriveList];
        for(Contact c :contacList){
            contactMap.put(c.Contrived_Key__c, c);
        }
        system.debug('ContactMap'+ContactMap);
    }
	
    //Populate RP Map From API Input
    public static void populateRpMap(List<String> rpList){
        for(Reponsible_Person__c rp :[select id, Resp_Person_ID__c from Reponsible_Person__c where Resp_Person_ID__c in :rpList]){
            rpMap.put(rp.Resp_Person_ID__c, rp);
        }
        system.debug('RP MAP'+rpMap);
    }    
    //used to get the contactId based on the Contrived Key
    public static Id getContactIdByContrivedKey(String contrived) {
        system.debug('CK: '+contrived);
        if(contactMap.get(contrived) != null){
        	return contactMap.get(contrived).id;
        }
        else{
            return null;
        }
    }
    
    //used to get the responsible person SFDC id from the responsible person id sent from the api
    public static Id getRpByRpId(String rpId) {
        system.debug('RPID: '+rpId);
        if(rpMap.get(rpId) != null){
        	return rpMap.get(rpId).id;
        }
        else{
            return null;
        }
    }

    //get the brithdate of the contact by contrived key
    public static date getContactBirthdayByContrivedKey(String contrived){
        if(contactMap.get(contrived) != null){
        	return contactMap.get(contrived).Birthdate;
        }
        else{
            return null;
        }
    }

    //get the first name of the contact by contrived key
    public static String getContactFNameByContrivedKey(String contrived){
        if(contactMap.get(contrived) != null){
        	return contactMap.get(contrived).FirstName;
        }
        else{
            return null;
        }
    }

    //get the last name of the contact by contrived key
    public static String getContactLNameByContrivedKey(String contrived){
        if(contactMap.get(contrived) != null){
        	return contactMap.get(contrived).LastName;
        }
        else{
            return null;
        }
	}
}