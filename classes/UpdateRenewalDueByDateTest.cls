/*
	Created by: Greg Hacic
	Last Update: 20 September 2016 by Greg Hacic
	Questions?: ghacic@fideliscare.org
	
	Notes:
		- tests UpdateRenewalDueByDate.class (93.18% coverage)
*/
@isTest
private class UpdateRenewalDueByDateTest {
	
	//tests UpdateRenewalDueByDate.class
	static testMethod void allLogic() {
		//BEGIN: Some Setup Items... 
		//create some Account records
		List<Account> accounts = new List<Account>();
		accounts.add(new Account(Name = 'Tess Trucking'));
		accounts.add(new Account(Name = 'Tess Financial'));
		insert accounts;
		//create some Contact records
		List<Contact> contacts = new List<Contact>();
		contacts.add(new Contact(AccountId = accounts[0].Id, Contrived_Key__c = '9999990000000001', FirstName = 'Tess', LastName = 'Dachshund'));
		contacts.add(new Contact(AccountId = accounts[1].Id, Contrived_Key__c = '9999990000000002', FirstName = 'Grachus', LastName = 'Dachshund'));
		contacts.add(new Contact(AccountId = accounts[1].Id, Contrived_Key__c = '9999990000000003', FirstName = 'Judy', LastName = 'Dachshund'));
		contacts.add(new Contact(AccountId = accounts[1].Id, Contrived_Key__c = '9999990000000004', FirstName = 'Pete', LastName = 'Dachshund'));
		insert contacts;
		//create some Renewal_Due_Date__c records
		List<Renewal_Due_Date__c> renewalDueDates = new List<Renewal_Due_Date__c>();
		renewalDueDates.add(new Renewal_Due_Date__c(Day_of_Month__c = 15.0, Enrollment_Source__c = 'Marketplace', LOB__c = 'CHP', Name = 'Marketplace - CHP'));
		renewalDueDates.add(new Renewal_Due_Date__c(Day_of_Month__c = 0.0, Enrollment_Source__c = 'Default', LOB__c = '', Name = 'Default'));
		renewalDueDates.add(new Renewal_Due_Date__c(Day_of_Month__c = 15.0, Enrollment_Source__c = 'Marketplace', LOB__c = 'NYM', Name = 'Marketplace - NYM'));
		renewalDueDates.add(new Renewal_Due_Date__c(Day_of_Month__c = 15.0, Enrollment_Source__c = 'Marketplace', LOB__c = 'EP', Name = 'Marketplace - EP'));
		insert renewalDueDates;
		//grab the appropriate RecordType Id
		RecordType caseRenewalRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Renewals'];
		//create some Case records
		List<Case> cases = new List<Case>();
		cases.add(new Case(ContactId = contacts[0].Id, RecordTypeId = caseRenewalRecordType.Id, Subject = 'Unit Testing Case 1'));
		cases.add(new Case(ContactId = contacts[1].Id, RecordTypeId = caseRenewalRecordType.Id, Subject = 'Unit Testing Case 2'));
		insert cases;
		//create some Applicant_Information__c records
		List<Applicant_Information__c> applicants = new List<Applicant_Information__c>();
		applicants.add(new Applicant_Information__c(Applicant_First_Name__c = 'Tess', Case_Number__c = cases[0].Id, Contact__c = contacts[0].Id, EX__c = 'YYYY001', Line_of_Business__c = 'EP', Member_ID__c = 'GHIJKL001', Recert_Date__c = Date.Today().addMonths(21)));
		applicants.add(new Applicant_Information__c(Applicant_First_Name__c = 'Grachus', Case_Number__c = cases[1].Id, Contact__c = contacts[1].Id, EX__c = 'XXXX001', Line_of_Business__c = 'CHP', Member_ID__c = 'ABCDEF001', Recert_Date__c = Date.Today().addDays(30), Renewal_Method__c = 'Marketplace'));
		applicants.add(new Applicant_Information__c(Applicant_First_Name__c = 'Judy', Case_Number__c = cases[1].Id, Contact__c = contacts[2].Id, EX__c = 'XXXX002', Line_of_Business__c = 'CHP', Member_ID__c = 'ABCDEF002', Recert_Date__c = Date.Today().addDays(30), Renewal_Method__c = 'Marketplace'));
		applicants.add(new Applicant_Information__c(Applicant_First_Name__c = 'Pete', Case_Number__c = cases[1].Id, Contact__c = contacts[3].Id, EX__c = 'XXXX003', Line_of_Business__c = 'CHP', Member_ID__c = 'ABCDEF003', Recert_Date__c = Date.Today().addDays(30), Renewal_Method__c = 'Marketplace'));
		insert applicants;
		//create a map for use in our initial method
		Map<Id, Applicant_Information__c> applicantsToProcess = new Map<Id, Applicant_Information__c>();
		applicantsToProcess.put(applicants[0].Id, applicants[0]);
		applicantsToProcess.put(applicants[1].Id, applicants[1]);
		applicantsToProcess.put(applicants[2].Id, applicants[2]);
		applicantsToProcess.put(applicants[3].Id, applicants[3]);
		//END: Some Setup Items...
		
		Test.startTest(); //denote testing context
		
		UpdateRenewalDueByDate.runLogic(applicantsToProcess);
		
		Test.stopTest(); //revert from testing context
	}

}