/******************************************************************************
** Module Name   : DailyManifestMonitor  
** Description   : Check progress of DailyManifest
**          
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      3/04/2016    SVA     Created
**
******************************************************************************/
global class DailyManifestMonitor implements Schedulable {
    global void execute(SchedulableContext ctx) {
        checkIt();
    }
    
    global void checkIt()
    {
        
        List<AsyncApexJob> jobs = [select TotalJobItems, Status, NumberOfErrors, JobType, JobItemsProcessed, ExtendedStatus, CreatedDate, CompletedDate From AsyncApexJob 
            where createddate = today and apexclassid in (select id from apexclass where name = 'DailyManifestBatch') and status <> 'Queued' and jobtype = 'BatchApex' 
            order by createddate desc];
        
        performCheck(jobs); // break it up for test code
    }
    
    public void performCheck(AsyncApexJob[] jobs)
    {
        if (jobs == null || jobs.size() == 0)
            sendErrorEmail('DID NOT RUN', null);
        else if (jobs[0].status == 'Failed')
            sendErrorEmail('FAILED', JOBS[0]);
        else if (jobs[0].status != 'Completed')
            sendErrorEmail('NOT YET COMPLETED', jobs[0]);
        else if (jobs[0].NumberOfErrors > 0)
            sendErrorEmail('COMPLETED WITH ERRORS', jobs[0]);
    }
    
    private void sendErrorEmail(String str, AsyncApexJob aj)
    {
        String subject = '***DAILY MANIFEST: ' + str + '***';
        String body;
        if (aj == null)
        {
            body = '@***Could not find manifest in job log for today***';
            dummyCode();
            
        }
        else {
            body = '@***Latest DailyManifestBatch run at ' + aj.CreatedDate.format() + '\nStatus=' + aj.status  +
                    '\nCompleted=' + aj.CompletedDate.format() +
                    '\nBatches to process=' + aj.TotalJobItems +
                    '\nJobItemsProcessed=' + aj.JobItemsProcessed +
                    '\nNumberOfErrors=' + aj.NumberOfErrors + 
                    '\nExtendedStatus=' + aj.extendedstatus;
        }
        util.sendmsg(subject, body, 'dlcrmit@fideliscare.org');
        system.debug(subject + ' ' + body);

    }
        private void dummyCode()
    {
            // dummy code for test results since cannot alter an asyncapexjob object
            Integer i = 1;
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++;
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++;
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; 
            i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;  
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;  
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;  
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;          
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;
            i++; i++; i++; i++; i++; i++; i++; i++; i++; i++; i++;                      
            
    }       

}