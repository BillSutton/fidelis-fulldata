public class TransportationProviderSearchController {
    private final CaseSidePanelController csp {get;set;}
    
    public Account theAcct {get;set;}
    public Account selAcct {get;set;}
    public Account newAcct {get;set;}
    public Contact newContact {get;set;}
    public List<Account> lstAcctRec {get;set;}
    public List<Account> lstAcctOther {get;set;}
    public Case theCase {get;set;}
    public Map<Id,Decimal> capacityMap {get;set;}
    public String selId {get;set;}
    public String dayOfWeek {get;set;}
    public String strQuery {get;set;}
    public Boolean isRecurring {get;set;}
    public Boolean isInNetworkVO {get;set;}
    public Set<DateTime> setRecurringDTs{get;set;}
    public boolean fieldsValidated {get;set;}
    public boolean skipVOConfirm {get;set;}
    public boolean doneSaveSelect{get;set;}
    
    public TransportationProviderSearchController(CaseSidePanelController Ctrl) {

        //this.theCase = (Case)Ctrl.getTheCase();
    }    

    //Used with Visualforce remoting to return datetimes as strings
    public Set<String> getSetRecurringDTsString(){
        Set<String> retSet = new Set<String>();
        for(DateTime t : setRecurringDTs){
            retSet.add('"'+t.format('yyyy-MM-dd hh:mm:ss')+'"');
        }
        return retSet;
    }
    //Remote action to get all necessary providers to javascript
    @RemoteAction
    public static List<ProviderWrapper> getProvidersRA(Boolean isRecommended,Id theCaseId,Boolean isRecurring,List<String> setRecurringDTsStr){
        system.debug(setRecurringDTsStr);
        Set<DateTime> setRecurringDTs = new Set<DateTime>();
        for(String s : setRecurringDTsStr){
            DateTime dt = Datetime.valueOf(s);
            setRecurringDTs.add(dt);
        }
        Case theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id=:theCaseId];
        List<ProviderWrapper> lstAcct = new List<ProviderWrapper>();

        //Recommended Checks
        if(isRecommended==true){
            List<Account> lstAcctTmp = new List<Account>();
            // find the day of the week to query correct fields
            String dayOfWeek = theCase.Pickup_Datetime__c.format('E');
            String dayOpen;
            String dayClose;
            if (dayOfWeek.equalsIgnoreCase('sat')){
                dayOpen = 'Open_Hours_of_Operation_Saturday__c';
                dayClose = 'Closed_Hours_of_Operation_Saturday__c';
            } else if (dayOfWeek.equalsIgnoreCase('sun')){
                dayOpen = 'Open_Hours_of_Operation_Sunday__c';
                dayClose = 'Closed_Hours_of_Operation_Sunday__c';
            } else {
                dayOpen = 'Open_Hours_of_Operation_Weekdays__c';
                dayClose = 'Closed_Hours_of_Operation_Weekdays__c';
            } 
                    
            String strQuery =  'SELECT Id, Name,EIN__c, Servicing_Pickup_Counties__c,BillingCity, Servicing_Dropoff_Counties__c, Daily_Trip_Capacity__c, Phone, Facets_ID__c, Type, '+
                        'Open_Hours_of_Operation_Saturday__c, Open_Hours_of_Operation_Sunday__c, Open_Hours_of_Operation_Weekdays__c, Hours_of_Operation__c, '+
                        'Closed_Hours_of_Operation_Saturday__c, Closed_Hours_of_Operation_Sunday__c, Closed_Hours_of_Operation_Weekdays__c, Type_of_Vehicle__c, '+
                        '( SELECT Id, Line_of_Business__c, Effective_Date__c, Termination_Date__c FROM Transportation_Contract_Histories__r ) '+
                        'FROM Account ' +
                        'WHERE RecordType.DeveloperName = \'Transportation_Provider\' AND Type = \'In Network\' AND Facets_ID__c != null AND Status__c = \'Active\' ' +
                        'AND Servicing_Pickup_Counties__c INCLUDES (\''+theCase.Pickup_County__c+'\') AND Servicing_Dropoff_Counties__c INCLUDES (\''+theCase.Drop_Off_County__c+'\') '+
                        'AND Type_of_Vehicle__c INCLUDES (\''+theCase.Vehicle_Type__c+'\') AND '+dayOpen+' != null AND '+dayOpen+' != \'Closed\' AND '+dayClose+' != null AND '+dayClose+' != \'Closed\' ';
            
            system.debug(strQuery);
            List<Account> lstTemp = Database.query(strQuery);
            
            //get time of pickup and return
            Time pickupTime = theCase.Pickup_Datetime__c.time();
            Time dropoffTime;
            if ( theCase.Return_Datetime__c != null ){
                dropoffTime = theCase.Return_Datetime__c.time().addHours(1);
            } else {
                dropoffTime = pickupTime.addHours(4);
            }
            system.debug('The Lst Of Accounts: '+lstTemp);
            // parse out open or close pickist time.  If pickup datetime is within HoH, add to lstAcctRec
            for (Account acc : lstTemp){
                Time openTime;
                Time closeTime;
                system.debug('Account Name: '+acc.Name);
                if (dayOfWeek.equalsIgnoreCase('sat')){
                        openTime = getTimeFromDT(acc.Open_Hours_of_Operation_Saturday__c);
                        closeTime = getTimeFromDT(acc.Closed_Hours_of_Operation_Saturday__c);
                } else if (dayOfWeek.equalsIgnoreCase('sun')){
                        openTime = getTimeFromDT(acc.Open_Hours_of_Operation_Sunday__c);
                        closeTime = getTimeFromDT(acc.Closed_Hours_of_Operation_Sunday__c);
                } else {
                        openTime = getTimeFromDT(acc.Open_Hours_of_Operation_Weekdays__c);
                        closeTime = getTimeFromDT(acc.Closed_Hours_of_Operation_Weekdays__c);           
                }
                for ( Transportation_Contract_History__c tch : acc.Transportation_Contract_Histories__r){
                    system.debug('the Contract History: '+tch);
                    if(tch.Line_of_Business__c == theCase.Contact.Line_of_Business__c && tch.Effective_Date__c <= theCase.Pickup_Datetime__c && (tch.Termination_Date__c == null || tch.Termination_Date__c >= theCase.Pickup_Datetime__c)){
                        system.debug(openTime);
                        system.debug(pickupTime);
                        system.debug(closeTime);
                        system.debug(dropoffTime);
                        if(openTime!= null && closeTime != null){
                            Time allDay = Time.newInstance(0,0,0,0);
                            if(opentime == allDay){
                                if(!isRecurring){
                                    lstAcctTmp.add(acc);
                                } else {
                                    // if ALL dates in url array fit within hours of operation, add to lstAccRec
                                    // 
                                    // check day of week for EACH date
                                    Boolean addToList = true;
                                    for(DateTime dt : setRecurringDTs){
                                        if(dt.date() < tch.Effective_Date__c || dt.date() > tch.Termination_Date__c){
                                            addToList = false;
                                        }

                                    }

                                    if(addToList){
                                        lstAcctTmp.add(acc);
                                    }
                                }
                            } else if (pickupTime >= openTime && dropoffTime < closeTime){
                                // list of recommended vendor accounts
                                if(!isRecurring){
                                    lstAcctTmp.add(acc);
                                } else {
                                    // if ALL dates in url array fit within hours of operation, add to lstAccRec
                                    // 
                                    // check day of week for EACH date
                                    Boolean addToList = true;
                                    for(DateTime dt : setRecurringDTs){
                                        if(dt.date() < tch.Effective_Date__c || dt.date() > tch.Termination_Date__c){
                                            if(dt.time() > openTime && dt.time() < closeTime){
                                                addToList = false;
                                            }
                                        }

                                    }

                                    if(addToList){
                                        lstAcctTmp.add(acc);
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
            system.debug('List After Time Check: '+lstAcctTmp);
            // determine capacity % for each vendor
            Map<Id,List<Case>> mapCase = new Map<Id,List<Case>>();
            //capacityMap = new Map<Id,Decimal>();
            for (Account acc : lstAcctTmp){ 
                mapCase.put(acc.ID, New List<Case>()); 
            //    capacityMap.put(acc.ID, 0);
            }
            //Capcacity calculation
            for (Case c : [SELECT Id, of_Trips__c, Transportation_Provider__c FROM Case 
                           WHERE Transportation_Provider__c IN :mapCase.keyset() AND RecordType.DeveloperName = 'Transportation' AND day_only(convertTimeZone(Pickup_Datetime__c)) = :theCase.Pickup_Datetime__c.date() 
                            AND Id != :theCase.Id And Status!='Cancelled']){
                               mapCase.get(c.Transportation_Provider__c).add(c);
                           }
            
            for (Account acc : lstAcctTmp){
                Decimal totalTrips = 0;
                system.debug('cases: '+mapCase.get(acc.Id));
                for (Case c : mapCase.get(acc.Id)){
                    if(c <> null){
                        totalTrips += 1;
                    }
                }
                system.debug('totalTrips: '+totalTrips);
                system.debug('tripcap: '+acc.Daily_Trip_Capacity__c);
                Decimal cap = 0.00;
                if(acc.Daily_Trip_Capacity__c!=null && acc.Daily_Trip_Capacity__c>0){
                    cap = totalTrips.divide(acc.Daily_Trip_Capacity__c, 2)*100;
                }
                ProviderWrapper pw = new ProviderWrapper(acc);
                pw.capacity = cap;
                lstAcct.add(pw);
            }
            System.debug('List After Capacity Check: '+lstAcct);
            
            if (lstAcctTmp.size() <= 0){
                Transportation_User_Error_Messages__c tpsError = Transportation_User_Error_Messages__c.getValues('Transp Provider Search No Result');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,tpsError.Transportation_Error_Msg_Detail__c));
            }

        }else{
            // list of vendor override accounts
            List<Account> lstAcctTmp = [SELECT Id,Hours_of_Operation__c, BillingCity,Name, Type, Servicing_Pickup_Counties__c, Servicing_Dropoff_Counties__c, Daily_Trip_Capacity__c, Open_Hours_of_Operation_Saturday__c, 
                            Open_Hours_of_Operation_Sunday__c, Open_Hours_of_Operation_Weekdays__c, Closed_Hours_of_Operation_Saturday__c, Closed_Hours_of_Operation_Sunday__c, 
                            Phone, Facets_ID__c,EIN__c, Closed_Hours_of_Operation_Weekdays__c, Type_of_Vehicle__c
                            FROM Account WHERE RecordType.DeveloperName = 'Transportation_Provider' AND Facets_ID__c != null AND Status__c = 'Active'
                            ORDER BY Type Desc, Name Asc];
            for(Account acc : lstAcctTmp){
                lstAcct.add(new ProviderWrapper(acc));
            }
        }
        return lstAcct;
    }
    //Used to return providers back to the page
    public class ProviderWrapper{
        public Account a{get;set;}
        public Decimal capacity{get;set;}
        public ProviderWrapper(Account a){
            this.a = a;
            capacity = 0;
        }
    }
    public TransportationProviderSearchController(){
        RecordType theRT = [SELECT Id FROM RecordType WHERE DeveloperName='Transportation_Provider'];
        theAcct = new Account(RecordTypeId = theRT.Id);
        newAcct = new Account(RecordTypeId = theRT.Id);
        newContact = new Contact();
        String caseId = apexpages.currentpage().getparameters().get('caseId'); 
        isRecurring =  apexpages.currentpage().getparameters().get('isRecurring') != null ? true : false;
        String DateTimeList = apexpages.currentpage().getparameters().get('dateList');
        setRecurringDTs = new Set<DateTime>();
        Decimal totalTripMileage = decimal.valueOf(apexpages.currentpage().getparameters().get('totalTripMileage'));
        
        system.debug(DateTimeList);
        if(isRecurring && DateTimeList <> null){
            // get recurring date array from url: apexpages.currentpage().getparameters().get('dateList')
            // Sample String:
            // dateList=[java.util.GregorianCalendar[time=1437592962000,areFieldsSet=true,areAllFieldsSet=true,lenient=true,zone=sun.util.calendar.ZoneInfo[id=%22GMT%22,offset=0,dstSavings=0,useDaylight=false,transitions=0,lastRule=null],firstDayOfWeek=1,minimalDaysInFirstWeek=1,ERA=1,YEAR=2015,MONTH=6,WEEK_OF_YEAR=30,WEEK_OF_MONTH=4,DAY_OF_MONTH=22,DAY_OF_YEAR=203,DAY_OF_WEEK=4,DAY_OF_WEEK_IN_MONTH=4,AM_PM=1,HOUR=7,HOUR_OF_DAY=19,MINUTE=22,SECOND=42,MILLISECOND=0,ZONE_OFFSET=0,DST_OFFSET=0],%20java.util.GregorianCalendar[time=1438111362000,areFieldsSet=true,areAllFieldsSet=true,lenient=true,zone=sun.util.calendar.ZoneInfo[id=%22GMT%22,offset=0,dstSavings=0,useDaylight=false,transitions=0,lastRule=null],firstDayOfWeek=1,minimalDaysInFirstWeek=1,ERA=1,YEAR=2015,MONTH=6,WEEK_OF_YEAR=31,WEEK_OF_MONTH=5,DAY_OF_MONTH=28,DAY_OF_YEAR=209,DAY_OF_WEEK=3,DAY_OF_WEEK_IN_MONTH=4,AM_PM=1,HOUR=7,HOUR_OF_DAY=19,MINUTE=22,SECOND=42,MILLISECOND=0,ZONE_OFFSET=0,DST_OFFSET=0]]
                while (String.isNotEmpty(DateTimeList)){
                String timeLiteral = '[time=';
                String milliseconds;
                DateTime dt;
                DateTimeList = DateTimeList.substringAfter(timeLiteral);
                milliseconds = DateTimeList.substring(0, 13);
                dt = DateTime.newInstance(Long.valueOf(milliseconds));
                setRecurringDTs.add(dt);
                if(!DateTimeList.contains(timeLiteral)){
                    DateTimeList = '';
                }
            }
            system.debug(setRecurringDTs);

        }
        isInNetworkVO = false;
        fieldsValidated = true;
        skipVOConfirm = false;
        
        if (totalTripMileage > 999)
        {
         	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please check your address criteria again for drop-off and pickup location by closing this tab and going back to the address section. This total trip mileage is ' +  totalTripMileage));
            return;
        }
        
        if (caseId == null || String.isEmpty(caseId)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No Case Id Specified. /apex/TransportationProviderSearch?caseId=[case.Id]'));
            //setRecVend = new ApexPages.StandardSetController(new List<Account>());
            //setORVend = new ApexPages.StandardSetController(new List<Account>());
            return;
        }
                    
        theCase = [SELECT Id, Pickup_Datetime__c, Pickup_County__c, Return_Datetime__c, Drop_Off_County__c, Contact.Line_of_Business__c, Vehicle_Type__c, of_Riders__c,
                   Accepting_Medicare_or_Medicaid_Rates__c, Procedure_Code__c, Rational_for_LoA__c, Reason_for_LoA__c
                   FROM Case Where Id=:caseId];
        theCase.Accepting_Medicare_or_Medicaid_Rates__c = 'Yes';
        doneSaveSelect = false;
    }
    
    // gets the actual time out of a AM/PM picklist
    public static Time getTimeFromDT(String timeValue){
        String strTime = timeValue.substring(0,timeValue.indexOf(':')+3).trim();
        Boolean isPM = timeValue.contains('PM') ? true : false;
        strTime = strTime.length() == 4 ? '0'+strTime : strTime;
        Time theTime;  
        if (isPM){
            theTime = Time.newInstance(integer.valueof(strTime.substring(0,2))+12,integer.valueof(strTime.substring(3,5)),0,0);
        } else if (strTime.substring(0,2) == '12'){
            theTime = Time.newInstance(0,0,0,0);
        } else {
            theTime = Time.newInstance(integer.valueof(strTime.substring(0,2)),integer.valueof(strTime.substring(3,5)),0,0);
        }
        
        return theTime;
    }
    
    // select a recommended account
    public void selectAcct(){
        system.debug('I HAVE DONE THE THING!');
        system.debug('selId: '+selId);
        List<Account> tmpAcctLst;
        if (selID != null && !String.IsEmpty(selID)){
            tmpAcctLst = [SELECT Id, Name, Servicing_Pickup_Counties__c, Daily_Trip_Capacity__c 
                          FROM Account WHERE Id=:selID];
            if (tmpAcctLst.size() > 0){
                selAcct = tmpAcctLst[0];
            }
            theCase.Transportation_Provider__c = selId;
            system.debug('TheCase: '+theCase);
            theCase.Accepting_Medicare_or_Medicaid_Rates__c = null;
            update theCase;
        }
    }
    
    // select a vendor override account
    public void selectVOAcct(){
        system.debug('selId: '+selId);
        List<Account> tmpAcctLst;
        if (selID != null){
            tmpAcctLst = [SELECT Id, Name, Type FROM Account WHERE Id=:selID];
            if (tmpAcctLst.size() > 0){
                selAcct = tmpAcctLst[0];
                system.debug(selAcct.Type);
                if(selAcct.Type == 'In Network'){
                    theCase.Transportation_Provider__c = selId;
                    skipVOConfirm = false;
                    
                    Contact c = [Select Id, Line_Of_Business__c from Contact where Id = :theCase.ContactId LIMIT 1];
                    List<Transportation_Contract_History__c> listTCH = [Select Id, Account__c,Line_Of_Business__c, Effective_Date__c, Termination_Date__c from Transportation_Contract_History__c where Account__c = :selAcct.Id];
                    if(listTCH.size() > 0){
                        for(Transportation_Contract_History__c tch : listTCH){
                            if(tch.Line_of_Business__c == c.Line_of_Business__c){
                                if(tch.Effective_Date__c <= theCase.Pickup_Datetime__c && (tch.Termination_Date__c == null || tch.Termination_Date__c >= theCase.Pickup_Datetime__c)){
                                    skipVOConfirm = true;
                                }
                            }
                        }
                    } else {
                        skipVOConfirm = false;
                    }
                    
                    if(skipVOConfirm){
                        theCase.Accepting_Medicare_or_Medicaid_Rates__c = null;
                        update theCase;
                    }
                }
            }
        }
    }
    // select a vendor override account
    public void finishVO(){
        if( newContact.LastName == null || newContact.Phone == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please complete all required fields before saving.'));
                fieldsValidated = false;
                return;
        }
        
        if(!Pattern.Matches('^(\\+0?1\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$', newContact.Phone)){
            newContact.Phone.addError('Not a valid phone number');
            fieldsValidated = false;
            return;
        } else {
            fieldsValidated = true;
        }

        try{
        User theUser = [SELECT Id, ManagerId FROM User WHERE Id=:UserInfo.getUserId()];
        if(theCase.Accepting_Medicare_or_Medicaid_Rates__c != 'Yes'){
            if(  newContact.LastName == null || newContact.Phone == null || theCase.Procedure_Code__c == null || theCase.Rational_for_LoA__c == null ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please complete all required fields before saving.'));
                fieldsValidated = false;
            } else {
                newContact.LastName = newContact.LastName.trim();
                if(newContact.LastName.contains(' ')){
                    String[] nameSplit = newContact.LastName.split(' ',2);
                    newContact.FirstName = nameSplit[0];
                    newcontact.LastName = nameSplit[1];
                }
                
                if(theCase.Accepting_Medicare_or_Medicaid_Rates__c == 'No'){
                    theCase.Service_Date_on_LoA__c = theCase.Pickup_Datetime__c.date().format();
                }
                theCase.Transportation_Provider__c = selId;
                if(newContact.FirstName == null){
                    theCase.Contact_Person_for_LoA__c = newContact.LastName;
                } else {
                        theCase.Contact_Person_for_LoA__c = newContact.FirstName + ' ' + newContact.LastName;
                }
                theCase.Contact_Number_for_LoA__c = newContact.Phone;
                
                system.debug(selId);
                system.debug(newContact.LastName);
                system.debug(newContact.FirstName);
                system.debug(newContact.Phone);
                system.debug(theCase.Procedure_Code__c);
                system.debug(theCase.Reason_for_LoA__c);
                system.debug(theCase.Rational_for_LoA__c);
                
                update theCase;
            }
        } else {
            if( newContact.LastName == null || newContact.Phone == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Required fields missing.'));
            } else {
                newContact.LastName = newContact.LastName.trim();
                if(newContact.LastName.contains(' ')){
                    String[] nameSplit = newContact.LastName.split(' ',2);
                    newContact.FirstName = nameSplit[0];
                    newcontact.LastName = nameSplit[1];
                }
                
                
                if(theCase.Accepting_Medicare_or_Medicaid_Rates__c == 'No'){
                    theCase.Service_Date_on_LoA__c = theCase.Pickup_Datetime__c.date().format();
                }
                theCase.Transportation_Provider__c = selId;
                if(newContact.FirstName == null){
                    theCase.Contact_Person_for_LoA__c = newContact.LastName;
                } else {
                    theCase.Contact_Person_for_LoA__c = newContact.FirstName + ' ' + newContact.LastName;
                }
                theCase.Contact_Number_for_LoA__c = newContact.Phone;
                
                system.debug(selId);
                system.debug(newContact.LastName);
                system.debug(newContact.FirstName);
                system.debug(newContact.Phone);
                system.debug(theCase.Procedure_Code__c);
                system.debug(theCase.Reason_for_LoA__c);
                system.debug(theCase.Rational_for_LoA__c);
                
                update theCase;
            }
        }
        } catch (System.Exception e) {
            system.debug(e);
        }
    }
    public Boolean checkBlankNull(String s){
        return (s==null || s == '');
    }
    // creates a new out of network vendor account
    public void createAcct(){
        Savepoint sp = Database.setSavepoint();
        try{
        if(theCase.Accepting_Medicare_or_Medicaid_Rates__c != 'Yes' && (checkBlankNull(newAcct.Name ) || checkBlankNull(newAcct.EIN__c ) || checkBlankNull(newAcct.BillingStreet ) || checkBlankNull(newAcct.BillingCity ) || checkBlankNull(newAcct.Fax ) || checkBlankNull(newAcct.BillingState ) || checkBlankNull(newAcct.BillingPostalCode ) || checkBlankNull(newContact.LastName ) || checkBlankNull(newContact.Phone ) || theCase.Accepting_Medicare_or_Medicaid_Rates__c == null || checkBlankNull(theCase.Procedure_Code__c) || checkBlankNull(theCase.Rational_for_LoA__c))){
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Required fields missing.'));
               return;
        } else if (theCase.Accepting_Medicare_or_Medicaid_Rates__c == 'Yes' && (checkBlankNull(newAcct.Name ) || checkBlankNull(newAcct.EIN__c ) || checkBlankNull(newAcct.BillingStreet ) || checkBlankNull(newAcct.BillingCity ) || 
                   checkBlankNull(newAcct.Fax ) || checkBlankNull(newAcct.BillingState ) || checkBlankNull(newAcct.BillingPostalCode ) || checkBlankNull(newContact.LastName ) || checkBlankNull(newContact.Phone ) || 
                   theCase.Accepting_Medicare_or_Medicaid_Rates__c == null)) {
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Required fields missing.'));
               return;
        } else {    
            RecordType theRT = [SELECT Id FROM RecordType WHERE DeveloperName='Transportation_Provider'];
            newAcct.RecordTypeId = theRt.Id;
            newAcct.Type = 'Out of Network';
            newAcct.Status__c = 'Active';
            
            insert newAcct;
            
            newcontact.Email = newAcct.PersonEmail;
            newcontact.AccountId = newAcct.Id;
            newContact.LastName = newContact.LastName.trim();
            if(newContact.LastName.contains(' ')){
                String[] nameSplit = newContact.LastName.split(' ',2);
                newContact.FirstName = nameSplit[0];
                newcontact.LastName = nameSplit[1];
            }
            insert newcontact;
            
            User theUser = [SELECT Id, ManagerId FROM User WHERE Id=:UserInfo.getUserId()];
            
            Task newTask = new Task();
                newTask.Subject = 'Please route a CSI in Facets';
                newTask.Description = 'Please route a CSI to request this OON provider be loaded in Facets';
                newTask.IsReminderSet = TRUE;
                newTask.ReminderDateTime = dateTime.now();
                newTask.ActivityDate = date.today();
                newTask.WhatId = newAcct.Id;
                newTask.OwnerId = theUser.Id;
            insert newTask;


            system.debug('About to create the addProviderId Task');
            Task addProviderId = new Task();
            addProviderId.Subject = 'Add Provider Id to the Auth Number';
            addProviderId.Description = 'Please add provider ID to the corresponding authorization number in Facets.';
            addProviderId.IsReminderSet = true;
            addProviderId.ReminderDateTime = dateTime.now().addDays(2);
            addProviderId.ActivityDate = date.today().addDays(2);
            addProviderId.WhatId = newAcct.id;
            addProviderId.OwnerId = theUser.Id;
            insert addProviderId;

            system.debug('supervisor note');
            TransportationNotesSupervisor__c noteSupName = TransportationNotesSupervisor__c.getOrgDefaults();
            system.debug('Custom Setting values: '+ noteSupName);
            if(noteSupName!= null && noteSupName.Name_For_Query__c!=null){
                List<User> noteSupList = [Select Id from User Where userName = :noteSupName.Name_For_Query__c];
                system.debug('supervisor user list: '+noteSupList);
                if(noteSupList!=null && noteSupList.size()>0){
                    User noteSupUser = noteSupList[0];
                    Task noteSupTask = new Task();
                        noteSupTask.Subject = 'Verify if OON provider is loaded in Facets';
                        noteSupTask.Description = 'Please add provider ID to the new provider profile in Salesforce';
                        noteSupTask.IsReminderSet = TRUE;
                        noteSupTask.ReminderDateTime = dateTime.now().addDays(2);
                        noteSupTask.ActivityDate = date.today().addDays(2);
                        noteSupTask.WhatId = newAcct.Id;
                        noteSupTask.OwnerId = noteSupUser.Id;
                    insert noteSupTask;
                }
            }

            if(theCase.Accepting_Medicare_or_Medicaid_Rates__c == 'No'){
                    theCase.Service_Date_on_LoA__c = theCase.Pickup_Datetime__c.date().format();
                }
            theCase.Transportation_Provider__c = newAcct.Id;
            if(newContact.FirstName == null){
                    theCase.Contact_Person_for_LoA__c = newContact.LastName;
                } else {
                        theCase.Contact_Person_for_LoA__c = newContact.FirstName + ' ' + newContact.LastName;
            }
            theCase.Contact_Number_for_LoA__c = newContact.Phone;
            
            system.debug(newAcct.Id);
            system.debug(newContact.LastName);
            system.debug(newContact.FirstName);
            system.debug(newContact.Phone);
            system.debug(theCase.Procedure_Code__c);
            system.debug(theCase.Reason_for_LoA__c);
            system.debug(theCase.Rational_for_LoA__c);
            
            update theCase;
            doneSaveSelect = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Out of Network Vendor created successfully.'));
        }
        } catch (System.Exception e) {
            system.debug(e);
            Database.rollback(sp);
        }
    }   
    
    // returns to vender override selection page 
    public void goBackToVO(){
        selAcct = null;
        theCase.Accepting_Medicare_or_Medicaid_Rates__c = 'Yes';
        isInNetworkVO = false;
    } 
    public void goBack(){
        
    }
}