public class TransportationAuthGridMassEditController{
    public List<Transportation_Authorization_Grid__c> selectedTags{get;set;}
    public ApexPages.StandardSetController standardController;
    public String msaNote{get;set;}
    public TransportationAuthGridMassEditController(ApexPages.StandardSetController sc) {
        selectedTags = sc.getSelected();
        standardController = sc;
    }
    public PageReference massSave(){
        for(Transportation_Authorization_Grid__c tag : selectedTags){
            tag.Note_for_MSA__c = msaNote;
        }
        return standardController.save();
    }
}