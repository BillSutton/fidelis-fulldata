@isTest public class DailyManifestControllerTest {
    
    static testMethod void test0(){
        RecordType rtAcct = [select Id from RecordType where sObjectType='Account' and DeveloperName = 'Transportation_Provider' limit 1];
        Account a = new Account(BillingStreet='123 main st',BillingState='NY',BillingPostalCode='14228',EIN__c='123456789',RecordTypeId=rtAcct.Id,Name = 'test', Last_Manifest_Sent_Date_Time__c = Datetime.newInstance(2015, 08, 15, 12, 00, 00), Preferred_Manifest_Delivery_Method__c = 'Fax', Fax = '0000000000', Status__c = 'Active');
        insert a;
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        
        Document d = new Document(Name = 'FidelisLogo_PDF', FolderId = Userinfo.getUserId());
        insert d;
        List<Document> documentList = [select name from document where Name='FidelisLogo_PDF'];
        
        List<Case> clist = new List<Case>();
        Case c = new Case(Transportation_Provider__c=a.Id,Last_Manifest_Sent_Date_Time__c = null, Status = 'Scheduled', Request_Type__c = 'Standard Request',Pickup_Datetime__c=DateTime.now().addDays(2),Return_Datetime__c=DateTime.now());
        clist.add(c);
        Case c2 = new Case(Transportation_Provider__c=a.Id,Last_Manifest_Sent_Date_Time__c = Datetime.now().addDays(-2), Status = 'Scheduled',Modified_DateTime__c=DateTime.now(), Request_Type__c = 'Standard Request',Pickup_Datetime__c=DateTime.now().addDays(2),Return_Datetime__c=DateTime.now());
        clist.add(c2);
        Case c3 = new Case(Transportation_Provider__c=a.Id, Status = 'Cancelled',Cancellation_DateTime__c = Datetime.now(), Last_Manifest_Sent_Date_Time__c = Datetime.now().addDays(-2), Request_Type__c = 'Standard Request',Pickup_Datetime__c=DateTime.now().addDays(2),Return_Datetime__c=DateTime.now());
        clist.add(c3);
        insert clist;
        DailyManifestController dmc = new DailyManifestController(sc);
        //System.assertEquals(1, dmc.newLst.size());
        //System.assertEquals(1, dmc.updateLst.size());
        //System.assertEquals(1, dmc.cancelLst.size());
    }

}