@isTest public class DailyManifestBatchTest {
    
    static testMethod void test0(){
        List<RecordType> rts = [Select id from RecordType where sObjectType = 'Account' and DeveloperName = 'Transportation_Provider'];

        Account a = new Account(Name = 'test',Type='In Network');
        if(rts!=null && rts.size()>0){
            a.RecordTypeId = rts[0].Id;
        }
        a.BillingStreet = '123 Main Street';
        a.BillingCity = 'Buffalo';
        a.BillingState = 'NY';
        a.BillingPostalCode = '14261';
        a.EIN__c = '123456789';
        insert a;
        
        DailyManifestBatch dmb = new DailyManifestBatch();
        Database.executeBatch(dmb);
        String CRON_EXP = '0 0 0 15 3 ? 2022';

        Test.startTest();
        System.schedule('DailyManifestBatchTest',
                        CRON_EXP,
                        new DailyManifestBatch());
    }
    
    /*static testMethod void Execute2(){
        Account a = new Account(Name = 'test');
        insert a;
        DailyManifestBatch dmb = new DailyManifestBatch();
        Database.BatchableContext BC;
        List<Account> aList = [select Id, Name from Account where Id = :a.Id];
        List<sObject> scope = aList;
        dmb.execute(BC, scope);
        
    }*/
    
    /*static testMethod void Finish(){
        DailyManifestBatch dmb = new DailyManifestBatch();
        Database.BatchableContext BC;
        dmb.finish(BC);
        
    }*/

}