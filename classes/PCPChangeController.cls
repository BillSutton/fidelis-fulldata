public class PCPChangeController {

    public List<resultsWrapper> results{get;set;}
    public String selectedItem {get;set;}
    public string pLastName{get;set;}
    public string pFirstName{get;set;}
    public string pWithin{get;set;}
    public string pStreet{get;set;}
    public string pCity{get;set;}
    public string pState{get;set;}
    public string pZip{get;set;}
    public string pPhone{get;set;}
    public string pGroup{get;set;}
    public string pSpeciality{get;set;}
    public string pFacilityName{get;set;}
    public integer arraySize{get;set;}
    public boolean renderResults{get;set;}
    public Case newCase{get;set;}
    public id callCenterID{get;set;}
    public id theContactID{get;set;}
    public list<familyList> theFamilyList{get;set;}
    public list<Contact> selectedFamilyMemebrs{get;set;}
    public boolean renderSearchArea{get;set;}
    public list<selectOption> radius {get;set;}
    public list<selectOption> pcpSpecialty{get;set;}
    public boolean showCreateCase{get;set;}
    public String AuthNoteId {get;set;}
    public List<selectOption> pcpStateList {get;set;}
    
    public void populatePcpStateList(){
        pcpStateList = new list<selectOption>();
        pcpStateList.add(new selectOption('NY','NY'));
        pcpStateList.add(new selectOption('NJ','NJ'));
        pcpStateList.add(new selectOption('CT','CT'));
        pcpStateList.add(new selectOption('VT','VT'));
        pcpStateList.add(new selectOption('PA','PA'));
    }

    public void populatePcpSpecialty(){
        pcpSpecialty = new list<selectOption>();
        List<Provider_Speciality__c> daList = Provider_Speciality__c.getAll().values();
        daList.sort();
        pcpSpecialty.add(new selectOption('','--None--'));
        for(Provider_Speciality__c ps: daList){
            pcpSpecialty.add(new selectOption(ps.Code__c,ps.Display_Name__c));
        }
    }
    
    public PCPChangeController(){
        populatePcpStateList();
        theFamilyList = new list<familyList>();
        renderResults = false;
        renderSearchArea = true;
        if(ApexPages.currentPage().getParameters().get('createCase') != null){
            showCreateCase = false;
        }
        else{
            showCreateCase = true;
        }
        if(showCreateCase){
            theContactId = ApexPages.currentPage().getParameters().get('id');
        }
        else{
            AuthNoteId = ApexPages.currentPage().getParameters().get('id');
        }
        pState = 'New York'; 
        results = new list<resultsWrapper>();
        newCase = new Case();
        radius = new list<selectOption>();
        radius.add(new selectOption('','--None--'));
        radius.add(new selectOption('5','5'));
        radius.add(new selectOption('10','10'));
        radius.add(new selectOption('15','15'));
        radius.add(new selectOption('20','20'));
        radius.add(new selectOption('25','25'));
        radius.add(new selectOption('50','50'));
        callCenterID = [select ID from recordType where sObjectType = 'Case' and Name = 'Call Center' limit 1].id;
        populatePcpSpecialty();
        system.debug('theID? '+ApexPages.currentPage().getParameters());
    }

    public void updateTransportNote(){
        system.debug('AuthNoteId: '+AuthNoteId);
        if(!String.isBlank(AuthNoteId)){
            Transportation_Authorization_Notes__c temp = [select id, Destination_Provider_Name__c,  Destination_Provider_Address__c,Destination_Provider_Specialty__c from Transportation_Authorization_Notes__c where id = :AuthNoteId limit 1];
            for(resultsWrapper rw :results){
                system.debug('in da loop: '+rw.rName);
                system.debug('The Selecm: '+selectedItem);
                system.debug('The Results: '+rw.rName == selectedItem);
                if(rw.rName.trim() == selectedItem.trim()){
                    //temp.Destination_Provider_Specialty__c = rw.Speciality;
                    temp.Destination_Provider_Address__c = rw.fullAddress;
                }
            }
            temp.Destination_Provider_Name__c = selectedItem;
            update temp;
        }
    }
    
    public void saveFamilyListPCPChange(){
        list<Case> insertList = new list<Case>();
        newCase.ContactId = theContactID;
        newCase.Date_Received__c=system.Today();
        newCase.RecordTypeId = callCenterID;
        insertList.add(newCase);
        system.debug('The Family List: '+theFamilyList);
        for(familyList c :theFamilyList){
            if(c.selected){
                case theCase = new Case(RecordTypeId = callCenterID, Status = 'Closed',contactId = c.theContact.ID, Description = newCase.Description ,Date_Received__c=system.Today());
                insertList.add(theCase);
            }
        }
        try{
            system.debug(insertList);
            insert insertList;
        }
        catch(Exception e){
            ApexPages.Message alertMessage = new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage());
            ApexPages.addMessage(alertMessage);
        }
    }
    
    public void getRelatedMembers(){
        renderResults = false;
        renderSearchArea = false;
        ID theFamilyID = [select accountID from contact where id = :theContactID limit 1].accountId;
        for(contact c :[select id, name,Primary_Care_Physician__c,OtherStreet,OtherCity,Home_County__c from contact where accountID  = :theFamilyID and ID != :theContactID]){
            theFamilyList.add(new familyList(c,false));
        }
        system.debug(thefamilyList);
    }
    
    public void pcpChangeSaveSingleCase(){
        newCase.ContactId = theContactId;
        newCase.RecordTypeId = callCenterID;
        newCase.Status = 'Closed';
        newCase.Date_Received__c=system.Today();
        newCase.RecordTypeId = callCenterID;
        try{
        insert newCase;
        }
        catch(Exception e){
            ApexPages.Message alertMessage = new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage());
            ApexPages.addMessage(alertMessage);
        }
    }
    
    public void clearPage(){
        pLastName = null;
        pFirstName = null;
        pWithin = null;
        pStreet = null;
        pCity = null;
        pZip = null;
        pGroup = null;
        pPhone = null;
        arraySize = null;
        results.clear();
        //return new pageReference('/apex/PCPChange');
    }
    public void PCPSelect(){
        system.debug('Did a Thing!');
        newCase.Description = 'PCP Change: '+selectedItem+' is effective on '+System.TODAY()+'.';
    }
    
    public void getProviders(){
        renderResults = true;
        results.clear();
        try{            
           FidelisProviderSearchParser parsedJson = FidelisParserTool.pSearchParser('{"myArray": '+FidelisAPIHelper.getProvidersBySearch(pFirstName,pLastName,pStreet,pCity,pState,pZip,pWithin,pSpeciality,pPhone,pFacilityName, null, theContactId)+'}');
           for(FidelisProviderSearchParser.arrayItem r :parsedJson.myArray){
                   results.add(new resultsWrapper(r.provider.name, r.provider.chpPanel, r.provider.address, r.provider.city, r.Proximity, r.provider.groupName, r.provider.phoneNumber, r.provider.zip));
           }
           renderResults = true;
        }
        Catch(System.Exception e){
        system.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, e.getMessage()));
        }
    }
    
    public class familyList{
        public boolean selected{get;set;}
        public Contact theContact{get;set;}
        
        public familyList(Contact c, Boolean s){
            theContact = c;
            selected = s;
        }
    }
    
    public class resultsWrapper{
        public string rName{get;set;}
        public string rNewPatients{get;set;}
        public string rNewStreet{get;set;}
        public string rCity{get;set;}
        public string rCounty{get;set;}
        public string rDistance{get;set;}
        public string rGroup{get;set;}
        public string rPhone{get;set;}
        public string fullAddress{get;set;}
        
        public resultsWrapper(String Name, String newPerson, String street, string city, string distance, string thegroup, string phone, string zipCode){
            rName = name;
            rNewPatients = newPerson;
            rNewStreet = street;
            rCity = city;
            rDistance = distance;
            rGroup = thegroup;
            rPhone = phone;
            fullAddress = street.trim()+' '+city.trim()+' '+'New York '+zipCode;
        }
    }
}