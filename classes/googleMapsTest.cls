@isTest
 private class googleMapsTest{
     
    static testMethod void testGoogleMaps() {
        String account1Address = EncodingUtil.urlEncode(
            + '576 Taunton Place' + ' '
            + 'Buffalo' + ', '
            + 'NY' + ' '
            + '14216' + ' '
            + 'USA' ,
            'UTF-8');
            
        String account2Address = EncodingUtil.urlEncode( 
            + '593 Laurel Run Rd' + ' '
            + 'Port matilda' + ', '
            + 'PA'  + ' '
            + '16870' + ' '
            + 'USA',
            'UTF-8');
        
        Case c = new Case();
        c.Total_Trip_Mileage__c = 100.00;
        insert c;
        Case cList = [select Id, Total_Trip_Mileage__c from Case where Id = :c.Id];
        
        Map<String, String> m = new Map<String, String>();
        m.put(account1Address, account2Address);
        
        googleMaps gm = new googleMaps();
        googleMaps.theCase = cList;
        googleMaps.travelTime = 10;
 
        googleMaps.getJson(m, c.Id);
        
        Decimal distance = googleMaps.getJson(account1Address, account2Address);
        system.assertEquals(191,distance);
    }
     
}