@isTest public class AuthGridQuickUpdateControllerTest {

    static testMethod void test0(){
		AuthGridQuickUpdateController controller = new AuthGridQuickUpdateController();   
        
        County__c c = new County__c();
        c.Name = 'test';
        insert c;
        controller.lstCounty = [SELECT Id, Name FROM County__c where Id = :c.Id limit 1];
        System.assertEquals(1, controller.lstCounty.size());
        
        controller.getsoPlan();
        controller.updatePlanList();
        controller.updateRecords();
    }
    
        static testMethod void test1(){
		AuthGridQuickUpdateController controller = new AuthGridQuickUpdateController();   
        
        County__c c = new County__c();
        c.Name = 'test';
        insert c;
        controller.lstCounty = [SELECT Id, Name FROM County__c WHERE Id = :c.Id];
        System.assertEquals(1, controller.lstCounty.size());
        
        Transportation_Plan__c p = new Transportation_Plan__c();
        p.Name = 'test';
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c();
        tag.County__c = c.Id;
        tag.Plan__c = p.Id;
        tag.Vehicle_Type__c = 'Ambulette';
        tag.Form_Type__c = 'Standard';
        tag.HD_Auth_Form__c = 'Ambulette'; 
        tag.Authorization_Number__c = 'test'; 
        tag.Effective_Date__c = Date.newInstance(2015, 08, 16);
        tag.Standard_Auth_Form__c = 'Ambulette';
        tag.Note_for_MSA__c = 'some note';
        tag.County_Covered__c = 'Yes';
        tag.Termination_Date__c = Date.newInstance(2015, 08, 31);
        insert tag;
        List<Transportation_Authorization_Grid__c> tagList = [select County__c, Plan__c, Vehicle_Type__c, Form_Type__c, HD_Auth_Form__c, Authorization_Number__c, Effective_Date__c, Standard_Auth_Form__c, Note_for_MSA__c, County_Covered__c, Termination_Date__c FROM Transportation_Authorization_Grid__c where Id = :tag.Id];
        System.assertEquals(1, tagList.size());
        Id aId = c.Id;    
        Map<Id, List<Transportation_Authorization_Grid__c>> m = new Map<Id, List<Transportation_Authorization_Grid__c>>();
        m.put(aId, tagList);
        controller.mapCountyPlan = m;  		
    }
    
    static testMethod void GetSoPlan(){
        AuthGridQuickUpdateController controller = new AuthGridQuickUPdateController();
        controller.selGroup = null;
        System.assertEquals(null, controller.selGroup);
        
        Transportation_Plan__c tpc = new Transportation_Plan__c();
        tpc.Name = '  ';
        tpc.CSPI_ID__c = 'test';
        insert tpc;
        controller.tmpPlanLst = [SELECT Id, Name, CSPI_ID__c FROM Transportation_Plan__c ORDER BY Name ASC limit 1];
        System.assertEquals(1, controller.tmpPlanLst.size());
        controller.getsoPlan();
    }
    
    static testMethod void UpdateRecords1(){
        AuthGridQuickUpdateController controller = new AuthGridQuickUpdateController();
        County__c c = new County__c();
        c.Name = 'test';
        insert c;
    	Transportation_Plan__c p = new Transportation_Plan__c();
        p.Name = 'test';
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c();
        tag.County__c = c.Id;
        tag.Plan__c = p.Id;
        tag.Vehicle_Type__c = 'Ambulette';
        tag.Form_Type__c = 'Standard';
        tag.HD_Auth_Form__c = 'Ambulette'; 
        tag.Authorization_Number__c = 'test'; 
        tag.Effective_Date__c = Date.newInstance(2015, 08, 16);
        tag.Standard_Auth_Form__c = 'Ambulette';
        tag.Note_for_MSA__c = 'null'; //If County_Covered__c = False, Note_for_MSA__c cannot be null
        tag.County_Covered__c = 'No';
        tag.Termination_Date__c = Date.newInstance(2015, 08, 31);
        insert tag;
        controller.tmpCP = tag;
        
        List<Id> a = new List<Id>{c.Id};
        List<Id> b = new List<Id>{p.Id};
        controller.selCounty = a;
        controller.selPlan = b;
        
        //Case: cp.Plan == p
        Id aId = c.Id;
        Id bId = p.Id;
        List<Transportation_Authorization_Grid__c> tagList = [select County__c, Plan__c, Vehicle_Type__c, Form_Type__c, HD_Auth_Form__c, Authorization_Number__c, Effective_Date__c, Standard_Auth_Form__c, Note_for_MSA__c, County_Covered__c, Termination_Date__c FROM Transportation_Authorization_Grid__c where Id = :tag.Id];
        Map<Id, List<Transportation_Authorization_Grid__c>> m = new Map<Id, List<Transportation_Authorization_Grid__c>>();
        m.put(aId, tagList);
        m.put(bId, tagList);
		controller.mapCountyPlan = m;
        
        controller.updateRecords();
    }
    
    static testMethod void UpdateRecords2(){
        AuthGridQuickUpdateController controller = new AuthGridQuickUpdateController();
        County__c c = new County__c();
        c.Name = 'test';
        insert c;
    	Transportation_Plan__c p = new Transportation_Plan__c();
        p.Name = 'test';
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c();
        tag.County__c = c.Id;
        tag.Plan__c = p.Id;
        tag.Vehicle_Type__c = null;
        tag.Form_Type__c = null;
        tag.HD_Auth_Form__c = null; 
        tag.Authorization_Number__c = null; 
        tag.Effective_Date__c = null;
        tag.Standard_Auth_Form__c = null;
        tag.Note_for_MSA__c = null; //If County_Covered__c = False, Note_for_MSA__c cannot be null
        tag.County_Covered__c = null;
        tag.Termination_Date__c = null;
        insert tag;
        controller.tmpCP = tag;
        
        List<Id> a = new List<Id>{c.Id};
        List<Id> b = new List<Id>{p.Id};
        controller.selCounty = a;
        controller.selPlan = b;
        
        //Case: cp.Plan == p
        Id aId = c.Id;
        Id bId = p.Id;
        List<Transportation_Authorization_Grid__c> tagList = [select County__c, Plan__c, Vehicle_Type__c, Form_Type__c, HD_Auth_Form__c, Authorization_Number__c, Effective_Date__c, Standard_Auth_Form__c, Note_for_MSA__c, County_Covered__c, Termination_Date__c FROM Transportation_Authorization_Grid__c where Id = :tag.Id];
        Map<Id, List<Transportation_Authorization_Grid__c>> m = new Map<Id, List<Transportation_Authorization_Grid__c>>();
        m.put(aId, tagList);
        m.put(bId, tagList);
		controller.mapCountyPlan = m;
        
        controller.updateRecords();
    }    
    
    static testMethod void UpdateRecords3(){
        AuthGridQuickUpdateController controller = new AuthGridQuickUpdateController();
        County__c c = new County__c();
        c.Name = 'test';
        insert c;
    	Transportation_Plan__c p = new Transportation_Plan__c();
        p.Name = 'test';
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c();
        tag.County__c = c.Id;
        tag.Plan__c = p.Id;
        tag.Vehicle_Type__c = 'Ambulette';
        tag.Form_Type__c = 'Standard';
        tag.HD_Auth_Form__c = 'Ambulette'; 
        tag.Authorization_Number__c = 'test'; 
        tag.Effective_Date__c = Date.newInstance(2015, 08, 16);
        tag.Standard_Auth_Form__c = 'Ambulette';
        tag.Note_for_MSA__c = 'null'; //If County_Covered__c = False, Note_for_MSA__c cannot be null
        tag.County_Covered__c = 'No';
        tag.Termination_Date__c = Date.newInstance(2015, 08, 31);
        insert tag;
        controller.tmpCP = tag;
        
        List<Id> a = new List<Id>{c.Id};
        List<Id> b = new List<Id>{p.Id};
        controller.selCounty = a;
        controller.selPlan = b;
        
        //Case: foundmatch = false
        Id aId = c.Id;
        List<Transportation_Authorization_Grid__c> tagList = new List<Transportation_Authorization_Grid__c>();
        Map<Id, List<Transportation_Authorization_Grid__c>> m = new Map<Id, List<Transportation_Authorization_Grid__c>>();
        m.put(aId, tagList);

		controller.mapCountyPlan = m;
        
        controller.updateRecords();
    }
    
        static testMethod void UpdateRecords4(){
        
        County__c c = new County__c();
        c.Name = 'test';
        insert c;
    	Transportation_Plan__c p = new Transportation_Plan__c();
        p.Name = 'test';
        insert p;
        Transportation_Authorization_Grid__c tag = new Transportation_Authorization_Grid__c();

        tag.County__c = c.Id;
        tag.Plan__c = p.Id;
        tag.Vehicle_Type__c = null;
        tag.Form_Type__c = null;
        tag.HD_Auth_Form__c = null; 
        tag.Authorization_Number__c = null; 
        tag.Effective_Date__c = null;
        tag.Standard_Auth_Form__c = null;
        tag.Note_for_MSA__c = null; //If County_Covered__c = False, Note_for_MSA__c cannot be null
        tag.County_Covered__c = null;
        tag.Termination_Date__c = null;
        insert tag;
        AuthGridQuickUpdateController controller = new AuthGridQuickUpdateController();
        controller.tmpCP = tag;
        
        List<Id> a = new List<Id>{c.Id};
        List<Id> b = new List<Id>{p.Id};
        controller.selCounty = a;
        controller.selPlan = b;
        
        //Case: foundmatch = false
        Id aId = c.Id;
        List<Transportation_Authorization_Grid__c> tagList = new List<Transportation_Authorization_Grid__c>();
        Map<Id, List<Transportation_Authorization_Grid__c>> m = new Map<Id, List<Transportation_Authorization_Grid__c>>();
        m.put(aId, tagList);

		controller.mapCountyPlan = m;
        
        controller.updateRecords();
    }
}