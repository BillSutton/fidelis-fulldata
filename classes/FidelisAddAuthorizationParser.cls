public class FidelisAddAuthorizationParser{
    public String umumRefId;
    public Integer seqNo;
    public Datetime umsvFromDt;
    public Datetime umsvToDt;
    public String procedureCode;
    public String errorMessage;

    public static List<FidelisAddAuthorizationParser> parse(String json) {
        try{
            return (List<FidelisAddAuthorizationParser>) System.JSON.deserialize(json, List<FidelisAddAuthorizationParser>.class);
        }catch(Exception e){
            List<FidelisAddAuthorizationParser> errLst = new List<FidelisAddAuthorizationParser>();
            if(json.contains('Err Message:')){
                String jsont = json.replace('\r','').replace('\n','');
                List<String> strl = jsont.split('Err Message:');
                List<String> actualErrs = new List<String>();
                Integer i = 0;
                for(String s : strl){
                    if(i!=0){
                        actualErrs.add(s.replaceAll('\\s<br/>.*','').replaceAll('^\\s',''));
                    }
                    i++;
                }
                
                for(String s:actualerrs){
                    FidelisAddAuthorizationParser faap = new FidelisAddAuthorizationParser();
                    faap.errorMessage = s;
                    errLst.add(faap);
                }
            }
            return errLst;
        }
    }
}