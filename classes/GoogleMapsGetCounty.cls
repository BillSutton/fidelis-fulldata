public class GoogleMapsGetCounty {
    
    public googleMapsGetCounty(){
        
    }
    //DEPRECATED- Moved to Client Side
    public static string getJson(String theAddress){
        Http http = new Http();
        httpRequest req = new httpRequest();
        req.setMethod('GET');
        string url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
        String account1Address = EncodingUtil.urlEncode(theAddress,'UTF-8');
        url += account1Address;        req.setEndPoint(url);
        if(!Test.isRunningTest()){
            HttpResponse resp = http.send(req);
            system.debug('REQUEST>>>>>>>>>>>'+url);
            system.debug('RESPONSE>>>>>>>>>>'+resp.getBody());
            return resp.getBody();
        }
        return '';
    }
    
    public static string parseTheResult(String theResult){
        string theReturn;
        GoogleGeoLocationParser parsed = GoogleGeoLocationParser.parse(theResult);
        for(GoogleGeoLocationParser.cls_results r :parsed.results){
            for(GoogleGeoLocationParser.cls_address_components a : r.address_components){
                for(string t :a.types){
                    if(t == 'administrative_area_level_2'){
                        if(a.short_Name != null){
                            if(a.short_Name.contains('County')){
                                theReturn = a.short_Name.subString(0,a.short_Name.indexOf('County') -1);
                            }
                            else{
                                theReturn = a.short_Name;
                            }
                        }
                    }
                }
            }
        }
        return theReturn;
    }
    
    public static string getCounty(String Address){
        return parseTheResult(getJson(Address));
    }
}