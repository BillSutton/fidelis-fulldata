/************************************************************************************************
**Name           : CaseStatusReasons
**Author         : Scott Van Atta
**Date           : August 2016
**Description    : Replace status reason workflows to set statuses
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      8/24/2016   SVA     Created

*************************************************************************************************/
global class CaseStatusReasons {
    map<String, String> statRes = new Map<String, String>();

    
    global CaseStatusReasons(list<Case> cases)
    {
        buildReasons();
        for (case c : cases)
        {
            if (statRes.containsKey(c.status_reason__c))
            {
                String stat = statRes.get(c.status_reason__c);
                if (stat != c.status)
                    system.debug('**CaseStatusReasons: ' + c.status_reason__c + ' old stat=' + c.status + '-->' + stat);
                if (stat == 'Qualified' && c.status != 'Qualified') 
                        c.Date_Qualified__c = date.today();
                if ((c.status=='PendU' || c.status == 'Unassigned') && stat == 'In Process')
                    c.Status = 'Unassigned';
                else c.status = stat;
            }
            else system.debug('**No status reason key for ' + c.status_reason__c);
        }
    }
    
    // Use for initial load, just send as true
    global CaseStatusReasons(boolean x)
    {
        if (x) InitialBuildStatusReasonObject();
    }
    
    private void buildReasons()
    {
        
        List<Case_Status_reasons__c> reasons = case_status_reasons__c.getall().values();
        if (reasons.size() < 1)
        {
            InitialBuildStatusReasonObject();
            reasons = case_status_reasons__c.getall().values();
        }
        for (case_status_reasons__c sr : reasons)
            statRes.put(sr.status_reason__c, sr.status__c);
        system.debug('***Found ' + reasons.size() + ' status reasons in custom settings');
        
    }
    
    private void InitialBuildStatusReasonObject()
    {
        List<case_status_reasons__c> del = [select id from case_status_reasons__c];
        delete del;
        List<case_status_reasons__c> reasons = new List<case_status_reasons__c>();
        
        Integer j = 1;
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Active Fidelis Insurance',                          status__c='Closed'));   
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Additional Documentation Required',                 status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='All Case Items Resolved',                           status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Alternate Program more Beneficial',                 status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Application Submitted by Broker/Nav for Fidelis',   status__c='Qualified'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Application Submitted by Fidelis',                  status__c='Qualified'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Application Submitted On-line by Applicant',        status__c='Qualified'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Application Submitted to Enrollment',               status__c='Qualified'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Application Taken for Another Plan by Fidelis',     status__c='Qualified'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Appointment Set',                                   status__c='In Process'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Assessment Completed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Assessment Completed & Uploaded',                   status__c='Closed'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Assessment Needed - Mbr Contact Unsuccessful'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Assessment Needed - Mbr Refused'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Awaiting Discharge from Hosp/Rehab',                status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Awaiting First Contact',                            status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Browsing HBX - Requested Callback',                 status__c='In Process'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Call Attempted - Unable to Leave Message'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Case is a Duplicate',                               status__c='Closed'));   
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Case Opened In Error',                              status__c='Closed'));       
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Code Exclusion',                                  status__c='Not Eligible'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Deceased',                                          status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Disconnected Phone #',                              status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Disenrollment Request Submitted (MLTC)',            status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Doesn\'t Meet Care Level',                          status__c='Not Eligible'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Doesn\'t Meet Income Qualifications',               status__c='Not Eligible'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Doesn\'t Score UAS Eligibility',                    status__c='Not Eligible'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Duplicate Case',                                    status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Eligibility Pending',                               status__c='In Process'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Enrollment Auto Assign'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Event Pending',                                     status__c='In Process'));   
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Final (MLTC) Notification Letter Sent'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Has Other Insurance',                               status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Home Environment not Safe',                         status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Homeless',                                          status__c='Closed'));   
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Immigrant Status',                                  status__c='Not Eligible'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Left Message',                                      status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Letter Sent',                                       status__c='In Process'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Member Active in Facets'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Member - Appointment Cancelled',                    status__c='In Process'));         
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Member Disenrolled',                                status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Member - No Show',                                  status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Member Moved/Not Available',                         status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='MLTC Packet Sent',                                  status__c='Closed'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Need Care Management Assistance'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='No Active Medicaid',                                status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='No Additional Contact Needed',                      status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='No Address',                                        status__c='Closed'));         
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='No Longer Interested',                              status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='No Medicare',                                       status__c='Not Eligible'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='No Medicare Part B',                                status__c='Not Eligible')); 
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Non-Par Provider',                                  status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='No Show at Appointment',                                status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Not Eligible',                                      status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Not Taken Under Care',                              status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Out of Service Area',                               status__c='Not Eligible'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Outside of OE Period',                              status__c='Closed'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Pending Assessment'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Pending Cancellation',                              status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Pending Completion',                                status__c='In Process'));   
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Pending Scheduling',                                status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Premium Too High',                                  status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Referred to Broker',                                status__c='Referred'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Referred to DSS',                                   status__c='Referred'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Referred to Marketplace',                           status__c='Referred'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Referred to Maximus (CFEEC)',                       status__c='Qualified'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Referred to Maximus (MCO)',                         status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Referred to MOMS/PCAP',                             status__c='Referred'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Referred to Navigator',                             status__c='Referred'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Rejection (Enrollment/Maximus)',                    status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Rejection (Maximus/LDSS) - Add\'l Info Needed',     status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Rejection (Maximus/LDSS) - Denied Enrollment',      status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Rejection - Medicaid Recertification (M027)',       status__c='Closed')); 
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Returned to Enrollment',                            status__c='Qualified'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Returned to Maximus',                               status__c='Qualified'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Returned to Member Services'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Returned to Outreach'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Sent Medicare Enrollment Packet',                   status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Task Pending',                                      status__c='In Process'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Unable to Make Contact',                            status__c='Closed'));
        //reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Unable to Make Contact (Auto Assign)'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Went to a Competitor to Apply',                     status__c='Closed')); 
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Will Call Back - Needs to Gather Documents',        status__c='In Process')); 
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Services Complete',                                 status__c='Closed'));
        reasons.add(new case_status_reasons__c(name='sva'+j++,Status_Reason__c='Unable to Complete',                                status__c='Closed'));
        insert reasons;

    }
}