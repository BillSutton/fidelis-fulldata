Public Class SendNewFormRequestController{
	public Form_Request__c newForm{get;set;}
	public boolean showSentMessage{get;set;}

	public sendNewFormRequestController(){
		newForm = new Form_Request__c();
		showSentMessage = false;
	}

	public void hideSentMessage(){
		showSentMessage = false;
	}

	public void resetTheForm(){
		newForm = new Form_Request__c(form_category__c = 'Transportation Department');
	}

	public void sendButton(){
		if(validate()){
			insert newForm;
			showSentMessage = true;
			resetTheForm();
		}
	}

	public boolean validate(){
		boolean goodToGo = true;
		if(String.ISBLANK(newForm.Send_To_Temp__c)){
			newForm.Send_To_Temp__c.addError('This Field Is Required');
			goodToGo = false;
		}
		if(String.ISBLANK(newForm.Delivery_Method__c)){
			newForm.Delivery_Method__c.addError('This Field Is Required');
			goodToGo = false;
		}
		if(String.ISBLANK(newForm.form_category__c)){
			newForm.form_category__c.addError('This Field Is Required');
			goodToGo = false;
		}
		if(String.ISBLANK(newForm.form_name__c)){
			newForm.form_name__c.addError('This Field Is Required');
			goodToGo = false;
		}
		if(!String.ISBLANK(newForm.Send_To_Temp__c) && !String.ISBLANK(newForm.Delivery_Method__c)){
			goodToGo = emailPhoneValidation(newForm.Send_To_Temp__c,newForm.Delivery_Method__c);
		}

		return goodToGo;
	}

	public boolean emailPhoneValidation(String s, String Method){
		if(Method.contains('Email')){
			system.debug('we in da email!');
			if(Pattern.matches('^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$', s)){
				newForm.Send_To__c = s;
				return true;
			}
			else{
				newForm.Send_To_Temp__c.addError('This is Not a Valid Email Address');
				return false;
			}
		}
		else{
			//s.remove('-');
			if(Pattern.Matches('^[0-9]{1,45}$',s)){
				if(s.length() != 11){
					newForm.Send_To_Temp__c.addError('This is not an appropriate Fax Number');
					return false;					
				}
				else{
					newForm.Send_To__c = s+'@concordsend.com';
					return true;
				}
			}
			else{
				newForm.Send_To_Temp__c.addError('This Fax Number Can Not Contains Letters or Special Charaters');
				return false;
			}
		}
	}
}