/******************************************************************************
** Module Name   : baCriteriaDupes  
** Description   : Find rep criteria dupes
**          
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      9/23/2015   SVA     Created
**
******************************************************************************/

global class baCriteriaDupes implements Database.Batchable<sObject>, Database.Stateful {
    global Integer numRead=0;
    global Integer numDupes=0;
    global Integer numShort = 0;
    global ID REPTYPE = [select id from recordtype where developername = 'Retention' and sobjectType = 'Rep_assignment__c'].Id;
    global Map<String, rep_assignment_criteria__c> gCrits = new Map<String,rep_assignment_criteria__c>();
    Global String xtra = '';
    Boolean gUpdate = false;

    
    global baCriteriaDupes()
    {
    }
    
    global baCriteriaDupes(Boolean bupdate)
    {
        gUpdate = bupdate;
    }
    
    
    global database.querylocator start(Database.BatchableContext BC){
            
        return Database.getQueryLocator(getQuery());
    }
        
    global String getQuery()
    {
        
        String query='select id, language__c, zip_code__c, rep_assignment__c ' +
                    'from  rep_assignment_criteria__c where rep_assignment__r.inactive__c = false';
                    
        System.debug(query);
        xtra += query + '\n';
        return query;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        list<rep_assignment_criteria__c> Dels = new List<rep_assignment_criteria__c>();
        list<rep_assignment_criteria__c> Shorts = new List<rep_assignment_criteria__c>();
       
    // walk the records to pull relevant rep_assignment_criteria__cs for case check
        for(Sobject s : scope){
            numRead++;
            rep_assignment_criteria__c c = (rep_assignment_criteria__c) s;
            String key = c.rep_assignment__c + c.language__c + c.zip_code__c;
            if (gCrits.containsKey(key))
            {
                Dels.add(c);
                system.debug('***dupe-->' + key);
            }
            else gCrits.put(key,c);
            if (c.zip_code__c.length() < 5) {
                while (c.zip_code__c.length() < 5)
                {
                    c.zip_code__c = '0' + c.zip_code__c;
                }
                Shorts.add(c);
            }
        }
        numShort += Shorts.size();
        numDupes += Dels.size();
        
        if (gUpdate)
        {
            database.delete(dels);
            database.update(Shorts);
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
        String email;
        String stat;
        Integer jobs;
        Integer errs;
        Integer TotalJobItems;
        Integer JbiProcessed;
        
        if (bc != null)
        {
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.email
                from AsyncApexJob where Id = :BC.getJobId()];
            email = a.CreatedBy.email; jobs = a.TotalJobItems; errs = a.NumberofErrors; stat = a.Status;
        }
        util.sendmsg('Batch Apex baCriteriaDupes ' + stat + ' ' + numRead,
                'Processed ' + jobs + ' batches with ' + errs + ' failures.\n\n' + 'Numread=' + numRead + '\nDupe criteria: ' + numDupes + '\nShort zips: ' + numShort
                      +  '\n\n' + xtra, 'svanatta@fideliscare.org');
    }


   static testMethod void myUnitTest() {
    baCriteriaDupes ttc = new baCriteriaDupes();
    String query = ttc.getQuery() + ' LIMIT 25'; 
    system.debug('***' + query);
    List<rep_assignment_criteria__c> qrRes = new List<rep_assignment_criteria__c>();
    rep_assignment__c ra = [select id from rep_assignment__c where inactive__c=false limit 1];
    rep_assignment_criteria__c c1 = new rep_assignment_criteria__c(rep_assignment__c = ra.id,zip_code__c = '1234',language__c='English');
    rep_assignment_criteria__c c2 = new rep_assignment_criteria__c(rep_assignment__c = ra.id,zip_code__c = '01234',language__c='English');
    rep_assignment_criteria__c c3 = new rep_assignment_criteria__c(rep_assignment__c = ra.id,zip_code__c = '01234',language__c='English');
    qrRes.add(c1); qrRes.add(c2); qrRes.add(c3);
    insert qrRes;
    Database.BatchableContext bc;
    ttc.gUpdate=true;
    ttc.execute(bc, qrRes);
    ttc.finish(null);
    ttc = new baCriteriaDupes(false);

        
    }   
}