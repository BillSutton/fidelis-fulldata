/******************************************************************************
** Module Name   : assessmentRedirect
** Description   : For New overridden button, ensure it is from a case and has no other assessments
**
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      6/12/15    SVA     Created
**
******************************************************************************/
public with sharing class assessmentRedirect {

private ApexPages.StandardController controller;
public Integer Error {get; set;}
public String IDCASEID {get; set;}
public String IDCASENUM {get; set; }
public String caseId { get; set; }
public String caseNum { get; set; }
//New from case:
        //https://cs10.salesforce.com/a0A/e?CF00NU0000004kgFS=00239482&CF00NU0000004kgFS_lkid=500J0000008R4Mj&retURL=%2F500J0000008R4Mj
//New from Assessment:
        //https://cs10.salesforce.com/a0A/e?retURL=%2Fa0A%2Fo

public assessmentRedirect(ApexPages.StandardController controller) {

    this.controller = controller;
    IDCASEID = 'CF00NU0000004kgFS_lkid';
    IDCASENUM = 'CF00NU0000004kgFS';

    caseId = ApexPages.currentPage().getParameters().get(IDCASEID);
    caseNum = ApexPages.currentPage().getParameters().get(IDCASENUM);
    if (caseId == '' || caseId == null) Error = 1;
    else {
        Integer i = [select count() from assessment__c where case__c = :caseId];
        if (i > 0) Error = 2;
        else error = 0;
    }

}
}