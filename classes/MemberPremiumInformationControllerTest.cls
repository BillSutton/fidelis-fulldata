/*
	Created by: Greg Hacic
	Last Update: 19 September 2016 by Greg Hacic
	Questions?: ghacic@fideliscare.org
	
	Notes:
		- tests MemberPremiumInformationController.class (92.38% coverage)
		- tests MemberPremiumInformationParser.class (86.67% coverage)
*/
@isTest
private class MemberPremiumInformationControllerTest {
	
	//tests MemberPremiumInformationController.class & MemberPremiumInformationParser.class
	static testMethod void successfulSubscriberPremium() {
	    //BEGIN: some setup items...
		//create an Account
		List<Account> accounts = new List<Account>();
		accounts.add(new Account(Name = 'Tess Trucking Co.'));
		insert accounts;
		//create an Contact
		List<Contact> contacts = new List<Contact>();
		contacts.add(new Contact(AccountId = accounts[0].Id, FirstName = 'Tess', LastName = 'Dachshund', Line_of_Business__c = 'CHP', Subscriber_Id__c = '999990001'));
		insert contacts;
		//END: some setup items...
		
		Test.startTest(); //denote testing context
		
		PageReference pageRef = Page.MemberPremiumInformationPage; //create a page reference to MemberPremiumInformationPage.page
		Test.setCurrentPage(pageRef); //set page context
		ApexPages.StandardController standardController = new ApexPages.standardController(contacts[0]); 	//instantiate the standard Contact object controller
		MemberPremiumInformationController ext = new MemberPremiumInformationController(standardController); //instantiate the extension
		//add a whole bunch of additional objects to the invoiceDueWrapper list - we do this in order to do some navigational methods
		ext.invoiceDueWrapper.add(new MemberPremiumInformationController.invoiceDueWrapper());
		ext.invoiceDueWrapper.add(new MemberPremiumInformationController.invoiceDueWrapper());
        ext.invoiceDueWrapper.add(new MemberPremiumInformationController.invoiceDueWrapper());
        ext.invoiceDueWrapper.add(new MemberPremiumInformationController.invoiceDueWrapper());
        ext.invoiceDueWrapper.add(new MemberPremiumInformationController.invoiceDueWrapper());
        //add a whole bunch of additional objects to the invoiceDetailsWrapper List
        ext.idw.add(new MemberPremiumInformationController.invoiceDetailsWrapper());
        ext.idw.add(new MemberPremiumInformationController.invoiceDetailsWrapper());
        ext.idw.add(new MemberPremiumInformationController.invoiceDetailsWrapper());
        ext.idw.add(new MemberPremiumInformationController.invoiceDetailsWrapper());
        ext.idw.add(new MemberPremiumInformationController.invoiceDetailsWrapper());
        //add a whole bunch of additional objects to the appliedPaymentsWrapper list
        ext.apw.add(new MemberPremiumInformationController.appliedPaymentsWrapper());
        ext.apw.add(new MemberPremiumInformationController.appliedPaymentsWrapper());
        ext.apw.add(new MemberPremiumInformationController.appliedPaymentsWrapper());
        ext.apw.add(new MemberPremiumInformationController.appliedPaymentsWrapper());
        ext.apw.add(new MemberPremiumInformationController.appliedPaymentsWrapper());
		//add a bunch of additional objects to the latestPaymentWrapper list
		ext.lpw.add(new MemberPremiumInformationController.latestPaymentWrapper());
		ext.lpw.add(new MemberPremiumInformationController.latestPaymentWrapper());
		ext.lpw.add(new MemberPremiumInformationController.latestPaymentWrapper());
		ext.lpw.add(new MemberPremiumInformationController.latestPaymentWrapper());
		ext.lpw.add(new MemberPremiumInformationController.latestPaymentWrapper());
		
		ext.apNext(); //execute the apNext method
		ext.apPrevious(); //execute the apPrevious method
		ext.lpNext();
		ext.lpPrevious();
		ext.idNext();
		ext.idPrevious();
		ext.iDueNext();
		ext.iDuePrevious();
		
		Test.stopTest(); //revert from test context
	}

}