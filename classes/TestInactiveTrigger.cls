/*
@Name            : TestInactiveTrigger
@Author          : sva
@Date            : October 2015
@Description     : Test Inactive User trigger
*/

@isTest(SeeAllData=true)
public class TestInactiveTrigger{

static testmethod void TestEr()
{
    List<Rep_Assignment__c> ras = [select id, marketing_rep_record__c, Rep_Assignment_lu__c, vacation__c from rep_assignment__c 
            where inactive__c = false and marketing_rep_record__c != null and marketing_rep_record__r.IsActive = true LIMIT 2];
    User u = [select id, IsActive from user where id = :ras[0].marketing_rep_record__c];
    
    ras[0].vacation__c = false;
    ras[1].vacation__c = true;
    ras[1].Rep_Assignment_lu__c = ras[0].Id;
    // not updating ra here because will fail on Mixed DML error. Simple user update should give sufficient coverage.
    u.IsActive = false;
    update u;
}   
}