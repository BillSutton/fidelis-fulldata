/* 
*  Retention Project
*  Web Service definition for creating cases based on the book of business
*  Developed By: Bill Sutton (Huron Consulting Group)
*  Email: wsutton@huronconsultinggroup.com
*
* Sample URL: {Salesforce Base URL}/services/apexrest/Retention
*
* Example Request String:
{
        "caseList": [
    {
        "ContrivedKey": "272033000",
        "lob": "EPP",
        "caseType": "R",
        "ResponsiblePersonId": "",
        "subscriberID": "743219838",
        "recertDate": "",
        "enrollmentSource": "",
        "APTCIndicator": "",
        "suffix": "0",
        "familyLinkId" : "1324"
    },
    {
        "ContrivedKey": "242104600",
        "lob": "NYHX",
        "caseType": "R",
        "ResponsiblePersonId": "",
        "subscriberID": "743219841",
        "recertDate": "",
        "enrollmentSource": "",
        "APTCIndicator": "",
        "suffix": "00",
        "familyLinkId" : "3245"
    }]
}
*/
@RestResource(urlMapping='/Retention/*')
global class Retention {
    //This is the method that is accessed via the web
    @HttpPost
    global static response generateList(List<TempCaseObject> caseList){
        return insertCases(caseList);
    }
    
    global static Map<String,RecordType> getCaseRT(){
                Map<String,RecordType> recordTypes = new Map<String,RecordType>();
                Map<String,String> rtConversion = new Map<String,String>{'Disenrollment'=>'de','Delinquencies'=>'d','Renewals'=>'r','Ad_Hoc'=>'ah','Initial_Premiums'=>'ip'};
                for(RecordType rt : (List<RecordType>)database.query('select Id, Name, DeveloperName from RecordType where SobjectType = \'Case\'')){
                                if(rtConversion.containsKey(rt.DeveloperName)){
                                                recordTypes.put(rtConversion.get(rt.DeveloperName),rt);
                                }
                }
                return recordTypes;
    }

   
    //Create cases from the webservice
    global static Response insertCases(List<TempCaseObject> inputList){
        List<Case> insertList = new List<Case>();
        List<Results> resultsList = new List<Results>();
        List<TempCaseObject> cleanedTCList = new List<TempCaseObject>();
        List<String> memberKeyList = new List<String>();
        List<CaseApplicantWrapper> wrapList = new List<CaseApplicantWrapper>();

        //Determin The Case Types, CKs, and RPIDs we will need
        Set<String> caseTypes = new set<String>();
        List<String> rpList = new List<String>();
        for(TempCaseObject tc :inputList){
            rpList.add(tc.ResponsiblePersonId);
            MemberKeyList.add(tc.contrivedKey);
            caseTypes.add(tc.caseType);
        }      

        //Get the Contacts and RPs that are in salesforce
        QueryHelper.populateRetentionMaps(MemberKeyList, rpList);

        //Get Family Members grouped together and validate each member
        Map<String, List<TempCaseObject>> familyMap = new Map<String, List<TempCaseObject>>();        
            map<String,RecordType> caseRTMap = getCaseRT();
            for(TempCaseObject tc :inputList){
                if(tc.contrivedKey == ''){
                    //create error result when we do not have contrived key
                    resultsList.add(new results(tc,'No Contrived Key Provided'));
                    continue;
                }
                else if(QueryHelper.getContactIdByContrivedKey(tc.contrivedKey) == null){
                    resultsList.add(new results(tc,'INVALID Contrived Key Provided'));
                    continue;
                }
                else if(tc.caseType == ''){
                    resultsList.add(new results(tc,'No Case Type Provided'));
                    continue;
                }
                //Removing Per Meeting Comments on 9/30/16
                /*else if(tc.subscriberID == '' && tc.ResponsiblePersonId == ''){
                    resultsList.add(new results(tc,'No subscriberID or ResponsiblePersonId Provided'));
                    continue;
                }*/
                else if(tc.subscriberID != '' && tc.suffix.length() != 2){
                    resultsList.add(new results(tc,'Invalid Suffix'));
                    continue;
                }
                else if(tc.familyLinkId == null ||  tc.familyLinkId == ''){
                    //Per Requirements discussed on 10/3-4/2016 not all members have a family link, creating a unique grouping based on contrivedKey and RecertDate
                    tc.familyLinkId = tc.contrivedKey+''+tc.recertDate;
                }
                if(tc.familyLinkId != null && tc.familyLinkId != ''){
                    if(familyMap.get(tc.familyLinkId) == null){
                        familyMap.put(tc.familyLinkId, new List<tempCaseObject>());
                        familyMap.get(tc.familyLinkId).add(tc);
                    }
                    else{
                        familyMap.get(tc.familyLinkId).add(tc);
                    }
                }
            }
        //create Applicant Case Combinations
        for(String familyGroup : familyMap.keySet()){
            Date oldestMemberDate;
            String oldestMemberId;
            Integer innerloopCount = 0;
            CaseApplicantWrapper tempWrap = new CaseApplicantWrapper();
            for(TempCaseObject tc :familyMap.get(familyGroup)){
            innerloopCount++;
            Applicant_Information__c i;
            //CREATE APPLICANTS
                if(QueryHelper.getContactIdByContrivedKey(tc.contrivedKey) != null){
                     i = new Applicant_Information__c(Contact__c = QueryHelper.getContactIdByContrivedKey(tc.contrivedKey), 
                        Recert_Date__c = (String.isBlank(tc.recertDate))? null : Date.valueOf(tc.recertDate),Renewal_Method__c = tc.enrollmentSource, 
                        aptc_indicator__c = (tc.aptcIndicator == 'y' || tc.aptcIndicator == 'Y'), Member_Id__c = tc.subscriberid,
                        Applicant_First_Name__c = QueryHelper.getContactFNameByContrivedKey(tc.ContrivedKey), 
                        Date_Of_Birth__c = QueryHelper.getContactBirthdayByContrivedKey(tc.ContrivedKey),Applicant_Status__c = 'Pending',
                        Name = QueryHelper.getContactLNameByContrivedKey(tc.ContrivedKey), Relationship__c = 'Parent', Reason__c = 'Renewal', 
                        Plan_Name__c = 'Fidelis Care', Reason_For_Selecting_Another_Plan__c = 'Not Applicable', Line_of_Business__c = tc.lob,
                        Type_of_Applicant__c = 'Renewal',Payment_Made__c = 'No', Payment_Method__c = 'Not Applicable');
                }
                else{
                    resultsList.add(new results(tc,'Member Not Found: Case for Family Grouping '+familyGroup+' not created'));
                    continue;
                }
            tempWrap.appList.add(i);
                if(familyMap.get(familyGroup).size() == innerloopCount){
                    Case c = new case();

                    //Below logic determins who we select as the primary case contact / responsible person
                    if(tc.lob == 'CHP'){
                        if(oldestMemberId == null){
                            oldestMemberId = tc.ContrivedKey;
                            oldestMemberDate = QueryHelper.getContactBirthdayByContrivedKey(tc.ContrivedKey);
                        }
                        else if(QueryHelper.getContactBirthdayByContrivedKey(tc.ContrivedKey) > oldestMemberDate){
                            oldestMemberId = tc.ContrivedKey;
                            oldestMemberDate = QueryHelper.getContactBirthdayByContrivedKey(tc.ContrivedKey);
                        }
                        if(QueryHelper.getRpByRpId(tc.ResponsiblePersonId) != null){
                        c.Responsible_Person__c = QueryHelper.getRpByRpId(tc.ResponsiblePersonId);
                        }
                        //Removing Per Meeting Comments on 9/30/16
                        /*else{
                            resultsList.add(new results(tc,'Invalid ResponsiblePersonId OR Responsible Person Does Not Exist'));
                            continue;
                        }    */
                        if(QueryHelper.getContactIdByContrivedKey(oldestMemberId) != null){
                            c.contactId = QueryHelper.getContactIdByContrivedKey(oldestMemberId); 
                        }
                        else{
                            resultsList.add(new results(tc,'Invalid Member or Member Does Not Exist'));
                            continue;
                        } 
                    }
                    //non-CHP Member HOH identifier
                    else if(tc.suffix == '00'){ // subject to change
                        if(QueryHelper.getContactIdByContrivedKey(tc.contrivedKey) != null){
                            c.contactId = QueryHelper.getContactIdByContrivedKey(tc.contrivedKey);
                        }
                        else{
                            resultsList.add(new results(tc,'Invalid Contrived Key OR Member Not Enrolled'));
                            continue;
                        }
                        if(tc.ResponsiblePersonId != null && tc.ResponsiblePersonId != '' && !String.isBlank(tc.ResponsiblePersonID)){
                            if(QueryHelper.getRpByRpId(tc.ResponsiblePersonId) != null){
                            c.Responsible_Person__c = QueryHelper.getRpByRpId(tc.ResponsiblePersonId);
                            }
                            //Removing Per Meeting Comments on 9/30/16
                            /*else{
                                resultsList.add(new results(tc,'Invalid ResponsiblePersonId OR Responsible Person Does Not Exist'));
                                continue;
                            }*/
                        }   
                    }                       
                    //Standard Case Creation activities here               
                    c.Primary_Category__c = tc.lob;
                    c.Status = 'Assigned';
                    cleanedTCList.add(tc);
                    c.recordtypeId = caseRtMap.get(tc.caseType.toLowerCase()).id;
                    tempWrap.c = c;
                    wrapList.add(tempWrap);
                }
            }
            
        }

        for(CaseApplicantWrapper caw :wrapList){
            insertList.add(caw.c);
        }
        //insert the records that passed our validation
        List<Database.SaveResult> sr = database.insert(insertList,false);
        List<Applicant_Information__c> appInsertList = new List<Applicant_Information__c>();
        for(CaseApplicantWrapper caw :wrapList){
            for(Applicant_Information__c a :caw.appList){
                a.Case_Number__c = caw.c.id;
                appInsertList.add(a);
            }
        }
        List<Database.SaveResult> sr2 = database.insert(appInsertList,false);
        List<Id> queryList = new List<Id>();
        for(DataBase.SaveResult s :sr){
            queryList.add(s.getId());
        }
        map<Id,Case> newCases = new map<Id,Case>();
        //generate a List of the results (system generated errors will appear in these results)
        Integer i = 0;
        for(Database.SaveResult s :sr){                  
            if(s.isSuccess()){
                resultsList.add(new results(cleanedTCList[i],s.getId(),''));
            }
            //below line returns bad inserts to consumer
            else if(!s.isSuccess()){
                resultsList.add(new results(cleanedTCList[i],'',String.valueOf(s.getErrors())));
            }
            i++;
        }
        //create response object
        Response r = new response();
        r.resultsList = resultsList;
        //sends response back to the consumer
        system.debug(r);
        Return r;
    }

    global class CaseApplicantWrapper{
        global Case c{get;set;}
        global List<Applicant_Information__c> appList {get;set;}

        global CaseApplicantWrapper(){
            this.c = new Case();
            this.appList = new List<Applicant_Information__c>();
        }
    }
      
    //Staging object for when we get the JSON request
    global class TempCaseObject{
        global String ContrivedKey {get;set;}
        global String lob {get;set;}
        global String caseType {get;set;}
        global String ResponsiblePersonId {get;set;}
        global String subscriberID {get;set;}
        global String recertDate {get;set;}
        global String enrollmentSource {get;set;}
        global String APTCIndicator {get;set;}
        global String suffix {get;set;}
        global String familyLinkId {get;set;}
       
        global TempCaseObject(){
            this.ContrivedKey = '';
            this.lob = '';
            this.caseType = '';
            this.ResponsiblePersonId = '';
            this.subscriberID = '';
            this.recertDate = '';
            this.enrollmentSource = '';
            this.APTCIndicator = '';
            this.suffix = '';
            this.familyLinkId = '';
        }
    }
   
    //This is the response object what we send back to the requesting party   
    global class Response{
        global String Status{get;set;}
        global List<Results> resultsList{get;set;}
        global response(){
            this.status = 'Okay';
            this.resultsList = new List<Results>();
        }
    }
    
    //results object to hold the information we send back to the consumer
    global class Results{
        global String Status {get;set;}
        global TempCaseObject record{get;set;}
        global String sfdcId{get;set;}
        global String message {get;set;}
       
        global Results(){
            this.Status ='';
            this.sfdcId ='';
            this.message='';
            this.record = new TempCaseObject();
        }
        //Pre-Insert Validation Call
        global Results(TempCaseObject tc, String errMessage){
            this.Status ='ERROR';
            this.sfdcId ='';
            this.message=errMessage;
            this.record = tc;
        }
        //System results
        global Results(TempCaseObject tc, String newId, String systemMessage){
            if(newId == null || newId == ''){
                this.Status ='ERROR';
            }
            else{
                this.Status = 'Success';
            }           
            this.sfdcId =newId;
            this.message=systemMessage;
            this.record = tc;
        }
    }
}