global class DailyManifestBatch implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts {

    global DailyManifestBatch() {}

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query =  'SELECT Id, Type FROM Account '+
                        'WHERE RecordType.DeveloperName = \'Transportation_Provider\' AND Type = \'In Network\' ';
             //query += 'AND Id = \'00155000003p52g\'';
        system.debug(query);
        return Database.getQueryLocator(query);
    }

    // call this method when schedulable
    global void execute(SchedulableContext ctx) {
        DailyManifestBatch batch1 = new DailyManifestBatch();
        system.debug('calling batch');
        Database.executeBatch(batch1,10);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Generate the pdf.  Attach it to chatter FeedItem and Email/Fax to vendor
        for (Account acc : (List<Account>) scope){
            DailyManifestREST.EmailAttachPDF(acc.Id);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        // nothing to do here
    }    
}