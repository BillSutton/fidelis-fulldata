/*
    Created by: Greg Hacic
    Last Update: 6 September 2016 by Greg Hacic
    Questions?: ghacic@fideliscare.org
    
    Notes:
        - tests checkDuplicates.class (100.00% coverage)
*/
@isTest
private class checkDuplicatesTest {
    
    //tests checkDuplicates.class - successful response
    static testMethod void lookForDupesSuccessReturnMatches() {
        //construct the list of ckWrapper objects to include in the API method
        List<checkDuplicates.ckWrapper> listOfContrivedKeyWrappers = new List<checkDuplicates.ckWrapper>();
        listOfContrivedKeyWrappers.add(new checkDuplicates.ckWrapper('9999990000000001'));
        listOfContrivedKeyWrappers.add(new checkDuplicates.ckWrapper('9999990000000002'));
        listOfContrivedKeyWrappers.add(new checkDuplicates.ckWrapper('9999990000000003'));
        listOfContrivedKeyWrappers.add(new checkDuplicates.ckWrapper('9999990000000004'));
        
        Test.startTest(); //denote testing context
        
        RestRequest req = new RestRequest(); //object used to pass data from an HTTP request to an Apex RESTful Web service method
        RestResponse res = new RestResponse(); //object used to pass data from an Apex RESTful Web service method to an HTTP response
        String json = '{'+
                '  "contrivedKeyList": ['+
                '    {"contrivedKey": "9999990000000001"},'+
                '    {"contrivedKey": "9999990000000002"},'+
                '    {"contrivedKey": "9999990000000003"},'+
                '    {"contrivedKey": "9999990000000004"}'+
                '  ]'+
                '}'; //JSON for the Request
        
        req.requestURI = '/services/apexrest/retention/applicants/checkDuplicates'; //request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(json); //set the JSON request body
        RestContext.request = req; //access the RestRequest object
        RestContext.response= res; //access the RestResponse object
        
        
        checkDuplicates.responseWrapper results = new checkDuplicates.responseWrapper(); //construct the responseWrapper object
        results = checkDuplicates.checkForDuplicates(listOfContrivedKeyWrappers); //invoke the checkForDuplicates method
        
        System.assertEquals('Okay', results.Status); //assert that the status was Okay
        System.assertEquals(2, results.Size); //assert there were 2 contrived keys returned
        System.assertEquals('', results.Message); //assert there was no message
        
        Test.stopTest(); //revert from testing context
    }
    
    //tests checkDuplicates.class - failure response
    static testMethod void lookForDupesFailNoKeysSent() {
        //construct the list of ckWrapper objects to include in the API method
        List<checkDuplicates.ckWrapper> listOfContrivedKeyWrappers = new List<checkDuplicates.ckWrapper>();
        listOfContrivedKeyWrappers.add(new checkDuplicates.ckWrapper()); //add a null contrived key > this gets us coverage for the ckWrapper method with no passed string
        listOfContrivedKeyWrappers.clear(); //clear the list
        
        Test.startTest(); //denote testing context
        
        RestRequest req = new RestRequest(); //object used to pass data from an HTTP request to an Apex RESTful Web service method
        RestResponse res = new RestResponse(); //object used to pass data from an Apex RESTful Web service method to an HTTP response
        String json = '{"contrivedKeyList": []}'; //JSON for the Request
        
        req.requestURI = '/services/apexrest/retention/applicants/checkDuplicates'; //request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(json); //set the JSON request body
        RestContext.request = req; //access the RestRequest object
        RestContext.response= res; //access the RestResponse object
        
        checkDuplicates.responseWrapper results = new checkDuplicates.responseWrapper(); //construct the responseWrapper object
        results = checkDuplicates.checkForDuplicates(listOfContrivedKeyWrappers); //invoke the checkForDuplicates method
        
        System.assertEquals('ERROR', results.Status); //assert that the status was ERROR
        System.assertEquals(0, results.Size); //assert there were 0 contrived keys returned
        System.assertEquals('No Contrived Keys were passed to API.', results.Message); //assert there was a message
        
        Test.stopTest(); //revert from testing context
    }
    
    //tests checkDuplicates.class - successful response
    static testMethod void lookForDupesSuccessNoReturn() {
        //construct the list of ckWrapper objects to include in the API method
        List<checkDuplicates.ckWrapper> listOfContrivedKeyWrappers = new List<checkDuplicates.ckWrapper>();
        listOfContrivedKeyWrappers.add(new checkDuplicates.ckWrapper('9999990000000001'));
        listOfContrivedKeyWrappers.add(new checkDuplicates.ckWrapper('9999990000000002'));
        
        Test.startTest(); //denote testing context
        
        RestRequest req = new RestRequest(); //object used to pass data from an HTTP request to an Apex RESTful Web service method
        RestResponse res = new RestResponse(); //object used to pass data from an Apex RESTful Web service method to an HTTP response
        String json = '{'+
                '  "contrivedKeyList": ['+
                '    {"contrivedKey": "9999990000000001"},'+
                '    {"contrivedKey": "9999990000000002"}'+
                '  ]'+
                '}'; //JSON for the Request
        
        req.requestURI = '/services/apexrest/retention/applicants/checkDuplicates'; //request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(json); //set the JSON request body
        RestContext.request = req; //access the RestRequest object
        RestContext.response= res; //access the RestResponse object
        
        
        checkDuplicates.responseWrapper results = new checkDuplicates.responseWrapper(); //construct the responseWrapper object
        results = checkDuplicates.checkForDuplicates(listOfContrivedKeyWrappers); //invoke the checkForDuplicates method
        
        System.assertEquals('Okay', results.Status); //assert that the status was Okay
        System.assertEquals(0, results.Size); //assert there were 0 contrived keys returned
        System.assertEquals('All passed contrived keys were duplicates.', results.Message); //assert there was a confirmation message
        
        Test.stopTest(); //revert from testing context
    }
    
    //create the testing data
    @testSetup //annotation allows for creation of common test records that are available for all test methods in the class
    static void allTheDataForThisTestClass() {
        //create some Account records
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name = 'Tess Trucking'));
        accounts.add(new Account(Name = 'Tess Financial'));
        insert accounts;
        //create some Contact records
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(AccountId = accounts[0].Id, Contrived_Key__c = '9999990000000001', FirstName = 'Tess', LastName = 'Dachshund'));
        contacts.add(new Contact(AccountId = accounts[1].Id, Contrived_Key__c = '9999990000000002', FirstName = 'Grachus', LastName = 'Dachshund'));
        insert contacts;
        //grab the appropriate RecordType Id
        RecordType caseRenewalRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Renewals'];
        //create some Case records
        List<Case> cases = new List<Case>();
        cases.add(new Case(ContactId = contacts[0].Id, RecordTypeId = caseRenewalRecordType.Id, Subject = 'Unit Testing Case 1'));
        cases.add(new Case(ContactId = contacts[1].Id, RecordTypeId = caseRenewalRecordType.Id, Subject = 'Unit Testing Case 2'));
        insert cases;
        //create some Applicant_Information__c records
        List<Applicant_Information__c> applicants = new List<Applicant_Information__c>();
        applicants.add(new Applicant_Information__c(Applicant_First_Name__c = 'Tess', Case_Number__c = cases[0].Id, Contact__c = contacts[0].Id, Recert_Date__c = Date.Today().addMonths(21)));
        applicants.add(new Applicant_Information__c(Applicant_First_Name__c = 'Grachus', Case_Number__c = cases[1].Id, Contact__c = contacts[1].Id, Recert_Date__c = Date.Today().addDays(30)));
        insert applicants;
    }

}