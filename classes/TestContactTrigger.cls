/*
@Name            : TestContactTrigger
@Author          : sva
@Date            : February 2015
@Description     : Test Contact account insertion trigger
*/

@isTest
public class TestContactTrigger{

static testmethod void TestEr()
{
// First test to see that two contacts added with no sub id creates two saccounts
    List<Contact> cons1 = new List<Contact>();
    Contact c1 = new contact(firstname='zork1', lastname='zork2', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY',phone='1234567890');
    cons1.add(c1);
    Contact c2 = new contact(firstname='zork2', lastname='zork4', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
    cons1.add(c2);
    insert cons1;
    // both should exist with their own account
    system.assertequals(2, [select count() from account]);
    
// Now test that changing the sub ids of the two contacts will change the null subid of account to that subid
    cons1[0].subscriber_id__c = '1';
    cons1[1].subscriber_id__c = '2';
    update cons1;
    system.assertequals(2, [select count() from account]);
    
// Now test that all contact subids match their account sub ids
    list<contact> ccc = [select id, subscriber_id__c, account.subscriber_id__c from contact];
    for (contact c : ccc)
        system.assertequals(c.subscriber_id__c, c.account.subscriber_id__c);
        
// Test the change of a contact's sub id from non-null to a new number (should create an account and leave the old one per the product manager)
    cons1[0].subscriber_id__c = '11';
    cons1[1].subscriber_id__c = '22';
    update cons1;
    system.assertequals(4, [select count() from account]); // per Michelle, the original two accounts s/b there as orphans
    system.assertequals(2, [select count() from contact]); // But still only two contacts
    list<contact> ccc2 = [select id, subscriber_id__c, account.subscriber_id__c from contact];
    for (contact c : ccc2)
        system.assertequals(c.subscriber_id__c, c.account.subscriber_id__c);
        
// Add two more contacts with null ids
    contact ccx1 = new contact(firstname='zork5', lastname='zork6', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
    contact ccx2 = new contact(firstname='zork7', lastname='zork8', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
    list<contact> cons2 = new List<Contact>();
    cons2.add(ccx1); cons2.add(ccx2);
    insert cons2;
    system.assertequals(6, [select count() from account]); // 
    system.assertequals(4, [select count() from contact]); // 
    
// Test the change of a subscriber id to an id that already exists in an account (should attach to it)  
    cons2[0].subscriber_id__c = '11';
    cons2[1].subscriber_id__c = '11';
    update cons2;
    system.assertequals(6, [select count() from account]); // Count should not change since we already had an account for 11
    system.assertequals(4, [select count() from contact]); // 
    system.assertequals(3, [select count() from contact where account.subscriber_id__c = '11']);
    
// Test new unattached contact attaches to proper account
    contact ccx3 = new contact(firstname='zork7', lastname='zork8', region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890', subscriber_id__c = '11');
    insert ccx3;
    system.assertequals(6, [select count() from account]); // Count should not change since we already had an account for 11
    system.assertequals(5, [select count() from contact]); // 
    system.assertequals(4, [select count() from contact where account.subscriber_id__c = '11']);

 }   

}