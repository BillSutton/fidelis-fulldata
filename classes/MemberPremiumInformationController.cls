public class MemberPremiumInformationController {
    public Contact currentContact {get;set;}
    public MemberPremiumInformationParser info {get;set;}
    public String pageMessages {get;set;}
    public ApexPages.StandardController controller {get;set;}
    //Lists to store our parsed data
    public List<latestPaymentWrapper> lpw {get;set;}
    public List<appliedPaymentsWrapper> apw{get;set;}
    public List<invoiceDetailsWrapper> idw{get;set;}
    public List<invoiceDueWrapper> invoiceDueWrapper{get;set;}
    //Below are Lists for Pagination
    public List<latestPaymentWrapper> displayLatestPayments {get;set;}
    public List<appliedPaymentsWrapper> displayAppliedPayments {get;set;}
    public List<invoiceDetailsWrapper> displayInvoiceDetails {get;set;}
    public List<invoiceDueWrapper> displayInvoiceDue {get;set;}
    //integer countes of pagination lists
    public integer dlpCurrent {get;set;}
    public integer dapCurrent {get;set;}
    public integer didCurrent {get;set;}
    public integer dindCurrent {get;set;}
    public integer listSize {get;set;}
    
    public MemberPremiumInformationController(ApexPages.StandardController c){
        listSize = 5;
        //storage wrappers
        lpw = new List<latestPaymentWrapper>();
        apw = new List<appliedPaymentsWrapper>();
        idw = new List<invoiceDetailsWrapper>();
        invoiceDueWrapper = new List<invoiceDueWrapper>();
		//display count defaults
        dlpCurrent = 0;
        dapCurrent = 0;
        didCurrent = 0;
        dindCurrent = 0;
        //display wrappers
        displayLatestPayments = new List<latestPaymentWrapper>();
    	displayAppliedPayments = new List<appliedPaymentsWrapper>();
    	displayInvoiceDetails = new List<invoiceDetailsWrapper>();
    	displayInvoiceDue = new List<invoiceDueWrapper>();
        //make sure we don't break the page with the following boolean
        boolean passedTheTry = false;
        currentContact = getCurrentContact(c.getId());
        try{
        	info = MemberPremiumInformationParser.parse(FidelisAPIHelper.getPremiumInfoBySubscriberID(currentContact.subscriber_id__c));
            if(info.memberPremiumLatestPayment == null){
                passedTheTry = false;
            }        
            else{
            	passedTheTry = true;
            }
        }
        catch(exception e){
            system.debug('Error: '+e.getMessage());
            pageMessages = 'Service Error Please Contact an Admin: '+e.getMessage()+' at line: '+e.getLineNumber();
            passedTheTry = false;
        }
        if(passedTheTry){
            //populate data holding wrappers
            for(MemberPremiumInformationParser.memberPremiumLatestPayment mpip :info.memberPremiumLatestPayment){
                lpw.add(new latestPaymentWrapper(mpip.grgR_CK, mpip.grgR_ID, mpip.invoiceNumber,mpip.monthlyPremium, mpip.paymentAmount, mpip.paymentDateReceived, mpip.totalCurrentDue, mpip.paymentPostedDate, mpip.subsidyTotal, mpip.aptcDeliquencyDate, mpip.aptcStatus));
            }
            for(memberPremiumInformationParser.MemberPaymentAppliedDetails mpad :info.memberPaymentAppliedDetails){
                apw.add( new appliedPaymentsWrapper(mpad.grgR_CK, mpad.grgR_ID, mpad.invoiceNumber,mpad.paymentAppliedDate, mpad.appliedAmount, mpad.checkNumber, mpad.paymentMethod, mpad.suspenseIndicator));
            }
            for(MemberPremiumInformationParser.MemberPremiumInvoicePaidDetails mpid :info.MemberPremiumInvoicePaidDetails){
                idw.add(new invoiceDetailsWrapper(mpid.grgR_CK, mpid.grgR_ID, mpid.invoiceNumber, mpid.premiumDueDate, mpid.appliedAmount, mpid.remainingInvoiceBalance, mpid.activityDate));
            }
            for(MemberPremiumInformationParser.MemberPremiumInvoiceDueDetails mpidd :info.memberPremiumInvoiceDueDetails){
                invoiceDueWrapper.add(new invoiceDueWrapper(mpidd.grgR_CK, mpidd.grgR_ID, mpidd.invoiceNumber, mpidd.billedAmount, mpidd.unpaidPremiumDueDate, mpidd.invoiceBalanceDue));
            }
            //populate display wrapper lists
            for(latestPaymentWrapper l :lpw){
                if(displayLatestPayments.size() < listSize){
                    displayLatestPayments.add(l);
                    dlpCurrent++;
                }
                else{
                    break;
                }
            }
            for(appliedPaymentsWrapper a :apw){
                if(displayAppliedPayments.size() < listSize){
                    displayAppliedPayments.add(a);
                    dapCurrent++;
                }
                else{
                    break;
                }
            }
            for(invoiceDetailsWrapper i :idw){
                if(displayInvoiceDetails.size() < listSize){
                    displayInvoiceDetails.add(i);
                    didCurrent++;
                }
                else{
                    break;
                }
            }
            for(invoiceDueWrapper l :invoiceDueWrapper){
                if(displayInvoiceDue.size() < listSize){
                    displayInvoiceDue.add(l);
                    dindCurrent++;
                }
                else{
                    break;
                }
            }
        }
        else if(pageMessages == null){
            pageMessages = 'Service Error Please Contact an Admin';
        }       
    }
    
    //next and previous buttons for pagination    
    public void apNext(){
        if(dapCurrent != apw.size()){
            displayAppliedPayments = new List<appliedPaymentsWrapper>();
            for(Integer i = dapCurrent; i < apw.size(); i++){
                if(displayAppliedPayments.size() < listSize){
                    displayAppliedPayments.add(apw[i]);
                    dapCurrent++;
                }
            }
        }
    }
    
    public void apPrevious(){
        if(dapCurrent > listSize){
			Integer startingPoint = dapCurrent-displayAppliedPayments.size();
            displayAppliedPayments = new List<appliedPaymentsWrapper>();
            for(Integer i = startingPoint; i >= 0; i--){
                if(displayAppliedPayments.size() <= listSize){
                    displayAppliedPayments.add(apw[i]);
                }
            }
            if(dapCurrent < listSize*2){
            	dapCurrent = listSize;
        	}
        	else{
            	dapCurrent = dapCurrent - listSize;
        	}
        }
    }
    
    public void lpNext(){
        if(dlpCurrent != lpw.size()){
            displayLatestPayments = new List<latestPaymentWrapper>();
            for(Integer i = dlpCurrent; i < lpw.size(); i++){
                if(displayLatestPayments.size() < listSize){
                    displayLatestPayments.add(lpw[i]);
                    dlpCurrent++;
                }
            }
        }
    }
    
    public void lpPrevious(){
        if(dlpCurrent > listSize){
        	Integer startingPoint = dlpCurrent-displayLatestPayments.size();
            displayLatestPayments = new List<latestPaymentWrapper>();
            for(Integer i = startingPoint; i >= 0; i--){
                if(displayLatestPayments.size() < listSize){
                    displayLatestPayments.add(lpw[i]);
                    //dapCurrent--;
                }
            }
            if(dlpCurrent < listSize*2){
            	dlpCurrent = listSize;
        	}
        	else{
            	dlpCurrent = dlpCurrent - listSize;
        	}
        }
    }
    
    public void idNext(){
        if(didCurrent != idw.size()){
            displayInvoiceDetails = new List<invoiceDetailsWrapper>();
            for(Integer i = didCurrent; i < idw.size(); i++){
                if(displayInvoiceDetails.size() < listSize){
                    displayInvoiceDetails.add(idw[i]);
                    didCurrent++;
                }
            }
        }
    }
    
    public void idPrevious(){
        if(didCurrent > listSize){
        	Integer startingPoint = didCurrent-displayInvoiceDetails.size();
            displayInvoiceDetails = new List<invoiceDetailsWrapper>();
            for(Integer i = startingPoint; i >= 0; i--){
                if(displayInvoiceDetails.size() < listSize){
                    displayInvoiceDetails.add(idw[i]);
                }
            }
            if(didCurrent < listSize*2){
            	didCurrent = listSize;
        	}
        	else{
            	didCurrent = didCurrent - listSize;
        	}
        }
    }
    
    public void iDueNext(){
        if(dindCurrent != invoiceDueWrapper.size()){
            displayInvoiceDue = new List<invoiceDueWrapper>();
            for(Integer i = dindCurrent; i < invoiceDueWrapper.size(); i++){
                if(displayInvoiceDue.size() < listSize){
                    displayInvoiceDue.add(invoiceDueWrapper[i]);
                    dindCurrent++;
                }
            }
        }
    }
    
    public void iDuePrevious(){
        if(dindCurrent > listSize){
        	Integer startingPoint = dindCurrent-displayInvoiceDue.size();
            displayInvoiceDue = new List<invoiceDueWrapper>();
            for(Integer i = startingPoint; i >= 0; i--){
                if(displayInvoiceDue.size() < listSize){
                    displayInvoiceDue.add(invoiceDueWrapper[i]);
                }
            }
            if(dindCurrent < listSize*2){
            	dindCurrent = listSize;
        	}
        	else{
            	dindCurrent = dindCurrent - listSize;
        	}
        }
    }
    
    public contact getCurrentContact(String theId){
        return [select id, firstName, LastName, Line_of_Business__c, subscriber_id__c from contact where Id = :theId];
    }
    
    public class latestPaymentWrapper{
        public Integer grgR_CK {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        public Double monthlyPremium {get;set;}
        public Double paymentAmount {get;set;}
        public String paymentDateReceived {get;set;}
        public Double totalCurrentDue {get;set;}
        public String paymentPostedDate {get;set;}
        public Double subsidyTotal {get;set;}
        public String aptcDeliquencyDate {get;set;}
        public String aptcStatus{get;set;}
        
        public latestPaymentWrapper(){
            grgR_CK = 0;
            grgR_ID = '';
            invoiceNumber = '';
            monthlyPremium = 0;
            paymentAmount = 0;
            paymentDateReceived = '';
            totalCurrentDue = 0;
            paymentPostedDate = '';
            subsidyTotal = 0;
            aptcDeliquencyDate = '';
            aptcStatus = '';
        }
        
        public latestPaymentWrapper(Integer gck, String gid, String inv, Double mp, Double pa, String pdr, Double tcd, String ppd, Double st, String add, String aptcs){
            grgR_ck = gck;
            grgR_ID = gid;
            invoiceNumber = inv;
            monthlyPremium = mp;
            paymentAmount = pa;
            paymentDateReceived = pdr;
            totalCurrentDue = tcd;
            paymentPostedDate = ppd;
            subsidyTotal = st;
            aptcDeliquencyDate = add;
            aptcStatus = aptcs;
        }
    }
    
    public class appliedPaymentsWrapper{
        public Integer grgR_CK {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        Public String paymentAppliedDate {get;set;}
        Public Double appliedAmount{get;set;}
        Public String checkNumber{get;set;}
        Public String paymentMethod{get;set;}
        Public String suspenseIndicator{get;set;}
        
        public appliedPaymentsWrapper(){
            grgR_CK = 0;
            grgR_ID = '';
            invoiceNumber = '';
            paymentAppliedDate = '';
            appliedAmount = 0;
            checkNumber = '';
            paymentMethod = '';
            suspenseIndicator = '';
        }
        
        public appliedPaymentsWrapper(Integer gck, String gid, String inv, String pad, Double aa, String cn, String pm, String si){
            grgR_ck = gck;
            grgR_ID = gid;
            invoiceNumber = inv;
            paymentAppliedDate = pad;
            appliedAmount = aa;
            checkNumber = cn;
            paymentMethod = pm;
            suspenseIndicator = si;
        }
    }
        
    public class invoiceDetailsWrapper{
        public Integer grgR_CK {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        public String premiumDueDate {get;set;}
        public double billedAmount {get;set;}
        public double remainingInvoiceBalance {get;set;}
        public String activityDate {get;set;}
        
        public invoiceDetailsWrapper(){
            grgR_CK = 0;
            grgR_ID = '';
            invoiceNumber = '';
            premiumDueDate = '';
            billedAmount = 0;
            remainingInvoiceBalance = 0;
            activityDate = '';
        }
        
        public invoiceDetailsWrapper(Integer gck, String gid, String inv, String pdd, double ab, double rib, String ad){
            grgR_ck = gck;
            grgR_ID = gid;
            invoiceNumber = inv;
            premiumDueDate = pdd;
            billedAmount = ab;
            remainingInvoiceBalance = rib;
            activityDate = ad;
        }
    }

    public class invoiceDueWrapper{
        public Integer grgR_ck {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        public double billedAmount {get;set;}
        public String unpaiedPremiumDueDate{get;set;}
        public double invoiceBalanceDue{get;set;}
        
        public invoiceDueWrapper(){
            grgR_ck = 0;
            grgR_ID = '';
            invoiceNumber = '';
            billedAmount = 0;
            unpaiedPremiumDueDate = '';
            invoiceBalanceDue = 0;
        }
        
        public invoiceDueWrapper(Integer gck, String gid, String inv, double ba, string updd, double ibd){
            grgR_ck = gck;
            grgR_ID = gid;
            invoiceNumber = inv;
            billedAmount = ba;
            unpaiedPremiumDueDate = updd;
            invoiceBalanceDue = ibd;
        }
    }
}