@isTest global class LetterOfAgreementControllerTest {
    static testMethod void test0(){
        
        RecordType rtAcct = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation_Provider' limit 1];
        Account a = new Account(Name = 'test', Status__c = 'Active', EIN__c = 'abc123', BillingStreet = '120st', BillingCity = 'Buffalo', BillingState = 'NY', BillingPostalCode = '14228', Facets_ID__c = 'abc123', Type = rtAcct.Id);
        insert a;
        List<Account> aList = [select Name, Status__c, EIN__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, Facets_ID__c, Type from Account where Id = :a.Id];
        System.assertEquals(1, aList.size());
        System.debug(a);
        
        RecordType rtCase = [SELECT Id FROM RecordType WHERE DeveloperName = 'Transportation' limit 1];
        Contact con = new Contact();
        con.FirstName = 'Helen';
        con.LastName = 'Smith';
        con.Group_Name__c = 'test';
        insert con;
        List<Contact> conList = [select Id, FirstName, LastName, Group_Name__c from Contact where Id = :con.Id];
        System.assertEquals(1, conList.size());
        System.debug(con);
        
        Case c = new Case();
            c.RecordTypeId = rtCase.Id;
            c.ContactId = con.Id;
            c.Pickup_County__c = 'Kings';
            c.Authorization_Number__c = 'test';
            c.Procedure_Code__c = 'Ambulance, ALS 1 - A0426';
            c.Service_Date_on_LoA__c = 'test';
            c.Rational_for_LoA__c = 'test';
            c.Contact_Person_for_LoA__c = 'test';
            c.Pickup_Datetime__c = Datetime.newInstance(2015, 08, 15, 12, 10, 20);
            c.Contact = con;
            c.Contact.FirstName = con.FirstName;
            c.Contact.LastName = con.LastName;
            c.Contact_Number_for_LoA__c = '0000000000';
            c.Contact.Group_Name__c = con.Group_Name__c;
            c.Transportation_Provider__r = a;
            c.Transportation_Provider__r.Facets_ID__c = a.Facets_ID__c;
            c.Transportation_Provider__r.Name = a.Id;
            c.Transportation_Provider__r.EIN__c = a.EIN__c;
            c.Transportation_Provider__r.BillingStreet = a.BillingStreet;
            c.Transportation_Provider__r.BillingCity = a.BillingCity;
            c.Transportation_Provider__r.BillingState = a.BillingState;
            c.Transportation_Provider__r.BillingPostalCode = a.BillingPostalCode;
            c.Pickup_Location__c = 'test';
            c.Drop_Off_Location__c = 'test';
            c.Reason_for_LoA__c = 'Dead Miles';
        insert c;
        LetterOfAgreementController loa = new LetterOfAgreementController();
        loa.cId = c.Id;
        loa.imageUrl = 'null';
        system.assertNotEquals(null,loa.theCase.Id);
        System.assertEquals(loa.theCase.Id,c.Id);
    }
}