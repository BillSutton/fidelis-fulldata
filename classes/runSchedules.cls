/******************************************************************************
** Module Name   : runSchedules
** Description   : Schedule baAgeIn monthly (but named to provide for other schedules)
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      3/20/2015    SVA     Created
**
******************************************************************************/

public class runSchedules {
    public runSchedules() 
    {
        String cron = '0 0 3 1 * ?'; // 3am, first of every month
        system.schedule('Age-In Process', cron, new scAgeIn());
        
        cron = '0 15 16 MON-FRI ?'; // run at 4:15pm monday thru friday
        system.schedule('Daily Manifest Monitor', cron, new DailyManifestMonitor());
    }
}