public class MemberPremiumInformationParser {

    public List<MemberPremiumLatestPayment> memberPremiumLatestPayment {get;set;}
    public List<MemberPaymentAppliedDetails> memberPaymentAppliedDetails{get;set;}
    public List<MemberPremiumInvoicePaidDetails> memberPremiumInvoicePaidDetails {get;set;}
    public List<MemberPremiumInvoiceDueDetails> memberPremiumInvoiceDueDetails {get;set;}

	public class MemberPaymentAppliedDetails {
        public Integer grgR_CK {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        public String subscriberId{get;set;}
        public String paymentAppliedDate{get;set;}
        public Double appliedAmount{get;set;}
        public String checkNumber{get;set;}
        public String paymentMethod{get;set;}
        public String suspenseIndicator{get;set;}
	}

	public class MemberPremiumInvoicePaidDetails {
        public Integer grgR_CK {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        public String subscriberId{get;set;}
        public String premiumDueDate{get;set;}
        public Double appliedAmount{get;set;}
        public Double remainingInvoiceBalance{get;set;}
        public String activityDate{get;set;}
	}

	public class MemberPremiumInvoiceDueDetails {
        public Integer grgR_CK {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        public String subscriberID{get;set;}
        public double billedAmount{get;set;}
        public String unpaidPremiumDueDate{get;set;}
        public double invoiceBalanceDue{get;set;}
	}

	public class MemberPremiumLatestPayment {
        public Integer grgR_CK {get;set;}
        public String grgR_ID {get;set;}
        public String invoiceNumber {get;set;}
        public String subscriberId{get;set;}
        public Double monthlyPremium{get;set;}
        public Double paymentAmount{get;set;}
        public String paymentDateReceived{get;set;}
        public Double totalCurrentDue{get;set;}
        public String paymentPostedDate{get;set;}
        public Double subsidyTotal{get;set;}
        public String aptcDeliquencyDate{get;set;}
        public String aptcStatus{get;set;}
	}

	
	public static MemberPremiumInformationParser parse(String json) {
		return (MemberPremiumInformationParser) System.JSON.deserialize(json, MemberPremiumInformationParser.class);
	}
}