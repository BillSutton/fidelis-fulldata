/******************************************************************************
** Description   : WebService Async Updates 
**          
** Author        : Scott Van Atta
**
** Revision History:-
** Version  Date        Author  Description of Action
** 1.0      2/28/2016    SVA   Created
**
******************************************************************************/
global class WebServiceLog
{
    @future
    // All calls should be to this async method
    // Sync method is provided for tsting
    public static void write(String intype, ID caseId, Id contactId, String inpath, string inrequest, string inresponse)
    {
        writeIt(intype, caseid, contactid, inpath, inrequest, inresponse);
    }
    
    // sync method if called directly by test; async if called by write (always use write)
    public static void writeIt(String intype, ID caseId, Id contactId, String inpath, string inrequest, string inresponse)
    {
        try {
            if (canLog(intype))
            {
                insert loadRecord(intype, caseId, contactId, inpath, inrequest, inresponse);
                system.debug('***Wrote log for ' + intype);
                checkDeletes();
            }
        } catch(Exception e) { 
            String s = e.getCause() + '\n' + e.getLinenumber() + '\n' + e.getMessage();
            system.debug('***WSL error: ' + s);
            util.sendmsg('webService Logging error', s, getEmail()); 
        }
        
    }
    public static WebServiceLog__c loadRecord(String intype, ID caseId, Id contactId, String inpath, string inrequest, string inresponse)
    {
        WebServiceLog__c ws = new WebServiceLog__c(case__c = caseId, contact__c = contactId);
        if (caseid != null && [select count() from case where id = :caseid] == 0)
                    ws.case__c = null;
        ws.request__c = util.getMaxChars(inrequest, WebServiceLog__c.request__c.getDescribe().getLength());
        ws.response__c = util.getMaxChars(inresponse, WebServiceLog__c.response__c.getDescribe().getLength());
        ws.path__c = util.getMaxChars(inpath, WebServiceLog__c.path__c.getDescribe().getLength());
        ws.type__c = util.getMaxChars(intype, WebServiceLog__c.type__c.getDescribe().getLength());
        return ws;
    }

    public static boolean canLog(string intype)
    {
        WebServiceLogSettings__c sets = getSafeWebServiceSettings();
        system.debug('***WebServiceLogging=' + sets.WebServiceLogging__c + ' AddAuth=' + sets.AddAuth__c);
        if (!sets.WebServiceLogging__c ) return false;
        if (intype == 'AddAuth' && !sets.AddAuth__c) return false;
        if (intype == 'CancelLineItem' && !sets.CancelLineItem__c) return false;
        if (intype == 'GetProvidersBySearch' && !sets.GetProvidersBySearch__c) return false;
        if (intype == 'GetBenefits' && !sets.GetBenefits__c) return false;
        return true;
    }
    
    private static void checkDeletes()
    {
        WebServiceLogSettings__c sets = getSafeWebServiceSettings();

        if (sets.delete_criteria__c != null)
        {
            String query = 'select id,type__c, createddate from webservicelog__c where ' + sets.delete_criteria__c + ' LIMIT 4900';

            try {
                List<WebServiceLog__c> wsls = database.query(query); 
                if (wsls != null && wsls.Size() > 0)
                {
                    delete wsls;
                    system.debug('*** deleted ' + wsls.Size() + ' logs for query: ' + query);
                    system.debug('*** first was ' + wsls[0].type__c + ' ' + wsls[0].createddate);
                }
                
            } catch (Exception e)
            {
                system.debug('***in checkDeletes: ' + query + e);
                util.sendmsg('WebServiceLogging error in deletes', query + '\n\n' + e, getEmail());
            }
        }
    }
    
    public static WebServiceLogSettings__c getSafeWebServiceSettings()
    {
        List<WebserviceLogSettings__c> WSLs = [select WebserviceLogging__c, Delete_Criteria__c, AddAuth__c, CancelLineItem__c, GetProvidersBySearch__c, Mail_Errors_To__c, GetBenefits__c FROM WebServiceLogSettings__c where name = 'Settings'];
        if (WSLs == null || WSLs.size() != 1)
        {
            WSLs.add(new WebServiceLogSettings__c());
            system.debug('***Cannot find webservicelogsettings or multiple Settings  in getSafeWebServiceSettings');
            Util.sendmsg('WebServiceLog Custom Settings Missing or too many!', 'Reverting to defaults', 'svanatta@fideliscare.org');
        }
        return WSLs[0];

    }
    
    public static String getEmail()
    {
        try {
            List<WebServiceLogSettings__c> wsls = [select Mail_Errors_To__c FROM WebServiceLogSettings__c where name = 'Settings'];
            if (wsls == null || wsls.size() != 1 || wsls[0].mail_errors_to__c == null)
                return 'svanatta@fideliscare.org';
            else return wsls[0].mail_errors_to__c;


        } catch (exception e) {     
            system.debug('***getEmail: problem with webservicelogsettings' + e);
            return 'svanatta@fideliscare.org'; }

    }

 }