@isTest
 private class GoogleMapsJsonParserTest{
     
    static testMethod void testParse() {
        String json = '{'+
        '\"destination_addresses\" : [ \"593 T313-3, Port Matilda, PA 16870, USA\" ],'+
        '\"origin_addresses\" : [ \"576 Taunton Place, Buffalo, NY 14216, USA\" ],'+
        '\"rows\" : ['+
        '{'+
        '\"elements\" : ['+
        '{'+
        '\"distance\" : {'+
        '\"text\" : \"191 mi\",'+
        '\"value\" : 307636'+
        ' },'+
        '\"duration\" : {'+
        '\"text\" : \"3 hours 48 mins\",'+
        '\"value\" : 13656'+
        '},'+
        '\"status\" : \"OK\"'+
        '}'+
        ']'+
        '}'+
        '],'+
        '\"status\" : \"OK\"'+
        '}';
        googleMapsJsonParser obj = googleMapsJsonParser.parse(json);
        System.assert(obj != null);
    }
     
}