global class LetterOfAgreementController {
    global String imageURL {get;set;}
    global String cId {get;set;}
    global Boolean deadMiles {get;set;}
    global Boolean carryDown {get;set;}
    global Boolean WaitTime {get;set;}
    global Boolean other {get;set;}
    global Case theCase{
        get{
            Case theCase = [SELECT Id, Pickup_County__c, Authorization_Number__c, Procedure_Code__c, Service_Date_on_LoA__c, Rational_for_LoA__c, Contact_Person_for_LoA__c, Pickup_Datetime__c, Contact.LastName, Contact_Number_for_LoA__c, Contact.FirstName, Transportation_Provider__r.Facets_ID__c, Contact.Group_Name__c, Contact.Line_Of_Business__c, Contact.Subscriber_ID__c, Transportation_Provider__r.Name, Transportation_Provider__r.EIN__c, Transportation_Provider__r.BillingStreet, Transportation_Provider__r.BillingCity, Transportation_Provider__r.BillingState, Transportation_Provider__r.BillingPostalCode, Pickup_Location__c, Drop_Off_Location__c, Reason_for_LoA__c FROM Case WHERE Id =:cId];
            if(theCase.Reason_for_LoA__c != null){
                deadMiles = theCase.Reason_for_LoA__c.contains('Dead Miles') ? true : false;
                carryDown = theCase.Reason_for_LoA__c.contains('Carry Down') ? true : false;
                waitTime = theCase.Reason_for_LoA__c.contains('Wait Time') ? true : false;
                other = theCase.Reason_for_LoA__c.contains('Other') ? true : false;
            }
            return theCase;
        }
        set;
    }
    
    global LetterOfAgreementController(){
        //List<Document> documentList=[select name from document where Name='FidelisLogo_PDF'];  
        //if(documentList.size()>0){
        //    imageURL='/servlet/servlet.FileDownload?file='+documentList[0].id;
        //}
    }
}