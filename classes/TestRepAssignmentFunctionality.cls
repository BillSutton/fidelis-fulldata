/*
@Name			: TestRepAssignmentFunctionality
@Author		  : customersuccess@cloud62.com
@Date			: October 16, 2013
@Description	 : Test Class for RepAssignmentFunctionality
*/

@isTest 
private class TestRepAssignmentFunctionality{

	static testMethod void repAssignmentFunctionalityTest(){
		
		List<Rep_Assignment__c> raLst = getRAList();
		List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
		List<Contact> cons = getContact(raLst);
		List<Case> cl = getCase(raLst, cons,'12/12/2008');
		//Lives__c lives = getLives(cl[0]);
		List<Rep_Goals__c> rgList = getRepGoals(raLst);
		
		Test.startTest();
		cl[0].Status = 'Unassigned';
		cl[1].Status = 'Qualified';
		cl[2].Status = 'Qualified';
		update cl;
		
		cl[0].Status = 'Assigned';
		update cl[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		raLst[0].Representative_Name__c = 'Rep3';
		
		update raLst[0];
		//System.assertEquals('Rep3', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(4, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		raLst[0].Vacation__c=true;
		raLst[0].Covering_Representative__c = 'Rep2';
		raLst[0].Rep_Assignment_lu__c = raLst[1].id;
		Test.stopTest();
		 
	}
	
	/*
	@Name :repAssignmentFunctionalityTest
	@Description : Test method for RepAssignmentFunctionality
	*/
	static testMethod void repAssignmentFunctionalityTest8(){
		
		List<Rep_Assignment__c> raLst = getRAList();
		List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
		List<Contact> cons = getContact(raLst);
		List<Case> cl = getCase(raLst, cons, '12/12/2008');
		//Lives__c lives = getLives(cl[0]);
		List<Rep_Goals__c> rgList = getRepGoals(raLst);
		
		Test.startTest();
		cl[1].Date_Assigned__c = Date.today();
		cl[0].Status = 'Assigned';
		cl[0].Primary_Category__c = 'MLTC';
		cl[1].Status = 'Qualified';
		cl[2].Status = 'Qualified';
		cons[1].Language__c = 'Other';
		update cons[1];
		update cl;
		
		cl[0].Status = 'Assigned';
		//update l[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		raLst[0].Representative_Name__c = 'Rep3';
		
		update raLst[0];
		
		//System.assertEquals('Rep3', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(4, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		raLst[0].Vacation__c=true;
		raLst[0].Covering_Representative__c = 'Rep2';
		raLst[0].Rep_Assignment_lu__c = raLst[1].id;
		Test.stopTest();
		
		update raLst[0];
		cons[0].otherpostalcode = '12345';
		cons[0].Language__c = 'Other';
		update cons[0];
		cl[0].Status = 'Assigned';
		update cl[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(2, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		cl[0].Status = 'Initiated';
		update cl[0];
		//Test.stopTest();
		delete cl[0];
		//delete lives;

		 
			
		//System.assertEquals('Rep2', l.Representative__c);
	}
	
	 static testMethod void repAssignmentFunctionalityTest2(){
		
		List<Rep_Assignment__c> raLst = getRAList();
		List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
		List<Contact> cons = getContact(raLst);
		List<Case> cl = getCase(raLst, cons, '1/12/2008');
		//Lives__c lives = getLives(cl[0]);
		List<Rep_Goals__c> rgList = getRepGoals(raLst);
		Test.startTest();
		cl[0].Status = 'Unassigned';
		cl[1].Status = 'Qualified';
		//l[2].Status = 'Qualified';
		update cl;
		
		cl[0].Status = 'Assigned';
		update cl[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		raLst[0].Representative_Name__c = 'Rep3';
		
		update raLst[0];
		//System.assertEquals('Rep3', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(4, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		raLst[0].Vacation__c=true;
		raLst[0].Covering_Representative__c = 'Rep2';
		raLst[0].Rep_Assignment_lu__c = raLst[1].id;
		
		update raLst[0];
		cons[0].otherpostalcode = '12345';
		cons[0].Language__c = 'English';
		update cons[0];
		cl[0].Status = 'In Progress';
		update cl[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(2, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		cl[0].Status = 'Initiated';
		update cl[0];
		Test.stopTest();

		//Test.stopTest(); 
			
		//System.assertEquals('Rep2', l.Representative__c);
	}
	
	/*
	@Name :repAssignmentFunctionalityTest
	@Description : Test method for RepAssignmentFunctionality
	*/
	static testMethod void repAssignmentFunctionalityTest4(){
		//grab the appropriate recordtype
		RecordType retentionRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Rep_Assignment__c' AND Name = 'Retention'];
		//create a rep assignment record
		List<Rep_Assignment__c> assignments = new List<Rep_Assignment__c>();
		assignments.add(new Rep_Assignment__c(MSA__c = UserInfo.getUserId(), RecordTypeId = retentionRecordType.Id, Rep_Code__c = 'NA00000000999', Rep_Phone__c = '(555) 555-5555'));
		insert assignments;
		
		Default_Region_Rep__c newDRR = new Default_Region_Rep__c();
		newDRR.Name = 'Amherst';
		newDRR.Rep_ID__c = assignments[0].Id;
		insert newDRR;
		
		List<Rep_Assignment__c> raLst = getRAList();
		List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
		List<Contact> cons = getContact(raLst);
		List<Case> cl = getCase(raLst, cons, '1/12/2008');
		//Lives__c lives = getLives(cl[0]);
		List<Rep_Goals__c> rgList = getRepGoals(raLst);
		
		
		cl[0].Status = 'Unassigned';
		cl[1].Status = 'In Process';
		cl[1].Rep_Assignment__c = raLst[0].Id;
		cl[2].Status = 'Qualified';
		cl[2].Rep_Assignment__c = raLst[0].Id;
		update cl;
		Test.startTest();
		cl[0].Status = 'Assigned';
		cl[0].Primary_Category__c = 'FHP';
		cons[0].Region__c = 'Amherst';
		update cons[0];
		update cl[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		raLst[0].Representative_Name__c = 'Rep3';
		
		update raLst[0];
		//System.assertEquals('Rep3', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(4, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		raLst[0].Vacation__c=true;
		raLst[0].Covering_Representative__c = 'Rep2';
		raLst[0].Rep_Assignment_lu__c = raLst[1].id;
		Test.stopTest(); /*
		update raLst[0];
		l[0].otherpostalcode = '12345';
		l[0].Language__c = 'English';
		l[0].Status = 'In Progress';
		update l[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(2, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		l[0].Status = 'Initiated';
		update l[0];
		//Test.stopTest();
		delete l[0];
		delete lives;*/

		
			
		//System.assertEquals('Rep2', l.Representative__c);
	}
	
	/*
	@Name :repAssignmentFunctionalityTest
	@Description : Test method for RepAssignmentFunctionality
	*/
	static testMethod void repAssignmentFunctionalityTest5(){
		//grab the appropriate recordtype
		RecordType retentionRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Rep_Assignment__c' AND Name = 'Retention'];
		//create a rep assignment record
		List<Rep_Assignment__c> assignments = new List<Rep_Assignment__c>();
		assignments.add(new Rep_Assignment__c(MSA__c = UserInfo.getUserId(), RecordTypeId = retentionRecordType.Id, Rep_Code__c = 'NA00000000999', Rep_Phone__c = '(555) 555-5555'));
		insert assignments;
		
		Default_Region_Rep__c newDRR = new Default_Region_Rep__c();
		newDRR.Name = 'Amherst';
		newDRR.Rep_ID__c = assignments[0].Id;
		insert newDRR;
		
		Test.startTest();
		
		List<Rep_Assignment__c> raLst = getRAList();
		List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
		List<Contact> cons = getContact(raLst);
		List<Case> cl = getCase(raLst, cons, '12/12/2008');
		//Lives__c lives = getLives(cl[0]);
		List<Rep_Goals__c> rgList = getRepGoals(raLst);
		
		

		Test.stopTest(); 
			
		//System.assertEquals('Rep2', l.Representative__c);
	}
	
	/*
	@Name :repAssignmentFunctionalityTest
	@Description : Test method for RepAssignmentFunctionality
	*/
	static testMethod void repAssignmentFunctionalityTest6(){
		//grab the appropriate recordtype
		RecordType retentionRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Rep_Assignment__c' AND Name = 'Retention'];
		//create a rep assignment record
		List<Rep_Assignment__c> assignments = new List<Rep_Assignment__c>();
		assignments.add(new Rep_Assignment__c(MSA__c = UserInfo.getUserId(), RecordTypeId = retentionRecordType.Id, Rep_Code__c = 'NA00000000999', Rep_Phone__c = '(555) 555-5555'));
		insert assignments;
		
		Default_Region_Rep__c newDRR = new Default_Region_Rep__c();
		newDRR.Name = 'Amherst';
		newDRR.Rep_ID__c = assignments[0].Id;
		insert newDRR;
		
		List<Rep_Assignment__c> raLst = getRAList();
		List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
		List<Contact> cons = getContact(raLst);
		List<Case> cl = getCase(raLst, cons, '1/12/2008');
		//Lives__c lives = getLives(cl[0]);
		List<Rep_Goals__c> rgList = getRepGoals(raLst);
		
		Test.startTest();
		
		cl[0].Status = 'Assigned';
		cl[0].Primary_Category__c = 'NYM';
		cons[0].Region__c = 'Amherst';
		cl[1].Status = 'Assigned';
		cl[1].Primary_Category__c = 'Dual';
		cl[2].Status = 'Assigned';
		cl[2].Primary_Category__c = 'CHP';
		cl[3].Status = 'Assigned';
		cl[3].Primary_Category__c = 'HBX';
		update cons[0];
		update cl;
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		raLst[0].Representative_Name__c = 'Rep3';
		
		update raLst[0];
		//System.assertEquals('Rep3', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(4, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		raLst[0].Vacation__c=true;
		//raLst[0].Covering_Representative__c = 'Rep2';
		raLst[0].Rep_Assignment_lu__c = raLst[1].id;
		
		update raLst[0];
		cons[0].otherpostalcode = '12345';
		cons[0].Language__c = 'English';
		update cons[0];
		cl[0].Status = 'In Progress';
		update cl[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(2, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		cl[0].Status = 'Initiated';
		update cl[0];
		//Test.stopTest();
		delete cl[0];
	   // delete lives;

		Test.stopTest(); 
			
		//System.assertEquals('Rep2', l.Representative__c);
	}
	
	/*
	@Name :repAssignmentFunctionalityTest
	@Description : Test method for RepAssignmentFunctionality
	*/
	static testMethod void repAssignmentFunctionalityTest7(){
		//grab the appropriate recordtype
		RecordType retentionRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Rep_Assignment__c' AND Name = 'Retention'];
		//create a rep assignment record
		List<Rep_Assignment__c> assignments = new List<Rep_Assignment__c>();
		assignments.add(new Rep_Assignment__c(MSA__c = UserInfo.getUserId(), RecordTypeId = retentionRecordType.Id, Rep_Code__c = 'NA00000000999', Rep_Phone__c = '(555) 555-5555'));
		insert assignments;
		
		Default_Region_Rep__c newDRR = new Default_Region_Rep__c();
		newDRR.Name = 'Amherst';
		newDRR.Rep_ID__c = assignments[0].Id;
		insert newDRR;
		
		List<Rep_Assignment__c> raLst = getRAList();
		List<Rep_Assignment_Criteria__c> racLst = getRACList(raLst);
		List<Contact> cons = getContact(raLst);
		List<Case> cl = getCase(raLst, cons, '12/12/2008');
		//Lives__c lives = getLives(cl[0]);
		List<Rep_Goals__c> rgList = getRepGoals(raLst);
		
		Test.startTest();
		
		cl[0].Status = 'Assigned';
		cl[0].Primary_Category__c = 'NYM';
		cons[0].Region__c = 'Amherst';
		cons[0].otherpostalcode = '99999';
		cons[1].otherpostalcode = '00000';
		cl[1].Status = 'Assigned';
		cl[1].Primary_Category__c = 'Dual';
		cl[2].Status = 'Assigned';
		cl[2].Primary_Category__c = 'CHP';
		cl[3].Status = 'Assigned';
		cl[3].Primary_Category__c = 'HBX';
		update cons;
		update cl;
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		raLst[0].Representative_Name__c = 'Rep3';
		
		update raLst[0];
		//System.assertEquals('Rep3', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(4, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		raLst[0].Vacation__c=true;
		//raLst[0].Covering_Representative__c = 'Rep2';
		raLst[0].Rep_Assignment_lu__c = raLst[1].id;
		
		update raLst[0];
		cons[0].otherpostalcode = '12345';
		cons[0].Language__c = 'English';
		cl[0].Status = 'In Progress';
		update cons[0];
		update cl[0];
		//System.assertEquals('Rep1', [Select Representative__c From Lead Where Id=:l.Id][0].Representative__c);
		//System.assertEquals(2, [SELECT count() FROM Outside_Rep_History_Tracking__c]);
		cl[0].Status = 'Initiated';
		update cl[0];
		//Test.stopTest();
		delete cl[0];
		//delete lives;

		Test.stopTest(); 
			
		//System.assertEquals('Rep2', l.Representative__c);
	}
	
	/*
	@Name :getContact
	@Description : prepare the test data for Contact record.
	*/
	private static List<Contact> getContact(List<Rep_Assignment__c> ra){
		Contact l1 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
		Contact l2 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
		Contact l3 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
		Contact l4 = new Contact(LastName='testName',otherpostalcode='12345',Language__c='English',Region__c = 'Buffalo', home_county__c = 'Erie', market__c='WNY', phone='1234567890');
		List<Contact> lList = new List<Contact>();
		lList.add(l1);
		lList.add(l2);
		lList.add(l3);
		lList.add(l4);
		insert lList;
		return lList;
	}
	/*
	@Name :getCase
	@Description : prepare the test data for Case record.
	*/
	private static List<Case> getCase(List<Rep_Assignment__c> ra, List<Contact> cons, String rdate){
		Case l1 = new Case(Rep_Assignment__c=ra[0].Id, Status='Unassigned', contactid=cons[0].id,
						  date_received__c=date.parse(rdate));
		Case l2 = new Case(Rep_Assignment__c=ra[0].Id, Status='Unassigned', contactid=cons[1].id
						  ,date_received__c=date.parse(rdate));
		Case l3 = new Case(Rep_Assignment__c=ra[0].Id, Status='Unassigned', contactid=cons[2].id
						  ,date_received__c=date.parse(rdate));
		Case l4 = new Case(Rep_Assignment__c=ra[0].Id, Status='Unassigned', contactid=cons[3].id
						  ,date_received__c=date.parse(rdate));
		List<Case> lList = new List<Case>();
		lList.add(l1);
		lList.add(l2);
		lList.add(l3);
		lList.add(l4);
		insert lList;
		return lList;
	}
	/*
	@Name :getLives
	@Description : prepare the test data for Lives record.
	*/
	private static Lives__c getLives(Case cs){
		Lives__c l = new Lives__c(Lives__c=10,Responsible_Member__c=cs.contactId);
		insert l;
		return l;
	}
	
	/*
	@Name :getRepGoals
	@Description : prepare the test data for Rep Goals record.
	*/
	private static List<Rep_Goals__c> getRepGoals(List<Rep_Assignment__c> r){
		List<Rep_Goals__c> rgList = new List<Rep_Goals__c>();
		Rep_Goals__c rg1 = new Rep_Goals__c(Rep__c=r[0].Id,Incentive_Quarter__c='Q1');
		Rep_Goals__c rg2 = new Rep_Goals__c(Rep__c=r[1].Id,Incentive_Quarter__c='Q1');
		Rep_Goals__c rg3 = new Rep_Goals__c(Rep__c=r[0].Id,Incentive_Quarter__c='Q2');
		Rep_Goals__c rg4 = new Rep_Goals__c(Rep__c=r[1].Id,Incentive_Quarter__c='Q2');
		Rep_Goals__c rg5 = new Rep_Goals__c(Rep__c=r[0].Id,Incentive_Quarter__c='Q3');
		Rep_Goals__c rg6 = new Rep_Goals__c(Rep__c=r[1].Id,Incentive_Quarter__c='Q3');
		Rep_Goals__c rg7 = new Rep_Goals__c(Rep__c=r[0].Id,Incentive_Quarter__c='Q4');
		Rep_Goals__c rg8 = new Rep_Goals__c(Rep__c=r[1].Id,Incentive_Quarter__c='Q4');
		rgList.add(rg1);
		rgList.add(rg2);
		rgList.add(rg3);
		rgList.add(rg4);
		rgList.add(rg5);
		rgList.add(rg6);
		rgList.add(rg7);
		rgList.add(rg8);
		insert rgList;
		return rgList;
	}
	
	/*
	@Name :getRAList
	@Description : prepare the test data for Rep_Assignment__c record.
	*/
	private static List<Rep_Assignment__c> getRAList(){
		List<Rep_Assignment__c> raLst = new List<Rep_Assignment__c>();
		Rep_Assignment__c ra1 = new Rep_Assignment__c(Representative_Name__c='Rep1',FHP__c = true, Rep_Phone__c= '1', Rep_Email__c= 'a@a.com', MSA__c = UserInfo.getUserId());
		
		Rep_Assignment__c ra2 = new Rep_Assignment__c(Representative_Name__c='Rep2',FHP__c = true, Rep_Phone__c= '1', Rep_Email__c= 'a@a.com', MSA__c = UserInfo.getUserId());
		raLst.add(ra1);
		raLst.add(ra2);
		insert raLst;
		Rep_Assignment__c ra3 = new Rep_Assignment__c(Representative_Name__c='Rep10',MLTC__C = true,CHP__C = true,HBX__C = true,Medicaid__C = true,Medicare__C = true, Vacation__c = true, Rep_Assignment_lu__c = ra1.Id, Rep_Phone__c= '1', Rep_Email__c= 'a@a.com', MSA__c = UserInfo.getUserId());
		insert ra3;
		raLst.add(ra3);
		return raLst;
	}
	
	/*
	@Name :getRACList
	@Description : prepare the test data for Rep_Assignment_Criteria__c record.
	*/
	
	private static List<Rep_Assignment_Criteria__c> getRACList(List<Rep_Assignment__c> ra){
		List<Rep_Assignment_Criteria__c> racLst = new List<Rep_Assignment_Criteria__c>();
		Rep_Assignment_Criteria__c rac1 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[0].Id);	
		Rep_Assignment_Criteria__c rac2 = new Rep_Assignment_Criteria__c(Zip_Code__c='67890',Language__c='Spanish',Rep_Assignment__c=ra[1].Id);	
		Rep_Assignment_Criteria__c rac3 = new Rep_Assignment_Criteria__c(Zip_Code__c='45678',Language__c='English',Rep_Assignment__c=ra[1].Id);
		Rep_Assignment_Criteria__c rac4 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[1].Id);
		Rep_Assignment_Criteria__c rac5 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[2].Id);	  
		racLst.add(rac1);
		racLst.add(rac2);
		racLst.add(rac3);
		racLst.add(rac4);
		racLst.add(rac5);
		insert racLst;
		//insert rac3;
		//Rep_Assignment_Criteria__c rac4 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[0].Id);	
		//Rep_Assignment_Criteria__c rac5 = new Rep_Assignment_Criteria__c(Zip_Code__c='12345',Language__c='English',Rep_Assignment__c=ra[1].Id);
		//insert rac4;
		//insert rac5;
		return racLst;	 
	}
		  
	

  
}