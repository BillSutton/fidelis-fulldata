Global class TransportationPDFController {
    Global String theId1 {get;set;}
    Global TimeZone tz {get {return System.TimeZone.getTimeZone('America/New_York');} set;}   
    Global Datetime CurrentTime {get { return DateTime.newInstance(System.Now().getTime() + tz.getOffset(System.Now())); } set;} 
    Global String pickupDatetime {get {return theCase.Pickup_Datetime__c.format('MM/dd/yyyy h:mm a');} set;}
    Global Case theCase{
        get{ 
            system.debug('The ID we Get: '+theId1);
            if(theId1 != null){
                theCase = [select id, Contact_Number_for_LoA__c,Caller_Name__c,Caller_Phone_Number__c,Contact_Person_for_LoA__c,Return_Datetime__c, entrance__c, room_number__c, bed_number__c, floor_number__c, contact.FirstName,Contact.Gender__c, contact.LastName, contact.Name, contact.subscriber_id__c,Pickup_Location__c,Transportation_Provider__r.name,Drop_Off_Location__c,contact.Birthdate,contact.Phone,Pickup_dateTime__c,form_type__c,of_Riders__c,Steps__c,Authorization_Number__c,weight__c,Vehicle_Type__c,Description,Special_Request__c from Case where ID = :theId1 limit 1];
            }
        return theCase;}
        set;}
    Global string theUser{get;set;}
    Global string theTime{get;set;}
    Global boolean renderHospitalInformation{
        get{
            if(theCase != null){
                if(theCase.Form_Type__c.contains('Hospital')){
                    renderHospitalInformation = true;
                    return renderHospitalInformation;
                }
                else{
                    renderHospitalInformation = false;
                    return renderHospitalInformation;
                }
            }
            else{
                    renderHospitalInformation = false;
                    return renderHospitalInformation;
            }
        }
        set;}
    
    
    Global TransportationPDFController(){
        /*string theId = theId1;
        if(ApexPages.currentPage().getParameters().get('id') != null){
         theId = ApexPages.currentPage().getParameters().get('id');
        }
        if(theId != null && theId != ''){
            
        }
        else{
            theCase = new Case();
        }*/
        if(theCase != null && theCase.Form_Type__c.contains('Hospital')){
            renderHospitalInformation = true;
        }
        else{
            renderHospitalInformation = false;
        }
        theUser = UserInfo.getName();
        theTime = String.valueOf(system.now());
    }
    
    
}