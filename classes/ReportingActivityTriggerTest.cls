// ReportingActivityTriggerTest
// @date: 7-20-2016
// @author: rgodard@fideliscare.org
// @description: Test class for creating/updating Reporting Activities when we create and update a Task or Event.
@isTest
public class ReportingActivityTriggerTest {

    static testMethod void testMethodOne() {
        ID rtId = [SELECT Id from RecordType WHERE sObjectType = 'Case' and DeveloperName = 'Renewals'].Id;
        Case c = new Case(Subject='TestCase', RecordTypeId=rtId);
        insert c;
        
        ID taskRtId = [SELECT Id from RecordType WHERE sObjectType = 'Task' and DeveloperName = 'Retention'].Id;
        Task t = new Task(WhatId = c.Id, Subject='Call', Status='Not Started', Type = 'Call', RecordTypeId = taskRtId, Result__c = 'Busy');
        insert t;
        
        t.Subject = 'Outbound Call';
        update t;
    }

}