public class ExternalSystemLinksController {

    public List<HelpfulLinks__c> linkList {get;set;}
    
    public ExternalSystemLinksController(){
        linkList = [Select Name, URL__c from HelpfulLinks__c order by Display_Order__c];
    }
}