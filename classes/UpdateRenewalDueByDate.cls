public class UpdateRenewalDueByDate {
    
    public static void runLogic(Map<id,Applicant_Information__c> newApplicantMap){
        // Var Declaration
        Map<String,List<Applicant_Information__c>> applicantMap = new Map<String,List<Applicant_Information__c>>();
        List<Case> updateList = new List<Case>();
        Map<String, renewalDueDateWrapper> dateMap = new Map<String, renewalDueDateWrapper>();
        ID rtCheck= [select ID from RecordType where SObjectType = 'Applicant_Information__c' AND DeveloperName = 'Renewal_Applicant' limit 1].id;
        // get default Date values
        for (Renewal_Due_Date__c rdd :Renewal_Due_Date__c.getAll().Values()) {
            if(dateMap.get(rdd.Enrollment_Source__c) == null){
                dateMap.put(rdd.Enrollment_Source__c, new renewalDueDateWrapper(rdd));
            }
            else{
                dateMap.get(rdd.Enrollment_Source__c).add(rdd);
            }
        }
        // Group Applicants by CaseId
        for (Applicant_Information__c ai :newApplicantMap.values()) {
            if(ai.recordTypeId == rtCheck){
                if (applicantMap.get(ai.Case_Number__c) == null) {
                    List<Applicant_Information__c> tempList = new List<Applicant_Information__c>();
                    tempList.add(ai);
                    applicantMap.put(ai.Case_Number__c, tempList);
                } else {
                    applicantMap.get(ai.Case_Number__c).add(ai);
                }
            }
        }
        
        // Get Existing Applicants that might be related to the case
        for (Applicant_Information__c ai :[select id, recordTypeId, Recert_date__c, case_number__c, Renewal_Method__c, line_of_business__c from Applicant_Information__c where Case_Number__c in :applicantMap.keySet()]) {
            if(ai.recordTypeId == rtCheck){
                // using trigger.newMap here to make sure we do not get duplicates in our lists
                if (applicantMap.get(ai.Case_Number__c) != null && newApplicantMap.get(ai.id) == null) {
                    applicantMap.get(ai.Case_Number__c).add(ai);
                }
            }
        }
        
        // Get Cases we need to try and update
        for (Case c :[select Id, completion_date__c from case where id in :applicantMap.keySet()]) {        
            Date compareDate = Date.newInstance(9999, 12, 31);
            c.Completion_Date__c = null;
            for (Applicant_Information__c ai :applicantMap.get(c.id)) {
                if(ai.recordTypeId == rtCheck){
                    Date applicantDate;
    
                    if (dateMap.get(ai.Renewal_Method__c) != null && dateMap.get(ai.Renewal_Method__c).lob.contains(ai.line_of_business__c)) {
                        applicantDate = Date.newInstance(ai.Recert_Date__c.year(), ai.Recert_Date__c.month(), Integer.valueOf(dateMap.get(ai.Renewal_Method__c).day));
                    } else {
                        applicantDate = ai.Recert_Date__c;
                    }
                    // Date Comparisons to make sure we get the earliest time to recert by
                    // ai.Recert_Date__c < applicantDate || 
                    if((ai.Recert_Date__c < compareDate)&& c.Completion_Date__c == null){
                        c.Completion_Date__c = ai.Recert_Date__c;
                    }
                    if (compareDate < applicantDate && compareDate < c.Completion_Date__c) {
                        c.Completion_Date__c = compareDate;
                    } 
                    else if(applicantDate < compareDate && applicantDate < c.Completion_Date__c) {
                        c.Completion_Date__c = applicantDate;   
                    }
                }
                
            }
            updateList.add(c);
        }
        update updateList;
    }

    public class renewalDueDateWrapper{
        public set<String> lob {get;set;}
        public decimal day {get;set;}

        public renewalDueDateWrapper(Renewal_Due_Date__c rdd){
            if(lob == null){               
                day = rdd.Day_of_Month__c;
                lob = new set<String>();
                lob.add(rdd.lob__c);
            }
            else{
                lob.add(rdd.lob__c);
            }
        }

        public void add(Renewal_Due_Date__c rdd){              
            day = rdd.Day_of_Month__c;
            lob.add(rdd.lob__c);
        }
    }
}