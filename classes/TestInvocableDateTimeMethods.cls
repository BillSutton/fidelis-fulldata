// TestInvocableDateTimeMethods
// @date: 9/23/2016
// @author: rgodard@fideliscare.org
// @description: Test class for invocable methods for Salesforce visual flow that, as of now, just parses a string and turns it into a valid DateTime.
@isTest
private class TestInvocableDateTimeMethods {

    private static testMethod void testMethodOne() {

        Test.startTest();
        String newDateTimeString = '9/23/2016 1:18 PM';

        List<String> dateStringList = new List<String>();
        dateStringList.add(newDateTimeString);
        
        InvocableDateTimeMethods.getParsedDateTime(dateStringList);

        Test.stopTest();

    }
}